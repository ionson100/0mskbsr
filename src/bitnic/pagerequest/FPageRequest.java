package bitnic.pagerequest;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.model.MRequest;
import bitnic.model.MRequestProduct;
import bitnic.model.RequestToServer;
import bitnic.orm.Configure;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.pagesale.MainForm;
import bitnic.senders.IAction;
import bitnic.senders.SenderRequestCoreToServer;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Support;
import bitnic.utils.UtilsOmsk;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FPageRequest extends GridPane implements Initializable {

    private double lastYposition=0d;
    private static final Logger log = Logger.getLogger(FPageRequest.class);
    private MRequest mRequest;
    public TableView table_error;
    public Label label_id, label_date, label_name, label_status,label_total_name,label_total_amount;
    public Button bt_1, bt_2;
    private ObservableList<Tovar> data;


    public FPageRequest(MRequest mRequest) {
        this.mRequest = mRequest;
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_page_request.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        List<MRequestProduct> mRequestProducts = Configure.getSession().getList(MRequestProduct.class, " id_request = ? order by name", mRequest.id);
        //new BuilderTable().build( mRequestProducts , table_error );
        Controller.writeMessage("Заявки поставщика");
        label_total_name.setText(String.valueOf(mRequestProducts.size()));

        double amount=0d;
        for (MRequestProduct mRequestProduct : mRequestProducts) {
            amount=amount+mRequestProduct.amount;
        }
        label_total_amount.setText(String.valueOf(amount));




        label_id.setText(String.valueOf(mRequest.id));
        label_date.setText(UtilsOmsk.simpleDateFormatE(mRequest.date));
        label_name.setText(mRequest.shipper);
        greateTable(mRequestProducts);
        UtilsOmsk.painter3d(bt_1, bt_2);

        bt_1.setOnAction(event -> DialogFactory.questionDialog(Support.str("name326"), Support.str("name324"), new ButtonAction("Да", new IAction<Object>() {
            @Override
            public boolean action(Object o) {

                RequestToServer server = new RequestToServer();
                server.id = mRequest.id;
                server.point = SettingAppE.getPointId();
                server.trader = SettingAppE.getInstance().user_id;
                server.status = false;

                for (Tovar datum : data) {
                    MRequest.Prod p=new MRequest.Prod();
                    p.id_prod =datum.id_product;
                    p.val1=datum.request.getValue();
                    p.val2=datum.fact.getValue();
                    server.products_core.add(p);
                }



                new SenderRequestCoreToServer().send(server, new IAction<String>() {
                    @Override
                    public boolean action(String s) {
                        mRequest.status = 1;
                        Configure.getSession().update(mRequest);
                        Controller.getInstans().factoryPage(new MainForm());
                        return false;
                    }
                });


                return false;
            }
        })));

        bt_2.setOnAction(event -> DialogFactory.questionDialog(Support.str("name326"), Support.str("name325"),
                new ButtonAction("Да", new IAction<Object>() {
            @Override
            public boolean action(Object o) {
                RequestToServer server = new RequestToServer();
                server.id = mRequest.id;
                server.point = SettingAppE.getPointId();
                server.trader = SettingAppE.getInstance().user_id;
                server.status = true;

                for (Tovar datum : data) {
                    MRequest.Prod p=new MRequest.Prod();
                    p.id_prod =datum.id_product;
                    p.val1=datum.request.getValue();
                    p.val2=datum.fact.getValue();
                    server.products_core.add(p);
                }

                new SenderRequestCoreToServer().send(server, new IAction<String>() {
                    @Override
                    public boolean action(String s) {
                        mRequest.status = 2;
                        Configure.getSession().update(mRequest);
                        Controller.getInstans().factoryPage(new MainForm());
                        return false;
                    }
                });

                return false;
            }
        })));
        if (mRequest.status != 0) {
            bt_1.setVisible(false);
            bt_2.setVisible(false);
        }
        if (mRequest.status == 1) {
            label_status.setText(Support.str("name327"));
        }
        if (mRequest.status == 2) {
            label_status.setText(Support.str("name328"));
        }

    }

    public void  refreshButton(){

        if(mRequest.status != 0){
            bt_1.setVisible(false);
            bt_2.setVisible(false);
            return;
        }
        for (Tovar datum : data) {
            double d1=datum.fact.getValue();
            double d2=datum.request.getValue();
            if(d1!=d2){
                bt_1.setVisible(true);
                bt_2.setVisible(false);
                return;
            }
            bt_1.setVisible(true);
            bt_2.setVisible(true);
        }
    }

    private void greateTable(List<MRequestProduct> mRequestProducts) {
        TableColumn firstCol = new TableColumn("");
        firstCol.setCellValueFactory(new PropertyValueFactory<>("name_null"));

        TableColumn firstCol1 = new TableColumn("наименование");
        firstCol1.setCellValueFactory(new PropertyValueFactory<>("name"));
        firstCol1.setPrefWidth(800);

        TableColumn firstCol2 = new TableColumn("заявка");
        firstCol2.setCellValueFactory(new PropertyValueFactory<>("request"));
        firstCol2.setPrefWidth(200);

        TableColumn firstCol3 = new TableColumn("факт");
        firstCol3.setCellValueFactory(new PropertyValueFactory<>("fact"));
        firstCol3.setPrefWidth(200);

        TableColumn firstCol4 = new TableColumn("код");
        firstCol4.setCellValueFactory(new PropertyValueFactory<>("code"));
        firstCol4.setPrefWidth(400);

        Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>> cellFactory
                = //
                new Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Tovar, String> param) {
                        final TableCell<Tovar, String> cell = new TableCell<Tovar, String>() {

                            final Button btn = new Button("вычерк");

                            @Override
                            public void updateItem(String item, boolean empty) {

                                super.updateItem(item, empty);
                                if (empty) {

                                    setText(null);
                                } else {
                                    btn.getStyleClass().add("button_dialog_3d");
                                    btn.setOnAction(event -> {
                                        Tovar person = getTableView().getItems().get(getIndex());
                                        DialogFactory.dialogEditPageRequest(person, new IAction() {
                                            @Override
                                            public boolean action(Object o) {
                                                Tovar tovar= (Tovar) o;
                                                table_error.refresh();
                                                try{
                                                    Configure.getSession().execSQL(" UPDATE "+MRequestProduct.TABLE_PRODUCT+" SET " +
                                                            "amount_fact  = ? where id = ?",tovar.fact.getValue(),tovar.primary_keu);
                                                }catch (Exception ex){
                                                 log.error(ex);
                                                }

                                                refreshButton();
                                                    return false;
                                            }
                                        });
                                    });
                                    if(mRequest.status == 0){
                                        setGraphic(btn);
                                    }

                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>> cellFactory2
                = //
                new Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Tovar, String> param) {
                        final TableCell<Tovar, String> cell = new TableCell<Tovar, String>() {
                            @Override
                            public void updateItem(String item, boolean empty) {

                                super.updateItem(item, empty);
                                if (empty) {

                                    setText(null);
                                } else {
                                    setStyle("-fx-font-style: italic; -fx-font-weight: bold;");
                                    setAlignment(Pos.CENTER_LEFT);
                                    Tovar person = getTableView().getItems().get(getIndex());
                                    setText(person.name.getValue());
                                    this.setOnMousePressed(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            lastYposition = event.getSceneY();
                                        }
                                    });

                                    this.setOnMouseDragged(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            double newYposition = event.getSceneY();
                                            double diff = newYposition - lastYposition;
                                            lastYposition = newYposition;
                                            CustomScrollEvent cse = new CustomScrollEvent();
                                            cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
                                        }
                                    });
                                }
                            }
                        };
                        return cell;
                    }
                };

        Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>> cellFactory3
                = //
                new Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Tovar, String> param) {
                        final TableCell<Tovar, String> cell = new TableCell<Tovar, String>() {
                            @Override
                            public void updateItem(String item, boolean empty) {

                                super.updateItem(item, empty);
                                if (empty) {

                                    setText(null);
                                } else {
                                    getStyleClass().addAll("table-cell");

                                    setStyle("-fx-font-size: 30px;-fx-font-weight: bold");


                                    setPadding(new Insets(0, 20, 0, 0));
                                    setAlignment(Pos.CENTER_RIGHT);
                                    Tovar person = getTableView().getItems().get(getIndex());
                                    setText(String.valueOf(person.request.getValue()));
                                    this.setOnMousePressed(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            lastYposition = event.getSceneY();
                                        }
                                    });

                                    this.setOnMouseDragged(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            double newYposition = event.getSceneY();
                                            double diff = newYposition - lastYposition;
                                            lastYposition = newYposition;
                                            CustomScrollEvent cse = new CustomScrollEvent();
                                            cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
                                        }
                                    });
                                }
                            }
                        };
                        return cell;
                    }
                };

        Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>> cellFactory4
                = //
                new Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Tovar, String> param) {
                        final TableCell<Tovar, String> cell = new TableCell<Tovar, String>() {
                            @Override
                            public void updateItem(String item, boolean empty) {

                                super.updateItem(item, empty);
                                if (empty) {

                                    setText(null);
                                } else {
                                    getStyleClass().addAll("table-cell");


                                    setPadding(new Insets(0, 20, 0, 0));
                                    setAlignment(Pos.CENTER_RIGHT);
                                    Tovar person = getTableView().getItems().get(getIndex());
                                    double d1=person.request.getValue(),
                                            d2=person.fact.getValue();
                                    if(d1==d2){
                                        setStyle("-fx-font-size: 30px;-fx-font-weight: bold");
                                    }else {
                                        setStyle("-fx-text-fill:#e31714;-fx-font-size: 30px;-fx-font-weight: bold");
                                    }

                                    setText(String.valueOf(person.fact.getValue()));
                                    this.setOnMousePressed(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            lastYposition = event.getSceneY();
                                        }
                                    });

                                    this.setOnMouseDragged(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            double newYposition = event.getSceneY();
                                            double diff = newYposition - lastYposition;
                                            lastYposition = newYposition;
                                            CustomScrollEvent cse = new CustomScrollEvent();
                                            cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
                                        }
                                    });
                                }
                            }
                        };
                        return cell;
                    }
                };

        Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>> cellFactory5
                = //
                new Callback<TableColumn<Tovar, String>, TableCell<Tovar, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Tovar, String> param) {
                        final TableCell<Tovar, String> cell = new TableCell<Tovar, String>() {
                            @Override
                            public void updateItem(String item, boolean empty) {

                                super.updateItem(item, empty);
                                if (empty) {

                                    setText(null);
                                } else {
                                    setAlignment(Pos.CENTER_LEFT);
                                    Tovar person = getTableView().getItems().get(getIndex());
                                    setText(person.code.getValue());
                                    this.setOnMousePressed(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            lastYposition = event.getSceneY();
                                        }
                                    });

                                    this.setOnMouseDragged(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            double newYposition = event.getSceneY();
                                            double diff = newYposition - lastYposition;
                                            lastYposition = newYposition;
                                            CustomScrollEvent cse = new CustomScrollEvent();
                                            cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
                                        }
                                    });
                                }
                            }
                        };
                        return cell;
                    }
                };
        data = FXCollections.observableArrayList();

        for (MRequestProduct mm : mRequestProducts) {

            Tovar t = new Tovar();
            t.primary_keu=mm.id;
            t.id_product=mm.id_product;
            t.name = new SimpleStringProperty(mm.name);
            t.request = new SimpleDoubleProperty(mm.amount);
            t.fact = new SimpleDoubleProperty(mm.amount_fact);
            t.code = new SimpleStringProperty(mm.barcode);
            data.add(t);
        }

        firstCol.setCellFactory(cellFactory);
        firstCol1.setCellFactory(cellFactory2);
        firstCol2.setCellFactory(cellFactory3);
        firstCol3.setCellFactory(cellFactory4);
        firstCol4.setCellFactory(cellFactory5);
        table_error.setItems(data);
        table_error.getColumns().addAll(firstCol, firstCol1, firstCol2, firstCol3, firstCol4);
        refreshButton();

    }

   public static class Tovar {

        public int primary_keu;
        public String id_product;
        public SimpleStringProperty name;
        public SimpleDoubleProperty request;
        public SimpleDoubleProperty fact;
        public SimpleStringProperty code;
    }
}
