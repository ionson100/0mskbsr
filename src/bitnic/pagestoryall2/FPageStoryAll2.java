package bitnic.pagestoryall2;

import bitnic.checkarchive.CheckArchiveE;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MError;
import bitnic.model.MTransactions;
import bitnic.orm.Configure;
import bitnic.pagetransactions.PageTransaction;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.utils.Pather;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FPageStoryAll2 extends GridPane implements Initializable {

    private static final Logger log = Logger.getLogger(PageTransaction.class);
    public TableView table_error;
    public GridPane grid1;
    List<MError> list = new ArrayList<>();

    public FPageStoryAll2() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_story_all_2.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        int currsesion= SettingAppE.getInstance().getSession();
        if(currsesion==-1){
            DialogFactory.errorDialog("Ошибка поиска текущей сесии");
            return;
        }
        List<CheckArchiveE> archives=  Configure.getSession().getList(CheckArchiveE.class,null);
        List<String> lines=new ArrayList<>();
        for (CheckArchiveE archive : archives) {
            lines.add("ddddddddddddd");
        }
       // Charset cset = Charset.forName("windows-1251");
        List<String> ll=new ArrayList<>() ;

        for (MTransactions tt : Configure.getSession().getList(MTransactions.class, null)) {
            ll.add(tt.str);
        }

        for (String s : ll) {
            String[] ss=s.split(";",-1);
            int idsession=Integer.parseInt(ss[13]);
            if(idsession==currsesion){
                lines.add(s);
            }
        }



        for (String line : lines) {

            MError mError=new MError();
            mError.msg=PageTransaction.reader(line);
            list.add(mError);
        }

        new BuilderTable().build( list , table_error );
        table_error.scrollTo(table_error.getItems ().size()-1);
    }
}