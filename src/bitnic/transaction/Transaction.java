package bitnic.transaction;

import bitnic.checkarchive.CheckArchiveE;
import bitnic.dialogfactory.closesessionsber.CloseSession;
import bitnic.kassa.OpenSession;
import bitnic.kassa.ProcedureOutKassa;
import bitnic.model.MProduct;
import bitnic.senders.IAction;

import bitnic.transaction.eventfrontol.EventBaseE;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Transaction {

    public List<EventBaseE> list=new ArrayList<>();
    public Date date;
    public Date time;

    public Integer numberDoc;
    public int numberSession;
    public int numberCheck;
    public double summSmena;
    public double выручка_отчет;
    public double сменный_итог;
    public double сумма_нал_ккм;
    public double summCheck;
   // private Double sum;



    public void printCheck ( List<MProduct> mProducts,IAction<Double> iAction){
        if(mProducts.size()==0) return;
        Transaction dd=this;
        new  bitnic.kassa.PrintCheck2().print(mProducts,dd,0d, iAction);

    }


    public void casheIn(Double sum){
       // this.sum = sum;

        Transaction dd=this;
        new bitnic.kassa.ContributingMoney().run(sum,dd);
    }

    public void casheInProcedure(Double sum, IAction iActionProcedure){
       // this.sum = sum;

        Transaction dd=this;
        new bitnic.kassa.ProcedureInKassa().run(dd, sum,iActionProcedure);
    }

    public String getDate() {
        //28.12.2017;8:42:16
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");// HH:mm:ss
        return dateFormat.format(date);
    }

    public String getTime() {
        //28.12.2017;8:42:16
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(time);
    }

    public void cashOut(double summ,IAction iActionProcedure) {
        Transaction dd=this;
        new bitnic.kassa.EntryMoney().run(summ,dd,iActionProcedure);
    }

    public void reportZ() {
        Transaction transaction=this;
        new bitnic.kassa.ZReport().print(transaction);
    }

    public void deleteCheck(CheckArchiveE mCheck, int nDoc, IAction< Integer>  iAction) {
       new  bitnic.kassa.DeleteCheck().deleteReturn(mCheck.link_bank,mCheck.getProductList(),nDoc,this,String.valueOf(mCheck.cod_doc),iAction  );
    }

    public void openSession() {
        Transaction transaction=this;
        new OpenSession().open(transaction,null);
    }

    public void openSession(IAction iActionProcedure) {
        Transaction transaction=this;
        new OpenSession().open(transaction,iActionProcedure);
    }

    public void reportX() {
        Transaction transaction=this;
        new bitnic.kassa.XReport().print(transaction);
    }

    public void printCheckBank ( List<MProduct> mProducts, String numberCard) {

        if(mProducts.size()==0) return;
        Transaction dd=this;
        new  bitnic.kassa.PrintCheck2().print(mProducts,dd,1d, null);

    }

    public void procedureOut(CloseSession cs,IAction iAction) {
        new ProcedureOutKassa().close(this,cs, iAction);
    }
}