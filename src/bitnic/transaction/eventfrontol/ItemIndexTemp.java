package bitnic.transaction.eventfrontol;

import bitnic.transaction.ItemIndex;

import java.lang.reflect.Field;

public class ItemIndexTemp{
    public final Field field;
    public final ItemIndex index;

    public ItemIndexTemp(Field field, ItemIndex index){

        this.field = field;
        this.index = index;
    }
}
