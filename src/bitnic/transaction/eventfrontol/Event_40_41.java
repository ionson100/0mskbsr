package bitnic.transaction.eventfrontol;

import bitnic.model.MProduct;
import bitnic.utils.UtilsOmsk;
import bitnic.transaction.ItemIndex;

import java.util.ArrayList;
import java.util.List;

import static bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14.parse;

//40 Оплата с вводом суммы клиента см. стр. 249
public class Event_40_41 extends EventBaseE {

    @ItemIndex(8)
    public String number_card_8;              //8 Строка Номер карт
    @ItemIndex(9)
    public String code_payment_9;              //9 Строка Код вида оплаты
    @ItemIndex(10)
    public Double type_operation_payment_10;    //10 Дробное Операция вида оплаты
    @ItemIndex(11)
    public Double summ_doc_11;                  //11 Дробное Сумма клиента в валюте оплаты   Итоговая суммадокумента в валюте
    @ItemIndex(12)
    public Double summ_client_12;               //12 Дробное Сумма клиента в базовой валютеИтоговая сумма
    @ItemIndex(13)
    public Integer kkm_operation_13 =0;            //13 Целое Операция
//    @ItemIndex(14)
//    public Integer number_smena_14=1;             //14 Целое Номер смены
    @ItemIndex(15)
    public Integer cod_action_15=0;              //15 Целое Код акции
    @ItemIndex(16)
    public Integer cod_action_16=0;               //16 Целое Код мероприятия
    @ItemIndex(17)
    public Integer code_print_group_17=0;         //17 Целое Код группы печати

    @ItemIndex(18)
    public String articul_18;                  //–/Артикул товара

    @ItemIndex(19)
    public Integer code_currency_19=1;            //19 Целое Код валюты
    @ItemIndex(20)
    public Double double_20;                  //20 Дробное –
    @ItemIndex(21)
    public Integer type_code_iterator_21=0;       //21 Целое Код вида счётчика
    @ItemIndex(22)
    public Integer kode_itrerator_22=0;           //22 Целое Код счётчика
    @ItemIndex(23)
    public Integer type_doc_23;                 //23 Целое Код вида документа
    @ItemIndex(24)
    public Integer int_10_24;                   //24 Целое –
    @ItemIndex(25)
    public Integer int_11_25;                   //25 Целое –
//    @ItemIndex(26)
//    public String info_doc_26="1/1/153";                  //26 Строка Информация о документе
//    @ItemIndex(27)
//    public Integer code_firma_27 =1;                 //27 Целое Идентификатор предприятия
    @ItemIndex(28)
    public Integer int_13_28;                   //28 Целое –
    @ItemIndex(29)
    public Integer number_ps_29=0;                //29 Целое Номер протокола ПС, через которую выполнялась оплата видом
    @ItemIndex(30)
    public String str_8_30;                     //30 Строка –
    @ItemIndex(31)
    public Integer int_15_31;                   //31 Целое –
    @ItemIndex(32)
    public Integer int_16_32;                   //32 Целое –
    @ItemIndex(33)
    public String str_9_33;                     //33 Строка –
    @ItemIndex(34)
    public String str_10_34;                    //34 Строка –
    @ItemIndex(35)
    public String str_11_35;                    //35 Строка –
    @ItemIndex(36)
    public String datetime_36;                  //36 Дата и время

    @ItemIndex(37)
    public String null_37;

    public Event_40_41() {
        super();
    }
    private static List<ItemIndexTemp> list=new ArrayList<>();
    public Event_40_41(String[] strings) {
        super();

        parse(strings,list,this);

    }


//    public static EventBaseE payment41(List<MProduct> mProducts) {
//
//        Event_40_41 e=new Event_40_41();
//        e.int_1_4=41;
//        double sum=0d;
//        for (MProduct mProduct : mProducts) {
//            sum=sum+mProduct.selectAmount*mProduct.price;
//        }
//        e.summ_doc_11= UtilsOmsk.round(sum,2);
//        e.summ_client_12=UtilsOmsk.round(sum,2);;
//        e.kkm_operation_13 =11;
//        return e;
//    }
}
