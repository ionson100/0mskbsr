package bitnic.transaction.eventfrontol;

import bitnic.transaction.ItemIndex;

import java.util.ArrayList;
import java.util.List;

import static bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14.parse;

public class Event_42_55 extends EventBaseE {

    @ItemIndex(8)
    public String number_cate_8;             //8 Строка Номера карт клиента через | –
    @ItemIndex(9)
    public String code_gut_9;                //9 Строка Коды значений разрезов через запятую0
    @ItemIndex(10)
    public Double vid_doc_egaiz_10;           //10 ДробноеВид документа ЕГАИС, на    основании  которого создан  документприхода или расхода
    @ItemIndex(11)
    public Double amount_prod_11;             //11 Дробное – Количес тво   товара
    @ItemIndex(12)
    public Double summ_doc_12;                //12 Дробное –Итогова  я сумма документа в базовой валюте
    @ItemIndex(13)
    public Integer operation_kkm_13=0;          //13 Целое Операция
//    @ItemIndex(14)
//    public Integer number_smena_14=1;           //14 Целое Номер смены
    @ItemIndex(15)
    public Double  code_klient_15=0d;            //15 Дробное Код клиента –
    @ItemIndex(16)
    public Double  double_1_16;               //16 Дробное –
    @ItemIndex(17)
    public Integer code_group_print_doc_17=0;   //17 Целое Код группы печати документа – Код ГП чека[Обмен с АСТУ]
    @ItemIndex(18)
    public String sum_bonusl_18;              //18 Строка – Сумма бонуса в базовой валюте
    @ItemIndex(19)
    public String id_prod_19;                 //19 Строка Идентификатор заказа
    @ItemIndex(20)
    public Double code_contragent_20;         //20 Дробное Код контрагента –
    @ItemIndex(21)
    public Integer amount_guest_21=0;       //21 Целое Количество посетителей –
    @ItemIndex(22)
    public Integer int_1_22=0;                  //22 Целое –
    @ItemIndex(23)
    public Integer code_doc_23=5;               //23 Целое Код вида документа
    @ItemIndex(24)
    public Integer code_coment_24=0;            //24 Целое Код комментария –
    @ItemIndex(25)
    public Integer number_parent_doc_25;      //25 Целое Номер документа, на основании которого создан текущий документ –
//    @ItemIndex(26)
//    public String  info_doc_26="1/1/155";               //26 Строка Информация о документе – Кассовый номер чека, документа и смены
//    @ItemIndex(27)
//    public Integer code_firma_27 =1;              //27 Целое Идентификатор предприятия
    @ItemIndex(28)
    public Integer cod_user_28=0;               //28 Целое Код сотрудника
    @ItemIndex(29)
    public Integer nimber_doc1_29;            //29 Целое Номер документа редактирования списка сотрудников
    @ItemIndex(30)
    public String  code_department_30;        //30 Строка Код подразделения –
    @ItemIndex(31)
    public Integer code_sale_rum_31;          //31 Целое Код зала
    @ItemIndex(32)
    public Integer code_point_32;             //32 Целое Код точки обслуживания
    @ItemIndex(33)
    public String  code_reserv_33;            //33 Строка Идентификато откладывания/резервирования –
    @ItemIndex(34)
    public String  info_user_34;              //34 Строка Пользоват. информация Значения полейкарточки
    @ItemIndex(35)
    public String  coment_35;                 //35 Строка Внешний   комментарий документа
    @ItemIndex(36)
    public String  datetime_reprice_36;       //36 Дата ивремя Дата и время  переоценки
    @ItemIndex(37)
    public Integer code_contragent2_37=0;       //37 Целое Код контрагента –
    @ItemIndex(38)
    public String  id_department_38;          //38 Строка Идентификатор  подразделения
    @ItemIndex(39)
    public String  nomber_doc_egaiz_39;       //39 Строка  Номер   документа  ЕГАИС


    @ItemIndex(40)
    public String  null_40;
    @ItemIndex(41)
    public String  null_41;
    @ItemIndex(42)
    public String  null_42;
    @ItemIndex(43)
    public String  null_43;
    @ItemIndex(44)
    public String  null_44;


    public Event_42_55() {
        super();
    }
    private static List<ItemIndexTemp> list=new ArrayList<>();
    public Event_42_55(String[] strings) {
        super();

        parse(strings,list,this);
    }
}
