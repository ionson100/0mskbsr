package bitnic.transaction.eventfrontol;


import bitnic.transaction.ItemIndex;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EventBaseE {

    @ItemIndex(14)
    public Integer number_smena_14=1;             //14 Целое Номер смены
    @ItemIndex(26)
    public String info_doc_26;           //26 Строка Информация о документе///////////////////////////
    @ItemIndex(27)
    public Integer code_firma_27 = 1;        //27 Целое Идентификатор предприятия


    @ItemIndex(1)
    public Integer numberAction_1;           //1 Целое № транзакции
    @ItemIndex(2)
    public String dateAction_2;         //2 Дата Дата транзакции
    @ItemIndex(3)
    public String timeAction_3;         //3 Время Время транзакции
    @ItemIndex(4)
    public Integer int_1_4;             //4 Целое
    @ItemIndex(5)
    public Integer code_pm_5 ;           //5 Целое Код РМ
    @ItemIndex(6)
    public Integer nimberDoc_6;         //6 Целое Номер документа
    @ItemIndex(7)
    public Integer codeKassir_7;        //7 Целое Код кассира

    public String join() throws Exception {//throws Exception


        List<Field> list=new ArrayList<>();
        {
            Field fields[] = this.getClass().getSuperclass().getDeclaredFields();
            for (Field field : fields) {
                list.add(field);
            }
        }
        {
            Field fields[] = this.getClass().getDeclaredFields();
            for (Field field : fields) {
                list.add(field);
            }
        }


        List<ItemIndexTemp> temps = new ArrayList<>(list.size());
        for (Field field : list) {
            ItemIndex f = field.getAnnotation(ItemIndex.class);
            if (f != null) {
                temps.add(new ItemIndexTemp(field, f));
            }
        }
        Collections.sort(temps, new Comparator<ItemIndexTemp>() {
            @Override
            public int compare(ItemIndexTemp o1, ItemIndexTemp o2) {
                return Integer.compare(o1.index.value(), o2.index.value());
            }
        });
        StringBuilder  sb=new StringBuilder();

        for (ItemIndexTemp temp : temps) {
            Object val=temp.field.get(this);
            if(val==null||val.toString().equals("")){
                sb.append("");
            }else {
                if(val.toString().equals("0.0")){
                    sb.append("0");
                }else {
                    sb.append(val);
                }

            }

            sb.append(";");
        }
        return sb.toString().substring(0,sb.length()-1);

    }


}

