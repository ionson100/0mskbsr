package bitnic.pagecheckarchive;

import bitnic.checkarchive.CheckArchiveE;
import bitnic.orm.Configure;
import bitnic.pagetableproduct.IFocusable;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FPagecheckArchive  extends GridPane implements Initializable, IFocusable {

    public TextField tf_search;

    public HBox vbox_search;
    public CheckBox cb_autoupdate;
    private static final Logger log = Logger.getLogger ( bitnic.pageerror.PageError.class );

    public TableView table_error;
    public GridPane grid1;

    List<CheckArchiveE> list = new ArrayList<>();

    public FPagecheckArchive () {
        try {
            InputStream inputStream = getClass ().getClassLoader ().getResource ( "aa_ru_RU.properties" ).openStream ();
            ResourceBundle bundle = new PropertyResourceBundle( inputStream );
            FXMLLoader fxmlLoader = new FXMLLoader ( getClass ().getResource ( "f_pagecheck_archive.fxml" ) , bundle );
            fxmlLoader.setRoot ( this );
            fxmlLoader.setController ( this );

            fxmlLoader.load ();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException ( exception );
        }
    }

    @Override
    public void focus() {

        Platform.runLater(() -> tf_search.requestFocus());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if ( SettingAppE.getInstance ().isTypeЫShow3D) {
            vbox_search.getStyleClass ().add ( "footer_3d" );

        } else {
            vbox_search.getStyleClass ().add ( "footer" );
        }

        UtilsOmsk.resizeNode ( this , grid1 );
        UtilsOmsk.painter3d ( tf_search );



        tf_search.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(newValue.trim());
            list= Configure.getSession().getList(CheckArchiveE.class,newValue.trim());
            if(list!=null){
                table_error.getItems().clear();
                new BuilderTable().build( list , table_error );
            }

        });
        tf_search.setText(tf_search.getText()+String.valueOf(SettingAppE.getInstance().getSession()));
//        list=new CheckArchiveE().getList(" session_id = ?",SettingAppE.getInstance().getSession());
//        new BuilderTable().build( list , table_error );

    }
}
