package bitnic.pagestoryall;

import bitnic.checkarchive.CheckArchiveE;
import bitnic.model.MError;
import bitnic.orm.Configure;
import bitnic.pagetransactions.PageTransaction;
import bitnic.table.BuilderTable;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FPageStoryAll extends GridPane implements Initializable {

    private static final Logger log = Logger.getLogger(PageTransaction.class);
    public TableView table_error;
    public GridPane grid1;
    List<MError> list = new ArrayList<>();

    public FPageStoryAll() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_story_all.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        List<CheckArchiveE> checkArchives= Configure.getSession().getList(CheckArchiveE.class,null );
        for (CheckArchiveE checkArchive : checkArchives) {
            String res= " "+checkArchive.session_id+"     11      data&&";
            MError mError=new MError();
            mError.msg=res;
            list.add(mError);
        }
        new BuilderTable().build( list , table_error );
        table_error.scrollTo(table_error.getItems ().size()-1);

    }
}