package bitnic.pagesettings.pagesettingsitem;

import bitnic.pagesale.CustomScrollEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FSettingsItem extends GridPane implements Initializable {

    Logger log=Logger.getLogger(getClass());
    private double lastYposition=0d;

    private Color color;
    private final Node node;
    private final String des;
    public VBox vbox_base;
    public   Label label_description;

    public void refreshColor(Color color){
        if(color!=null){
            String aa="-fx-text-fill: #"+ colorToHex(color);
            label_description.setStyle(aa);
        }
    }


    public FSettingsItem(Node  node,String des) {

        this.node = node;
        this.des = des;
        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_settings_item.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    public FSettingsItem(Color color,Node  node, String des) {
        this.color = color;
        this.node = node;
        this.des = des;
        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_settings_item.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if(color!=null){
            String aa="-fx-text-fill: #"+ colorToHex(color);
            label_description.setStyle(aa);
        }

        label_description.setWrapText(true);
        vbox_base.getChildren().add(node);
        label_description.setText(des);

        this.setOnMousePressed(event -> lastYposition = event.getSceneY());

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });


    }
    public static String colorToHex(Color color) {
        return colorChanelToHex(color.getRed())
                + colorChanelToHex(color.getGreen())
                + colorChanelToHex(color.getBlue())
                + colorChanelToHex(color.getOpacity());
    }
    private static String colorChanelToHex(double chanelValue) {
        String rtn = Integer.toHexString((int) Math.min(Math.round(chanelValue * 255), 255));
        if (rtn.length() == 1) {
            rtn = "0" + rtn;
        }
        return rtn;
    }
}
