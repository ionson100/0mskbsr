package bitnic.pagesettings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SettingItem {
    int index() default 0;
    TypeSettings type() default TypeSettings.integer;
    String description() default "";
    boolean isAdmin() default false;
    String key() default "";
    boolean isFontSize() default false;

}

