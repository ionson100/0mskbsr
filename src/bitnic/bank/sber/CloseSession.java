package bitnic.bank.sber;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.kassa.BaseKassa;
import bitnic.orm.Configure;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class CloseSession extends BaseKassa {

    private static final Logger log = Logger.getLogger(CloseSession.class);
    private bitnic.dialogfactory.closesessionsber.CloseSession closeSession;
    private IAction iActionProcedure;

    public void  close(bitnic.dialogfactory.closesessionsber.CloseSession closeSession, IAction iActionProcedure){
        this.closeSession = closeSession;
        this.iActionProcedure = iActionProcedure;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            IFptr fptr = null;

            try {
                fptr = new Fptr();
                fptr.create();
                setWorkDir(fptr);
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                if (fptr.GetStatus() < 0)
                    checkError(fptr);

//               7
//               Сверка итогов
//Число наличных оплат, 0, сумма наличных оплат, коп, номер кассовой смены]


                int sum=(int)closeSession.summ_action*100;


                log.error(String.format("INFO: Закрытие банковской смены : итого чеков - %s   итого на сумму - %s коп.",closeSession.amount_transaction,sum));

                BankResult result = ActionSale.exe(7, closeSession.amount_transaction,0,sum,5);
               // BankResult result = ActionSale.exe(7, 1,0,200,5);
                if(result.result==0){
                    for (String ss : result.linesP) {
                        printText(fptr, ss, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    }
                }else {
                    throw new RuntimeException(result.linesE.get(0));
                }


            }catch (Exception ex){
                exception=ex;
                log.error ( ex );

            }finally {
                if(fptr!=null){
                    fptr.ResetMode();
                    fptr.destroy();
                }

            }


            return null;
        }

        @Override
        protected void onPreExecute(){

            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {

            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.errorDialog(exception);
                if(iActionProcedure!=null){
                    iActionProcedure.action(exception.getMessage());
                }
            }else {

                if(iActionProcedure!=null){
                    iActionProcedure.action(null);
                }
                Configure.getSession().execSQL("Update archive set is_send_bank = ?",true);
            }

        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }


    }



}
