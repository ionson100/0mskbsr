package bitnic.bank.sber;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.log4j.Logger;

import java.io.File;

public class ActionSale {


    private static final Logger log = Logger.getLogger(ActionSale.class);


    public static BankResult exe(Object... params) throws Exception {


        org.apache.commons.exec.CommandLine commandLine = new CommandLine(BankBase.workDir + "sb_pilot");
        for (Object argument : params) {
            commandLine.addArgument(String.valueOf(argument));
        }

        DefaultExecutor exec = new DefaultExecutor();
        exec.setWatchdog(new ExecuteWatchdog(ExecuteWatchdog.INFINITE_TIMEOUT));
        exec.setWorkingDirectory(new File(BankBase.workDir));
        exec.setExitValues(BankBase.good);
        exec.execute(commandLine);
        BankResult res = new BankResult();

        return res;
    }


}

