package bitnic.bank.sber;

import bitnic.checkarchive.CheckArchiveE;
import bitnic.dialogfactory.closesessionsber.CloseSession;

import bitnic.orm.Configure;
import bitnic.settingscore.SettingAppE;

import java.util.List;

public class SummatorBankingAction {



    public static bitnic.dialogfactory.closesessionsber.CloseSession  getCloseSession() {

        CloseSession closeSession=new CloseSession();
        List<CheckArchiveE> checkArchiveES= Configure.getSession().getList(CheckArchiveE.class," is_send_bank =?", false);
        int amount=0;
        Double sum =0d;
        for (CheckArchiveE checkArchiveE : checkArchiveES) {
            if(checkArchiveE.is_bank){
                amount=amount+1;
                sum = sum +checkArchiveE.summ;
            }
        }
        closeSession.session=SettingAppE.getInstance().getSession();
        closeSession.amount_transaction=amount;
        closeSession.summ_action= sum;
        return closeSession;


    }
}
