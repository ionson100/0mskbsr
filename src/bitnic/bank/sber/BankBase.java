package bitnic.bank.sber;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.log4j.Logger;

import java.io.File;

public class BankBase {

    private static final Logger log = Logger.getLogger(BankBase.class);
    public static final String workDir = "/home/bsr/bank/sb/";
    public static final int[] good;

    static {
        good = new int[5200];
        for (int i = 0; i < 5200; i++) {
            good[i] = i;
        }
    }

    private IAction<BankResult> bankResultIAction;
    private Object[] params;


    private BankResult exe() throws Exception {

        System.out.println(workDir + "sb_pilot");
        org.apache.commons.exec.CommandLine commandLine = new CommandLine(workDir + "sb_pilot");
        for (Object argument : params) {
            System.out.println(argument);
            commandLine.addArgument(String.valueOf(argument));
        }

        DefaultExecutor exec = new DefaultExecutor();
        exec.setWorkingDirectory(new File(workDir));
        exec.setWatchdog(new ExecuteWatchdog(ExecuteWatchdog.INFINITE_TIMEOUT));
        exec.setExitValues(good);
        exec.execute(commandLine);
        BankResult res = new BankResult();
        return res;

    }

    public void execute(IAction<BankResult> bankResultIAction, Object... params) {
        this.bankResultIAction = bankResultIAction;

        this.params = params;
        new MyWorker().execute(null);
    }

    private Exception exceptione;
    private BankResult result;

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            try {

                result = exe();

            } catch (Exception ex) {
                exceptione = ex;
                log.error(ex);

            }

            return null;
        }

        @Override
        protected void onPreExecute()  {

            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {

            FactoryBlender.stop();
            if (exceptione != null) {
                DialogFactory.errorDialog(exceptione);
            } else {
                bankResultIAction.action(result);
            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }


}

