package bitnic.bank.sber;


import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class BankResult {

    private static final Logger log = Logger.getLogger(BankResult.class);

    public BankResult() throws Exception{

            List<String> lines ;
            Charset cset = Charset.forName("KOI8-R");
            if(new File(BankBase.workDir+"e").exists()){
                lines = Files.readAllLines(Paths.get(BankBase.workDir+"e"),cset);
                for (String line : lines) {
                    System.out.println(line);
                }
                for (Field field : BankResult.class.getDeclaredFields()) {
                    BankLines bankLines= field.getAnnotation(BankLines.class);
                    if(bankLines==null) continue;
                    int i=bankLines.value();
                    if(i>lines.size()) continue;
                    field.set(this,lines.get(i-1));
                }
            }
            if(new File(BankBase.workDir+"p").exists()){
                linesP=Files.readAllLines(Paths.get(BankBase.workDir+"p"),cset);
            }

            if(new File(BankBase.workDir+"e").exists()){
                linesE=Files.readAllLines(Paths.get(BankBase.workDir+"e"),cset);
                result=Integer.parseInt(linesE.get(0).substring(0,linesE.get(0).indexOf(",")));
            }
    }

    public List<String> linesP;
    public List<String> linesE;

    public int transaction;
    public int result=-1;

    public List<Object> params=new ArrayList<>();

    @BankLines(1)
    public String result_1;                     //  1   0,Успешно                  Код результата, текст сообщения

    @BankLines(2)
    public String numberCard_2;                 //  2   4276********2106           Номер карты (маскированный)

    @BankLines(3)
    public String SrokCard_3;                   //  3   10/09                      Срок действия карты

    @BankLines(4)
    public String codeAutorurize_4;             //  4   013AU3                     Код авторизации

    @BankLines(5)
    public String innerNumberOperation_5;       //  5   0007                       Внутренний номер операции

    @BankLines(6)
    public String nameCard_6;                   //  6   VISA                       Название типа карты

    @BankLines(7)
    public String isSberbank_7;                 //  7   1                          Признак карты Сбербанка (1)

    @BankLines(8)
    public String numberTerminal_8;             //  8   00870001                   Номер терминала

    @BankLines(9)
    public String dateOperation_9;              //  9   20120403173415             Дата-время операции (ГГГГММДДччммсс)

    @BankLines(10)
    public String linkNumberCard_10;             //  10                             Ссылочный номер операции (может быть пустым)

    @BankLines(11)
    public String hash_11;                       //  11  481CF86160609155A23        Хеш от номера карты

    @BankLines(12)
    public String trackCard_12;                  //  12                             Трек3 карты

    @BankLines(13)
    public String summBonus_13;                  //  13  234                        Сумма, оплаченная бонусами «Спасибо», коп

    @BankLines(14)
    public String numberMerchage_14;             //  14  9944444445555              Номер мерчанта

    @BankLines(15)
    public String typeMessage_15;                //  15  00                         Тип сообщения система мониторинга. 0 – обычное, 1 срочное

    @BankLines(16)
    public String stateGPZ_16;                   //  16  00                         Состояние ГПЦ. 0 - работает, 1-возможны сбои, 2-не работает.

    @BankLines(17)
    public String messageProcessing_17;          //  17  Процессинг Cбербанка работ Сообщение системы мониторинга. Если сообщение содержит символы переноса строк, они будут за  менены символом '|'

    @BankLines(18)
    public String numberProgrammLoyalnocty_18;   //  18  3                          Номер программы лояльности в который попадает карта. Если программы не настроены на терминале, или карта ни под одну из программ лояльности, возвращается 0

    @BankLines(19)
    public String answerUser_19;                 //  19  +1231234567                oтвет пользователя на команду 49

    @BankLines(20)
    public String GuidOperation_20;              //  20  80C336EB                   Уникальный идентификатор операции




}

