package bitnic.bank.sber;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.kassa.BaseKassa;
import bitnic.senders.AsyncTask;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class ControlCheck extends BaseKassa {

    private static final Logger log = Logger.getLogger(ControlCheck.class);
    //Exception exception;
    private int param;
    private BankResult resultAction1;


    public void print(int param) {

        this.param = param;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            IFptr fptr = null;

            try {
                fptr = new Fptr();
                fptr.create();
                setWorkDir(fptr);
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                if (fptr.GetStatus() < 0)
                    checkError(fptr);

                FactoryBlender.addMessage("Запрос в банк");
                log.info("Начало транзакции снятия контрольной ленты ");
                resultAction1 = ActionSale.exe(9,param);

                if (resultAction1.result == 0) {
                    log.info("Транзакция код завершения  - 0");
                    log.info("печать отчета");

                    try{
                        printBankCheck(fptr,resultAction1.linesP);
                    }catch (Exception ex){
                        log.error("При распечатке чека банка при продаже произошла ошибка: "+ex.getMessage());
                    }

                } else {
                    throw new RuntimeException(resultAction1.linesE.get(0));
                }

            } catch (Exception ex) {
                exception = ex;
                log.error(ex);

            } finally {
                if(fptr!=null){
                    fptr.ResetMode();
                    fptr.destroy();
                }
            }


            return null;
        }

        @Override
        protected void onPreExecute()  {

            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {

            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.errorDialog(exception.getMessage());
            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
