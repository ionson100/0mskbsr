package bitnic.core;

import bitnic.blenderloader.FactoryBlender;
import org.apache.commons.exec.*;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;


public class ExeScript {
    private static final Logger log = Logger.getLogger(ExeScript.class);
    int iExitValue;
    String sCommandString;

    public void runScript(String command){


        sCommandString = command;
        CommandLine oCmdLine = CommandLine.parse(sCommandString);

        DefaultExecutor oDefaultExecutor = new DefaultExecutor();
        oDefaultExecutor.setExitValue(0);
        try {
            iExitValue = oDefaultExecutor.execute(oCmdLine);
        } catch (Exception e) {
            log.error ( e );
        }
    }

}
