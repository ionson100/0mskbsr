package bitnic.core;

import bitnic.bank.sber.BankBase;
import bitnic.bank.sber.CloseSession;
import bitnic.bank.sber.ControlCheck;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.header.Cheader;
import bitnic.kassa.GetIsOpenSession;
import bitnic.kassa.PrintReportTovar;

import bitnic.message.MessageBody;
import bitnic.model.MRequest;
import bitnic.model.MTest;
import bitnic.model.MTransactions;
import bitnic.orm.Configure;
import bitnic.orm.ISession;
import bitnic.pageadmin.TableAdmin;
import bitnic.pageamountself.PageSelfAmount;
import bitnic.pageamountselfcore.PageSelfAmountCore;
import bitnic.pagechat2.FChat2;
import bitnic.pagecheck.FWorkChesk;
import bitnic.pagecheckarchive.FPagecheckArchive;
import bitnic.pageconstrictor.FConsdtructorCheck;
import bitnic.pageerror.PageError;
import bitnic.pagegoods.FPageGoods;
import bitnic.pagehistory.FPageHistory;
import bitnic.pagemessage.PageMessage;
import bitnic.pagereport.PageReport;
import bitnic.pagerequest.FPageRequest;
import bitnic.pagereturncheck.PageReturnCheck;
import bitnic.pagesale.MainForm;
import bitnic.pagesettings.FEditSettings;
import bitnic.pagesettingskkm.FSettingsKkm;
import bitnic.pagestoryall2.FPageStoryAll2;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.pagetableproduct.TableProduct;
import bitnic.pagetransactions.PageTransaction;
import bitnic.parserfrontol.Testfrontol;
import bitnic.procedureInOut.ProcedureIn;
import bitnic.procedureInOut.ProcedureOut;
import bitnic.senders.IAction;
import bitnic.senders.SenderPointHistory;
import bitnic.senders.SenderRequestFromServer;

import bitnic.senderstate.CheckStateImage;
import bitnic.settingscore.SettingAppE;
import bitnic.settingscore.SettingAppMode;
import bitnic.settingscore.SettingsE;
import bitnic.transaction.Transaction;
import bitnic.updateapp.RollBackApp;
import bitnic.updateapp.StarterUpdate;
import bitnic.updateapp.TimerAppUpdate;
import bitnic.updateapp.UpdateApp;
import bitnic.utils.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private static final Logger log = Logger.getLogger(Controller.class);
    public static TimerAppUpdate timerUpdate;
    public static Node curnode;
    public static MainForm ccmainform;
    private static Controller instans;
    static final Object lock = new Object();
    public Label label_acquery;
    public MenuButton bt_action_menu;
    public MenuItem bmOFD;
    public MenuItem bm_story_1;
    public MenuItem bm_story_2;
    public MenuItem bm_aquering_1;
    public MenuItem bm_aquering_2;
    //public MenuItem bm21;
    public ImageView image_state;
    public HBox hbox_footer;
    public MenuItem bm1;
    public MenuItem bm2;
    public MenuItem bm3;
    public MenuItem bm4;
    public MenuItem bm5;
    public MenuItem bm6;

    public MenuItem bm8;
    public MenuItem bm9;
    public MenuItem bm10;
    public MenuItem bm11;
    public MenuItem bm12;
    public MenuItem bm13;
    public MenuItem bm14;
    public MenuItem bm15;
    public MenuItem bm16;
    public MenuItem bm20;
    //public MenuItem bm17;
    public MenuItem bm18;

    public HBox panel_buttons;
    public MenuButton bt_menu_admin, bt_menu_kassa, bt_menu_story, bt_menu_aquering;//bt_menu_settings, bt_menu_kassa_settings,;
    public Label label_profile, label_1, label_2, label_3, label_4, label_version, label_point_id, label_session, label_is_open_session;
    public GridPane grid_host;
    public StackPane stack_panel;
    @FXML
    public GridPane header;
    public MenuItem bm212;
    public MenuItem bm100;
    @FXML
    Button bt_menu_sale;
    private ResourceBundle bundle;
    private TextField text_status_app;

    public Controller(){
        instans=this;
    }

    public static void timerUpdateStop() {
        timerUpdate.stop();
    }

    public static Controller getInstans() {
        return instans;
    }

    public static void refreshProfileApp() {
        instans.label_profile.setText(String.format("Профиль: %s", SettingAppE.getInstance().getNameProfile()));
        instans.label_acquery.setText(String.format("Эквайринг: %s", SettingAppE.getInstance().getNameAcquaring()));
        if (SettingAppE.getInstance().getAcquire() != 0) {

        }
    }

    public static void writeStateKkm(String s1, String s2, String s3, String s4) {//, boolean isOpenSession
        // выручка
        //Сумма наличности в ККМ (Summ)
        //Cумма сменного итога приход
        // сумма безнал
        try {
            if (instans != null)
                Platform.runLater(() -> {
                    instans.label_1.setText("Выручка: " + s1);
                    //instans.label_1.setVisible(false);
                    instans.label_2.setText("Наличность в Д.Я.: " + s2);
                    instans.label_3.setText("Сменный итог: " + s3);
                    instans.label_session.setText("Смена: " + s4);

                    refreshBezNal();
                    refreshOpenCloseSession();//isOpenSession
                });
        } catch (Exception ex) {
            log.error(ex);
        }


    }

    public static void writeMessage(String message) {
        synchronized (lock) {
            Platform.runLater(() -> {
                if (instans == null || instans.text_status_app == null) return;
                instans.text_status_app.setText(message);
            });
        }
    }

    public synchronized static void writeMessageEntrnetOk() {

        Platform.runLater(() -> {
            if (instans == null || instans.text_status_app == null) return;
            instans.text_status_app.setStyle("");
            if (instans.text_status_app.getText().equals(Support.str("name301"))) {
                instans.text_status_app.setText(Support.str("name303"));
            }
        });

    }

    public synchronized static void writeMessageEror(String message) {

        Platform.runLater(() -> {
            if (instans == null || instans.text_status_app == null) return;
            instans.text_status_app.setStyle("-fx-background-color: #f61234;-fx-text-fill: white");
            instans.text_status_app.setText(message);
        });

    }

    public static void openChat() {
        FXMLLoader loader = new FXMLLoader(FChat2.class.getResource("f_chat2.fxml"));
        Node pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        Controller.instans.stack_panel.getChildren().add(pane);
    }

    public static void refreshBezNal() {

        if (instans != null) {
            try {
                Object res = Configure.getSession().
                        executeScalar(" select sum(summ) from archive where session_id = ? and  date_return = 0  and is_bank = 1 ", SettingAppE.getSession());
                if (res == null) {
                    instans.label_4.setText("Б.Нал.: 0.0");
                } else {
                    instans.label_4.setText("Б.Нал.: " + String.valueOf((double) res));
                }
            } catch (Exception ex) {
                log.error(ex);
            }

        }

    }

    public static void refreshImageState(String s1, String s2) {
        try {

            log.info("Статус отправки");
            log.info(s1);
            log.info(s2);
            Platform.runLater(() -> {
                if (instans == null) return;
                if (s1.equals(s2)) {
                    instans.image_state.setImage(new Image("/bitnic/image/oksender.png"));
                } else {
                    instans.image_state.setImage(new Image("/bitnic/image/nosender.png"));
                }
            });


        } catch (Exception e) {
            log.error(e);
        }
    }

    public static void refreshOpenCloseSession() {//Boolean isOpen
        if (instans == null) return;
        Platform.runLater(() -> {

            int i = SettingAppE.getShowButtonUpdate();

            switch (i) {

                case 0: {
                    instans.bm13.setVisible(false);
                    instans.bm18.setVisible(true);
                    instans.label_is_open_session.setText("Смена открыта");
                    break;
                }
                case -1: {
                    instans.bm13.setVisible(true);
                    instans.bm18.setVisible(true);
                    instans.label_is_open_session.setText("");
                    break;
                }
                default: {
                    instans.bm13.setVisible(true);
                    instans.bm18.setVisible(false);
                    instans.label_is_open_session.setText("Смена закрыта");
                }
            }
//
//
//            if (i == -1) {
//
//            } else if (isOpen == true) {
//
//            } else if (isOpen == false) {
//
//            }
        });

    }

    public void factoryPage(Node node) {

        //todo !!!
        curnode = node;

        if (node instanceof MainForm) {
            ccmainform = (MainForm) node;
        } else {
            ccmainform = null;
        }
        if (grid_host.getChildren().size() == 0) {
            grid_host.getChildren().add(node);
        } else {
            Node o = grid_host.getChildren().get(0);
            if (o instanceof IDestroy) {
                ((IDestroy) o).destroy();
            }
            grid_host.getChildren().clear();
            grid_host.getChildren().add(node);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


       // Configure.getSession().deleteTable("test_1");

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                int dd=0;
//                try{
//                    while (1==1){
//                        System.out.println("_"+dd++);
//                        MTest mTest=new MTest();
//                        mTest.datas=100;
////                        ISession ses=Configure.getSession();
////                        ses.beginTransaction();
//                        Configure.getSession().insert(mTest);
//                       // ses.commitTransaction();
//                        //Thread.sleep(1);
//                    }
//
//                }catch (Exception ex){
//                    System.out.println("add______________"+ex);
//                }
//
//            }
//        }).start();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                int dd=0;
//                try{
//                    while (1==1){
//                        System.out.println(dd++);
//                       // ISession ses=Configure.getSession();
//                      //  ses.beginTransaction();
//                        List<MTest> mTests=Configure.getSession().getList(MTest.class,null);
//                        for (MTest mTest : mTests) {
//                            Configure.getSession().delete(mTest);
//                        }
//                       // ses.commitTransaction();
//                        //Thread.sleep(2);
//                    }
//
//                }catch (Exception ex){
//                    System.out.println("delete______________"+ex);
//                }
//
//            }
//        }).start();


//        try {
//            Thread.sleep(100000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }


        instans = this;
        CheckStateImage.check();
        timerUpdate = new TimerAppUpdate();
        timerUpdate.run(SettingAppE.getInstance().tumerIntervalUpdate, this);


        KeyCombination key1 = new KeyCodeCombination(KeyCode.F1, KeyCombination.CONTROL_ANY);
        KeyCombination key2 = new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_ANY);
        KeyCombination key3 = new KeyCodeCombination(KeyCode.F3, KeyCombination.CONTROL_ANY);
        KeyCombination key4 = new KeyCodeCombination(KeyCode.F4, KeyCombination.CONTROL_ANY);
        KeyCombination key5 = new KeyCodeCombination(KeyCode.F5, KeyCombination.CONTROL_ANY);
        KeyCombination key6 = new KeyCodeCombination(KeyCode.F6, KeyCombination.CONTROL_ANY);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stack_panel.addEventHandler(KeyEvent.KEY_RELEASED, event -> {
            if (SettingAppE.getInstance().user_id == null) return;
            if (SettingAppE.getInstance().user_id.equals("1") == false) return;
            if (key1.match(event)) {
                bt_menu_sale.requestFocus();
                bt_menu_sale.fire();

            }
            if (key2.match(event)) {
                bt_action_menu.requestFocus();
                bt_action_menu.fire();

            }
            if (key3.match(event)) {
                bt_menu_kassa.requestFocus();
                bt_menu_kassa.fire();

            }
            if (key4.match(event)) {
                bt_menu_story.requestFocus();
                bt_menu_story.fire();

            }
            if (key5.match(event)) {
                bt_menu_aquering.requestFocus();
                bt_menu_aquering.fire();

            }
            if (key6.match(event)) {
                bt_menu_admin.requestFocus();
                bt_menu_admin.fire();

            }

        });


        bundle = resources;
        String line = null;
        if (DesktopApi.getOs() == DesktopApi.EnumOS.linux) {
            String[] cmd = {
                    "/bin/sh", "-c", "cat /etc/*-release"};


            try {

                Process p = Runtime.getRuntime().exec(cmd);
                try(BufferedReader bri = new BufferedReader(new InputStreamReader(
                        p.getInputStream()))){
                    while ((line = bri.readLine()) != null) {
                        break;
                    }
                }


            } catch (IOException e) {
                log.error(e);
            }
        }

        String name = ManagementFactory.getRuntimeMXBean().getName();
        String str = "ИНФО: Старт программы Process ID for this app = " + name;
        if (line != null) {
            str = str + " Linux: " + line;
        }
        str = str + " версия: " + String.valueOf(SettingAppE.getVersion());
        log.error(str);


        try {
            label_version.setText(bundle.getString("name120") + String.valueOf(SettingAppE.getInstance().getVersion()));
        } catch (Exception ex) {
            log.error(ex);
        }

        refreshPointid();


        //StarterUpdate.checkUpdate(this);
        refreshMenu();
        refreshProfileApp();


        if (header != null) {
            Label label_user = (Label) header.lookup("#label_user");
            text_status_app = (TextField) header.lookup("#status_app");
            label_user.setText(SettingAppE.getInstance().getUserName());
            text_status_app.setText("");
        }


        factoryPage(new MainForm());
        writeMessage(bundle.getString("name107"));


        //new StateKassaMoney().print();

        if (SettingAppE.getInstance().isTypeЫShow3D) {
            hbox_footer.getStyleClass().add("footer_3d");
        } else {
            hbox_footer.getStyleClass().add("footer");
        }


    }

    public void onMenu(ActionEvent actionEvent) {

        String id = "";
        if (actionEvent.getSource() instanceof Node) {
            id = ((Node) actionEvent.getSource()).getId();
        } else if (actionEvent.getSource() instanceof MenuItem) {
            id = ((MenuItem) actionEvent.getSource()).getId();
        }
        if (id.equals("bt_menu_sale")) {
            factoryPage(new MainForm());
        } else if (id.equals("bt_menu_admin")) {
            factoryPage(new TableAdmin());
        }
    }

    public void onRegistration(ActionEvent actionEvent) {

    }

    public void onUpdateDateFromServer() {

        if (SettingAppE.getInstance().getPointId() == 0) {
            DialogFactory.infoDialog("Обновление", bundle.getString("name108"));
        } else {
            if (SettingAppE.getInstance().selectProduct.size() > 0) {
                DialogFactory.infoDialog("Обновление", bundle.getString("name109"));
            } else {
                new Testfrontol().test(o -> {
                    if (o == null) {
                        if (Controller.curnode != null) {
                            if (curnode instanceof IRefreshLoader) {
                                ((IRefreshLoader) curnode).refresh();
                            }
                        }
                    }

                    return false;
                });
            }
        }


    }

    public void onFtpSettings() {
        DialogFactory.settingsFtpDialog();
    }

    public void onRegUser() {
        if (SettingAppE.getInstance().selectProduct.size() > 0) {
            DialogFactory.infoDialog("Смена пользователя", bundle.getString("name110"));
        } else {
            DialogFactory.regUserDialog();
        }

    }

    public void onUnRegUser() {

        if (SettingAppE.getInstance().selectProduct.size() > 0) {
            DialogFactory.infoDialog("Выход пользователя", bundle.getString("name111"));
        } else {

            DialogFactory.questionDialog(Support.str("name191"), Support.str("name192"),
                    new ButtonAction(Support.str("name193"), o -> {
                        SettingAppE.getInstance().user_id = null;
                        SettingAppE.save();
                        SettingAppE.refreshInstance();
                        Cheader.refreshUser();
                        refreshMenu();
                        Controller.getInstans().factoryPage(new MainForm());
                        return false;
                    }));
        }
    }

    public void onTest() {
        log.error("test");
    }

    public void onSettingsKassa() {
        bitnic.kassa.SettingKassa.print();
    }

    public void onStateKassa() {
        new bitnic.kassa.StateKassa().print(stateKKM -> {
            DialogFactory.stateKkmDialog(stateKKM);
            return false;
        });
    }

    public void onUserSettingsKassa() {
        new bitnic.kassa.UserSettungsKmm().print(null);
    }

    public void onCloseCheckKassa() {
        new bitnic.kassa.CloseCheckKmm().print();
    }

    public void onZReport() {
        DialogFactory.printCheck(Support.str("name308"), o -> {
            new Transaction().reportZ();
            return false;
        });
    }

    public void onCheckOfd() {
        new bitnic.kassa.CheckOfd().print();
    }

    public void onGridProducts() {

        factoryPage(new TableProduct());
    }

    public void onSendErrorToServer() {

        DialogFactory.dialogSendToEmail();
        // new Thread(new SenderToEmail()).start();

    }

    public void onSettingOfd() {
        DialogFactory.settingsOfdDialog();
    }

    public void onAppMode() {
        DialogFactory.appModeDialog();
    }

    public void onSend1C() {
//        if (SettingAppE.getInstance().getProfile() == 1) {
//            DialogFactory.infoDialog("Отправка 1С", bundle.getString("name112"));
//        } else {
//            new Sender_1C().send();
//        }

    }

    public void onChat() {


        openChat();
    }

    public void onMenuWorkCheck() {
        factoryPage(new FWorkChesk());
    }


    public void onShutdown() {
        if (DesktopApi.getOs() == DesktopApi.EnumOS.linux) {
            DialogFactory.questionDialog(Support.str("name146"), Support.str("name147"),
                    new ButtonAction(Support.str("name148"), o -> {
                        //Shutdown
                        log.error("ИНФО: Выключение компьютера");
                        SettingAppE.save();
                        Cheader.timmerPingStop();
                        Controller.timerUpdateStop();
                        WorkingFile.sendBitnic(o1 -> {

                            Main.stopSocked();
                            ExeScript testScript = new ExeScript();
                            testScript.runScript("shutdown -h now");
                            return false;
                        });
                        return false;
                    }),
                    new ButtonAction(Support.str("name149"), o -> {
                        log.info("ИНФО: Перезагрузка компьютера");
                        SettingAppE.save();
                        Controller.timerUpdateStop();
                        Cheader.timmerPingStop();

                        Cheader.timmerPingStop();
                        WorkingFile.sendBitnic(o12 -> {
                            Main.stopSocked();
                            ExeScript testScript = new ExeScript();
                            testScript.runScript("reboot");
                            return false;
                        });
                        return false;
                    }), new ButtonAction("3", o -> {
                        log.info("ИНФО: Закрытие программы");
                        Controller.timerUpdateStop();
                        SettingAppE.save();


                        WorkingFile.sendBitnic(o13 -> {
                            //todo !!
                            Main.stopSocked();
                            Platform.exit();
                            System.exit(0);
                            return false;
                        });

                        return false;
                    }));
        } else {
            DialogFactory.infoDialog(Support.str("name146"), Support.str("name151"));
        }

    }

    public void onSettingsKkm() {
        factoryPage(new FSettingsKkm());
    }

    public void onPrintBarcode() {
        DialogFactory.printBarcodeDialog();
    }

    public void onContributingMoney() {
        DialogFactory.contributindMoneyDialog();
    }

    public void onEntryMoney() {
        DialogFactory.entryMoneyDialog();
    }

    public void onPrintLastDoc() {
        new bitnic.kassa.PrintLastCheck().print();
    }

    public void onXReport() {
        new Transaction().reportX();

    }

    public void onOpenSmena() {

        new Transaction().openSession();

    }

    public void onRegFirma() {
        DialogFactory.segCodeFirmDialig();
    }

    public void onPrintLabelUser() {
        PrintUserLabel.print();
    }

    public void onConstructorCheck() {
        factoryPage(new FConsdtructorCheck());
    }

    public void onCheckStory() {
        // DialogFactory.checkStoryDialog();
        factoryPage(new PageTransaction());
    }

    public void onOpenInFile() {

        try {
            String patch = Pather.inFilesData;
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.errorDialog(ex);
            ex.printStackTrace();
            log.error(ex);
        }

    }

    public void onOpenOutFile() {
        //todo assa
        try {
            String patch = "temp_report.txt";
            File file=new File(patch);
            if(file.exists()==false){
                file.createNewFile();
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (MTransactions tt : Configure.getSession().getList(MTransactions.class, null)) {
                stringBuilder.append(tt.str).append(System.lineSeparator());
            }
            UtilsOmsk.rewriteFile(patch,stringBuilder.toString());
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.errorDialog(ex);
            ex.printStackTrace();
            log.error(ex);
        }
    }

    public void onSettingsEShow() {
        try {
            SettingsE s = new SettingsE();
            String patch = s.getFileName();
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.errorDialog(ex);
            ex.printStackTrace();
            log.error(ex);
        }
    }

    public void onSettingAppModeShow() {
        try {
            SettingAppMode s = new SettingAppMode();
            String patch = s.getFileName();
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.errorDialog(ex);
            ex.printStackTrace();
            log.error(ex);
        }
    }

    public void onSettingAppEShow() {
        try {
            SettingAppE s = new SettingAppE();
            String patch = s.getFileName();
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.errorDialog(ex);
            ex.printStackTrace();
            log.error(ex);
        }
    }

    public void refreshMenu() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                bm1.setVisible(false);
                bm2.setVisible(false);
                bm3.setVisible(false);
                bm4.setVisible(false);
                bm5.setVisible(false);
                bm100.setVisible(false);
                bm6.setVisible(false);
                bm8.setVisible(false);
                bm9.setVisible(false);
                bm10.setVisible(false);
                bm11.setVisible(false);
                bm12.setVisible(false);
                bm13.setVisible(false);
                bm14.setVisible(false);
                bm15.setVisible(false);
                bm16.setVisible(true);
                bm20.setVisible(false);
                bm18.setVisible(false);
                // bm21.setVisible(false);


                if (SettingAppE.getInstance().user_id == null) {
                    bt_menu_admin.setVisible(false);
                    bt_menu_kassa.setVisible(false);
                    bt_menu_story.setVisible(false);
                    bt_menu_aquering.setVisible(false);
                    bm14.setVisible(true);
                    // bm15.setVisible(true);
                    //bm17.setVisible(true);
                } else if (SettingAppE.getInstance().user_id.equals("1")) {
                    bt_menu_admin.setVisible(true);
                    bt_menu_kassa.setVisible(true);
                    bt_menu_story.setVisible(true);
                    bt_menu_aquering.setVisible(true);

                    bm1.setVisible(true);
                    bm2.setVisible(false);
                    bm3.setVisible(true);
                    bm4.setVisible(true);
                    bm5.setVisible(true);
                    bm100.setVisible(true);
                    bm6.setVisible(true);
                    bm8.setVisible(false);
                    bm9.setVisible(false);
                    bm10.setVisible(false);
                    bm11.setVisible(true);
                    bm12.setVisible(true);
                    bm13.setVisible(true);
                    bm14.setVisible(true);
                    bm15.setVisible(true);
                    bm16.setVisible(true);
                    bm20.setVisible(true);
                    bm18.setVisible(true);
                    //bm21.setVisible(true);


                } else if (SettingAppE.getInstance().user_id.equals(SettingAppE.UniversalUserKey)) {
                    bt_menu_admin.setVisible(false);
                    bt_menu_kassa.setVisible(false);
                    bt_menu_story.setVisible(false);
                    bt_menu_aquering.setVisible(false);


                    bm14.setVisible(true);
                    bm15.setVisible(true);
                    //bm17.setVisible(true);
                    bm5.setVisible(true);

                } else {
                    bt_menu_admin.setVisible(false);
                    bt_menu_kassa.setVisible(true);
                    bt_menu_story.setVisible(true);
                    bt_menu_aquering.setVisible(true);

                    bm1.setVisible(false);
                    bm2.setVisible(false);
                    bm3.setVisible(true);
                    bm4.setVisible(false);
                    bm5.setVisible(true);
                    bm100.setVisible(true);
                    bm6.setVisible(true);
                    bm8.setVisible(false);
                    bm9.setVisible(false);
                    bm10.setVisible(false);
                    bm11.setVisible(true);
                    bm12.setVisible(true);
                    bm13.setVisible(true);
                    bm14.setVisible(true);
                    bm15.setVisible(true);
                    bm16.setVisible(true);
                    bm20.setVisible(true);
                    //bm21.setVisible(true);
                    //bm17.setVisible(true);
                    bm18.setVisible(true);
                    if (SettingAppE.getInstance().getProfile() == 0 || SettingAppE.getInstance().getProfile() == 2) {
                        bm3.setVisible(true);
                    } else {
                        bm3.setVisible(false);
                    }

                }
                //удаление желтых кнопок
                List<Node> nodeList = new ArrayList<>();
                for (Node node : panel_buttons.getChildren()) {
                    if (node instanceof javafx.scene.control.Button && node.getUserData() != null) {
                        if (node.getUserData().toString().startsWith(Main.BUTTONUPDATE)) {
                            nodeList.add(node);
                        }
                    }
                }

                for (Node node : nodeList) {
                    panel_buttons.getChildren().remove(node);
                }


                if (SettingAppE.getInstance().user_id != null) {

                    if (SettingAppE.getInstance().user_id.equals(SettingAppE.UniversalUserKey)) {
                        bm13.setVisible(false);
                        bm18.setVisible(false);
                        return;
                    }

                    new GetIsOpenSession().getIsOpen(isOpen -> {
                        if (isOpen == null) {
                            bm13.setVisible(true);
                            bm18.setVisible(true);
                        } else if (isOpen == true) {
                            bm13.setVisible(false);
                            bm18.setVisible(true);
                        } else if (isOpen == false) {
                            bm13.setVisible(true);
                            bm18.setVisible(false);
                        }
                        return false;
                    });
                }
                StarterUpdate.checkUpdate(Controller.this);

                if (SettingAppE.getInstance().getAcquire() == 0) {
                    bt_menu_aquering.setDisable(true);
                }

                addButtonMessage();


                if (SettingAppE.getInstance().user_id != null) {
                    if (SettingAppE.getInstance().user_id.equals("1")) {
                        bmOFD.setVisible(true);
                        bm_story_1.setVisible(true);
                        bm_story_2.setVisible(true);
                        bm_aquering_1.setVisible(true);
                        bm_aquering_2.setVisible(true);
                    } else {
                        bmOFD.setVisible(false);
                        bm_story_1.setVisible(false);
                        bm_story_2.setVisible(false);
                        bm_aquering_1.setVisible(false);
                        bm_aquering_2.setVisible(false);
                    }
                }
                refreshButtonUpdate();

            }
        });


    }

    public synchronized void addButtonMessage() {


        List<MessageBody> messageBodyList = MessageBody.getMessageBodyNoReadList();

        List<Node> nodeList = new ArrayList<>();

        for (Node node : panel_buttons.getChildren()) {
            if (node.getUserData() != null) {
                if (node.getUserData().toString().startsWith(Main.BUTTONMESSAGE)) {
                    nodeList.add(node);
                }
            }
        }
        for (Node node : nodeList) {
            panel_buttons.getChildren().remove(node);
        }

        if (messageBodyList.size() > 0) {
            javafx.scene.control.Button button = new Button("Сообщение");
            button.getStyleClass().add("button_message");
            button.setUserData(Main.BUTTONMESSAGE);
            button.setOnAction(event -> onReadMessage());
            button.setPrefHeight(panel_buttons.getPrefHeight());
            panel_buttons.getChildren().add(button);
            log.info("ИНФО: Сообщение доставлено");
        }


    }

    public void onDllDirShow() {
        DialogFactory.infoDialog("java.library.path", System.getProperty("java.library.path"));
    }

    public void onDllDirShow2() {
        DialogFactory.infoDialog("java.library.path2", Pather.dllDirectory);
    }

    public void onEditSettingsPage() {
        factoryPage(new FEditSettings());
    }

    public void onPrintOutFile() {
        DialogFactory.printOutFileDialog();
    }

    public void onRollBackApp() {

        RollBackApp backApp = new RollBackApp();
        backApp.run();
    }

    public void onOpenLog() {
        try {
            String patch = System.getProperty("user.home") + File.separator + "omsk.log";
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.errorDialog(ex);
            ex.printStackTrace();
            log.error(ex);
        }
    }

    public void onUpdateApp() {

        if (SettingAppE.getInstance().user_id.equals(SettingAppE.UniversalUserKey)) {
            DialogFactory.infoDialog("Обновление", bundle.getString("name167"));
            return;
        }
        if (SettingAppE.getInstance().selectProduct.size() > 0) {
            DialogFactory.infoDialog("Обновление", bundle.getString("name113"));
            return;
        }

        new UpdateApp().run(this);

    }

    public void onHomeDirectory() {
        if (DesktopApi.getOs().isLinux()) {
            ExeScript testScript = new ExeScript();
            testScript.runScript("xdg-open " + Pather.curdir + "/");
        } else {
            Desktop desktop = Desktop.getDesktop();
            File dirToOpen = null;
            try {
                dirToOpen = new File(Pather.curdir);
                desktop.open(dirToOpen);
            } catch (Exception iae) {
                log.error(iae);
            }
        }

    }

    public void onOpenFolderApp() {
        if (DesktopApi.getOs().isLinux()) {
            ExeScript testScript = new ExeScript();
            testScript.runScript("xdg-open " + System.getProperty("user.dir"));
        } else {
            Desktop desktop = Desktop.getDesktop();
            File dirToOpen = null;
            try {
                dirToOpen = new File(System.getProperty("user.home"));
                desktop.open(dirToOpen);
            } catch (Exception iae) {
                log.error(iae);
            }
        }

    }

    public void refreshPointid() {
        label_point_id.setText("Точка: " + String.valueOf(SettingAppE.getInstance().getPointId()));
    }

    public void onShowErrorPage() {
        factoryPage(new PageError());
    }

    public void onReadMessage() {

        factoryPage(new PageMessage(false));
    }

    public void onReadMessageAll() {
        factoryPage(new PageMessage(true));
    }

    public void onReportShow() {
        factoryPage(new PageReport());
    }

    public void onAcquireSelect() {
        DialogFactory.acquaryDialog();
    }

    public void onCheckStoryAll() {

        factoryPage(new PageSelfAmount());
    }

    //проверка эквайринга
    public void onAcquaryCheck() {


        new BankBase().execute(bankResult -> {
            DialogFactory.acquaryResultDialog(bankResult);
            return false;
        }, 36);


    }

    public void onCheckStoryAll2() {
        factoryPage(new FPageStoryAll2());
    }

    public void onAcquaryReportAll() {
        DialogFactory.closeSessionCber(closeSession -> {
            new CloseSession().close(closeSession, null);
            return false;
        });
    }

    public void onShowGoods() {
        factoryPage(new FPageGoods());
    }

    public void onShowPageCheckArchive() {
        factoryPage(new FPagecheckArchive());
    }

    public void onAcquaryControlFullr() {

        new ControlCheck().print(1);
    }

    public void onAcquaryControl() {

        new ControlCheck().print(0);
    }

    public void onProcedureIn() {
        if (SettingAppE.getInstance().selectProduct.size() > 0) {
            DialogFactory.infoDialog(Support.str("name185"), bundle.getString("name182"));
            return;
        }
        DialogFactory.questionDialog(Support.str("name185"), Support.str("name186"),
                new ButtonAction(Support.str("name187"), o -> {
                    new ProcedureIn().start();
                    return false;
                }));
    }

    public void onProcedureOut() {

        if (SettingAppE.getInstance().selectProduct.size() > 0) {
            DialogFactory.infoDialog(Support.str("name188"), bundle.getString("name182"));
            return;
        }

        DialogFactory.questionDialog(Support.str("name188"), Support.str("name189"),
                new ButtonAction(Support.str("name190"), o -> {
                    new ProcedureOut().start();
                    return false;
                }));


    }

    public void onTemplatesCheck() {
        DialogFactory.templateCheckDialog();
    }

    public void onAmountSelfTable() {
        factoryPage(new PageSelfAmountCore());
    }

    public void onSetUserKkm() {
        new bitnic.kassa.SetUserSettungsKmm().print(o -> {
            if (o != null) {
                DialogFactory.errorDialog("Смена имени кассира ккм Ошибка:\n" + String.valueOf(o));
            }
            return false;
        });
    }

    public void onTableAdmin() {
        factoryPage(new TableAdmin());
    }

    public void onPrintReportTovar() {
        new PrintReportTovar().print();
    }

    public void onReturnCheck() {
       factoryPage(new PageReturnCheck());
    }

    public void onRequestMessage() {
        DialogFactory.messageRequestDialog();
    }

    public void onDoubleProduct() {
        new GetDoubleGoods().getDoubleString();
    }

    public void onMyHistory() {
        factoryPage(new FPageHistory());
    }

    public void onSendReportToServer() {
        WorkingFile.sendBitnic(o -> false);
    }

    public void refreshButtonUpdate() {

        int i = SettingAppE.getShowButtonUpdate();
        String id = SettingAppE.getInstance().user_id;

        for (Node node : panel_buttons.getChildren()) {
            if (node instanceof javafx.scene.control.Button && node.getUserData() != null) {
                if (node.getUserData().toString().equals(Main.BUTTONUPDATE)) {
                    if (id == null) {
                        node.setVisible(false);
                    } else if (id.equals("1")) {
                        node.setVisible(true);
                    } else {
                        if (i == 1) {
                            node.setVisible(true);
                        } else {
                            node.setVisible(false);
                        }
                    }
                }
            }

        }

    }

    public void onPointHistory() {
        new SenderPointHistory().send();
    }

    public void onWieverLog() {
        FileChooser fileChooser = new FileChooser();

        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile == null) {

        } else {


 //           try {

                //Runtime.getRuntime().exec(" rm lnav "+selectedFile.getPath() +" &");
//                String command= "/usr/bin/xterm";
//                Runtime rt = Runtime.getRuntime();
//                Process pr = rt.exec(command);
//            } catch (IOException e) {
//                log.error(e);
//            }

            org.apache.commons.exec.CommandLine commandLine = new CommandLine("xfce4-terminal" );//
            commandLine.addArgument("lnav "+selectedFile.getPath());
            DefaultExecutor exec = new DefaultExecutor();
           // exec.setWorkingDirectory(new File("/usr/bin/"));
            exec.setWatchdog(new ExecuteWatchdog(ExecuteWatchdog.INFINITE_TIMEOUT));

            try {
                new ProcessBuilder("xfce4-terminal","-x","lnav",selectedFile.getPath()).start();
               // int i=exec.execute(commandLine);
            } catch (IOException e) {
                log.error(e);
            }

        }

    }

    public void onUpdateRequestFromServer() {

        new SenderRequestFromServer().send(null);
    }

    public void onShowRequest() {

        DialogFactory.dialogRequest(o -> {

            if(o!=null){
                MRequest mRequest= (MRequest) o;
                factoryPage(new FPageRequest(mRequest));
            }
            return false;
        });
    }

    public void onClearFileReport() {
            DialogFactory.questionDialog("Очистка выхлдных данных","Очистить выходные данные?",new ButtonAction("Очистить", new IAction<Object>() {
                @Override
                public boolean action(Object o) {
                    WorkingFile.clearReportDate();
                        return false;
                }
            }));
    }

    public void onAddReport() {
        DialogFactory.dialogAddReport();

    }
}



