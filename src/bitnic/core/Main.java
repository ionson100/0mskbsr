package bitnic.core;

import bitnic.appender.MyWebSocket;
import bitnic.dialogfactory.DialogFactory;
import bitnic.header.Cheader;
import bitnic.settings.Reanimation;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Pather;
import bitnic.utils.Starter;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import sun.security.action.GetPropertyAction;

import java.io.*;
import java.nio.charset.Charset;
import java.security.AccessController;
import java.util.*;

import static java.nio.charset.Charset.forName;

public class Main extends Application {


    public static final String BUTTONUPDATE = "312873";//признак что кнопка сообщения обновлений
    public static final String BUTTONMESSAGE = "1212"; //признак что кнопка прибытия нового сообщения пользователю
    private static final Logger log = Logger.getLogger(Main.class);
    public static MyWebSocket myWebSockerError;
    public static Stage myStage;

    public static Scene myScene;
    public static ResourceBundle myBundle;


    static {
        new Starter().start();

        java.security.PrivilegedAction pa =
                new GetPropertyAction("file.encoding");

    }



    public static void startSocked() {
        myWebSockerError = new MyWebSocket();

    }

    public static void stopSocked() {
        myWebSockerError.stop();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private static void showError(Thread t, Throwable e) {
        System.err.println("***Default exception handler***");
        StringWriter errorMsg = new StringWriter();
        e.printStackTrace(new PrintWriter(errorMsg));
        log.error(errorMsg.toString());

        if (Platform.isFxApplicationThread()) {
            showErrorDialog(e);
        } else {
            System.err.println( t);
        }
    }

    private static void showErrorDialog(Throwable e) {
        StringWriter errorMsg = new StringWriter();
        e.printStackTrace(new PrintWriter(errorMsg));
        DialogFactory.errorDialog(errorMsg.toString());
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        startSocked();
        Thread.setDefaultUncaughtExceptionHandler(Main::showError);
        Locale locale = new Locale("ru", "RU");

        myBundle = ResourceBundle.getBundle("aa", locale);
        Reanimation.SetBaseSettingsPath(Pather.base_sqlite_settings);
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"), myBundle);
        primaryStage.setTitle("BS Retail");
        primaryStage.setScene(new Scene(root, 1100, 800));
        myScene = primaryStage.getScene();
        primaryStage.show();
        myStage = primaryStage;
        myStage.getIcons().add(new Image(getClass().getResourceAsStream("/bitnic/image/logo-mini.png")));
        myStage.requestFocus();

       // myStage.setFullScreen(true);
       myStage.setMaximized(true);

//        myScene.addEventFilter(KeyEvent.KEY_PRESSED,
//                event -> {
//                    System.out.println("Pressed: " + event.getCode());
//                    if (Controller.ccmainform != null) {
//                        Controller.ccmainform.refreshKeyBoard(event.getText());
//                    }
//                });


        myStage.setOnCloseRequest(we -> {
            SettingAppE.save();
            try {
                SettingAppE.save();

                myWebSockerError.stop();
                Cheader.timmerPingStop();
                Controller.timerUpdateStop();
                Platform.exit();
                System.exit(0);
            } catch (Exception ex) {

                log.error(ex);
            } finally {
//                String name = ManagementFactory.getRuntimeMXBean().getName();
//                String pid=name.substring(0,name.indexOf("@"));
//                ExeScript testScript = new ExeScript();
//                if(DesktopApi.getOs()== DesktopApi.EnumOS.linux){
//                    SettingAppE.save();
//                    log.info("Выключение компьютера");
//                    Platform.exit();
//                    System.exit(0);
//
//                   // testScript.runScript("kill "+pid);
//                }
//                if(DesktopApi.getOs ()== DesktopApi.EnumOS.windows){
//                    //Taskkill /PID 26356 /F
//                    testScript.runScript("Taskkill /PID "+pid+" /F");
//                }
            }
        });



    }

}
