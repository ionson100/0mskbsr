package bitnic.pagetableproduct.itemmenu;

import bitnic.model.MProduct;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.pagetableproduct.itemproduct.ItemProductOne;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemMenu extends GridPane implements Initializable {

    private double lastYposition = 0d;
    public Button button_menu;
    private Logger log=Logger.getLogger(FItemMenu.class);
    private MProduct mProduct;
    private IAction<Button> iAction;


    public FItemMenu(MProduct mProduct, IAction<Button> iAction){
        this.mProduct = mProduct;
        this.iAction = iAction;
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_menu.fxml"),bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }








    boolean click=false;
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        button_menu.setStyle("-fx-font-size: "+ SettingAppE.getInstance().fontSizeMenuProduct);
        mProduct.userObject=button_menu;
        button_menu.setUserData(mProduct);
        button_menu.setText(mProduct.name);
        button_menu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(click)
                if(iAction!=null){
                    iAction.action(button_menu);
                }
            }
        });


        button_menu.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                click=true;
                lastYposition = event.getSceneY();
            }
        });

        button_menu.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                click=false;
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });



    }
}
