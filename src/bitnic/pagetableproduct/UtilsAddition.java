package bitnic.pagetableproduct;

import bitnic.model.MProduct;
import bitnic.pagetableproduct.itemproduct.ItemProductOne;
import bitnic.pagetableproduct.itemproductgroup.ItemProductGroupOne;
import bitnic.senders.IAction;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.List;

public class UtilsAddition {


    public static void addAll(ListView <IIttemProduct> listView , List <MProduct> mProductList , int id_product , IAction iActionClick ) {
        int i = 0;
        for (IIttemProduct o : listView.getItems()) {
            MProduct p = o.getProduct ();
            if (p.id == id_product) {
                break;
            }
            i = i + 1;

        }


        List<IIttemProduct> products=new ArrayList <> (  );
        for (MProduct mProduct : mProductList) {


            if (mProduct.isGroup == 1) {
                ItemProductOne one = new ItemProductOne(mProduct);
                one.setBlue ();
                products.add ( one );
            } else {
                ItemProductGroupOne one = new ItemProductGroupOne(mProduct, iActionClick);
                one.setBlue ();
                products.add ( one );
            }

        }



        if ((i == 0&&listView.getItems().size()==1) || i == listView.getItems().size() - 1) {
            listView.getItems().addAll(products);
        } else {
            listView.getItems().addAll(i + 1, products);
        }

    }

    public static void deleteAll(ListView <IIttemProduct> listView , List <MProduct> mproductsList ) {

        int dd=0;
       // List<IIttemProduct> deleteList = new ArrayList<>();

        int i=-1;
        for (IIttemProduct o : listView.getItems()) {
            i++;
            MProduct mProduct =  o.getProduct ();
            for (MProduct product : mproductsList) {
                if (mProduct.code_prod.equals(product.code_prod)) {

                    if(dd==0){
                        dd=i;
                    }
                    break;
                }

            }
        }

        //listView.getItems().removeAll(deleteList);
        listView.getItems ().remove ( dd,dd+ mproductsList.size ());
        listView.refresh ();
//        List<MProduct> mProducts = new ArrayList<>();
//
//        for (IIttemProduct o : listView.getItems()) {
//            mProducts.add(o.getProduct ());
//        }
       // new AdditorTableItem().build(listView, mProducts, gridPane, isBlue, iAction);

//////////////////////

//        ObservableList observableList = FXCollections.observableArrayList();
//        for (IIttemProduct iIttemProduct : listView.getItems ()) {
//            observableList.add ( iIttemProduct);
//        }
//        listView.getItems ().clear ();
//        listView.setItems(null);
//        listView.refresh ();
//        observableList.remove ( dd,dd+mproductsList.size () );
//        listView.setItems ( observableList );

    }

}
