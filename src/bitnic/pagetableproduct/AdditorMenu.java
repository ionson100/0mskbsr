package bitnic.pagetableproduct;

import bitnic.model.MProduct;
import bitnic.pagetableproduct.itemmenu.FItemMenu;
import bitnic.senders.IAction;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;


import java.util.List;

public class AdditorMenu {
   // private List<MProduct> mProducts;
    private ListView listView;
    private IAction<Button> iActionClisk;

    public void builder(List<MProduct> mProducts, ListView listView,IAction iActionClisk){
       // this.mProducts = mProducts;
        this.listView = listView;
        this.iActionClisk = iActionClisk;

        listView.setItems(null);
        ObservableList observableList = FXCollections.observableArrayList();
        for (MProduct mProduct : mProducts) {
            observableList.add(mProduct);
        }
        listView.setCellFactory(param -> new CheckCell());
        listView.setItems(observableList);

    }


    class CheckCell extends ListCell<MProduct> {

        @Override
        public void updateItem(MProduct item, boolean empty) {
            super.updateItem(item, empty);


            if (empty || item == null) {
                setGraphic(null);
            } else {

                FItemMenu check = new FItemMenu(item,iActionClisk);
                check.setPrefWidth(listView.getWidth()-30);
                setGraphic(check);
            }



        }
    }
}
