package bitnic.pagetableproduct.itemproductgroup;

import bitnic.dialogfactory.DialogFactory;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.pagetableproduct.IIttemProduct;
import bitnic.utils.UtilsOmsk;
import javafx.event.EventTarget;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class ItemProductGroupOne extends GridPane implements Initializable, IIttemProduct {


    private double lastYposition = 0d;
    private Logger log = Logger.getLogger ( ItemProductGroupOne.class );
    public Label name_groip_product;
    public ImageView image_open;
    public MProduct mProduct;
    private IAction iAction;

    public ItemProductGroupOne ( MProduct mProduct , IAction iAction ) {

        this.mProduct = mProduct;
        this.iAction = iAction;

        try {
            InputStream inputStream = getClass ().getClassLoader ().getResource ( "aa_ru_RU.properties" ).openStream ();
            ResourceBundle bundle = new PropertyResourceBundle ( inputStream );
            FXMLLoader fxmlLoader = new FXMLLoader ( getClass ().getResource ( "item_product_group.fxml" ) , bundle );
            fxmlLoader.setRoot ( this );
            fxmlLoader.setController ( this );

            fxmlLoader.load ();
        } catch (IOException exception) {
            log.error ( exception );
            DialogFactory.errorDialog( exception );
            throw new RuntimeException ( exception );
        }
        setUserData ( mProduct );
    }

    @Override
    public void initialize ( URL location , ResourceBundle resources ) {

        if ( mProduct.isOppenGroup == true ) {
            image_open.setImage ( new Image ( "/bitnic/image/down.png" ) );
        } else {
            image_open.setImage ( new Image ( "/bitnic/image/right.png" ) );
        }

        this.setOnMouseClicked ( new EventHandler <MouseEvent> () {
            @Override
            public void handle ( MouseEvent event ) {
                if ( iAction != null ) {
                    iAction.action ( ItemProductGroupOne.this );
                }
            }
        } );

        this.setOnMousePressed ( event -> lastYposition = event.getSceneY () );

        this.setOnMouseDragged ( new EventHandler <MouseEvent> () {
            @Override
            public void handle ( MouseEvent event ) {
                double newYposition = event.getSceneY ();
                double diff = 0;
                if ( newYposition > lastYposition ) {
                    diff = newYposition - lastYposition + UtilsOmsk.deltaScrol;
                } else {
                    diff = newYposition - lastYposition - UtilsOmsk.deltaScrol;
                }
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent ();
                cse.fireVerticalScroll ( (int) diff , this , (EventTarget) event.getSource () );
            }
        } );

        this.setPadding ( new Insets ( 0 , 0 , 0 , mProduct.delta + 10 ) );
        name_groip_product.setText ( mProduct.name );

    }


    public void setOpen () {
        name_groip_product.setStyle ( "-fx-text-fill:#5a8e99 " );
        image_open.setImage ( new Image ( "/bitnic/image/down.png" ) );
    }

    public void setClose ( ListView <IIttemProduct> listView ) {

        name_groip_product.setStyle ( "-fx-text-fill:black " );
        image_open.setImage ( new Image ( "/bitnic/image/right.png" ) );

        for (IIttemProduct item : listView.getItems ()) {
            if ( item.getProduct ().code_prod.equals ( mProduct.group ) ) {
                if ( item.getProduct ().isOppenGroup ) {
                    name_groip_product.setStyle ( "-fx-text-fill:#5a8e99 " );
                    image_open.setImage ( new Image ( "/bitnic/image/right.png" ) );
                } else {
                    name_groip_product.setStyle ( "-fx-text-fill:black " );
                    image_open.setImage ( new Image ( "/bitnic/image/right.png" ) );
                }
            }
        }

    }

    @Override
    public MProduct getProduct () {
        return mProduct;
    }

    @Override
    public void setBlue () {
        name_groip_product.setStyle ( "-fx-text-fill:#5a8e99 " );
        image_open.setImage ( new Image ( "/bitnic/image/right.png" ) );
    }
}
