package bitnic.pagetableproduct;

import bitnic.core.Controller;
import bitnic.model.MBarcode;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.pagetableproduct.itemproductgroup.ItemProductGroupOne;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;

public class TableProduct extends GridPane implements Initializable, IRefreshLoader, IFocusable {

    private Logger log = Logger.getLogger(TableProduct.class);

    public ColumnConstraints ck21;
    public GridPane grid_tree;
    public GridPane grid1;
    public Button button_1;
    public TextField text_field_search;
    public ListView list_menu;
    public ListView<IIttemProduct> list_product;
    private List<Button> buttons = new ArrayList<>();
    private List<Field> fields = new ArrayList<>();



    public TableProduct() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("table_product.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        UtilsOmsk.resizeNode(this, grid1);
        initcore();
        Controller.writeMessage(Support.str("name6"));
    }

    private void initcore() {

        button_1.setOnAction(e -> firstStart());

        for (Field field : MProduct.class.getFields()) {
            Finder f = field.getAnnotation(Finder.class);
            if (f != null) {
                fields.add(field);
            }
        }

        ////////////////////////////////////////////
        List<MProduct> listGroup = Configure.getSession().getList(MProduct.class, " isGroup = 0 ");
        Collections.sort(listGroup, Comparator.comparing(o -> o.name));


        new AdditorMenu().builder(listGroup, list_menu, new IAction() {
            @Override
            public boolean action(Object o) {
                buttonMenuClick((Button) o);
                return false;
            }



            private void buttonMenuClick(Button button) {
                setDefaultStyleMenu ();
                button.getStyleClass().clear();
                button.getStyleClass().add("button_blue_product_table_select");
                text_field_search.setText("");
                MProduct product = (MProduct) button.getUserData();
                List<MProduct> mProductList = Configure.getSession().getList(MProduct.class,
                        " group_product = ? ", product.code_prod);

                new AdditorTableItem().build(list_product, mProductList, new IAction() {
                    @Override
                    public boolean action(Object o) {
                        Click((IIttemProduct) o);
                        return false;
                    }
                });
            }
        });

        ///////////////////////////////////////////////////////////////////////

        List<MProduct> totallist = Configure.getSession().getList(MProduct.class, null);
        Collections.sort(totallist, Comparator.comparing(o -> o.name));
        text_field_search.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.trim().length() == 0) {
                list_product.setItems(null);
            }
            if (newValue.length() < 2) return;
            List<MProduct> productList = new ArrayList<>();


            if (text_field_search.getText().trim().length() >= 6 && isNimberAll(text_field_search.getText().trim())) {
                getListProductBarcodesContext(text_field_search.getText(), productList, totallist);
            } else {
                for (MProduct mProduct : totallist) {
                    if (mProduct.isGroup == 0) continue;
                    if (SettingAppE.getInstance().isCheckFinishProduct) {
                        if (productList.size() > 50) {
                            break;
                        }
                    }

                    for (Field field : fields) {
                        String f = null;
                        try {
                            f = field.get(mProduct).toString();
                        } catch (IllegalAccessException e) {
                            log.error ( e );
                            e.printStackTrace();
                            System.out.println(field.getName());
                        }
                        if (f.toUpperCase().contains(newValue.toUpperCase())) {
                            productList.add(mProduct);
                        }
                    }
                }
            }

            new AdditorTableItem().build(list_product, productList, new IAction() {
                @Override
                public boolean action(Object o) {
                    Click((IIttemProduct) o);
                    return false;
                }
            });
        });

        firstStart();

        if (listGroup.size() == 0) {
            grid1.getColumnConstraints().remove(ck21);

        } else {
            if (grid1.getColumnConstraints().size() == 1) {
                grid1.getColumnConstraints().add(ck21);
            }
        }
        Platform.runLater(() -> text_field_search.requestFocus());
    }

    void setDefaultStyleMenu(){
        for (Object o : list_menu.getItems()) {
            if (o instanceof MProduct) {
                Button but = (Button) ((MProduct) o).userObject;
                if (but != null) {

                        but.getStyleClass().clear();
                        but.getStyleClass().add("button_blue_product_table");

                }
            }
        }
    }

    private void getListProductBarcodesContext(String text, List<MProduct> list, List<MProduct> total) {
        List<MBarcode> mBarcodes = Configure.getSession().getList(MBarcode.class, " barcode LIKE '%" + text + "%'");
        for (MBarcode mBarcode : mBarcodes) {
            if (SettingAppE.getInstance().isCheckFinishProduct) {
                if (list.size() > 50) {
                    break;
                }
            }
            for (MProduct mProduct : total) {
                if (mProduct.code_prod.equals(mBarcode.id_product)) {
                    list.add(mProduct);
                }
            }
        }
    }

    private boolean isNimberAll(String trim) {
        for (char c : trim.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    private void firstStart() {

        setDefaultStyleMenu ();
        text_field_search.setText("");
        for (Button bb : buttons) {
            bb.getStyleClass().clear();
            bb.getStyleClass().add("button_blue_product_table");
        }

        List<MProduct> curlist = Configure.getSession().getList(MProduct.class,
                "isGroup = 0 and group_product = '' order by isGroup, name ");
        Collections.sort(curlist, Comparator.comparing(o -> o.name));

        new AdditorTableItem().build(list_product, curlist, new IAction() {
            @Override
            public boolean action(Object o) {
                Click( (IIttemProduct) o );
                return false;
            }
        });
    }

    private void Click(IIttemProduct ob) {

        text_field_search.setText("");
        MProduct mProduct=((ItemProductGroupOne)ob).mProduct;
        if (mProduct.isGroup == 1) return;

        ItemProductGroupOne groupOne = (ItemProductGroupOne)ob;
        if (mProduct.isOppenGroup == false) {

            mProduct.isOppenGroup = true;
            setSelectStyleMenu(mProduct.code_prod);

            groupOne.setOpen();
            String cod = mProduct.code_prod.trim();
            List<MProduct> mProducts = Configure.getSession().getList(MProduct.class, " group_product = ?", cod);
            for (MProduct product : mProducts) {
                product.delta = product.delta + mProduct.delta + 20;
            }
            UtilsAddition.addAll( list_product , mProducts , mProduct.id , new IAction () {
                @Override
                public boolean action ( Object o ) {
                    Click ((IIttemProduct) o );
                    return false;
                }
            } );
        } else {
            groupOne.setClose(list_product);
            setDefaultStyleMenu ();
            mProduct.isOppenGroup = false;
            String group = mProduct.code_prod;
            List<MProduct> mProductsList = new ArrayList<>();
            recursion(group, mProductsList );
            UtilsAddition.deleteAll(list_product, mProductsList );
        }
    }

    private void setSelectStyleMenu (String group ) {
        setDefaultStyleMenu ();
        for (Object o : list_menu.getItems()) {
            if (o instanceof MProduct) {
                if(((MProduct)o).code_prod.equals ( group )){
                    Button but = (Button) ((MProduct) o).userObject;
                    if (but != null) {

                            but.getStyleClass().clear();
                            but.getStyleClass().add("button_blue_product_table_select");

                    }
                }

            }
        }
    }

    void recursion(String group, List<MProduct> nodes) {
        for (IIttemProduct o : list_product.getItems()) {
            MProduct p = o.getProduct ();
            if (p.group.equals(group)) {
                nodes.add(p);
                recursion(p.code_prod, nodes);
            }
        }
    }


    @Override
    public void refresh() {

        initcore();
    }

    @Override
    public void focus() {
        Platform.runLater( () -> text_field_search.requestFocus() );
    }
}
