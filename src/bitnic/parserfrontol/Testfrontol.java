package bitnic.parserfrontol;


import bitnic.model.ModelCollection;
import bitnic.senders.IAction;
import bitnic.senders.SenderFtpOrSever;
import org.apache.log4j.Logger;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ion100 on 10.01.2018.
 */
public class Testfrontol {
    private static final Logger log = Logger.getLogger(Testfrontol.class);
    Map<String, cacheMetaDateFrontol> dateFrontolMap = new HashMap<>();

   List<Class> classes = null;

    public void test(IAction refresh) {
        try {
            classes = ModelCollection.classes;// CreatorTable.getClasses("bitnic.model");
        } catch (Exception e) {
            log.error ( e );
            e.printStackTrace();
        }
        for (Class aClass : classes) {
            Annotation f = aClass.getAnnotation(Frontol.class);
            if (f != null) {
                cacheMetaDateFrontol ff = CacheDictionaryFrontol.GetCacheMetaDate(aClass);
                dateFrontolMap.put(ff.command, ff);
            }
        }


        SenderFtpOrSever senderFtp = new SenderFtpOrSever ();
        senderFtp.send(dateFrontolMap, refresh);

    }
}
