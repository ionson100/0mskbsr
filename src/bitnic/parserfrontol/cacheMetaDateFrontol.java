package bitnic.parserfrontol;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

public class cacheMetaDateFrontol<T> {
    public Class aClass;
    public int count;
    public int position;
    public String command;
    public Map<Integer,Field> fieldMap= new HashMap<>();
    public cacheMetaDateFrontol(Class<T> aClass) {
        this.aClass = aClass;
        command= aClass.getAnnotation(Frontol.class).command();
        List<Field> fields=getAllFields(aClass);
        for (Field field : fields) {
            Annotation[] an =  field.getAnnotations();
            for (Annotation annotation : an) {
                if(annotation instanceof IndexFrontol){
                    IndexFrontol s= (IndexFrontol) annotation;
                    fieldMap.put(s.index(),field);
                }
            }
        }
    }

    private static List<Field> getAllFields(Class clazz) {
        List<Field> fields = new ArrayList<>();
        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
        Class superClazz = clazz.getSuperclass();
        if (superClazz != null) {
            fields.addAll(getAllFields(superClazz));
        }
        return fields;
    }
}



