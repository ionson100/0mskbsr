package bitnic.parserfrontol;



import java.util.Dictionary;
import java.util.Hashtable;

public class CacheDictionaryFrontol {

    private static final Object lock = new Object();

    private static final Dictionary<String, cacheMetaDateFrontol> dic = new Hashtable();

    public static cacheMetaDateFrontol GetCacheMetaDate(Class aClass) {
        if (dic.get(aClass.getName()) == null) {
            synchronized (lock) {
                if (dic.get(aClass.getName()) == null) {
                    dic.put(aClass.getName(), new cacheMetaDateFrontol<>(aClass));
                }
            }
        }
        return dic.get(aClass.getName());
    }


}
