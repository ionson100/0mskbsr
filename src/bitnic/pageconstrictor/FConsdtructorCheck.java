package bitnic.pageconstrictor;

import bitnic.pagecheck.itemcheck.FItemCheck;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import bitnic.model.MProduct;
import bitnic.utils.UtilsOmsk;
import bitnic.transaction.Transaction;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FConsdtructorCheck extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger ( FConsdtructorCheck.class );
    public TextField tf_name, tf_amount, tf_price;
    public Label label_itogo;
    public Button bt_print;
    private double amount;
    private double price;

    private static final String itogo = "Итого: ";
    private static final String rub = " руб.";

    GridPane pane;

    public FConsdtructorCheck() {

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_constructor_check.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException(exception);
        }


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        label_itogo.setText(itogo + String.valueOf(0d) + rub);
        tf_amount.setText(String.valueOf(0d));
        tf_price.setText(String.valueOf(0d));
        tf_price.textProperty().addListener((observable, oldValue, newValue) -> validate());
        tf_amount.textProperty().addListener((observable, oldValue, newValue) -> validate());
        tf_name.textProperty().addListener((observable, oldValue, newValue) -> validate());

        bt_print.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MProduct d = new MProduct();
                d.code_prod = "0012";
                d.selectAmount = amount;
                d.price = price;
                d.name = tf_name.getText();
                d.name_check = tf_name.getText();
                List<MProduct> mProducts = new ArrayList<>(1);
                mProducts.add(d);

                new Transaction().printCheck (mProducts,null);
            }
        });
    }

    private void validate() {
        if (tf_name.getText() == null || tf_price.getText() == null || tf_amount.getText() == null) {
            bt_print.setDisable(true);
            return;
        }
        if (tf_name.getText().trim().length() == 0) {
            bt_print.setDisable(true);
            return;
        }

        try {
            amount = Double.parseDouble(tf_amount.getText().trim().replace(",", "."));
        } catch (Exception ex) {
            tf_amount.setText("0");
            bt_print.setDisable(true);
            return;

        }
        try {
            price = Double.parseDouble(tf_price.getText().trim().replace(",", "."));
        } catch (Exception ex) {
            tf_price.setText("0");
            bt_print.setDisable(true);
            return;

        }
        if (price == 0 || amount == 0) {
            bt_print.setDisable(true);
        }
        label_itogo.setText(itogo + String.valueOf(UtilsOmsk.round((price * amount), 2)) + rub);
        bt_print.setDisable(false);

    }
}
