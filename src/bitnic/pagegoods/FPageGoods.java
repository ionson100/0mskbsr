package bitnic.pagegoods;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MError;
import bitnic.pagetableproduct.IFocusable;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FPageGoods extends GridPane implements Initializable,IFocusable {

    private static final Logger log = Logger.getLogger ( FPageGoods.class );


    public HBox vbox_search;
    public TextField tf_search;

    public CheckBox cb_autoupdate;


    public TableView table_error;
    public GridPane grid1;

    List <MError> list = new ArrayList <> ();

    public FPageGoods () {
        try {
            InputStream inputStream = getClass ().getClassLoader ().getResource ( "aa_ru_RU.properties" ).openStream ();
            ResourceBundle bundle = new PropertyResourceBundle ( inputStream );
            FXMLLoader fxmlLoader = new FXMLLoader ( getClass ().getResource ( "f_page_goods.fxml" ) , bundle );
            fxmlLoader.setRoot ( this );
            fxmlLoader.setController ( this );

            fxmlLoader.load ();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException ( exception );
        }
    }


    @Override
    public void initialize ( URL location , ResourceBundle resources ) {
        if( SettingAppE.getInstance ().isTypeЫShow3D){
            vbox_search.getStyleClass ().add ( "footer_3d" );

        }else {
            vbox_search.getStyleClass ().add ( "footer" );
        }

        UtilsOmsk.resizeNode ( this , grid1 );
        UtilsOmsk.painter3d ( tf_search );

        if ( new File ( Pather.inFilesData ).exists () == false )
            return;

        List <String> lines = new ArrayList <> ();
        try {
            lines = Files.readAllLines ( Paths.get ( Pather.inFilesData ) , Charset.forName ( "windows-1251" ) );//
        } catch (IOException e1) {
            log.error ( e1 );
            DialogFactory.errorDialog( e1.getMessage () + System.lineSeparator () + "Возможно не совпадает кодировка" );
        }

        for (String line : lines) {
            MError er = new MError ();
            er.msg = line;
            list.add ( er );
        }

        log.info ( "Строк goods - " + list.size () );
        new BuilderTable ().build( list , table_error );
        // table_error.scrollTo(table_error.getItems ().size()-1);

        tf_search.textProperty ().addListener ( new ChangeListener <String> () {
            @Override
            public void changed ( ObservableValue<? extends String> observable , String oldValue , String newValue ) {
                UtilsOmsk.searchErrorMessage(newValue,list,table_error);
            }
        } );
        focus ();
        Controller.writeMessage( "Данные с сервера" );
    }

    @Override
    public void focus () {
        Platform.runLater ( () -> tf_search.requestFocus () );
    }
}
