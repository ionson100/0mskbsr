package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.utils.WorkingFile;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_60_61_62_63_64;
import org.apache.log4j.Logger;

public class XReport extends BaseKassa {

    private static final Logger log = Logger.getLogger(XReport.class);

    private Transaction transaction;

    public void print(Transaction transaction) {
        this.transaction = transaction;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();
            log.info("Отчет без гашения");
            try {

                fptr = new Fptr();
                fptr.create();
                setWorkDir(fptr);

                if (fptr.put_DeviceEnabled(true) < 0) {
                    checkError(fptr);
                }

                if (fptr.GetStatus() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                if (fptr.put_Mode(IFptr.MODE_REPORT_NO_CLEAR) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_ReportType(IFptr.REPORT_X) < 0) {
                    checkError(fptr);
                }
                if (fptr.Report() < 0) {
                    checkError(fptr);
                }

                SettingAppE.setSession(getNumberSession(fptr));
                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = getNumberDoc(fptr);
                transaction.numberSession = getNumberSession(fptr);
                transaction.summSmena = fptr.get_Summ();
                transaction.numberCheck=getNumberCheck(fptr);

                transaction.summSmena = fptr.get_Summ();

                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет=fptr.get_Summ();


                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();



                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }

                transaction.сумма_нал_ккм = fptr.get_Summ();


                printState(fptr, false);

                return true;
            } catch (Exception ex) {
                //Controller.refreshOpenCloseSession(null);
                log.error(ex);
                exception = ex;
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {
            FactoryBlender.run();
            Controller.writeMessage("Отчет без гашения");
        }

        @Override
        public void onPostExecute(Boolean params) {

            int delta=1;
            if (exception != null) {
                Controller.writeMessage("Отчет без гашения - ошибка");
                DialogFactory.errorDialog(exception);
            }else {
                Controller.writeMessage("");
                try {
                    Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                    z.int_1_4=60;
                    z.cod_print_group_17=1;
                    z.number_smena_14=transaction.numberSession;
                    z.nimberDoc_6 = transaction.numberDoc;
                    z.sum_smena_10 = transaction.выручка_отчет;
                    delta++;
                    z.numberAction_1 = Iterator.getId(delta);
                    z.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    z.code_firma_27 = SettingAppE.getInstance().getPointId();
                    z.dateAction_2 = transaction.getDate();
                    z.timeAction_3 = transaction.getTime();
                    z.code_pm_5 = SettingAppE.getInstance().getPointId();;
                    z.int_1_13=9;// операция в ккм
                    z.sum_kassa_11   = transaction.сумма_нал_ккм;
                    z.itogo_smena_12 = transaction.сменный_итог;
                    z.info_doc_26=""+transaction.numberCheck+"/"+transaction.numberDoc+"/"+transaction.numberSession;
                    transaction.list.add(z);
                }catch (Exception ex){
                    ex.printStackTrace();
                    DialogFactory.errorDialog(ex);
                    log.error ( ex );
                }

                Controller.writeMessage("Отчет без гашения - успешно");
                WorkingFile.appenderReportDate(transaction.list);
                if(Controller.curnode instanceof IRefreshLoader){
                    ((IRefreshLoader) Controller.curnode).refresh();
                }

            }
            FactoryBlender.stop();

        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
