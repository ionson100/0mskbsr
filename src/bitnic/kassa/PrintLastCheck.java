package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import org.apache.log4j.Logger;

public class PrintLastCheck extends BaseKassa {

    private static final Logger log = Logger.getLogger(PrintLastCheck.class);
    public void print(){
        new MyWorker().execute(null);
    }
    private class MyWorker extends AsyncTask<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params)  {
            IFptr fptr = new Fptr();
            try {
                fptr.create();
                setWorkDir(fptr);

                if (fptr.put_DeviceEnabled(true) < 0) {
                    checkError(fptr);
                }

                if (fptr.GetStatus() < 0) {
                    checkError(fptr);
                }

                cancelCheck(fptr);

                if (fptr.PrintLastCheckCopy() < 0) {
                    checkError(fptr);
                }
                fptr.destroy();


                log.info("Печать последнего чека из ккм");
                return true;

            } catch (Exception e) {
                log.error(e);
                exception =e;
                e.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute()  {

            Controller.writeMessage("Печать последнего чека");
            FactoryBlender.run();
        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.errorDialog(exception);
            }else {
                Controller.writeMessage("");
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}

