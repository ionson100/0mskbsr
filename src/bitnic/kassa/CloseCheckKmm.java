package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

;

public class CloseCheckKmm extends BaseKassa {
    private static final Logger log = Logger.getLogger(CloseCheckKmm.class);

    public void print() {
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Integer, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                // не являются ошибками, если мы хотим просто отменить чек
                //("Отмена чека...");
                log.info("Ручное закрытие чека");
               cancelCheck(fptr);


                SettingAppE.setSession(getNumberSession(fptr));
                return true;
            } catch (Exception e) {
                log.error(e);
                exception = e;
                e.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {

            Controller.writeMessage("Закрытие чека");
            FactoryBlender.run();
        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if (params == false && exception != null) {
                DialogFactory.errorDialog(exception);
                Controller.writeMessage("Закрытие чека - ошибка");
            } else {
                Controller.writeMessage("");
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
