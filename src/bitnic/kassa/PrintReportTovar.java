package bitnic.kassa;

import bitnic.checkarchive.CheckArchiveE;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

public class PrintReportTovar {
    private static final Logger log = Logger.getLogger(PrintReportTovar.class);
    public void print() {

        List<CheckArchiveE> archiveEList = Configure.getSession().getList(CheckArchiveE.class,
                "session_id = ? and date_return = 0 ", SettingAppE.getSession());

        if (archiveEList.size() == 0) {
            DialogFactory.infoDialog("Печать отчета Товары", "Смена: " + SettingAppE.getSession() + "\n" +
                    "Записей: 0\nПечать прервана");
            return;
        }
        List<WrapperPrint> wrapperPrints=new ArrayList<>();
        wrapperPrints.add(new WrapperPrint("Отчет товары",WrapperPrint.ALIGNMENT_CENTER));
        wrapperPrints.add(new WrapperPrint("Смена: "+SettingAppE.getSession(),WrapperPrint.ALIGNMENT_CENTER));

        Map<String,MProduct> map=new HashMap<>();
        for (CheckArchiveE checkArchiveE : archiveEList) {
            List<MProduct> mProducts=checkArchiveE.getProductList();
            for (MProduct mProduct : mProducts) {
                if(map.containsKey(mProduct.code_prod)){
                    double d=map.get(mProduct.code_prod).selectAmount;
                    map.get(mProduct.code_prod).selectAmount=d+mProduct.selectAmount;
                }else {
                    map.put(mProduct.code_prod,mProduct);
                }
            }
        }

        Map<String,List<MProduct>> listMap=new HashMap<>();
        for (MProduct mProduct : map.values()) {
            if(listMap.containsKey(mProduct.group)){
                listMap.get(mProduct.group).add(mProduct);
            }else {
                List<MProduct> list=new ArrayList<>();
                list.add(mProduct);
                listMap.put(mProduct.group,list);
            }

        }
        wrapperPrints.add(new WrapperPrint("Первая операция:",WrapperPrint.ALIGNMENT_LEFT));

        wrapperPrints.add(new WrapperPrint(UtilsOmsk.simpleDateFormatE(archiveEList.get(0).date),WrapperPrint.ALIGNMENT_RIGHT));
        wrapperPrints.add(new WrapperPrint("Последняя операция:",WrapperPrint.ALIGNMENT_LEFT));
        wrapperPrints.add(new WrapperPrint(UtilsOmsk.simpleDateFormatE(archiveEList.get(archiveEList.size()-1).date),WrapperPrint.ALIGNMENT_RIGHT));


        double summ=0;

        for (Map.Entry<String, List<MProduct>> ss : listMap.entrySet()) {

            List<MProduct> list= Configure.getSession().getList(MProduct.class," group_product = ? ",ss.getKey());
            if(list.size()>0){
                //  wrapperPrints.add(new WrapperPrint("",WrapperPrint.ALIGNMENT_RIGHT));
                // wrapperPrints.add(new WrapperPrint(list.get(0).name,WrapperPrint.ALIGNMENT_LEFT));
                //  wrapperPrints.add(new WrapperPrint("",WrapperPrint.ALIGNMENT_RIGHT));
            }else {
                // wrapperPrints.add(new WrapperPrint(s+"_____________",WrapperPrint.ALIGNMENT_LEFT));
            }
            for (MProduct mProduct : ss.getValue()) {
                wrapperPrints.add(new WrapperPrint(mProduct.name,WrapperPrint.ALIGNMENT_LEFT));
                double d=UtilsOmsk.round(mProduct.selectAmount*mProduct.price,2);
                summ=summ+d;

                String sss=String.format(" %s                 =%s",mProduct.selectAmount,d);
                wrapperPrints.add(new WrapperPrint(String.valueOf(sss),WrapperPrint.ALIGNMENT_RIGHT));
            }
        }


        {
            String ss=String.format("ИТОГО:                   =%s",summ);
            wrapperPrints.add(new WrapperPrint(ss,WrapperPrint.ALIGNMENT_RIGHT));
        }

        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");
            String ddd= dateFormat.format(new Date());
            String ss=String.format("Дата Время:       %s",ddd);
            wrapperPrints.add(new WrapperPrint(ss,WrapperPrint.ALIGNMENT_RIGHT));
        }
        {
            String ss=String.format("Продавец:");
            wrapperPrints.add(new WrapperPrint(ss,WrapperPrint.ALIGNMENT_LEFT));
        }
        {
            String ss=String.format("%s",SettingAppE.getInstance().getUserName());
            wrapperPrints.add(new WrapperPrint(ss,WrapperPrint.ALIGNMENT_RIGHT));
        }
        {
            String ss=String.format("Точка:                    %s",SettingAppE.getInstance().getPointId());
            wrapperPrints.add(new WrapperPrint(ss,WrapperPrint.ALIGNMENT_LEFT));
        }


        log.info("Печать отчета по товарам");
        new PrintFreeText().printWrapper(wrapperPrints);





    }


}
