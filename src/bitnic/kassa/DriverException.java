package bitnic.kassa;

public  class DriverException extends Exception {
    private static final long serialVersionUID = 6164921645357791803L;

    public DriverException(String msg) {
        super(msg);
    }
}
