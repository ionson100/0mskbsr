package bitnic.kassa;

import bitnic.utils.Support;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import jdk.nashorn.internal.ir.IfNode;
import org.apache.log4j.Logger;

import java.util.List;


public class BaseKassa {

    private static final Logger log = Logger.getLogger(BaseKassa.class);

    protected Exception exception;

    public void printBankCheck(IFptr fptr, List<String> strings) throws Exception {


        int i = 0;

        for (String ss : strings) {
            if (ss.contains("~S") && i == 0) {
                i++;
                try {
                    Thread.sleep(SettingAppE.getInstance().sleepCheckBank);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            printText(fptr, ss, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
        }

        printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

    }

    public static int getNumberDoc(IFptr fptr) {
        return fptr.get_DocNumber();
    }


    public static void  cancelCheck(IFptr fptr) throws DriverException {
        try {
            if (fptr.CancelCheck() < 0) {
                checkError(fptr);
            }
        } catch (Exception e) {
            int rc = fptr.get_ResultCode();
            if (rc != -16 && rc != -3801) {
                throw e;
            }

        }
    }

    public static int getNumberCheck(IFptr fptr) {
        return fptr.get_CheckNumber();
    }

    public static int getNumberSession(IFptr fptr) throws DriverException {

        if (fptr.get_SessionOpened()) {
            return fptr.get_Session() + 1;
        } else {
            return fptr.get_Session();
        }

    }

    public static void printState(IFptr fptr, boolean isOpenSession) {


        String выручка = "0", нал = "0", приходсмена = "0";int ses=0;
        try {
            fptr.put_RegisterNumber(11); // выручка
            if(fptr.GetRegister()<0){
                checkError(fptr);
            }
            выручка = String.valueOf(fptr.get_Summ());
            ///////////////////////////////////////////////////////////////////////////////////Сумма наличности в ККМ (Summ)
            if (fptr.put_RegisterNumber(10) < 0) {
                checkError(fptr);
            }
            if (fptr.GetRegister() < 0) {
                checkError(fptr);
            }
            SettingAppE.getInstance().summNallKmm = fptr.get_Summ();
            SettingAppE.save();
            нал = String.valueOf(fptr.get_Summ());
            /////////////////
            //////////////////////////////////////////////////////////////////////////////////  сумма сменного итога приход
            if (fptr.put_RegisterNumber(12) < 0) {
                checkError(fptr);
            }
            if (fptr.put_OperationType(0) < 0) {
                checkError(fptr);
            }
            if (fptr.GetRegister() < 0) {
                checkError(fptr);
            }
            приходсмена = String.valueOf(fptr.get_Summ());

             ses = getNumberSession(fptr);
            ///////////////////////////////
//            boolean sess=fptr.get_SessionOpened();
//            if(isZReport){
//                sess=false;
//            }
            if (isOpenSession) {
                // sess=true;
                ses = ses + 1;
            }

        } catch (Exception ex) {
            log.error(ex);
        }finally {
            Controller.writeStateKkm(выручка, нал, приходсмена, String.valueOf(ses));//,sess
        }

    }

    public static void setWorkDir(IFptr fptr) throws DriverException {

        if (DesktopApi.getOs().isWindows()) {

            if (SettingAppE.getInstance().settings_kassa != null && SettingAppE.getInstance().settings_kassa.length() > 0) {
                if (fptr.put_DeviceSettings(SettingAppE.getInstance().settings_kassa) < 0)
                    checkError(fptr);
            } else {
                DialogFactory.infoDialog("Касса", Support.str("name118"));
            }
        } else {
            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_PORT, IFptr.SETTING_PORT_USB) < 0)
                checkError(fptr);
            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_VID, 0x2912) < 0)
                checkError(fptr);
            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_PID, 0x0005) < 0)
                checkError(fptr);

            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_MODEL, IFptr.MODEL_ATOL_11F) < 0)
                checkError(fptr);

            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_BAUDRATE, 115200) < 0)
                checkError(fptr);
            if (fptr.ApplySingleSettings() < 0)
                checkError(fptr);
        }

        fptr.put_DeviceSingleSetting(fptr.SETTING_SEARCHDIR, System.getProperty("java.library.path"));//
        fptr.ApplySingleSettings();
    }

    public static void checkError(IFptr fptr) throws DriverException {
        int rc = fptr.get_ResultCode();
        if (rc < 0) {
            String rd = fptr.get_ResultDescription(), bpd = null;
            if (rc == -6) {
                bpd = fptr.get_BadParamDescription();
            }
            if (bpd != null)
                throw new DriverException(String.format("[%d] %s (%s)", rc, rd, bpd));
            else
                throw new DriverException(String.format("[%d] %s", rc, rd));
        }
    }

    public static void printText(IFptr fptr, String text, int alignment, int wrap) throws DriverException {
        if (fptr.put_Caption(text) < 0)
            checkError(fptr);
        if (fptr.put_TextWrap(wrap) < 0)
            checkError(fptr);
        if (fptr.put_Alignment(alignment) < 0)
            checkError(fptr);
        if (fptr.PrintString() < 0)
            checkError(fptr);
    }

    static void closeCheck(IFptr fptr, int typeClose) throws DriverException {
        if (fptr.put_TypeClose(typeClose) < 0)
            checkError(fptr);
        if (fptr.CloseCheck() < 0)
            checkError(fptr);

    }



}

