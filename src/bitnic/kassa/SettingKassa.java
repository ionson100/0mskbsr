package bitnic.kassa;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;


public class SettingKassa extends BaseKassa {

    private static final Logger log = Logger.getLogger(SettingKassa.class);
    public static void print() {
        IFptr fptr =null;
        log.info("Настройки кассы");
        try {
            fptr = new Fptr();
            fptr.create();
            //setWorkDir(fptr);

            if (fptr.ShowProperties() < 0)
                checkError(fptr);

            if (fptr.put_DeviceEnabled(true) < 0)
                checkError(fptr);

            // Проверка связи
            if (fptr.GetStatus() < 0)
                checkError(fptr);

            SettingAppE.setSession(getNumberSession(fptr));

            // Убедились, что настройки подходят и касса отвечает - вытаскиваем актуальные настройки из драйвера

            String settings = fptr.get_DeviceSettings();

            SettingAppE.getInstance().settings_kassa=settings;
            SettingAppE.save();



        } catch (Exception e) {

            log.error(e);
            System.out.println(e);
        } finally {
            if(fptr!=null){
                fptr.ResetMode();
                fptr.destroy();
            }

        }

    }
}
