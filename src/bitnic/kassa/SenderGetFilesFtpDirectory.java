package bitnic.kassa;



import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SenderGetFilesFtpDirectory {

    private static final Logger log = Logger.getLogger(SenderGetFilesFtpDirectory.class);

    private StringBuilder error = new StringBuilder();
    String res = "";
    private SettingAppE ftp = SettingAppE.getInstance();


    private List<String> files = new ArrayList<>();

    public List<String> getNameFile() {

        try {
            new MyWorker().get();
        } catch (Exception e) {
            log.error ( e );


        }

        return files;
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            FTPClient ftpClient = new FTPClient();
            try {
                ftpClient.connect(ftp.getUrl(), ftp.ftp_port);
                ftpClient.login(ftp.ftp_user, ftp.ftp_password);
                FTPFile[] filesE = ftpClient.listFiles();
                for (FTPFile file : filesE) {
                    String name = file.getName();
                    files.add(name);
                    System.out.println(name);
                }


            } catch (Exception e) {
                log.error(e);
                error.append(e.getMessage());
                try {
                    ftpClient.disconnect();
                    ftpClient.logout();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Void params) {

            if (error.length() > 0) {
                throw new RuntimeException(error.toString());
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {

            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
