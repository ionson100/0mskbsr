package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrintFreeText extends BaseKassa {

    private static final Logger log = Logger.getLogger(PrintFreeText.class);
    Exception ex;
    private List<String>  strings=new ArrayList<>();
    private List<WrapperPrint> wrapperPrints=new ArrayList<>();

    public void print(String s){
        strings= Arrays.asList(s.split(System.lineSeparator()));
        new MyWorker().execute(null);
    }
//    public void print(List<String> stringss){
//        strings=stringss;
//        new MyWorker().execute(null);
//    }

    public void printWrapper(List<WrapperPrint> wrapperPrints){

        this.wrapperPrints = wrapperPrints;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            IFptr fptr = null;

            try {
                fptr = new Fptr();
                fptr.create();
                setWorkDir(fptr);
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                for (String ss : strings) {
                    printText(fptr, ss, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                }

                for (WrapperPrint wrapperPrint : wrapperPrints) {
                    printText(fptr, wrapperPrint.string, wrapperPrint.aligment, IFptr.WRAP_WORD);
                }
                if(wrapperPrints.size()>0){

                    String kkm= fptr.get_SerialNumber();
                    printText(fptr,"ЗН ККТ            "+ kkm, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr,"", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr,"", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr,"", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                }


            }catch (Exception ex){
                exception=ex;
                log.error ( ex );

            }finally {
                if(fptr!=null){
                    fptr.ResetMode();
                    fptr.destroy();
                }
            }


                return null;
        }

        @Override
        protected void onPreExecute()  {

            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {

            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.errorDialog(exception);
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }


    }

}
