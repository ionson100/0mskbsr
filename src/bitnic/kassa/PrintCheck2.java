package bitnic.kassa;

import bitnic.bank.sber.ActionSale;
import bitnic.bank.sber.BankResult;
import bitnic.blenderloader.FactoryBlender;
import bitnic.checkarchive.AmountSelf;
import bitnic.checkarchive.AmountSelfTotal;
import bitnic.checkarchive.CheckArchiveE;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.model.MTaxrates;
import bitnic.orm.Configure;
import bitnic.pagesale.MainForm;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14;
import bitnic.transaction.eventfrontol.Event_40_41;
import bitnic.transaction.eventfrontol.Event_42_55;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.WorkingFile;
import bitnic.utils.Support;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PrintCheck2 extends BaseKassa {

    //sudo apt-get install visualvm
    //jvisualvm
    //su31HiM12
    private static final Logger log = Logger.getLogger(PrintCheck2.class);

    private String code_autorize;

    private Exception exception;
    private List<MProduct> selectListProduct = new ArrayList<>();
    private Transaction transaction;
    private double payment_type;

    private IAction<Double> iAction;
    private BankResult resultAction1;
    private BankResult resultAction15_suspens;//suspens
    private BankResult resultAction16_commit;
    private BankResult resultAction13_rolback;
    private int summ = 0;

    private StringBuilder stringBuilder = new StringBuilder();

    public void addMessage(String msq) {
        stringBuilder.append(msq).append(System.lineSeparator());
        log.info(msq);
    }

    public String getMessageE() {
        return stringBuilder.toString();
    }

    public void print(List<MProduct> selectListProduct,
                      Transaction trans,
                      double PAYMENT_TYPE,
                      IAction<Double> iAction) {

        for (MProduct mProduct : selectListProduct) {
            if (mProduct.selectAmount <= 0) continue;
            this.selectListProduct.add(mProduct.clone());
        }


        transaction = trans;
        payment_type = PAYMENT_TYPE;
        this.iAction = iAction;
        new MyWorker().execute(null);
    }
    public static void registrationFZ54E(IFptr fptr, MProduct mProduct) throws Exception {


        List<MTaxrates> mTaxrates = Configure.getSession().getList(MTaxrates.class, "cod_nalog = ?", mProduct.cod_nalog_group);

        if (mTaxrates.size() > 0) {
            if (fptr.put_TaxNumber(mTaxrates.get(0).nalog_number_kkm) < 0) {
                checkError(fptr);
            }
        } else {
            if (fptr.put_TaxNumber(Fptr.TAX_VAT_18) < 0) {
                checkError(fptr);
            }
        }

        if (fptr.put_Quantity(mProduct.selectAmount) < 0) {
            checkError(fptr);
        }
        if (fptr.put_Price(mProduct.price) < 0) {
            checkError(fptr);
        }
        if (fptr.put_PositionSum(mProduct.selectAmount * mProduct.price) < 0) {
            checkError(fptr);
        }
        if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
            checkError(fptr);
        }
        if (fptr.put_Name(mProduct.name_check) < 0) {
            checkError(fptr);
        }
        if (fptr.Registration() < 0) {
            checkError(fptr);
        }
    }


    private class MyWorker extends AsyncTask<Void, Integer, Boolean> {




        private void openCheckE(IFptr fptr, int type) throws Exception {
            if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                checkError(fptr);
            }
            if (fptr.SetMode() < 0) {
                checkError(fptr);
            }

            if (fptr.put_CheckType(type) < 0) {
                checkError(fptr);
            }
            if (fptr.OpenCheck() < 0) {
                checkError(fptr);
            }
        }

//        ExecutorService service = Executors.newCachedThreadPool();
        @Override
        public Boolean doInBackground(Void... params) {

            //////////////////////////////////////////////////////////////////////////

            log.info("ПЕЧАТЬ ЧЕКА");
            IFptr fptr =null;

            try {


                {
                    double ssumm=0;
                    for (MProduct product : selectListProduct) {
                        ssumm = ssumm + product.price * product.selectAmount;
                    }

                    if (summ > SettingAppE.maxSumm) {//

                        throw new Exception("Сумма чека превышает допустимую: " + String.valueOf(SettingAppE.maxSumm));
                    }
                    ssumm = UtilsOmsk.round(ssumm, 2);
                    summ= (int) (ssumm*100);
                }




                if (payment_type == 1) {

                    log.info("Запрос в банк. транзакция - 1");

                    FactoryBlender.addMessage("Внимание! При отказе клиента оплаты, клиент должен нажать красную кнопку на пинпаде.\n" +
                            "Программа сама обработает это действие и отменит чек!!!");
                    FactoryBlender.addMessage("Запрос в банк. транзакция - 1");

                    resultAction1 = ActionSale.exe(1, summ, 0);

                    code_autorize = resultAction1.codeAutorurize_4;

                    if (resultAction1.result == 0) {

                        log.info("Запрос в банк УСПЕШНО. Переводим т. в подвешенное состояние для печати чека");
                        FactoryBlender.addMessage("Запрос в банк УСПЕШНО. Переводим транзакцию в подвешенное состояние для печати чека");
                        resultAction15_suspens = ActionSale.exe(15, summ, code_autorize);
                        if (resultAction15_suspens.result == 0) {
                            log.info("Перевод в подвешенное состояние удачный. Приступаем к печати чеков");
                            FactoryBlender.addMessage("Перевод в подвешенное состояние удачный. Приступаем к печати чеков");


                        } else {
                            throw new RuntimeException("Перевод в подвешенное состояние ошибка  - " + resultAction15_suspens.linesE.get(0));
                        }

                    } else {
                        throw new RuntimeException("Транзакция  1 Завершена с ошибкой  - " + resultAction1.linesE.get(0));
                    }
                }

             ///////////////////////////////////



                fptr= new Fptr();
                log.info("fptr created");


                FactoryBlender.addMessage("Печать основного чека");
                fptr.create();
                setWorkDir(fptr);

                FactoryBlender.addMessage("Инициализация настроек");
                log.info("Инициализация настроек");
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                if (fptr.GetStatus() < 0)
                    checkError(fptr);

                if (fptr.get_SessionOpened() == false) {
                    throw new RuntimeException(Support.str("name306"));
                }

                FactoryBlender.addMessage("Изменение статуса");

                log.info("Изменение статуса");

                cancelCheck(fptr);


                /////////////////////////////////////////// открытие сменыж


                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет = fptr.get_Summ();
                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();
                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сумма_нал_ккм = fptr.get_Summ();


                ///////////////////////////////////////////


                try {
                    openCheckE(fptr, IFptr.CHEQUE_TYPE_SELL);
                } catch (Exception e) {
                    // Проверка на превышение смены
                    if (fptr.get_ResultCode() == -3822) {
                        Controller.writeMessage("Требуется z - отчет.");
                        throw e;
                    } else {
                        throw e;
                    }
                }


                FactoryBlender.addMessage("Открытие чека");
                Collections.sort(selectListProduct, Comparator.comparing(lhs -> lhs.name));
                FactoryBlender.addMessage("Инициализация чека продуктами");
                log.info("Инициализация чека продуктами");
                double totalAmount = 0;
                for (MProduct product : selectListProduct) {

                    if (product.selectAmount <= 0) continue;
                    totalAmount = totalAmount + product.selectAmount;
                    registrationFZ54E(fptr, product);

                }

                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberCheck = getNumberCheck(fptr);

                if (fptr.put_RegisterNumber(20) < 0) {// сумма чека
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }

                transaction.summCheck = fptr.get_Summ();




                transaction.numberSession = getNumberSession(fptr);

                try {
                    SettingAppE.setSession(getNumberSession(fptr));
                    printState(fptr, false);
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }

                log.info("closeCheck");
                if (payment_type == 1) {
                    closeCheck(fptr, 1);//кредит
                } else {
                    closeCheck(fptr, 0);// cach

                }
                log.info("Чек отпечатан");
                FactoryBlender.addMessage("Чек отпечатан");

                if (payment_type == 1) {

                    try {
                        log.info("Печатаем банковские чеки");
                        FactoryBlender.addMessage("Печатаем банковские чеки");
                        printBankCheck(fptr, resultAction1.linesP);
                    } catch (Exception ex) {
                        log.error("При распечатке чека банка при продаже произошла ошибка: " + ex.getMessage());
                        DialogFactory.errorDialog("При распечатке чека банка произошла ошибка: " + ex.getMessage());
                    }
                }


                transaction.numberDoc = getNumberDoc(fptr);
                printState(fptr, false);

                return true;

            } catch (Exception ex) {

                if(fptr!=null){
                    fptr.CancelCheck();
                }

                log.error(ex);
                if (payment_type == 1) {

                    if (resultAction15_suspens != null && resultAction15_suspens.result == 0) {

                        log.info("Отменяем чек");
                        FactoryBlender.addMessage("Отменяем чек");
                        try {
                            resultAction13_rolback = ActionSale.exe(13, summ, code_autorize);
                            if (resultAction13_rolback.result == 0) {
                                log.info("Чек отменен");
                                FactoryBlender.addMessage("Чек отменен");
                            } else {
                                throw  new RuntimeException("При отмене чека произошла ошибка\nЧек считать не действительным\nОбратиться в сбербанк");
                            }
                        } catch (Exception e) {
                            DialogFactory.errorDialog(e.getMessage());
                            log.error(e);
                        }
                    }
                }

                exception = ex;
                ex.printStackTrace();
                return false;
            } finally {
                if(fptr!=null){
                    fptr.ResetMode();
                    fptr.destroy();
                    log.info("fptr.destroy()");
                }
                FactoryBlender.stop();
            }
        }


        @Override
        public void onPreExecute() {

            FactoryBlender.run();
            Controller.writeMessage("Печать чека");

        }

        @Override
        public void onPostExecute(Boolean params) {


            int delta = 1;


            if (params == true) {


                try {


                    if (payment_type == 1) {
                        if (resultAction15_suspens.result == 0) {
                            FactoryBlender.addMessage("Закрепляем транзакцию");
                            log.info("Закрепляем транзакцию");
                            resultAction16_commit = ActionSale.exe(16, summ, code_autorize);//comit

                            if (resultAction16_commit.result == 0) {
                                log.info("Транзакция закреплена");
                            } else {

                                DialogFactory.errorDialog("При закреплении транзакции произошла ошибка.\nДеньги будут возвращены клиенту.\n" +
                                        "Позвоните администратору.");
                                log.info("При закреплении транзакции произошла ошибка - " + resultAction16_commit.linesE.get(0));

                            }
                        }
                    }

                    CheckArchiveE check = new CheckArchiveE();
                    check.setProduct(selectListProduct);
                    check.amount_product = selectListProduct.size();
                    check.date = new Date();
                    check.doc_id = transaction.numberDoc;
                    check.session_id = transaction.numberSession;
                    check.summ = transaction.summCheck;
                    check.cod_doc = SettingAppE.getInstance().getPointId() + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                    if (payment_type == 1) {
                        check.is_bank = true;
                        check.check_body_bank = String.join("\n", resultAction1.linesP);
                        check.result_bank = resultAction1.result_1;
                        check.link_bank = resultAction1.linkNumberCard_10;
                    }
                    check.insert();
                    log.info("Добавление в таблицу чеков");

                    Controller.writeMessage("");

                } catch (Exception ex) {
                    log.error(ex);
                }

                try {
                    AmountSelf.addProductSell(selectListProduct, transaction.numberDoc);
                    AmountSelfTotal.addProductSell(selectListProduct, transaction.numberDoc, transaction.numberSession);
                    log.info("Добавление в таблицу остатков");
                } catch (Exception ex) {
                    log.error(ex);
                }


                try {
                    for (MProduct mProduct : selectListProduct) {

                        Event_1_11_2_12_4_14 e = new Event_1_11_2_12_4_14();
                        e.codeProd_8 = mProduct.code_prod;
                        e.price_not_discount_10 = mProduct.price;
                        e.amount_prod_11 = mProduct.selectAmount;
                        delta++;
                        e.numberAction_1 = Iterator.getId(delta);
                        e.dateAction_2 = transaction.getDate();
                        e.timeAction_3 = transaction.getTime();
                        e.int_1_4 = 11;
                        e.code_pm_5 = SettingAppE.getInstance().getPointId();
                        e.nimberDoc_6 = transaction.numberDoc;
                        e.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                        e.info_doc_26 = SettingAppE.getInstance().getPointId() + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                        e.code_firma_27 = SettingAppE.getInstance().getPointId();
                        double sum = UtilsOmsk.round(mProduct.price * mProduct.selectAmount, 2);
                        e.amount_rount_prod_12 = sum;
                        e.price_discount_rub_16 = sum;
                        e.summ_itogo_20 = sum;
                        e.priee_discount_15 = mProduct.price;
                        e.kkm_operation_13 = 0;
                        e.code_group_print_17 = 1;
                        e.kmm_section_21 = 1;
                        e.code_vid_code_23 = 1;
                        e.number_smena_14 = transaction.numberSession;
                        transaction.list.add(e);
                    }
                } catch (Exception ex) {
                    log.error(ex);
                }

                try {


                    delta++;
                    Event_40_41 e = new Event_40_41();//куплено за нал
                    e.numberAction_1 = Iterator.getId(delta);
                    e.dateAction_2 = transaction.getDate();
                    e.timeAction_3 = transaction.getTime();
                    e.int_1_4 = 41;
                    e.code_pm_5 = SettingAppE.getInstance().getPointId();
                    ;
                    e.nimberDoc_6 = transaction.numberDoc;
                    e.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    //e.number_card_8 = numberCard;
                    if (payment_type == 0) {
                        e.code_payment_9 = "1";// codePayment;nal
                    } else {
                        e.code_payment_9 = "4";// codePayment;безнал
                    }
                    e.number_smena_14 = transaction.numberSession;


                    e.type_operation_payment_10 = payment_type;
                    e.summ_doc_11 = transaction.summCheck;// totalPriceE;
                    e.summ_client_12 = transaction.summCheck;// totalPriceE;
                    e.info_doc_26 = SettingAppE.getInstance().getPointId() + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                    e.code_firma_27 = SettingAppE.getInstance().getPointId();
                    e.kkm_operation_13 = 0;
                    e.type_doc_23 = 1;
                    transaction.list.add(e);
                } catch (Exception ex) {
                    log.error(ex);
                }
                try {
                    Event_42_55 ee = new Event_42_55();// закрытие документа
                    double d = 0d;//, summ = 0d;
                    for (MProduct mProduct : selectListProduct) {
                        d = d + mProduct.selectAmount;
                    }
                    ee.amount_prod_11 = d;
                    ee.summ_doc_12 = transaction.summCheck;// UtilsOmsk.round(summ, 2);
                    ee.number_smena_14 = transaction.numberSession;
                    ee.code_group_print_doc_17 = 1;
                    ee.sum_bonusl_18 = "0";
                    delta++;
                    ee.numberAction_1 = Iterator.getId(delta);
                    ee.dateAction_2 = transaction.getDate();
                    ee.timeAction_3 = transaction.getTime();
                    ee.int_1_4 = 55;
                    ee.code_pm_5 = SettingAppE.getInstance().getPointId();
                    ee.nimberDoc_6 = transaction.numberDoc;
                    ee.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    ee.operation_kkm_13 = 0;
                    ee.code_doc_23 = 1;
                    ee.summ_doc_12 = transaction.summCheck;//totalPriceE;
                    ee.info_doc_26 = SettingAppE.getInstance().getPointId() + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                    ee.code_firma_27 = SettingAppE.getInstance().getPointId();
                    transaction.list.add(ee);
                } catch (Exception ex) {
                    log.error(ex);
                }



                try {

                    SettingAppE.getInstance().selectProduct.clear();
                    SettingAppE.save();


                    if (Controller.curnode instanceof MainForm) {
                        ((MainForm) Controller.curnode).refreshCheck();
                    }
                    if (Controller.curnode instanceof IRefreshLoader) {
                        ((IRefreshLoader) Controller.curnode).refresh();
                    }
                } catch (Exception ex) {
                    log.error(ex);
                }

                if (iAction != null) {
                    iAction.action(transaction.summCheck);
                }

            } else {
                if (exception != null) {
                    Controller.writeMessage("Печать чека - ошибка");
                    DialogFactory.errorDialog(getMessageE() + System.lineSeparator() + exception.getMessage());
                    if (getMessageE() != null && getMessageE().length() > 0) {
                        log.info(getMessageE());
                    }

                }
            }


            if (transaction.list.size() > 0) {
                WorkingFile.appenderReportDate(transaction.list);
                log.info("Сохранение данных на диск.");
            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }


}
