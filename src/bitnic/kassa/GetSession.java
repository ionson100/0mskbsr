package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class GetSession extends BaseKassa {

    Integer res;
    private static final Logger log = Logger.getLogger(GetSession.class);


    private IAction<Integer> iActionIsOpen;

    public void getSession(IAction<Integer> iActionIsOpen) {

        this.iActionIsOpen = iActionIsOpen;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
//                if (fptr.put_UserPassword("00000030") < 0) {
//                    checkError(fptr);
//                }
                res = getNumberSession(fptr);
                log.info("Получение номера смены");


            } catch (Exception ex) {
                exception = ex;
                log.error(ex);


            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
            return null;
        }

        @Override
        public void onPreExecute() {

            Controller.writeMessage("Получение номера смены");
        }

        @Override
        public void onPostExecute(Void params) {


            if (iActionIsOpen != null) {
                iActionIsOpen.action(res);
            }


        }

        @Override
        protected void onErrorInner(Throwable ex) {

            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
