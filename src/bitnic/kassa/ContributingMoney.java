package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.utils.WorkingFile;
import bitnic.utils.Support;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_50_51;
import org.apache.log4j.Logger;

public class ContributingMoney extends BaseKassa {
    private static final Logger log = Logger.getLogger(ContributingMoney.class);
    private double aDouble;
    private Transaction transaction;


    public void run(double aDouble, Transaction dd) {

        this.aDouble = aDouble;
        transaction = dd;

        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();

                setWorkDir(fptr);


                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                if(fptr.get_SessionOpened()==false){
                    throw new RuntimeException(Support.str("name304"));
                }

                if (fptr.put_Mode(1) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }

                SettingAppE.setSession(getNumberSession(fptr));

                if (fptr.put_Summ(aDouble) < 0) {
                    checkError(fptr);
                }
                if (fptr.CashIncome() < 0) {
                    checkError(fptr);
                }


                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = getNumberDoc(fptr);
                transaction.numberCheck=getNumberCheck(fptr);
                transaction.numberSession= getNumberSession(fptr);

                log.info("Внесение денег ручное: "+String.valueOf(aDouble));
                printState(fptr, false);
                return true;
            } catch (Exception e) {
                //Controller.refreshOpenCloseSession(null);
                log.error(e);
                exception = e;
                e.printStackTrace();
                return false;
            } finally {
               
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {
            FactoryBlender.run();
            Controller.writeMessage("Внесение денег в кассу");
        }

        @Override
        public void onPostExecute(Boolean params) {

            int delta=1;
            FactoryBlender.stop();
            if ( exception != null) {
                DialogFactory.errorDialog(exception.getMessage());
                Controller.writeMessage("Внесение денег в кассу - ошибка");

            } else {
                Event_50_51 e = new Event_50_51();
                delta++;
                e.numberAction_1 = Iterator.getId(delta);
                e.dateAction_2 = transaction.getDate();
                e.timeAction_3 = transaction.getTime();
                e.int_1_4 = 50;//внесение денег
                e.code_pm_5 = SettingAppE.getInstance().getPointId();;
                e.nimberDoc_6 = transaction.numberDoc;
                e.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                e.double_3_12 = aDouble;
                e.info_doc_26 = SettingAppE.getInstance().getPointId()+"/"+transaction.numberCheck+"/" + transaction.numberDoc;
                e.code_firma_27 = SettingAppE.getInstance().getPointId();
                e.kkm_operation_13 = 4; //внесение денег
                e.code_print_group_17=1;
                e.int_4_23=5;
                e.number_smena_14=transaction.numberSession;
                transaction.list.add(e);
                WorkingFile.appenderReportDate(transaction.list);
                Controller.writeMessage("");

                try{
                    if (Controller.curnode instanceof IRefreshLoader) {
                        ((IRefreshLoader) Controller.curnode).refresh();
                    }
                }catch (Exception ex){
                    log.error(ex);
                }
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}

