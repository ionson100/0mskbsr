package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.model.MTaxrates;
import bitnic.orm.Configure;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_56;
import bitnic.utils.Support;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.WorkingFile;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class DeleteCheckConstructor extends BaseKassa {


    private static final Logger log = Logger.getLogger(DeleteCheckConstructor.class);

    private Transaction transaction =new Transaction();
    private StringBuilder stringBuilder = new StringBuilder();

    private Exception exception;






    public void deleteReturn(int user_id,int point_id,String userName,List<MProduct> selectListProduct, boolean payment_type,boolean isFiscal,String code_doc) {



        new AsyncTask<Void, String, Void>() {


            private Fptr fptr;




            private void openCheck() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CheckType(IFptr.CHEQUE_TYPE_RETURN) < 0) {
                    checkError(fptr);
                }
                if (fptr.OpenCheck() < 0) {
                    checkError(fptr);
                }
            }


            private void reportZ() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError(fptr);
                }
                if (fptr.Report() < 0) {
                    checkError(fptr);
                }
                throw new Exception("Отмена отклоненна, пробит z отчет!");

            }

            private void registrationFZ54(MProduct mProduct, String name, double price, double quantity) throws Exception {

                List<MTaxrates> mTaxrates = Configure.getSession().getList(MTaxrates.class, "cod_nalog = ?", mProduct.cod_nalog_group);
                if (mTaxrates.size() > 0) {
                    if (fptr.put_TaxNumber(mTaxrates.get(0).nalog_number_kkm) < 0) {
                        checkError(fptr);
                    }
                } else {
                    if (fptr.put_TaxNumber(Fptr.TAX_VAT_18) < 0) {
                        checkError(fptr);
                    }
                }
                if (fptr.put_Quantity(quantity) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Price(price) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_PositionSum(quantity * price) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Name(name) < 0) {
                    checkError(fptr);
                }
                if (fptr.Return() < 0) {
                    checkError(fptr);
                }
            }

            @Override
            public Void doInBackground(Void... params) {

                if(isFiscal){
                    return fiscalCheck();
                }else {
                    return noFiscalCheck();
                }


            }

            private Void noFiscalCheck() {
                Fptr fptr =null;
                try {
                    fptr= new Fptr();
                    fptr.create();
                    setWorkDir(fptr);

                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError(fptr);
                    }

                    if (fptr.GetStatus() < 0) {
                        checkError(fptr);
                    }

                    cancelCheck(fptr);

                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError(fptr);
                    }
                    printText(fptr, "КОНСТРУКТОР ЧЕКА", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText(fptr, "КАССОВЫЙ ЧЕК", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText(fptr, "ВОЗВРАТ ПРИХОДА", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);


                    double total = 0;
                    double totalNds = 0;
                    for (MProduct product : selectListProduct) {
                        printText(fptr, product.name_check, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText(fptr, String.valueOf(product.selectAmount) + "х" + String.valueOf(product.price)
                                        + " =" + String.valueOf(UtilsOmsk.round(product.selectAmount * product.price, 2)),
                                IFptr.ALIGNMENT_RIGHT, IFptr.WRAP_WORD);
                        total = total + product.selectAmount * product.price;
                        double nds = ((product.selectAmount * product.price) / 100) * 18;
                        totalNds = totalNds + nds;
                        printText(fptr, " Сумма НДС 18%      =" + String.valueOf(UtilsOmsk.round(nds, 2)),
                                IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    }
                    String tota = String.valueOf(UtilsOmsk.round(total, 2));
                    PrintCheckConstructor.printTextItogo(fptr, "ИТОГ =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "ОПЛАТА", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

                    printText(fptr, "ВСЕГО ОПЛАЧЕНО", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    if (payment_type) {
                        printText(fptr, "НАЛИЧНЫМИ          =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    } else {
                        printText(fptr, "ЭЛЕКТРОННЫМИ       =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    }

                    printText(fptr, "Сумма НДС           =" + String.valueOf(UtilsOmsk.round(totalNds, 2)),
                            IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "Кассир: " + userName, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "Торговая точка: ", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, UtilsOmsk.simpleDateFormatE(new Date()), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

                    transaction.date = fptr.get_Date();
                    transaction.time = fptr.get_Time();
                    transaction.numberCheck = getNumberCheck(fptr);
                    transaction.summCheck = UtilsOmsk.round(total, 2);
                    transaction.numberSession = getNumberSession(fptr);
                    transaction.numberDoc = new Random().nextInt(10000);


                } catch (Exception e) {
                    log.error(e);
                    exception = e;


                } finally {
                    if(fptr!=null){
                        fptr.ResetMode();
                        fptr.destroy();
                    }

                }
                return null;
            }

            @Nullable
            private Void fiscalCheck() {
                try {
                    fptr = new Fptr();
                    double max = 0;
                    for (MProduct product : selectListProduct) {
                        max = max + product.price + product.selectAmount;
                    }
                    if (max > SettingAppE.maxSumm) {
                        throw new Exception(Support.str("name119"));
                    }
                    fptr.create();
                    BaseKassa.setWorkDir(fptr);

                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetStatus() < 0) {
                        checkError(fptr);
                    }
                    cancelCheck(fptr);

                    PrintCheckConstructor.checkUser(fptr,userName);

                    try {
                        openCheck();
                    } catch (Exception e) {
                        // Проверка на превышение смены
                        if (fptr.get_ResultCode() == -3822) {
                            reportZ();
                            openCheck();
                        } else {
                            throw e;
                        }
                    }

                    SettingAppE.setSession(getNumberSession(fptr));

                    Collections.sort(selectListProduct, Comparator.comparing(o -> o.name));


                    if (payment_type == true) {
                        if (fptr.put_TypeClose(1) < 0)// Закрываем через банк
                            checkError(fptr);
                    } else {
                        if (fptr.put_TypeClose(0) < 0)// Закрываем через нал
                            checkError(fptr);
                    }


                    for (MProduct product : selectListProduct) {


                        registrationFZ54(product, product.name_check, product.price, product.selectAmount);

                    }

                    if (fptr.CloseCheck() < 0)
                        checkError(fptr);



                    PrintCheckConstructor.checkUser(fptr,SettingAppE.getInstance().getUserCheck());

                    printState(fptr, false);

                } catch (Exception e) {
                    if (fptr != null) {
                        fptr.CancelCheck();
                    }

                    log.error(e);
                    exception = e;
                    e.printStackTrace();
                } finally {
                    if (fptr != null) {
                        fptr.ResetMode();
                        fptr.destroy();
                    }

                }
                return null;
            }

            @Override
            public void onPreExecute() {
                FactoryBlender.run();
                Controller.writeMessage("Возврат прихода");
            }

            @Override
            public void onPostExecute(Void aVoid) {

                FactoryBlender.stop();


                if (exception != null) {
                    Controller.writeMessage("Отмена чека - ошибка");
                    DialogFactory.errorDialog(exception);
                }else {

                    if(isFiscal==false){
                        int delta=1;
                        try {
                            Controller.writeMessage("");
                            Event_56 e = new Event_56();
                            delta++;
                            e.numberAction_1 = Iterator.getId(delta);
                            e.dateAction_2 = transaction.getDate();
                            e.timeAction_3 = transaction.getTime();
                            e.int_1_4 = 56;
                            e.code_pm_5 = point_id;
                            e.nimberDoc_6 = transaction.numberDoc;
                            e.codeKassir_7 = user_id;
                            e.kkm_operation_13=1;//возврат
                            e.number_smena_14=transaction.numberSession;
                            e.code_print_group_17=1;
                            e.type_doc_23=2;
                            e.info_doc_26=code_doc;
                            transaction.list.add(e);
                            WorkingFile.appenderReportDate(transaction.list);
                            if (Controller.curnode instanceof IRefreshLoader) {
                                ((IRefreshLoader) Controller.curnode).refresh();
                            }

                        } catch (Exception ex) {
                            log.error ( ex );
                            DialogFactory.errorDialog(ex);
                        }
                    }
                }
            }

            @Override
            protected void onErrorInner(Throwable ex) {
                FactoryBlender.stop();
                log.error(ex);
                DialogFactory.errorDialog(ex.getMessage());
            }
        }.execute(null);
    }
}
