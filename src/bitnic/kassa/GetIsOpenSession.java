package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class GetIsOpenSession extends BaseKassa {

    Boolean res;
    private static final Logger log = Logger.getLogger(GetIsOpenSession.class);


    private IAction<Boolean> iActionIsOpen;

    public void  getIsOpen(IAction<Boolean> iActionIsOpen){

        this.iActionIsOpen = iActionIsOpen;
        new MyWorker().execute(null);
    }
    private class MyWorker extends AsyncTask<Void,Void,Void> {

        @Override
        public Void doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
//                if (fptr.put_UserPassword("00000030") < 0) {
//                    checkError(fptr);
//                }
                SettingAppE.setSession(getNumberSession(fptr));
               res = fptr.get_SessionOpened();
                log.info("Проверка состояния смены");

            }catch (Exception ex){
                //Controller.refreshOpenCloseSession(null);
                exception=ex;
                log.error(ex);

            } finally {
                printState(fptr, false);
                fptr.ResetMode();
                fptr.destroy();
            }
            return null;
        }

        @Override
        public void onPreExecute() {
           // FactoryBlender.run();
            Controller.writeMessage("Состояние смены");
        }

        @Override
        public void onPostExecute(Void params) {
           // FactoryBlender.stop();
            if(exception!=null){
                if(iActionIsOpen!=null){
                    iActionIsOpen.action(null);
                }
            }else{
                if(iActionIsOpen!=null){
                    iActionIsOpen.action(res);
                }
            }

        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);

        }
    }
}
