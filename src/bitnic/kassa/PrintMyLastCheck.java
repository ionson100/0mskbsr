package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;


import bitnic.checkarchive.CheckArchiveE;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.senders.AsyncTask;
import bitnic.utils.UtilsOmsk;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

public class PrintMyLastCheck extends BaseKassa {
    private static final Logger log = Logger.getLogger(PrintMyLastCheck.class);

    public static void printLast(CheckArchiveE mCheck) {
        new AsyncTask<Void, Void, Void>() {
            private IFptr fptr;
            Exception exception;

            @Override
            protected Void doInBackground(Void... params) {

                fptr = new Fptr();
                try {
                    fptr.create();
                    setWorkDir(fptr);

                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError(fptr);
                    }

                    if (fptr.GetStatus() < 0) {
                        checkError(fptr);
                    }

                    cancelCheck(fptr);

                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError(fptr);
                    }
                    printText(fptr, "ДУБЛИКАТ ДОКУМЕНТА", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText(fptr, "КАССОВЫЙ ЧЕК", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText(fptr, "ПРИХОД", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

                    double total = 0;
                    double totalNds = 0;
                    for (MProduct product : mCheck.getProductList()) {
                        printText(fptr,product.name_check, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText(fptr, String.valueOf(product.selectAmount) + "х" + String.valueOf(product.price)
                                        + " =" + String.valueOf(UtilsOmsk.round(product.selectAmount * product.price, 2)),
                                IFptr.ALIGNMENT_RIGHT, IFptr.WRAP_WORD);
                        total = total + product.selectAmount * product.price;
                        double nds = ((product.selectAmount * product.price) / 100) * 18;
                        totalNds = totalNds + nds;
                        printText(fptr, " Сумма НДС 18%      =" + String.valueOf(UtilsOmsk.round(nds, 2)),
                                IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    }
                    String tota = String.valueOf(UtilsOmsk.round(total, 2));
                    PrintCheckConstructor.printTextItogo(fptr, "ИТОГ =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "ОПЛАТА", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, " НАЛИЧНЫМИ          =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "ВСЕГО ОПЛАЧЕНО", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "НАЛИЧНЫМИ           =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "ЭЛЕКТРОННЫМИ        =0.00", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "Сумма НДС           =" + String.valueOf(UtilsOmsk.round(totalNds, 2)),
                            IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "Кассир:" + SettingAppE.getInstance().getUserName(), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "Торговая точка:", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, UtilsOmsk.simpleDateFormatE(mCheck.date), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

                    log.info("Печать последнего чека не из ккм");

                } catch (Exception e) {
                    log.error(e);
                    exception = e;

                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                FactoryBlender.run();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                FactoryBlender.stop();
                if (exception != null) {
                    DialogFactory.errorDialog(exception);
                }
                System.gc();
            }

            @Override
            protected void onErrorInner(Throwable ex) {
                FactoryBlender.stop();
                log.error(ex);
                DialogFactory.errorDialog(ex.getMessage());
            }
        }.execute(null);
    }
}
