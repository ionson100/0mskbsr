package bitnic.kassa.itoger;

public class NonNullableSums {

    public double buy;
    public double buyCorrection;
    public double buyReturn;
    public double sell;
    public double sellCorrection;
    public double sellReturn;
}
