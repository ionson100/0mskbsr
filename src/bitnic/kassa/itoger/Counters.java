package bitnic.kassa.itoger;

public class Counters {

   public double revenue;
   public double shiftNumber;

   public CashIn cashIn;
   public CashOut cashOut;
   public NonNullableSums nonNullableSums;
   public Totals totals;
}
