package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.IAction;
import bitnic.utils.WorkingFile;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_50_51;
import org.apache.log4j.Logger;

public class EntryMoney extends BaseKassa {
    private static final Logger log = Logger.getLogger(EntryMoney.class);
    private double aDouble;
    private Transaction transaction;
    private IAction iActionProcedure;

    public void run(double aDouble, Transaction transaction, IAction iActionProcedure ) {

        this.aDouble = aDouble;
        this.transaction = transaction;
        this.iActionProcedure = iActionProcedure;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Mode(1) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Summ(aDouble) < 0) {
                    checkError(fptr);
                }
                if (fptr.CashOutcome() < 0) {
                    checkError(fptr);
                }
                log.info("Выдача денег из кассы - "+String.valueOf(aDouble));
                SettingAppE.setSession(getNumberSession(fptr));
                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = getNumberDoc(fptr);
                transaction.numberCheck=getNumberCheck(fptr);
                transaction.numberSession= getNumberSession(fptr);
                printState(fptr, false);
                return true;
            } catch (Exception e) {
                //Controller.refreshOpenCloseSession(null);
                log.error(e);
                exception = e;
                e.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute()  {

            FactoryBlender.run();
            Controller.writeMessage("Выдача денег из кассы");
        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
            int delta=1;

            if (params == false && exception != null) {
                Controller.writeMessage("Выдача денег из кассы - ошибка");
                DialogFactory.errorDialog(exception);
            } else {
                Controller.writeMessage("");
                Event_50_51 e = new Event_50_51();
                delta++;
                e.numberAction_1 = Iterator.getId(delta);
                e.dateAction_2 = transaction.getDate();
                e.timeAction_3 = transaction.getTime();
                e.int_1_4 = 51;//вынесение денег
                e.code_pm_5 = SettingAppE.getInstance().getPointId();;
                e.code_print_group_17=1;
                e.int_4_23=6;
                e.number_smena_14=transaction.numberSession;
                e.nimberDoc_6 = transaction.numberDoc;
                e.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                e.double_3_12 = aDouble;
                e.info_doc_26 = SettingAppE.getInstance().getPointId()+"/"+transaction.numberCheck+"/" + transaction.numberDoc;
                e.code_firma_27 = SettingAppE.getInstance().getPointId();
                e.kkm_operation_13 = 5; //вынесение денег
                transaction.list.add(e);
                WorkingFile.appenderReportDate(transaction.list);
                Controller.writeMessage("");


                if(iActionProcedure!=null){
                    iActionProcedure.action(null);
                }
                try{
                    if (Controller.curnode instanceof IRefreshLoader) {
                        ((IRefreshLoader) Controller.curnode).refresh();
                    }
                }catch (Exception ex){
                    log.error(ex);
                }
            }
            System.gc();

        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
