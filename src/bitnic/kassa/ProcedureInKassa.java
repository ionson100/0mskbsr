package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_50_51;
import bitnic.transaction.eventfrontol.Event_60_61_62_63_64;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.WorkingFile;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

import java.util.Date;

public class ProcedureInKassa extends BaseKassa {

    public static class ResultProcedureIn {
        public Exception exception1;
        public Exception exception2;
    }

    private Exception exception2;
    private static final Logger log = Logger.getLogger(ProcedureInKassa.class);
    private Transaction transaction;
    private double summ;
    private IAction<ResultProcedureIn> iActionProcedure;
    boolean sessionOpen;


    public void run(Transaction transaction, double summ, IAction<ResultProcedureIn> iAction) {
        this.transaction = transaction;

        this.summ = summ;
        this.iActionProcedure = iAction;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            log.info("Процедура открытия смены");
            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                if (fptr.put_Mode(1) < 0) {
                    checkError(fptr);
                }

/////////////////////////////////////////////////////////////////////////////////////// setting
                if (fptr.put_Mode(4) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                //("Сохранение состояния...");
                if (fptr.put_ValuePurpose(337) < 0) {// печать штрих кода
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Печать штрих кода");
                if (fptr.put_Value(1) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                //("Печатать штрихкод...");
                if (fptr.put_ValuePurpose(57) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Value(1) < 0) {// печать сквозного номера документа
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Печать скводного номера дукумента");
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_ValuePurpose(83) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Value(3) < 0) {// печать при помощи PrintString
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Разрешить печать PrintString");
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }

                //("wifi офд...");
                if (fptr.put_ValuePurpose(302) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Value(1) < 0) {// EoU
                    checkError(fptr);
                }
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }

                if (fptr.put_ValuePurpose(277) < 0) {// повторная печать
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Разрешить повторную печать");
                if (fptr.put_Value(1) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                ///////////////////
                Date dat = UtilsOmsk.curDate();
                if (fptr.put_Time(dat) < 0) {
                    checkError(fptr);
                }
                int dd1 = fptr.SetTime();
                if (dd1 == -3893) {
                    dd1 = fptr.SetTime();
                }
                if (dd1 < 0) {
                    checkError(fptr);
                }
                ////////////////////
                if (fptr.put_Date(UtilsOmsk.curDate()) < 0) {
                    checkError(fptr);
                }
                int dd = fptr.SetDate();
                if (dd == -3893) {
                    dd = fptr.SetDate();
                }
                if (dd < 0) {
                    checkError(fptr);
                }
                //("Set Date");
                //////////////////////// dns ofd
                FactoryBlender.addMessage("Синхронизация даты");

                FactoryBlender.addMessage("Установка адреса сервера ОФД");
                if (fptr.put_Caption(SettingAppE.getInstance().getUserCheck()) < 0) {
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Установка имени кассира");
                if (fptr.put_CaptionPurpose(118) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //("save username...");
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(72) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                ////////////////////////////////////////////////////////////////
                //("нах титул1");
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(73) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //("нах титул2");
                ///////////////////////////////////////
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(69) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //("нах титул3");
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(70) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }

                /////////////////////////////////////////////////////////////////////// open session

                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }


                if (fptr.OpenSession() < 0) {
                    checkError(fptr);
                }


                //SettingAppE.setSession(getNumberSession(fptr));

                ////////////////////////////////////////////////////////////////// вненсение денег


                if(summ>0){
                    try {
                        if (fptr.put_Summ(summ) < 0) {
                            checkError(fptr);
                        }

                        if (fptr.CashIncome() < 0) {
                            checkError(fptr);
                        }
                        log.info("Вненсение денег Открытие смены - "+String.valueOf(summ));
                    } catch (Exception ex) {
                        exception2 = ex;
                        log.error(ex);
                    }

                }



                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = getNumberDoc(fptr);

                transaction.summSmena = fptr.get_Summ();
                transaction.numberCheck = getNumberCheck(fptr);
                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет = fptr.get_Summ();
                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();
                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сумма_нал_ккм = fptr.get_Summ();

                transaction.numberSession = getNumberSession(fptr);

                SettingAppE.setSession(getNumberSession(fptr) + 1);
                System.out.println(SettingAppE.getSession());
                transaction.numberSession = getNumberSession(fptr) + 1;
                System.out.println(transaction.numberSession);


                sessionOpen = fptr.get_SessionOpened();
                SettingAppE.setShowButtonUpdate(0);
                printState(fptr, true);


            } catch (Exception ex) {
                //Controller.refreshOpenCloseSession(null);
                exception = ex;
                log.error(ex);


            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }

            return null;
        }

        @Override
        public void onPreExecute() {

            Controller.writeMessage("Открытие смены");
            FactoryBlender.run();
        }

        @Override
        public void onPostExecute(Void params) {

            FactoryBlender.stop();
            ResultProcedureIn procedureIn = new ResultProcedureIn();
            procedureIn.exception1 = exception;
            procedureIn.exception2 = exception2;

            int delta = 1;
            if (exception != null) {

            } else {

                {
                    Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                    z.int_1_4 = 64;
                    z.number_smena_14 = transaction.numberSession;
                    z.nimberDoc_6 = transaction.numberDoc;
                    z.sum_smena_10 = transaction.выручка_отчет;
                    delta++;
                    z.numberAction_1 = Iterator.getId(delta);
                    z.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    z.code_firma_27 = SettingAppE.getInstance().getPointId();
                    z.dateAction_2 = transaction.getDate();
                    z.timeAction_3 = transaction.getTime();
                    z.code_pm_5 = SettingAppE.getInstance().getPointId();
                    ;
                    z.int_1_13 = 9;// операция в ккм
                    z.sum_kassa_11 = transaction.сумма_нал_ккм;
                    z.itogo_smena_12 = transaction.сменный_итог;
                    z.info_doc_26 = "" + transaction.numberCheck + "/" + transaction.numberDoc + "/" + transaction.numberSession;
                    transaction.list.add(z);

                }
                {

                    if(exception2==null){
                        Event_50_51 e = new Event_50_51();
                        delta++;
                        e.numberAction_1 = Iterator.getId(delta);
                        e.dateAction_2 = transaction.getDate();
                        e.timeAction_3 = transaction.getTime();
                        e.int_1_4 = 50;//внесение денег
                        e.code_pm_5 = SettingAppE.getInstance().getPointId();
                        ;
                        e.nimberDoc_6 = transaction.numberDoc;
                        e.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                        e.double_3_12 = summ;
                        e.info_doc_26 = SettingAppE.getInstance().getPointId() + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                        e.code_firma_27 = SettingAppE.getInstance().getPointId();
                        e.kkm_operation_13 = 4; //внесение денег
                        e.code_print_group_17 = 1;
                        e.int_4_23 = 5;
                        e.number_smena_14 = transaction.numberSession;
                        transaction.list.add(e);
                    }


                }

                WorkingFile.appenderReportDate(transaction.list);

                if (iActionProcedure != null) {
                    iActionProcedure.action(procedureIn);
                }
            }

            if(exception==null&&exception2==null){
                Controller.writeMessage("Открытие смены - успешно");
            }
            if(exception!=null){
                Controller.writeMessage("Открытие смены - ошибка");
            }
            if(exception2!=null){
                Controller.writeMessage("Вненсение денег - ошибка");
            }


            try {
                if (Controller.curnode instanceof IRefreshLoader) {
                    ((IRefreshLoader) Controller.curnode).refresh();
                    Controller.getInstans().refreshMenu();
                }
            } catch (Exception ex) {
                log.error(ex);
            }
            Controller.getInstans().refreshButtonUpdate();
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
