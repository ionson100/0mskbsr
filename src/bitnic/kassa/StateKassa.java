package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.settingscore.SettingAppE;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import com.sun.prism.es2.ES2Graphics;
import org.apache.log4j.Logger;


public class StateKassa extends BaseKassa {

    private static final Logger log = Logger.getLogger(StateKassa.class);
    private MStateKKM state=new MStateKKM();
    private IAction<MStateKKM> kkmiAction;

    public void print(IAction<MStateKKM> kkmiAction) {
        this.kkmiAction=kkmiAction;
        new MyWorker().execute(null);
    }
    private class MyWorker extends AsyncTask<Void,Integer,Boolean> {


        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr =null;
            log.info("Состояние кассы");
            try {
                fptr = new Fptr();
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                //("Установка программирования...");
                if (fptr.put_Mode(4) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }

                SettingAppE.setSession(getNumberSession(fptr));

                if (fptr.put_CaptionPurpose(118) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetCaption() < 0) {
                    checkError(fptr);
                }
                state.username = fptr.get_Caption();
                //////////////////////////////////// не отправленные в офд
                if (fptr.put_RegisterNumber(44) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                state.notSenderOfd = fptr.get_Count();
                ////////////////////////////////////////////// дата первого не отправленного
                if (state.notSenderOfd > 0) {
                    if (fptr.put_RegisterNumber(45) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    state.notSenderFirstDate = MStateKKM.getDate(fptr.get_Date(), fptr.get_Time());
                }
                state.operator = fptr.get_Operator();
                state.fiscal = fptr.get_Fiscal();
/////////////////////
                if (fptr.put_RegisterNumber(17) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                state.date = MStateKKM.getDate(fptr.get_Date(), fptr.get_Time());
///////////////////////
                if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(256) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetCaption() < 0) {
                    checkError(fptr);
                }
                state.ofdUrl = fptr.get_Caption();
                ////////////////////////////////////////////////////////////////                   //////////////////////////
                if (fptr.put_ValuePurpose(301) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetValue() < 0) {
                    checkError(fptr);
                }
                state.ofdPort = (int) fptr.get_Value();
/////////////////////////////////////////////////////////////////                    ///////////////////////////
                state.serialNumer = fptr.get_SerialNumber();
                state.inn = fptr.get_INN();
                state.mode = fptr.get_Mode();
                state.sessionOpen = fptr.get_SessionOpened();
                state.checkPaperPresent = fptr.get_CheckPaperPresent();
                state.userPassword = fptr.get_UserPassword();
                state.model = fptr.get_Model();
                state.checkNumber = fptr.get_CheckNumber();
                state.sessionOpened = fptr.get_SessionOpened();
                state.session = getNumberSession(fptr);
                state.checkState = fptr.get_CheckState();
                state.batteryLow = fptr.get_BatteryLow();
                state.fiscal = fptr.get_Fiscal();

                printState(fptr, true);


                return true;
            }catch (Exception ex){
                //Controller.refreshOpenCloseSession(null);
                exception=ex;
                log.error(ex);
                return false;
            } finally {
                if(fptr!=null){
                    try{
                        fptr.ResetMode();
                        fptr.destroy();
                    }catch (Exception ex){
                        exception=ex;
                    }

                }

            }
        }



        @Override
        public void onPreExecute() {
            FactoryBlender.run();
            Controller.writeMessage("Состояние кассы");
        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.errorDialog(exception);
                Controller.writeMessage("");

            }else{
                kkmiAction.action(state);
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
