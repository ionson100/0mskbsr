//package bitnic.kassa;
//
//import bitnic.blenderloader.FactoryBlender;
//import com.atol.drivers.fptr.Fptr;
//import com.atol.drivers.fptr.IFptr;
//import bitnic.core.Controller;
//import bitnic.dialogfactory.DialogFactory;
//import bitnic.senders.AsyncTask;
//import org.apache.log4j.Logger;
//
//import java.math.BigDecimal;
//
//public class PrintTestCheck extends BaseKassa {
//
//    private static final Logger log = Logger.getLogger(PrintTestCheck.class);
//    private static Exception exception;
//    private static void registration(IFptr fptr, String name, double price, double quantity) throws DriverException {
//        if (fptr.put_Quantity(quantity) < 0)
//            checkError(fptr);
//        if (fptr.put_Price(price) < 0)
//            checkError(fptr);
//        if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0)
//            checkError(fptr);
//        if (fptr.put_Name(name) < 0)
//            checkError(fptr);
//        if (fptr.Registration() < 0)
//            checkError(fptr);
//    }
//
//    private static void registrationFZ54(IFptr fptr, String name, double price, double quantity,
//                                         double positionSum, int taxNumber) throws DriverException {
//        if (fptr.put_PositionSum(positionSum) < 0)
//            checkError(fptr);
//        if (fptr.put_Quantity(quantity) < 0)
//            checkError(fptr);
//        if (fptr.put_Price(price) < 0)
//            checkError(fptr);
//        if (fptr.put_TaxNumber(taxNumber) < 0)
//            checkError(fptr);
//        if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0)
//            checkError(fptr);
//        if (fptr.put_Name(name) < 0)
//            checkError(fptr);
//        if (fptr.Return() < 0)
//            checkError(fptr);
//    }
//
//    private static void payment(IFptr fptr, double sum, int type) throws DriverException {
//        if (fptr.put_Summ(sum) < 0)
//            checkError(fptr);
//        if (fptr.put_TypeClose(type) < 0)
//            checkError(fptr);
//        if (fptr.Payment() < 0)
//            checkError(fptr);
//        System.out.println(String.format("Remainder: %.2f, Change: %.2f", fptr.get_Remainder(), fptr.get_Change()));
//    }
//
//
//
//
//
//    private static void discount(IFptr fptr, double sum, int type, int destination) throws DriverException {
//        if (fptr.put_Summ(sum) < 0)
//            checkError(fptr);
//        if (fptr.put_DiscountType(type) < 0)
//            checkError(fptr);
//        if (fptr.put_Destination(destination) < 0)
//            checkError(fptr);
//        if (fptr.Discount() < 0)
//            checkError(fptr);
//    }
//
//    private static void charge(IFptr fptr, double sum, int type, int destination) throws DriverException {
//        if (fptr.put_Summ(sum) < 0)
//            checkError(fptr);
//        if (fptr.put_DiscountType(type) < 0)
//            checkError(fptr);
//        if (fptr.put_Destination(destination) < 0)
//            checkError(fptr);
//        if (fptr.Charge() < 0)
//            checkError(fptr);
//    }
//
//    public static boolean innerPrint() {
//
//        IFptr fptr = null;
//
//        try {
//            fptr = new Fptr();
//            fptr.create();
//
//            setWorkDir(fptr);
//
//
//
//            if (fptr.put_DeviceEnabled(true) < 0)
//                checkError(fptr);
//
//
//            if (fptr.GetStatus() < 0)
//                checkError(fptr);
//
//            try {
//                if (fptr.CancelCheck() < 0)
//                    checkError(fptr);
//            } catch (DriverException e) {
//                int rc = fptr.get_ResultCode();
//                if (rc != -16 && rc != -3801)
//                    throw e;
//            }
//
//            if (true) {
//
//                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
//                    checkError(fptr);
//                }
//                if (fptr.SetMode() < 0) {
//                    checkError(fptr);
//                }
//                if (fptr.put_CheckType(IFptr.CHEQUE_TYPE_RETURN) < 0) {
//                    checkError(fptr);
//                }
//                if (fptr.OpenCheck() < 0) {
//                    checkError(fptr);
//                }
//
//
//                BigDecimal sum = new BigDecimal(0);
//                for (int i = -2; i < 5; ++i) {
//                    if (true) {
//                        double price = Math.pow(10, i), quantity = 1;
//                        registrationFZ54(fptr, String.format("тестовый чек %d", i + 3), price, quantity, price * quantity, IFptr.TAX_VAT_18);
//                        sum = sum.add(new BigDecimal(price).multiply(new BigDecimal(quantity)));
//                    }
//                }
//
//                if (true) {
//
//                    discount(fptr, 0, IFptr.DISCOUNT_SUMM, IFptr.DESTINATION_CHECK);
//                } else {
//                    discount(fptr, 1, IFptr.DISCOUNT_PERCENT, IFptr.DESTINATION_CHECK);
//                }
//
//
//
//                payment(fptr, sum.divide(new BigDecimal(4)).doubleValue(), 1);
//                payment(fptr, sum.divide(new BigDecimal(4)).doubleValue(), 2);
//                payment(fptr, sum.divide(new BigDecimal(4)).doubleValue(), 3);
//                payment(fptr, sum.doubleValue(), 0);
//
//                closeCheck(fptr, 0);
//
//
//                return true;
//            }
//
//
//
//            return true;
//        } catch (Exception e) {
//
//
//            log.error(e);
//            exception=e;
//            System.out.println(e);
//            return false;
//
//        } finally {
//            if(fptr!=null){
//                fptr.ResetMode();
//                fptr.destroy();
//            }
//        }
//    }
//
//    public void print() {
//
//
//        new MyWorker().execute(null);
//    }
//
//    private static class MyWorker extends AsyncTask<Void, Integer, Boolean> {
//
//
//        @Override
//        public Boolean doInBackground(Void... params) {
//            return innerPrint();
//
//        }
//
//        @Override
//        public void onPreExecute() {
//            Controller.writeMessage("Тестовый чек");
//
//        }
//
//        @Override
//        public void onPostExecute(Boolean params) {
//            FactoryBlender.stop();
//            if(exception!=null){
//                DialogFactory.errorDialog(exception);
//                Controller.writeMessage("Тестовый чек - ошибка");
//            }
//            Controller.writeMessage("Печать окончена");
//        }
//    }
//
//
//}
