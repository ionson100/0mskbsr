package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import org.apache.log4j.Logger;


public class TestConnect extends BaseKassa {

    private static final Logger log = Logger.getLogger(TestConnect.class);

    public void print(){


        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void,Integer,Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();
            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                fptr.destroy();

                return true;
            }catch (Exception ex){
                log.error(ex);
                exception=ex;
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {


            FactoryBlender.run();
            Controller.writeMessage("Тестирование соединения");
        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
            if(params){
                Controller.writeMessage("Тестирование соединения - успешно");
            }else {

                if(exception!=null){
                    Controller.writeMessage("Тестирование соединения - щшибка");
                    DialogFactory.errorDialog(exception);
                }
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }

}
