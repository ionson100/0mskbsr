package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class SetTemplateCheck extends BaseKassa {

    private static final Logger log = Logger.getLogger(SetTemplateCheck.class);
    private int temp;

    public void set(int temp){

        this.temp = temp;
        new MyWorker().execute(null);
    }
    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                //("Установка программирования...");
                if (fptr.put_Mode(4) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }


                if (fptr.put_ValuePurpose(346) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetValue() < 0) {
                    checkError(fptr);
                }

                if (fptr.put_ValuePurpose(346) < 0) {// повторная печать
                    checkError(fptr);
                }

                if (fptr.put_Value(temp) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }


            } catch (Exception ex) {
                exception = ex;
                log.error(ex);


            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
            return null;
        }

        @Override
        public void onPreExecute() {

            FactoryBlender.run();
        }

        @Override
        public void onPostExecute(Void params) {
            FactoryBlender.stop();

            if(exception!=null){
                DialogFactory.errorDialog(exception);
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
