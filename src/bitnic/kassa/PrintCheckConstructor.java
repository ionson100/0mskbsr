package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.pagesale.MainForm;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14;
import bitnic.transaction.eventfrontol.Event_40_41;
import bitnic.transaction.eventfrontol.Event_42_55;
import bitnic.utils.Support;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.WorkingFile;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class PrintCheckConstructor extends BaseKassa {


    private static final Logger log = Logger.getLogger(PrintCheckConstructor.class);


    private Exception exception;
    private List<MProduct> selectListProduct = new ArrayList<>();
    private Transaction transaction;
    private boolean payment_type;
    private int user_id;
    private int point_id;
    private String userName;
    private boolean isFiscal;


    public void print(int user_id,int point_id,String userName,List<MProduct> selectListProduct, boolean PAYMENT_TYPE, boolean isFiscal) {
        this.user_id = user_id;
        this.point_id = point_id;
        this.userName = userName;

        this.isFiscal = isFiscal;

        for (MProduct mProduct : selectListProduct) {
            if (mProduct.selectAmount <= 0) continue;
            this.selectListProduct.add(mProduct.clone());
        }


        transaction = new Transaction();
        payment_type = PAYMENT_TYPE;

        new MyWorker().execute(null);
    }


    public static void checkUser(IFptr fptr,String user_name) throws DriverException {
        if (fptr.put_Mode(4) < 0) {
            checkError(fptr);
        }
        if (fptr.SetMode() < 0) {
            checkError(fptr);
        }
        ////////////////////////
        if (fptr.put_Caption(user_name) < 0) {
            checkError(fptr);
        }

        if (fptr.put_CaptionPurpose(118) < 0) {
            checkError(fptr);
        }
        if (fptr.SetCaption() < 0) {
            checkError(fptr);
        }
    }
    public static void printTextItogo(IFptr fptr, String text, int alignment, int wrap) throws Exception {


        if (fptr.put_FontDblHeight(true) < 0) {
            checkError(fptr);
        }
        if (fptr.put_FontDblWidth(true) < 0) {
            checkError(fptr);
        }
        if (fptr.put_FontBold(true) < 0) {
            checkError(fptr);
        }
        if (fptr.put_Caption(text) < 0) {
            checkError(fptr);
        }
        if (fptr.put_TextWrap(wrap) < 0) {
            checkError(fptr);
        }
        if (fptr.put_Alignment(alignment) < 0) {
            checkError(fptr);
        }
        fptr.AddTextField();

        fptr.PrintFormattedText();


    }

    private class MyWorker extends AsyncTask<Void, Integer, Boolean> {






        private void openCheckE(IFptr fptr, int type) throws Exception {
            if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                checkError(fptr);
            }
            if (fptr.SetMode() < 0) {
                checkError(fptr);
            }

            if (fptr.put_CheckType(type) < 0) {
                checkError(fptr);
            }
            if (fptr.OpenCheck() < 0) {
                checkError(fptr);
            }
        }

        @Override
        public Boolean doInBackground(Void... params) {

            //////////////////////////////////////////////////////////////////////////

            if (isFiscal) {
                return fiscalCheck();
            } else {
                return noFiscalCheck();
            }

        }

        public  Boolean noFiscalCheck() {
            Fptr fptr =null;
            try {
                fptr= new Fptr();
                fptr.create();
                setWorkDir(fptr);

                if (fptr.put_DeviceEnabled(true) < 0) {
                    checkError(fptr);
                }

                if (fptr.GetStatus() < 0) {
                    checkError(fptr);
                }

                cancelCheck(fptr);

                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                printText(fptr, "КОНСТРУКТОР ЧЕКА", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                printText(fptr, "КАССОВЫЙ ЧЕК", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                printText(fptr, "ПРИХОД", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);


                double total = 0;
                double totalNds = 0;
                for (MProduct product : selectListProduct) {
                    printText(fptr, product.name_check, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, String.valueOf(product.selectAmount) + "х" + String.valueOf(product.price)
                                    + " =" + String.valueOf(UtilsOmsk.round(product.selectAmount * product.price, 2)),
                            IFptr.ALIGNMENT_RIGHT, IFptr.WRAP_WORD);
                    total = total + product.selectAmount * product.price;
                    double nds = ((product.selectAmount * product.price) / 100) * 18;
                    totalNds = totalNds + nds;
                    printText(fptr, " Сумма НДС 18%      =" + String.valueOf(UtilsOmsk.round(nds, 2)),
                            IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                }
                String tota = String.valueOf(UtilsOmsk.round(total, 2));
                printTextItogo(fptr, "ИТОГ =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "ОПЛАТА", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

                printText(fptr, "ВСЕГО ОПЛАЧЕНО", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                if (payment_type) {
                    printText(fptr, "НАЛИЧНЫМИ          =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                } else {
                    printText(fptr, "ЭЛЕКТРОННЫМИ       =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                }

                printText(fptr, "Сумма НДС           =" + String.valueOf(UtilsOmsk.round(totalNds, 2)),
                        IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "Кассир: " + userName, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "Торговая точка: ", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, UtilsOmsk.simpleDateFormatE(new Date()), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberCheck = getNumberCheck(fptr);
                transaction.summCheck = UtilsOmsk.round(total, 2);
                transaction.numberSession = getNumberSession(fptr);
                transaction.numberDoc = new Random().nextInt(10000);


            } catch (Exception e) {
                log.error(e);
                exception = e;
                return false;

            } finally {
                if(fptr!=null){
                    fptr.ResetMode();
                    fptr.destroy();
                }

            }
            return true;
        }

        @NotNull
        private Boolean fiscalCheck() {
            IFptr fptr = null;

            try {


                fptr = new Fptr();

                log.info("Печать основного чека конструктор продажа  fiscal: " + isFiscal + " нал: " + payment_type);
                FactoryBlender.addMessage("Печать основного чека");
                fptr.create();
                setWorkDir(fptr);

                FactoryBlender.addMessage("Инициализация настроек");
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                if (fptr.GetStatus() < 0)
                    checkError(fptr);

                if (fptr.get_SessionOpened() == false) {
                    throw new RuntimeException(Support.str("name306"));
                }

                FactoryBlender.addMessage("Изменение статуса");


                cancelCheck(fptr);


                /////////////////////////////////////////// открытие сменыж


                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет = fptr.get_Summ();
                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();
                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сумма_нал_ккм = fptr.get_Summ();




                checkUser(fptr,userName);


                try {
                    openCheckE(fptr, IFptr.CHEQUE_TYPE_SELL);
                } catch (Exception e) {
                    // Проверка на превышение смены
                    if (fptr.get_ResultCode() == -3822) {
                        Controller.writeMessage("Требуется z - отчет.");
                        throw e;
                    } else {
                        throw e;
                    }
                }


                FactoryBlender.addMessage("Открытие чека");
                Collections.sort(selectListProduct, Comparator.comparing(lhs -> lhs.name));
                FactoryBlender.addMessage("Инициализация чека продуктами");
                double totalAmount = 0;
                for (MProduct product : selectListProduct) {

                    if (product.selectAmount <= 0) continue;
                    totalAmount = totalAmount + product.selectAmount;
                    PrintCheck2.registrationFZ54E(fptr, product);

                }

                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberCheck = getNumberCheck(fptr);

                if (fptr.put_RegisterNumber(20) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }

                transaction.summCheck = fptr.get_Summ();

                transaction.numberSession = getNumberSession(fptr);

                try {
                    printState(fptr, false);
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }




                if (payment_type == false) {
                    closeCheck(fptr, 1);//кредит
                } else {
                    closeCheck(fptr, 0);// cach

                }

                checkUser(fptr,SettingAppE.getInstance().getUserCheck());


                if (fptr.put_Mode(4) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Caption(SettingAppE.getInstance().getUserCheck()) < 0) {
                    checkError(fptr);
                }

                if (fptr.put_CaptionPurpose(118) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                /////////////////////////
                log.info("Чек отпечатан");
                FactoryBlender.addMessage("Чек отпечатан");


                transaction.numberDoc = getNumberDoc(fptr);


                return true;

            } catch (Exception ex) {

                if (fptr != null) {
                    fptr.CancelCheck();
                }

                log.error(ex);

                exception = ex;
                ex.printStackTrace();
                return false;
            } finally {
                if (fptr != null) {
                    fptr.ResetMode();
                    fptr.destroy();
                }

            }
        }


        @Override
        public void onPreExecute() {

            FactoryBlender.run();
            Controller.writeMessage("Печать чека");

        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();

            int delta = 1;


            if (params == true && isFiscal == false) {


                for (MProduct mProduct : selectListProduct) {

                    Event_1_11_2_12_4_14 e = new Event_1_11_2_12_4_14();
                    e.codeProd_8 = mProduct.code_prod;
                    e.price_not_discount_10 = mProduct.price;
                    e.amount_prod_11 = mProduct.selectAmount;
                    delta++;
                    e.numberAction_1 = Iterator.getId(delta);
                    e.dateAction_2 = transaction.getDate();
                    e.timeAction_3 = transaction.getTime();
                    e.int_1_4 = 11;
                    e.code_pm_5 = point_id;
                    e.nimberDoc_6 = transaction.numberDoc;
                    e.codeKassir_7 = user_id;
                    e.info_doc_26 = point_id + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                    e.code_firma_27 = point_id;
                    double sum = UtilsOmsk.round(mProduct.price * mProduct.selectAmount, 2);
                    e.amount_rount_prod_12 = sum;
                    e.price_discount_rub_16 = sum;
                    e.summ_itogo_20 = sum;
                    e.priee_discount_15 = mProduct.price;
                    e.kkm_operation_13 = 0;
                    e.code_group_print_17 = 1;
                    e.kmm_section_21 = 1;
                    e.code_vid_code_23 = 1;
                    e.number_smena_14 = transaction.numberSession;
                    transaction.list.add(e);
                }


                delta++;
                Event_40_41 e = new Event_40_41();//куплено за нал
                e.numberAction_1 = Iterator.getId(delta);
                e.dateAction_2 = transaction.getDate();
                e.timeAction_3 = transaction.getTime();
                e.int_1_4 = 41;
                e.code_pm_5 = point_id;
                ;
                e.nimberDoc_6 = transaction.numberDoc;
                e.codeKassir_7 = user_id;
                //e.number_card_8 = numberCard;
                if (payment_type) {
                    e.code_payment_9 = "1";// codePayment;nal
                } else {
                    e.code_payment_9 = "4";// codePayment;безнал
                }
                e.number_smena_14 = transaction.numberSession;


                if (payment_type) {
                    e.type_operation_payment_10 = 0d;
                } else {
                    e.type_operation_payment_10 = 1d;
                }

                e.summ_doc_11 = transaction.summCheck;// totalPriceE;
                e.summ_client_12 = transaction.summCheck;// totalPriceE;
                e.info_doc_26 = point_id + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                e.code_firma_27 = point_id;
                e.kkm_operation_13 = 0;
                e.type_doc_23 = 1;
                transaction.list.add(e);


                Event_42_55 ee = new Event_42_55();// закрытие документа
                double d = 0d;//, summ = 0d;
                for (MProduct mProduct : selectListProduct) {
                    d = d + mProduct.selectAmount;
                }
                ee.amount_prod_11 = d;
                ee.summ_doc_12 = transaction.summCheck;// UtilsOmsk.round(summ, 2);
                ee.number_smena_14 = transaction.numberSession;
                ee.code_group_print_doc_17 = 1;
                ee.sum_bonusl_18 = "0";
                delta++;
                ee.numberAction_1 = Iterator.getId(delta);
                ee.dateAction_2 = transaction.getDate();
                ee.timeAction_3 = transaction.getTime();
                ee.int_1_4 = 55;
                ee.code_pm_5 = point_id;
                ee.nimberDoc_6 = transaction.numberDoc;
                ee.codeKassir_7 = user_id;
                ee.operation_kkm_13 = 0;
                ee.code_doc_23 = 1;
                ee.summ_doc_12 = transaction.summCheck;//totalPriceE;
                ee.info_doc_26 = point_id + "/" + transaction.numberCheck + "/" + transaction.numberDoc;
                ee.code_firma_27 = user_id;
                transaction.list.add(ee);



                try {

                    SettingAppE.getInstance().selectProduct.clear();
                    SettingAppE.save();


                    if (Controller.curnode instanceof MainForm) {
                        ((MainForm) Controller.curnode).refreshCheck();
                    }
                    if (Controller.curnode instanceof IRefreshLoader) {
                        ((IRefreshLoader) Controller.curnode).refresh();
                    }
                } catch (Exception ex) {
                    log.error(ex);
                }


            } else {
                if (exception != null) {
                    Controller.writeMessage("Печать чека - ошибка");
                    DialogFactory.errorDialog(exception.getMessage());


                }
            }


            if (transaction.list.size() > 0) {
                WorkingFile.appenderReportDate(transaction.list);
                FactoryBlender.addMessage("Сохранение данных на диск.");

            }



        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }


}
