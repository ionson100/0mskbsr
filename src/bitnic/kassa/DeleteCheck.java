package bitnic.kassa;

import bitnic.bank.sber.ActionSale;
import bitnic.bank.sber.BankResult;
import bitnic.blenderloader.FactoryBlender;

import bitnic.checkarchive.AmountSelf;
import bitnic.checkarchive.AmountSelfTotal;
import bitnic.checkarchive.CheckArchiveE;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.model.MTaxrates;
import bitnic.orm.Configure;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_56;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.WorkingFile;
import bitnic.utils.Support;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DeleteCheck extends BaseKassa {


    private static final Logger log = Logger.getLogger(DeleteCheck.class);

    private BankResult resultAction1;
    private StringBuilder stringBuilder = new StringBuilder();
    private List <MProduct> mProductList;

    public void addMessage(String msq) {
        stringBuilder.append(msq).append(System.lineSeparator());
        log.info(msq);
    }

    public String getMessageE() {
        return stringBuilder.toString();
    }

    public  void deleteReturn(String linkBankDoc , List<MProduct> mProductList, int doc_id, Transaction transaction, String code_doc, IAction<Integer> iAction) {
        this.mProductList = mProductList;


        new AsyncTask<Void, String, Void> () {
            Exception exception;

            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            private void openCheck() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_CheckType(IFptr.CHEQUE_TYPE_RETURN) < 0) {
                    checkError();
                }
                if (fptr.OpenCheck() < 0) {
                    checkError();
                }
            }


            private void reportZ() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError();
                }
                if (fptr.Report() < 0) {
                    checkError();
                }
                throw new Exception("Отмена отклоненна, пробит z отчет!");

            }

            private void registrationFZ54(MProduct mProduct,String name, double price, double quantity) throws Exception {

                List<MTaxrates> mTaxrates = Configure.getSession().getList(MTaxrates.class, "cod_nalog = ?", mProduct.cod_nalog_group);
                if (mTaxrates.size() > 0) {
                    if (fptr.put_TaxNumber(mTaxrates.get(0).nalog_number_kkm) < 0) {
                        checkError();
                    }
                } else {
                    if (fptr.put_TaxNumber(Fptr.TAX_VAT_18) < 0) {
                        checkError();
                    }
                }
                if (fptr.put_Quantity(quantity) < 0) {
                    checkError();
                }
                if (fptr.put_Price(price) < 0) {
                    checkError();
                }
                if (fptr.put_PositionSum(quantity * price) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                    checkError();
                }
                if (fptr.put_Name(name) < 0) {
                    checkError();
                }
                if (fptr.Return() < 0) {
                    checkError();
                }
            }

            @Override
            public Void doInBackground(Void... params) {

                int summ=0;

                try {

                    {
                        double max = 0;
                        for (MProduct product : mProductList) {
                            max = max + product.price*product.selectAmount;
                        }
                        if (max > SettingAppE.maxSumm) {
                            throw new Exception(Support.str("name119"));
                        }
                        max= UtilsOmsk.round(max,2);
                        summ= (int) (max*100);
                    }


                    log.info("Отмена чека - Приступаем");

                    if(linkBankDoc!=null&&linkBankDoc.length()>5) {
                        FactoryBlender.addMessage("Запрос в банк. Начало транзакции - 8 отмены");
                        log.info("Запрос в банк. Начало транзакции - 8 отмены");
                        log.info("summa: "+summ);
                        resultAction1 = ActionSale.exe(8, summ, 0,linkBankDoc);
                        // resultAction1.result = 0;///////////////////////////////////////////////////////////////////////////
                        if (resultAction1.result == 0) {
                            FactoryBlender.addMessage("Запрос в банк УСПЕШНО");
                            log.info("Транзакция 8 зарегистрирована в банке  - 0");

                        } else {
                            throw new RuntimeException("Транзакция  8 Завершена с ошибкой  - " + resultAction1.linesE.get(0));

                        }
                    }

                    log.info("fptr created");
                    fptr = new Fptr();

                    fptr.create();

                    BaseKassa.setWorkDir(fptr);

                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }

                    log.info("CancelCheck");
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }
                    log.info("open check");
                    try {
                        openCheck();
                    } catch (Exception e) {
                        // Проверка на превышение смены
                        if (fptr.get_ResultCode() == -3822) {
                            reportZ();
                            openCheck();
                        } else {
                            throw e;
                        }
                    }

                    SettingAppE.setSession(getNumberSession(fptr));

                    Collections.sort(mProductList, Comparator.comparing(o -> o.name));



                    if(linkBankDoc!=null&&linkBankDoc.length()>5){
                        if (fptr.put_TypeClose(1) < 0)// Закрываем через банк
                            checkError();
                    }else {
                        if (fptr.put_TypeClose(0) < 0)// Закрываем через нал
                            checkError();
                    }

                    double totalAmount = 0;

                    log.info("add product");
                    for (MProduct product : mProductList) {

                        totalAmount = totalAmount + product.selectAmount;
                        registrationFZ54(product,product.name_check, product.price, product.selectAmount);

                    }


                    if (fptr.put_RegisterNumber(20) < 0) {// выручка
                        checkError();
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError();
                    }

                    transaction.summCheck = fptr.get_Summ();
                    transaction.date = fptr.get_Date();
                    transaction.time = fptr.get_Time();
                    transaction.numberDoc = getNumberDoc(fptr);
                    transaction.numberCheck = getNumberCheck(fptr);
                    transaction.numberSession= getNumberSession(fptr);


                    log.info("close check");

                    if (fptr.CloseCheck() < 0)
                        checkError();

                    if(linkBankDoc!=null&&linkBankDoc.length()>5){
                        try {
                            log.info("print sber document");
                            FactoryBlender.addMessage("Печатаем банковские чеки");
                            printBankCheck(fptr,resultAction1.linesP);

                        }catch (Exception ex){
                            log.error("При распечатке чека банка возврата произошла ошибка: "+ex.getMessage());
                        }
                    }

                    log.info("finish print delete check");
                    printState(fptr, false);

                } catch (Exception e) {

                    if(fptr!=null){
                        fptr.CancelCheck();
                    }

                    log.error ( e );
                    exception = e;
                    e.printStackTrace();
                } finally {
                    if(fptr!=null){
                        fptr.ResetMode();
                        fptr.destroy();
                    }

                }
                return null;
            }

            @Override
            public void onPreExecute() {
                FactoryBlender.run();
                Controller.writeMessage("Отмена чека");
            }

            @Override
            public void onPostExecute(Void aVoid) {

                FactoryBlender.stop();

                log.info("getMessageE() "+getMessageE());
                if (exception != null) {
                    Controller.writeMessage("Отмена чека - ошибка");
                    DialogFactory.errorDialog(exception.getMessage());
                } else {


                    try{
                        AmountSelf.addProductReturn (mProductList,transaction.numberDoc);
                        AmountSelfTotal.addProductReturn (mProductList,transaction.numberDoc,transaction.numberSession);
                    }catch (Exception ex){
                        log.error ( ex );
                    }
                    int delta=1;
                    CheckArchiveE.updateReturn(doc_id);

                    try {
                        Controller.writeMessage("");
                        Event_56 e = new Event_56();
                        delta++;
                        e.numberAction_1 = Iterator.getId(delta);
                        e.dateAction_2 = transaction.getDate();
                        e.timeAction_3 = transaction.getTime();
                        e.int_1_4 = 56;
                        e.code_pm_5 = SettingAppE.getInstance().getPointId();;
                        e.nimberDoc_6 = transaction.numberDoc;
                        e.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                        e.kkm_operation_13=1;//возврат
                        e.number_smena_14=transaction.numberSession;
                        e.code_print_group_17=1;
                        e.type_doc_23=2;
                        e.info_doc_26=code_doc;
                        System.out.println(code_doc);
                        transaction.list.add(e);

                        WorkingFile.appenderReportDate(transaction.list);
                        if (iAction != null) {
                            iAction.action(transaction.numberDoc);
                        }
                        if (Controller.curnode instanceof IRefreshLoader) {
                            ((IRefreshLoader) Controller.curnode).refresh();
                        }



                    } catch (Exception ex) {
                        log.error ( ex );
                        DialogFactory.errorDialog(ex);
                        throw new RuntimeException(ex);

                    }


                }
                System.gc();
            }

            @Override
            protected void onErrorInner(Throwable ex) {
                FactoryBlender.stop();
                log.error(ex);
                DialogFactory.errorDialog(ex.getMessage());
            }


        }.execute(null);
    }
}
