package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_60_61_62_63_64;
import bitnic.utils.WorkingFile;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class OpenSession extends BaseKassa {

    private static final Logger log = Logger.getLogger(OpenSession.class);
    private Transaction transaction;
    private IAction iActionProcedure;


    public void open(Transaction transaction, IAction iActionProcedure) {
        this.transaction = transaction;
        this.iActionProcedure = iActionProcedure;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();


            try {

                fptr = new Fptr();
                fptr.create();
                setWorkDir(fptr);

                if (fptr.put_DeviceEnabled(true) < 0) {
                    checkError(fptr);
                }

                if (fptr.GetStatus() < 0) {
                    checkError(fptr);
                }

                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }

                if (fptr.OpenSession() < 0) {
                    checkError(fptr);
                }

                SettingAppE.setSession(getNumberSession(fptr));


                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();


                transaction.summSmena = fptr.get_Summ();
                transaction.numberCheck = getNumberCheck(fptr);
                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет = fptr.get_Summ();
                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();
                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сумма_нал_ккм = fptr.get_Summ();

                transaction.numberSession = getNumberSession(fptr);


                SettingAppE.setSession(getNumberSession(fptr) + 1);
                System.out.println(SettingAppE.getSession());
                transaction.numberSession = getNumberSession(fptr) + 1;
                System.out.println(transaction.numberSession);
                transaction.numberDoc = getNumberDoc(fptr);
                SettingAppE.setShowButtonUpdate(0);
                printState(fptr, true);

                log.info("Открытие смены");
                return true;
            } catch (Exception ex) {
               // Controller.refreshOpenCloseSession(null);
                log.error(ex);
                exception = ex;
                ex.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {
            Controller.writeMessage("Открытие смены");
            FactoryBlender.run();
        }

        @Override
        public void onPostExecute(Boolean params) {

            int delta = 1;
            if (exception != null) {
                Controller.writeMessage("Открытие смены - ошибка");
                DialogFactory.errorDialog(exception);
                if (iActionProcedure != null) {
                    iActionProcedure.action(exception.getMessage());
                }
            } else {

                // CheckBody.clearTable();
                Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                z.int_1_4 = 64;
                z.number_smena_14 = transaction.numberSession;
                z.nimberDoc_6 = transaction.numberDoc;
                z.sum_smena_10 = transaction.выручка_отчет;
                delta++;
                z.numberAction_1 = Iterator.getId(delta);
                z.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                z.code_firma_27 = SettingAppE.getInstance().getPointId();
                z.dateAction_2 = transaction.getDate();
                z.timeAction_3 = transaction.getTime();
                z.code_pm_5 = SettingAppE.getInstance().getPointId();
                ;
                z.int_1_13 = 9;// операция в ккм
                z.sum_kassa_11 = transaction.сумма_нал_ккм;
                z.itogo_smena_12 = transaction.сменный_итог;
                z.info_doc_26 = "" + transaction.numberCheck + "/" + transaction.numberDoc + "/" + transaction.numberSession;
                transaction.list.add(z);

                WorkingFile.appenderReportDate(transaction.list);

                if (iActionProcedure != null) {
                    iActionProcedure.action(null);
                }
            }
            FactoryBlender.stop();

            Controller.writeMessage("Открытие смены - успешно");
            try{
                if (Controller.curnode instanceof IRefreshLoader) {
                    ((IRefreshLoader) Controller.curnode).refresh();
                }
            }catch (Exception ex){
                log.error(ex);
            }
            Controller.getInstans().refreshButtonUpdate();
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
