package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class GetTemplateCheck extends BaseKassa {

    private static final Logger log = Logger.getLogger(GetTemplateCheck.class);

    private int temp = -1;

    private IAction<Integer> iAction;

    public void get(IAction<Integer> iAction) {

        this.iAction = iAction;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                setWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                if (fptr.put_ValuePurpose(346) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetValue() < 0) {
                    checkError(fptr);
                }

                temp = (int) fptr.get_Value();

                log.info("Изменение темплейта чека");

            } catch (Exception ex) {
                exception = ex;
                log.error(ex);


            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
            return null;
        }

        @Override
        public void onPreExecute() {

            FactoryBlender.run();
        }

        @Override
        public void onPostExecute(Void params) {
            FactoryBlender.stop();
            if (iAction != null) {
                iAction.action(temp);
            }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
