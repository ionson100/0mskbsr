package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.checkarchive.CheckArchiveE;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.senders.AsyncTask;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_50_51;
import bitnic.transaction.eventfrontol.Event_60_61_62_63_64;
import bitnic.utils.WorkingFile;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class ZReport extends BaseKassa {

    private static final Logger log = Logger.getLogger(ZReport.class);
    private Transaction transaction;


    private Transaction transaction2 = new Transaction();

    public void print(Transaction transaction) {
        this.transaction = transaction;

        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Integer, Boolean> {


        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();
            log.info("Отчет с гашением");
            try {

                fptr.create();
                setWorkDir(fptr);

                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = getNumberDoc(fptr);
                transaction.numberSession = getNumberSession(fptr);
                transaction.summSmena = fptr.get_Summ();

                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет = fptr.get_Summ();
                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();
                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сумма_нал_ккм = fptr.get_Summ();
                transaction.numberCheck = getNumberCheck(fptr);


                //("Отмена чека...");
                cancelCheck(fptr);

                if (fptr.put_Mode(IFptr.MODE_REPORT_CLEAR) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError(fptr);
                }

                if (fptr.Report() < 0) {
                    checkError(fptr);
                }

                {
                    transaction2.date = fptr.get_Date();
                    transaction2.time = fptr.get_Time();
                    transaction2.numberDoc = getNumberDoc(fptr);

                    transaction2.summSmena = fptr.get_Summ();

                    if (fptr.put_RegisterNumber(11) < 0) {// выручка
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction2.выручка_отчет = fptr.get_Summ();
                    if (fptr.put_RegisterNumber(12) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_OperationType(0) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction2.сменный_итог = fptr.get_Summ();
                    if (fptr.put_RegisterNumber(10) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction2.сумма_нал_ккм = fptr.get_Summ();
                    transaction2.numberCheck = getNumberCheck(fptr);
                }

                transaction.numberSession = getNumberSession(fptr);

                log.info("------------------------------------------------------------");
                log.info("transaction.выручка_отчет   "+transaction.выручка_отчет );
                log.info("transaction.сменный_итог    "+transaction.сменный_итог );
                log.info("transaction.сумма_нал_ккм   "+transaction.сумма_нал_ккм );
                log.info("------------------------------------------------------------");
                log.info("transaction2.выручка_отчет  "+transaction2.выручка_отчет );
                log.info("transaction2.сменный_итог   "+transaction2.сменный_итог );
                log.info("transaction2.сумма_нал_ккм  "+transaction2.сумма_нал_ккм );
                log.info("------------------------------------------------------------");
                SettingAppE.setSession(getNumberSession(fptr));
                SettingAppE.setShowButtonUpdate(1);
                printState(fptr, false);

                return true;

            } catch (Exception ex) {
                //Controller.refreshOpenCloseSession(null);
                log.error(ex);
                exception = ex;
                return false;
            } finally {

                fptr.ResetMode();
                fptr.destroy();

            }
        }

        @Override
        public void onPreExecute() {
            Controller.writeMessage("Отчет c гашением");
            FactoryBlender.run();
        }

        @Override
        public void onPostExecute(Boolean params) {

            int delta = 1;
            FactoryBlender.stop();
            if (exception != null) {
                DialogFactory.errorDialog(exception);
                Controller.writeMessage("Отчет c гашением - ошибка");
            }
            if (params == true) {

                CheckArchiveE.zReport(transaction.numberSession);


                {
                    Event_50_51 e = new Event_50_51();
                    delta++;
                    e.numberAction_1 = Iterator.getId(delta);
                    e.dateAction_2 = transaction.getDate();
                    e.timeAction_3 = transaction.getTime();
                    e.int_1_4 = 51;//вынесение денег
                    e.code_pm_5 = SettingAppE.getInstance().getPointId();;
                    e.code_print_group_17=1;
                    e.int_4_23=6;
                    e.number_smena_14=transaction.numberSession;
                    e.nimberDoc_6 = transaction.numberDoc;
                    e.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    e.double_3_12 = transaction.сумма_нал_ккм;
                    e.info_doc_26 = SettingAppE.getInstance().getPointId()+"/"+transaction.numberCheck+"/" + transaction.numberDoc;
                    e.code_firma_27 = SettingAppE.getInstance().getPointId();
                    e.kkm_operation_13 = 5; //вынесение денег
                    transaction.list.add(e);
                }
                {
                    Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                    z.int_1_4 = 63;
                    z.cod_print_group_17 = 2;
                    z.number_smena_14 = transaction.numberSession;
                    z.nimberDoc_6 = transaction.numberDoc;
                    z.sum_smena_10 = transaction.выручка_отчет;
                    delta++;
                    z.numberAction_1 = Iterator.getId(delta);
                    z.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    z.code_firma_27 = SettingAppE.getInstance().getPointId();
                    z.dateAction_2 = transaction.getDate();
                    z.timeAction_3 = transaction.getTime();
                    z.code_pm_5 = SettingAppE.getInstance().getPointId();
                    ;
                    z.int_1_13 = 9;// операция в ккм
                    z.sum_kassa_11 = transaction.сумма_нал_ккм;
                    z.itogo_smena_12 = transaction.сменный_итог;
                    z.info_doc_26 = "" + transaction.numberCheck + "/" + transaction.numberDoc + "/" + transaction.numberSession;
                    transaction.list.add(z);
                }
                {
                    Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                    z.int_1_4 = 63;
                    z.cod_print_group_17 = 2;
                    z.number_smena_14 = transaction2.numberSession;
                    z.nimberDoc_6 = transaction2.numberDoc;
                    z.sum_smena_10 = transaction2.выручка_отчет;
                    delta++;
                    z.numberAction_1 = Iterator.getId(delta);
                    z.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    z.code_firma_27 = SettingAppE.getInstance().getPointId();
                    z.dateAction_2 = transaction2.getDate();
                    z.timeAction_3 = transaction2.getTime();
                    z.code_pm_5 = SettingAppE.getInstance().getPointId();

                    z.int_1_13 = 9;// операция в ккм
                    z.sum_kassa_11 = transaction2.сумма_нал_ккм;
                    z.itogo_smena_12 = transaction2.сменный_итог;
                    z.info_doc_26 = "" + transaction2.numberCheck + "/" + transaction2.numberDoc + "/" + transaction2.numberSession;
                    transaction.list.add(z);
                }
                {
                    Event_60_61_62_63_64 z = new Event_60_61_62_63_64();// закрытие смены
                    z.int_1_4 = 61;

                    z.info_doc_26 = "" + transaction2.numberCheck + "/" + transaction2.numberDoc + "/" + transaction.numberSession;
                    z.int_6_23 = 9;
                    z.number_smena_14 = transaction.numberSession;
                    z.nimberDoc_6 = transaction.numberDoc;
                    z.sum_smena_10 = transaction.выручка_отчет;
                    delta++;
                    z.numberAction_1 = Iterator.getId(delta);
                    z.codeKassir_7 = Integer.parseInt(SettingAppE.getInstance().user_id);
                    z.code_firma_27 = SettingAppE.getInstance().getPointId();
                    z.dateAction_2 = transaction.getDate();
                    z.timeAction_3 = transaction.getTime();
                    z.code_pm_5 = SettingAppE.getInstance().getPointId();
                    ;
                    z.int_1_13 = 10;// операция в ккм
                    z.sum_kassa_11 = transaction.сумма_нал_ккм;
                    z.itogo_smena_12 = transaction.сменный_итог;
                    transaction.list.add(z);
                }

                Controller.writeMessage("Отчет c гашением - успешно");
                WorkingFile.appenderReportDate(transaction.list);
                try{
                    if (Controller.curnode instanceof IRefreshLoader) {
                        ((IRefreshLoader) Controller.curnode).refresh();
                    }
                }catch (Exception ex){
                    log.error(ex);
                }
                SettingAppE.save();
            }
            Controller.getInstans().refreshButtonUpdate();


        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
