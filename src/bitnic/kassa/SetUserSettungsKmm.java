package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import org.apache.log4j.Logger;

public class SetUserSettungsKmm extends BaseKassa {

    private static final Logger log = Logger.getLogger(SetUserSettungsKmm.class);
    private IAction iActionCommit;

    public void print(IAction iActionCommit) {
        this.iActionCommit = iActionCommit;

        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Integer, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {

            IFptr fptr = new Fptr();

            log.info("Ввод имени пользователя");
            try {
                fptr.create();

                setWorkDir(fptr);
                FactoryBlender.addMessage("Подключаемся к устройству");
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                //("Ввод паспорта...");
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Ввод пароля");
                //("Установка программирования...");
                if (fptr.put_Mode(4) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                String userName=SettingAppE.getInstance().getUserCheck();
                if (fptr.put_Caption(userName) < 0) {
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Установка имени кассира");
                if (fptr.put_CaptionPurpose(118) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                FactoryBlender.addMessage("Закончили");

                SettingAppE.setSession(getNumberSession(fptr));

                printState(fptr, false);

                return true;

            } catch (Exception ex) {
               // Controller.refreshOpenCloseSession(null);
                log.error(ex);
                exception = ex;
                return false;
            } finally {
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {
            FactoryBlender.run();
            Controller.writeMessage("Ввод в ккм имени пользователя");
        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
                if (exception != null) {
                    Controller.writeMessage("Ввод в ккм имени пользователя - ошибка");
                    if(iActionCommit!=null){
                        iActionCommit.action(exception.getMessage());
                    }
                }else {
                    Controller.writeMessage("Ввод в ккм имени пользователя - успешно");
                    if(iActionCommit!=null){
                        iActionCommit.action(null);
                    }
                }
            System.gc();
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
