package bitnic.pageamountselfcore;

import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableTestDouble;
import bitnic.table.celldescriptions.TableTestName;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AmountSelfCore {
    private static final Logger log = Logger.getLogger(AmountSelfCore.class);




    @DisplayColumn(name_column = "ид продукта")
    public String code_prod;
    @DisplayColumn(name_column = "наименование", width = 700,ClassTableCell = TableTestName.class)
    public String name;
    @DisplayColumn(name_column = "остаток точка " ,ClassTableCell = TableTestDouble.class)
    public double amount_self;
    @DisplayColumn(name_column = "остаток 1С",ClassTableCell = TableTestDouble.class)
    public double amount_server;
    @DisplayColumn(name_column = "цена",ClassTableCell = TableTestDouble.class)
    public double price;

    public static List<AmountSelfCore> getList() {
        List<AmountSelfCore> coreList = new ArrayList<>();
        List<MProduct> list = Configure.getSession().getList(MProduct.class, null);
        for (MProduct product : list) {

            if(product.amount_self==0&&product.getAmountSelf()!=0){
                AmountSelfCore core = new AmountSelfCore();
                core.amount_self = UtilsOmsk.round(product.getAmountSelf(),2);
                core.code_prod = product.code_prod;
                core.amount_server = product.amount_self;
                core.name = product.name;
                core.price = product.price;
                coreList.add(core);
            }else if(product.amount_self!=0){
                AmountSelfCore core = new AmountSelfCore();
                core.amount_self = UtilsOmsk.round(product.getAmountSelf(),2);
                core.code_prod = product.code_prod;
                core.amount_server = product.amount_self;
                core.name = product.name;
                core.price = product.price;
                coreList.add(core);
            }
        }
        Collections.sort(coreList, Comparator.comparing(o -> o.name));
        return coreList;
    }
}
