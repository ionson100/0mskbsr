package bitnic.pageamountselfcore;


import bitnic.core.Controller;
import bitnic.pageerror.PageError;
import bitnic.pagetableproduct.IFocusable;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.utils.UtilsOmsk;
import com.sun.javafx.scene.control.skin.TableViewSkin;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class PageSelfAmountCore extends GridPane implements Initializable, IRefreshLoader, IFocusable {


    private static final Logger log = Logger.getLogger(PageError.class);
    public HBox hbox;
    public TextField tf_search;
    public TableView<AmountSelfCore> table_view;
    private List<AmountSelfCore> list;
    public Button bt_top, bt_down;
    private ScrollBar verticalBar;
    private  final AtomicInteger i = new AtomicInteger(0);
    private ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
    private ScheduledExecutorService serviceapp = Executors.newSingleThreadScheduledExecutor();

    public PageSelfAmountCore() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_page_self_amount_core.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }




    @Override
    public void initialize(URL location, ResourceBundle resources) {


        refresh();

        tf_search.textProperty().addListener((observable, oldValue, newValue) -> {
            i.set(0);
            List<AmountSelfCore> cores = new ArrayList<>();
            for (AmountSelfCore core : list) {
                if (core.name.toLowerCase(Locale.ROOT).contains(newValue.toLowerCase(Locale.ROOT))) {
                    cores.add(core);
                    continue;

                }
                if (String.valueOf(core.price).contains(newValue)) {
                    cores.add(core);
                    continue;
                }

                if (core.code_prod.contains(newValue)) {
                    cores.add(core);
                    continue;
                }

            }
            table_view.getItems().clear();
            Controller.writeMessage(getTitle(cores));
            new BuilderTable().build(cores, table_view);
            serviceapp.schedule(new RunnerApp(), 2, TimeUnit.SECONDS);

        });



        bt_down.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {

            ScheduledFuture<?> h = null;

            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    h = service.scheduleAtFixedRate(new Runner(true), 0, UtilsOmsk.millsecButtonScroll, TimeUnit.MILLISECONDS);
                } else if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if(h!=null){
                        h.cancel(false);
                    }
                }
            }
        });


        bt_top.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {

            ScheduledFuture<?> h = null;

            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    h = service.scheduleAtFixedRate(new Runner(false), 0, UtilsOmsk.millsecButtonScroll, TimeUnit.MILLISECONDS);
                } else if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if(h!=null){
                        h.cancel(false);
                    }

                }
            }
        });


    }




    private String getTitle(List<AmountSelfCore> list) {
        double summ1 = 0d, summ2 = 0d;
        for (AmountSelfCore ac : list) {
            if (ac.amount_server == 0) continue;
            summ1 = summ1 + (ac.amount_server * ac.price);
            summ2 = summ2 + (ac.amount_self * ac.price);
        }
        return String.format("  На утро: %.2f Текущая: %.2f", summ1, summ2);
    }




    @Override
    public void refresh() {
        i.set(0);
        tf_search.setText("");
        if (SettingAppE.getInstance().isTypeЫShow3D) {
            hbox.getStyleClass().add("footer_3d");
        } else {
            hbox.getStyleClass().add("footer");
        }
        UtilsOmsk.painter3d(tf_search);
        list = AmountSelfCore.getList();
        new BuilderTable().build(list, table_view);


        Controller.writeMessage(getTitle(list));
        focus();
        serviceapp.schedule(new RunnerApp(), 2, TimeUnit.SECONDS);

    }

    @Override
    public void focus() {
        Platform.runLater(() -> tf_search.requestFocus());
    }




////////////////////

    void app() {

        if(verticalBar==null){
            verticalBar = (ScrollBar) table_view.lookup(".scroll-bar:vertical");
            if (verticalBar != null) {
                verticalBar.valueProperty().addListener((ObservableValue<? extends Number> obs, Number oldValue, Number newValue) -> {
                    VirtualFlow<?> vf = (VirtualFlow<?>) ((TableViewSkin<?>) table_view.getSkin()).getChildren().get(1);
                    if(vf!=null){
                        if(vf.getFirstVisibleCell()!=null){
                            i.set(vf.getFirstVisibleCell().getIndex());
                            System.out.println("event " + i);
                        }
                    }

                });
            }
        }



    }
    class Runner implements Runnable {

        private boolean isAdd;

        public Runner(boolean isAdd) {

            this.isAdd = isAdd;
        }

        @Override
        public void run() {
            if (isAdd) {
                if (i.get() < table_view.getItems().size())
                    i.incrementAndGet();
            } else {
                if (i.get() > 0)
                    i.decrementAndGet();
            }

            Platform.runLater(() -> {
                table_view.scrollTo(i.get());
            });

        }
    }

    class RunnerApp implements Runnable {

        @Override
        public void run() {

            app();
        }
    }
}

