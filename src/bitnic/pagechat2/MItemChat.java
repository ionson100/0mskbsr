package bitnic.pagechat2;

import bitnic.pagechat2.itemchat.FItemChat;

import java.util.Date;

public class MItemChat {
    public final Date date;
    public final String message;
    public boolean isSelf;
    public FItemChat gridPane;


    public MItemChat(Date date, String message,boolean isSelf){

        this.date = new Date(date.getTime());
        this.message = message;
        this.isSelf = isSelf;
    }
}
