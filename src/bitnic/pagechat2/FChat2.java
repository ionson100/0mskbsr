package bitnic.pagechat2;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import bitnic.pagechat2.addmessage.FAddMessage;
import bitnic.pagechat2.itemchat.FItemChat;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class FChat2 implements Initializable {
    private static final Logger log = Logger.getLogger ( FChat2.class );
    public ListView list_chat;
    public Button bt_add;
    public Button bt_close;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        bt_add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(FAddMessage.class.getResource("f_add_message.fxml"));
                Node pane = null;
                try {
                    pane = loader.load();
                } catch (IOException e) {
                    log.error ( e );
                    e.printStackTrace();
                }
                Controller.getInstans().stack_panel.getChildren().add(pane);
            }
        });


        bt_close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DialogBase.deleteBlenda();
            }
        });

        List<MItemChat> mItemChatList=new ArrayList<>();
        mItemChatList.add(new MItemChat(new Date(),"<a alt=\"Download Discord\" href=\"https://discordapp.com/api/download?platform=win\" class=\"download-btn\">Download Now</a>",true));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwu      " +
                " uuuuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuuuuuuuuuerwer",false));
        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwuuuuuuuuuuu\n " +
                "uuuuuuuuuuuuuuu\n" +
                " uuuuuuuuuuuuuerwer",true));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwu      " +
                " uuuuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuuuuuuuuuerwer",false));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwuuuuuuuuuuu\n " +
                "uuuuuuuuuuuuuuu\n" +
                " uuuuuuuuuuuuuerwer",true));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwu      " +
                " uuuuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuuuuuuuuuerwer",false));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwuuuuuuuuuuu\n " +
                "uuuuuuuuuuuuuuu\n" +
                " uuuuuuuuuuuuuerwer",true));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwu      " +
                " uuuuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuuuuuuuuuerwer",false));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwuuuuuuuuuuu\n " +
                "uuuuuuuuuuuuuuu\n" +
                " uuuuuuuuuuuuuerwer",true));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwu      " +
                " uuuuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuuuuuuuuuerwer",false));
        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwuuuuuuuuuuu\n " +
                "uuuuuuuuuuuuuuu\n" +
                " uuuuuuuuuuuuuerwer",true));

        mItemChatList.add(new MItemChat(new Date(),"orueworu ewriuiewuriwu      " +
                " uuuuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuu" +
                " uuuuuuuuuuuuuuuuuuuuuuerwer",false));

        mItemChatList.add(new MItemChat(new Date(),"This is a <b>Sample</b> sentence",true));

        mItemChatList.add(new MItemChat(new Date(),"Простой текст",false));










        List<MItemChat> mCheckList=new ArrayList<>(mItemChatList);

        ObservableList<MItemChat> data = FXCollections.observableArrayList(mCheckList);
        list_chat.setItems(data);
        list_chat.setCellFactory((Callback<ListView<MItemChat>, ListCell<MItemChat>>) list -> new ChatCellFactory()
        );




    }
    static class ChatCellFactory extends ListCell<MItemChat> {
        @Override
        public void updateItem(MItemChat item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemChat fItemCheck=new FItemChat(item);
                item.gridPane=fItemCheck;
                setGraphic(fItemCheck);
            }
        }
    }

}
