package bitnic.utils;

import bitnic.core.Controller;

import bitnic.model.MError;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import javafx.css.Styleable;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import com.google.gson.*;
import bitnic.pagetableproduct.IFocusable;
import bitnic.dialogfactory.DialogFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;


import java.io.*;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Stream;

public class UtilsOmsk {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UtilsOmsk.class);
    private static final String nodate = "Данные отсутствуют";
    public static final double deltaScrol=5;
    public static final int millsecButtonScroll=150;
    public static final int START_TIMER_BLENDER_VALUE =181;


    public static <T> T getChildByID(Parent parent, String id) {

        String nodeId = null;

        if (parent instanceof TitledPane) {
            TitledPane titledPane = (TitledPane) parent;
            Node content = titledPane.getContent();
            nodeId = content.idProperty().get();

            if (nodeId != null && nodeId.equals(id)) {
                return (T) content;
            }

            if (content instanceof Parent) {
                T child = getChildByID((Parent) content, id);

                if (child != null) {
                    return child;
                }
            }
        }

        for (Node node : parent.getChildrenUnmodifiable()) {
            nodeId = node.idProperty().get();
            if (nodeId != null && nodeId.equals(id)) {
                return (T) node;
            }

            if (node instanceof SplitPane) {
                SplitPane splitPane = (SplitPane) node;
                for (Node itemNode : splitPane.getItems()) {
                    nodeId = itemNode.idProperty().get();

                    if (nodeId != null && nodeId.equals(id)) {
                        return (T) itemNode;
                    }

                    if (itemNode instanceof Parent) {
                        T child = getChildByID((Parent) itemNode, id);

                        if (child != null) {
                            return child;
                        }
                    }
                }
            } else if (node instanceof Accordion) {
                Accordion accordion = (Accordion) node;
                for (TitledPane titledPane : accordion.getPanes()) {
                    nodeId = titledPane.idProperty().get();

                    if (nodeId != null && nodeId.equals(id)) {
                        return (T) titledPane;
                    }

                    T child = getChildByID(titledPane, id);

                    if (child != null) {
                        return child;
                    }
                }
            } else if (node instanceof Parent) {
                T child = getChildByID((Parent) node, id);

                if (child != null) {
                    return child;
                }
            }
        }
        return null;
    }

    public static void openKeyBoard() {
        if(SystemUtils.IS_OS_LINUX){
            try{
                Runtime.getRuntime().exec("onboard");
            }catch (Exception ex){
                log.error ( ex );
                ex.printStackTrace();
            }
           if(Controller.curnode instanceof IFocusable){
               ((IFocusable) Controller.curnode).focus();
           }
        }
    }

    public static String getKey2() {

        final String spath = "C:\\Windows\\storma";

        String res = null;
        File f = new File(spath);
        if (f.exists() == false) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                log.error ( e );
                e.printStackTrace();
                DialogFactory.errorDialog(e);
            }

            StringBuilder sb = new StringBuilder();
            res = UUID.randomUUID().toString();
            sb.append(res).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString());
            try {
                writeToFile(spath, sb.toString());
            } catch (IOException e) {
                log.error ( e );
                e.printStackTrace();
            }

            return res;
        }
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(spath), Charset.forName("utf8"));
        } catch (IOException e) {
            log.error ( e );
            e.printStackTrace();
            DialogFactory.errorDialog(e);
        }
        if (lines.size() == 0) {
            return "";
        } else {
            return lines.get(0);
        }
    }

    public static String getMyKey() {

        final String spath = "C:\\Windows\\stormaticus";
        File field = new File(spath);
        if (field.exists() == false) {
            return "";
        }
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(spath), Charset.forName("utf8"));
        } catch (IOException e) {
            log.error ( e );
            e.printStackTrace();
            DialogFactory.errorDialog(e);
        }
        if (lines.size() == 0) {
            return "";
        } else {
            return lines.get(0);
        }
    }

    public static void writeToFile(String patch, String content) throws IOException {


        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            fw = new FileWriter(patch);
            bw = new BufferedWriter(fw);
            bw.write(content);
            bw.flush ();
            fw.flush ();

        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                log.error ( ex );
                DialogFactory.errorDialog(ex);
                ex.printStackTrace();
            }

        }
    }

    public static Gson getGson() {

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (json, typeOfT, context) -> {

            String ss = json.getAsJsonPrimitive().getAsString();
            if (ss.equals("null")) {
                return null;
            }else {
                long dd = json.getAsJsonPrimitive().getAsLong();
                return new Date(dd);
            }
        });

        builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {

                if (date == null) {
                    return new JsonPrimitive("null");
                }
                int dd = (int) (date.getTime() / 1000);
                return new JsonPrimitive(dd);
            }
        });

        return builder.serializeNulls().create();
    }

    public static String simpleDateFormatE(Date date) {

        if (date == null) {
            return nodate;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return dateFormat.format(date);
    }

    public static String simpleDateFormatforFileNameStory(Date date) {

        if (date == null) {
            return nodate;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        return dateFormat.format(date);
    }


    public static String simpleDateFormatForCommetsE(Date date) {
        if (date == null) {
            return nodate;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }



    public static Date curDate() {
        return Calendar.getInstance().getTime();
    }





    public static void resizeNode(GridPane thise, GridPane inject) {

        thise.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) -> {
            System.out.println("Width: " + newSceneWidth);
            inject.setPrefWidth((Double) newSceneWidth);
            //Здесь мы можем отправить к любому нашему объекту новое значение Width
        });


        thise.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue observableValue, Number oldSceneHeight, Number newSceneHeight) {
                System.out.println("Height: " + newSceneHeight);
                inject.setPrefHeight((Double) newSceneHeight);
                //Здесь мы можем отправить к любому нашему объекту новое значение Height
            }
        });

    }







  








    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }



    public static String readFile(String filepatch) {

            StringBuilder contentBuilder = new StringBuilder();

            try (Stream<String> stream = Files.lines( Paths.get(filepatch), StandardCharsets.UTF_8))
            {
                stream.forEach(s -> contentBuilder.append(s).append("\n"));
                return contentBuilder.toString();
            }
            catch (IOException e)
            {
                log.error ( e );
                return null;
            }



    }

    public static void rewriteFile(String fileName, String s) throws Exception {
        File myFoo = new File(fileName);
        try(FileOutputStream fooStream = new FileOutputStream(myFoo, false)){ // true to append
            byte[] myBytes = s.getBytes();
            fooStream.write(myBytes);
        }


    }



    public static void painter3d(Styleable ... nodes){
        for (Object node : nodes) {
            if(SettingAppE.getInstance().isTypeЫShow3D){
                if(node instanceof TextField){
                    ((TextField)node).getStyleClass().add("delivery_3d");
                }
                if(node instanceof Button){
                    if(node==null )continue;
                    if(((Button) node).idProperty ().getValue ().equals ( "bt_open_board" )){
                        ((Button)node).getStyleClass().add("button_open_board_3d");
                    }else {
                        ((Button)node).getStyleClass().add("button_dialog_3d");
                    }
                }
            }else {
                if(node instanceof TextField){
                    ((TextField)node).getStyleClass().add("delivery");
                }
                if(node instanceof Button){

                    if(((Button) node).idProperty ().getValue ().equals ( "bt_open_board" )){
                        ((Button)node).getStyleClass().add("button_open_board");
                    }else {
                        ((Button)node).getStyleClass().add("button_dialog");
                    }

                }
            }

        }
    }


    public static Object getStringTimeForError(Date date) {
        //2018-03-14 09:12:46

        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);

    }

    public static void searchErrorMessage(String newValue, List<MError> list, TableView table_error) {
        List <MError> mErrors = new ArrayList<>();

        for (MError mError : list) {
            if ( mError.msg.toUpperCase ().contains ( newValue.toUpperCase () ) ) {
                mErrors.add ( mError );
            }

        }
        if(mErrors.size()==0){
            mErrors.add ( new MError () );
        }
        System.out.println ( mErrors.size () );
        new BuilderTable ().build( mErrors , table_error );
    }
    public static void createTarGZFolder(String from, String to)
            throws IOException {
        FileOutputStream fOut = null;
        BufferedOutputStream bOut = null;
        GzipCompressorOutputStream gzOut = null;
        TarArchiveOutputStream tOut = null;


        try {
            fOut = new FileOutputStream(new File(to));
            bOut = new BufferedOutputStream(fOut);
            gzOut = new GzipCompressorOutputStream(bOut);
            tOut = new TarArchiveOutputStream(gzOut);
            addFileToTarGz(tOut, from, "");
        } finally {

            if(tOut!=null){
                tOut.finish();
                tOut.close();
            }

            if(gzOut!=null){
                gzOut.close();
            }

            if(bOut!=null){
                bOut.close();
            }

            if(fOut!=null){
                fOut.close();
            }

        }
    }

    private static void addFileToTarGz(TarArchiveOutputStream tOut, String path, String base)
            throws IOException {
        File f = new File(path);
        System.out.println(f.exists());
        String entryName = base + f.getName();
        TarArchiveEntry tarEntry = new TarArchiveEntry(f, entryName);
        tOut.putArchiveEntry(tarEntry);

        if (f.isFile()) {
            IOUtils.copy(new FileInputStream(f), tOut);
            tOut.closeArchiveEntry();
        } else {
            tOut.closeArchiveEntry();
            File[] children = f.listFiles();
            if (children != null) {
                for (File child : children) {
                    System.out.println(child.getName());
                    addFileToTarGz(tOut, child.getAbsolutePath(), entryName + "/");
                }
            }
        }
    }


//    public static void copyFolder(File src, File dest)
//            throws IOException {
//
//        if (src.isDirectory()) {
//
//            //if directory not exists, create it
//            if (!dest.exists()) {
//                dest.mkdir();
//                System.out.println("Directory copied from "
//                        + src + "  to " + dest);
//            }
//
//            //list all the directory contents
//            String files[] = src.list();
//
//            if(files!=null){
//                for (String file : files) {
//                    //construct the src and dest file structure
//                    File srcFile = new File(src, file);
//                    File destFile = new File(dest, file);
//                    //recursive copy
//                    copyFolder(srcFile, destFile);
//                }
//
//            }
//
//        } else {
//           // System.out.println("File copied from " + src + " to " + dest);
//            InputStream in = null;
//            OutputStream out = null;
//            try {
//                in = new FileInputStream(src);
//                out = new FileOutputStream(dest);
//
//                byte[] buffer = new byte[1024];
//
//                int length;
//                //copy the file content in bytes
//                while ((length = in.read(buffer)) > 0) {
//                    out.write(buffer, 0, length);
//                }
//
//                in.close();
//                out.close();
//            } catch (Exception ex) {
//                log.error(ex);
//            } finally {
//
//                if(in!=null){
//                    in.close();
//                }
//                if(out!=null){
//                    out.close();
//                }
//            }
//
//           // System.out.println("File copied from " + src + " to " + dest);
//        }
//    }



    public static Date atStartOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date) {
        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return localDateTimeToDate(endOfDay);
    }
    private static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}

