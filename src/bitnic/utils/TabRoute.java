package bitnic.utils;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TabRoute {

    // интерфей, при помощинего можем передать поведение для контрола при нажиме ентер,если контрол находится в фокусе
    public interface IAction{
        void action(Node node);
    }


    //boolean run = true;
    int focusIndexItem = 0;
    List<Node> nodeList = new ArrayList<>();
    Map<Node,IAction> iActionMap=new HashMap<>();
    Node currnode;


    public void addNode(Node node) {

        if(node==null) return;
        nodeList.add(node);
        node.setOnKeyPressed(event -> {
            // отлавливаем клик таб
            if (event.getCode() == KeyCode.TAB) {
                if (TabRoute.this.focusIndexItem < TabRoute.this.nodeList.size() - 1) {

                    // пропускаем скрытые и не активне контролы
                    for (int i = TabRoute.this.focusIndexItem + 1; i < TabRoute.this.nodeList.size(); i++) {
                        currnode = TabRoute.this.nodeList.get(i);
                        if (currnode.isVisible() == true && currnode.isDisabled() == false) {
                            focusIndexItem = i;
                            System.out.println(i);
                            Platform.runLater(() -> currnode.requestFocus());
                            break;
                        }
                    }
                } else {
                    // закольцовываем переходы если подошли к концу списка
                    focusIndexItem = 0;
                    Platform.runLater(() -> TabRoute.this.nodeList.get(TabRoute.this.focusIndexItem).requestFocus());
                }
            }
            // если это кнопка то по ENTER нажимаем
            if (event.getCode() == KeyCode.ENTER ) {
                if(event.getSource() instanceof Button)
                    ((Button) event.getSource()).fire();
                if(currnode!=null&&iActionMap.containsKey(currnode)){
                    iActionMap.get(currnode).action(currnode);
                }
            }


        });
        int e = nodeList.size();
        // если контрол в списке помечен как принимающий фокус, начинаем путешествовать с него, срабатывает один раз
        node.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
           // if (run) {
                if (newPropertyValue) {
                    TabRoute.this.focusIndexItem = e - 1;
                    currnode=node;
                    //run = false;
                }
           // }
        });
        // если мышкой встали на контрол, перемещаем фокус в него. в дальнейшем путешествуем  отсюда
        node.setOnMouseClicked(event -> {
            TabRoute.this.focusIndexItem = e - 1;
            currnode=node;
        });
    }


    public void addNode(Node  node,IAction iAction){
        if(node==null) return;
        if(iAction!=null){
            iActionMap.put(node,iAction);
        }
        addNode(node);
    }
}
