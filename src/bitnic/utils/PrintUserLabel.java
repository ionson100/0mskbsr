package bitnic.utils;

import bitnic.barcodegenerate.BarcodePrint;
import bitnic.model.MUser;
import bitnic.orm.Configure;
import bitnic.settingscore.SettingAppE;

import java.util.List;

public class PrintUserLabel {
    public static void print() {
        List<MUser> mUsers = Configure.getSession().getList(MUser.class, " cod = ?", SettingAppE.getInstance().user_id);
        if (mUsers.size() > 0) {
            MUser user = mUsers.get(0);
            if (user.map_user != null && user.map_user.length() > 0) {
                new BarcodePrint().print(user.map_user);
            }
        }
    }
}
