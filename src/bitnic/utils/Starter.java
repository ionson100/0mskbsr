package bitnic.utils;


import bitnic.core.Main;
import bitnic.dialogfactory.DialogFactory;
import bitnic.orm.Configure;
import javafx.application.Platform;
import org.apache.log4j.Logger;

import javax.net.ssl.*;
import java.io.File;
//import java.io.FileOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.security.cert.X509Certificate;
import java.util.UUID;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class Starter {


    private StringBuilder sb = new StringBuilder();
    private static final Logger log = Logger.getLogger(Starter.class);


    public Starter() {

        System.out.println(System.getProperty("java.library.path"));
    }

    public void start() {

        disableSslVerification();


        {
            File f = new File(Pather.appSctiptUpdate);
            if (f.exists() == false) {
                try {
                    if (f.createNewFile()) {
                        UtilsOmsk.writeToFile(Pather.appSctiptUpdate, "#!/bin/sh"
                                + System.lineSeparator() + "kill $1"
                                + System.lineSeparator() + "tar -xvzf $2 -C  $3"
                                + System.lineSeparator() + "shutdown -h now");
                    }

                } catch (IOException e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }
        }


        {

            String path = Pather.settingsFolder;
            File f = new File(path);
            if (f.exists()) {
            } else {
                if (f.mkdir()) {
                    sb.append(path + " settings - create").append(System.lineSeparator());
                } else {
                    sb.append(path + " Не могу созать").append(System.lineSeparator());

                }
            }


        }


        boolean isrunung = isFileshipAlreadyRunning();// если false то запущена вторая программа и мы закрываем приложение
        if (isrunung == false) {
            Platform.exit();
        }

        {
            File f = new File(Pather.pointid);
            if (f.exists() == false) {
                try {
                    if (f.createNewFile()) {
                        UtilsOmsk.writeToFile(Pather.pointid, "0");
                    }

                } catch (IOException e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }
        }

//        {
//            File f = new File(Pather.sender_report_file);
//            if (f.exists() == false) {
//                try {
//                    f.createNewFile();
//                } catch (IOException e) {
//                    sb.append(e.getMessage()).append(System.lineSeparator());
//
//                }
//            }
//        }

        {
            File f = new File(Pather.senderStateFile);
            if (f.exists() == false) {
                try {
                    f.createNewFile();
                    UtilsOmsk.writeToFile(Pather.senderStateFile, "0" + System.lineSeparator() + "0");
                } catch (IOException e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }
        }

        {
            File f = new File(Pather.showButtonUpdate);
            if (f.exists() == false) {
                try {
                    f.createNewFile();
                    UtilsOmsk.writeToFile(Pather.showButtonUpdate, "0");
                } catch (IOException e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }
        }


        {

            File f = new File(Pather.ftpSettings);
            if (f.exists() == false) {
                try {
                    f.createNewFile();

                } catch (IOException e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }

        }

        {

            File f = new File(Pather.patchprofile);
            if (f.exists() == false) {
                try {
                    f.createNewFile();
                    UtilsOmsk.writeToFile(Pather.patchprofile, "0");
                } catch (IOException e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }

        }

        {

            File f = new File(Pather.pathAcquire);
            if (f.exists() == false) {
                try {
                    f.createNewFile();
                    UtilsOmsk.writeToFile(Pather.patchprofile, "0");
                } catch (IOException e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }

        }

        {
            File file = new File(Pather.patchUrlFile);
            if (file.exists() == false) {
                try {
                    file.createNewFile();
                    String urls = "91.144.170.205\nbsr000.net\n178.62.249.118\n138.197.161.95";
                    UtilsOmsk.writeToFile(Pather.patchUrlFile, urls);

                } catch (Exception e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }
        }
        {
            File file = new File(Pather.uuidFile);
            if (file.exists() == false) {
                try {
                    file.createNewFile();
                    UtilsOmsk.writeToFile(Pather.uuidFile, UUID.randomUUID().toString());
                } catch (Exception e) {
                    sb.append(e.getMessage()).append(System.lineSeparator());
                }
            }
        }

        if (sb.length() > 0) {
            log.error(sb.toString());
        }


        new Configure(Pather.base_sqlite);


        File ff = new File(Pather.directoryBuilder2);
        if (ff.exists()) {
            File[] s = ff.listFiles();
            if (s != null) {
                for (File myFile : s)
                    if (myFile.isFile()) {
                        if (myFile.delete() == false) {
                            log.info("not delete file");
                        }
                    }
            }

        }
        if (DesktopApi.getOs() == DesktopApi.EnumOS.linux) {
            String d = Pather.curdir + "/reload.sh";
            File file = new File(d);
            if (file.exists() == false) {
                try {
                    if (file.createNewFile()) {
                        UtilsOmsk.writeToFile(d, "#!/bin/sh\n" +
                                "kill $1\n" +
                                "java -jar /home/bsr/omsk/omsk.jar");
                    }
                } catch (Exception e) {
                    log.error(e);
                }
            }
        }


        {
            File file = new File(Pather.versionFolder);
            if (file.exists() == false) {
                try {
                    file.mkdir();
                } catch (Exception e) {
                    log.error(e);
                    e.printStackTrace();
                }
            }


            if (file.exists()) {
                try {
                    File file1 = new File(Pather.versionFolder + File.separator + "version.txt");
                    if (file1.exists() == false) {
                        file1.createNewFile();
                    }
                    String version = getStringVersion();

                    System.out.println("Implementation-Version: " + version);
                    try (FileOutputStream fooStream = new FileOutputStream(file1, false)) {
                        byte[] myBytes = version.getBytes();
                        fooStream.write(myBytes);
                        fooStream.close();
                    }


                } catch (Exception ex) {
                    log.error(ex);
                    ex.printStackTrace();
                }
            }
        }
        {
            File file = new File(Pather.checkStory);
            if (file.exists() == false) {
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    log.error(e);
                    e.printStackTrace();
                }
            }
        }


        // new CheckArchiveE().init();
        // new AmountSelf ().init ();
        // new Requests().init ();
        // new MessageBody().init();


    }

    public static String getStringVersion() throws IOException {
        String classPath = Main.class.getResource(Main.class.getSimpleName() + ".class").toString();
        String libPath = classPath.substring(0, classPath.lastIndexOf("bitnic/core"));
        String filePath = libPath + "META-INF/MANIFEST.MF";
        System.out.println("File:  " + filePath);
        Manifest manifest = null;
        manifest = new Manifest(new URL(filePath).openStream());
        Attributes attr = manifest.getMainAttributes();
        return attr.getValue("Implementation-Version");
    }

    private static boolean isFileshipAlreadyRunning() {

        try {
            File file = new File(Pather.fileShipReserved);
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            FileLock fileLock = randomAccessFile.getChannel().tryLock();
            if (fileLock != null) {
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    public void run() {
                        try {
                            fileLock.release();
                            randomAccessFile.close();
                            if (file.delete() == false) {
                                log.info("not delete file " + file.getPath());
                            }
                        } catch (Exception e) {
                            DialogFactory.errorDialog(e);
                            log.error("Unable to remove lock file: " + Pather.fileShipReserved);
                        }
                    }
                });
                return true;
            }
        } catch (Exception e) {

            log.error("Запуск второго экземпляра программы  lock file: " + Pather.fileShipReserved);
        }
        return false;
    }

    // добавление путей для java
    public static void addDir(String s) throws IOException {
        try {
            Field field = ClassLoader.class.getDeclaredField("usr_paths");
            field.setAccessible(true);
            String[] paths = (String[]) field.get(null);
            for (int i = 0; i < paths.length; i++) {
                if (s.toUpperCase().equals(paths[i].toUpperCase())) {
                    return;
                }
            }
            String[] tmp = new String[paths.length + 1];
            System.arraycopy(paths, 0, tmp, 0, paths.length);
            tmp[paths.length] = s;
            field.set(null, tmp);
            String sd = System.getProperty("java.library.path") + File.pathSeparator + s;
            System.setProperty("java.library.path", sd);
            log.info(sd + " dll загружена");
        } catch (IllegalAccessException e) {
            log.info(s + " проблемы заргузки ddl Failed to get permissions to set library path");
            throw new IOException("Failed to get permissions to set library path");
        } catch (NoSuchFieldException e) {
            log.info(s + " проблемы заргузки ddl Failed to get field handle to set library path");
            throw new IOException("Failed to get field handle to set library path");
        }
    }


    private static void disableSslVerification() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            DialogFactory.errorDialog(e);
        }
    }


}
