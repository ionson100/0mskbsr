package bitnic.utils;

import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.orm.Configure;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GetDoubleGoods {
    public void getDoubleString(){
        List<MProduct> mProducts=new ArrayList<>();

        List<MProduct> mProductList = Configure.getSession().getList(MProduct.class,null);
        Set<String> set=new HashSet<>(mProductList.size());
        for (MProduct mProduct : mProductList) {
            if(set.contains(mProduct.code_prod)){
                mProducts.add(mProduct);
            }else {
                set.add(mProduct.code_prod);
            }
        }
        if(mProducts.size()==0){
            DialogFactory.infoDialog("Проверка дублирования продукта","Не найдено");
        }else {
            StringBuilder stringBuilder=new StringBuilder();
            for (MProduct mProduct : mProducts) {
               stringBuilder.append(String.format("Код: %s  Остаток: %-5s    Продукт: %s  ",
                       mProduct.code_prod,
                       mProduct.amount_self,
                       mProduct.name));
               stringBuilder.append(System.lineSeparator());
            }
            DialogFactory.errorDialog(stringBuilder.toString());

        }
    }
}
