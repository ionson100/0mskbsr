package bitnic.utils;

import bitnic.sendertoemail.SenderToEmail;
import org.apache.log4j.Logger;

import java.io.File;

public class Pather {

    private static final Logger log = Logger.getLogger(Pather.class);
    public static final String curdir;
    public static final String curdirdata;
    //public final static String basenameArchive = "checkarchive";


    static {
        if ( DesktopApi.getOs ().isLinux () ) {
            curdir = System.getProperty ( "user.home" );


        } else {
            curdir = System.getProperty ( "user.dir" );

        }
        curdirdata = curdir + File.separator + "omskdata";
        File file = new File ( curdirdata );
        if ( !file.exists () ) {
            if(file.mkdir ()==false){
                log.info("not mkdir "+curdirdata);
            }
        }
//        String absolute = Main.class.getProtectionDomain().getCodeSource().getLocation().toExternalForm();
//        System.out.println("absolute path "+absolute);
    }
    public static final String senderStateFile=curdir + File.separator + ".senderstate.txt";
    public static final String uuidFile=curdir + File.separator + ".uuidomsk.txt";

    public static final String appSctiptUpdate = curdir + File.separator + "appupdate.sh";
    public static final String settingsFolder = curdir + File.separator + "settings";
    public static final String directoryBuilder = "assa";
    public static final String directoryBuilder2 = "assa2";
    public static final String base_sqlite = settingsFolder + File.separator + "omsk_22.sqlite";
    public static final String base_sqlite_settings = settingsFolder + File.separator + "omsk242.sqlite";
    public static final String dllDirectory = curdir + File.separator + "dllnative";
    //public static final String sender_report_file = settingsFolder + File.separator + "sender_report.txt";
    public static final String barcodeImage = settingsFolder + File.separator + "barcode.jpeg";
    public static final String inFilesData = settingsFolder + File.separator + "goods.txt";
    // для провеки запуска второго приложения

    public static final String fileShipReserved = settingsFolder + File.separator + "FileshipReserved.txt";
    public static final String reportstory = curdir + File.separator + "reportstory";
    //    public static  String numberSession=settingsFolder+File.separator+"number_session.txt";
    public static final String versionFolder = curdirdata + File.separator + "version";
    public static final String ftpSettings = curdirdata + File.separator + ".ftp";
    public static final String checkStory = curdir + File.separator + "storycheck.txt";
    public static final String pointid = curdirdata + File.separator + "pointid.txt";
    public static final String patchprofile = curdirdata + File.separator + "profile.txt";
    public static final String pathAcquire = curdirdata + File.separator + "acquire.txt";
    public static final String patchUrlFile = curdir + File.separator + ".url.txt";
    public static final String showButtonUpdate = curdirdata + File.separator + ".showbutton";



}