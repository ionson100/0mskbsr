package bitnic.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static bitnic.utils.UtilsOmsk.writeToFile;

public class UtilsSettings {
    private static Logger log = Logger.getLogger(UtilsSettings.class);
    static Object lock=new Object();
    public static <T> Object deserializerE(Class<T> aClass) throws IOException, IllegalAccessException, InstantiationException {


        synchronized (lock){

            T res=aClass.newInstance();

            String filename=((IFileStorage)res).getFileName();
            File file = new File(filename);
            List<Class> classes=new ArrayList<>();
            recursion(classes,aClass);
            if (file.exists()) {
                BufferedReader br = null;
                FileReader fr = null;
                try {
                    //br = new BufferedReader(new FileReader(file));
                    fr = new FileReader(file);
                    br = new BufferedReader(fr);
                    StringBuilder sb=new StringBuilder();
                    String sCurrentLine;
                    while ((sCurrentLine = br.readLine()) != null) {
                        sb.append(sCurrentLine);
                    }
                    Gson gson=UtilsOmsk.getGson();
                    if(classes.size()>0){
                        GsonBuilder s = new GsonBuilder();
                        for (Class ss : classes) {
                            try {
                                s.setExclusionStrategies((ExclusionStrategy) ss.newInstance());
                            } catch (Exception e) {
                                log.error ( e );
                                e.printStackTrace();
                            }
                        }
                        gson=s.create();
                    }
                    res=gson.fromJson(sb.toString(), (Class<T>) res.getClass());
                } finally {
                    if (br != null)
                        br.close();
                    if (fr != null)
                        fr.close();
                }
            } else {
                if(file.createNewFile()){
                    serializerE(res,null);
                }else {
                    log.info(" not create file "+file.getPath());
                }

            }

            for (Class aClass1 : classes) {
                Object ss=FactorySettings.getSettingsE(aClass1);
                recursionType(ss,res,res.getClass().getSuperclass());
            }
            return res;
        }
    }

    private static  <T> void recursionType(T o,Object res,Class clas){
        if(o.getClass()==clas){
            Field[]f=o.getClass().getDeclaredFields();
            for (Field field : f) {
                try {
                    if(field.getName().equals("instance"))continue;
                    if(Modifier.isStatic(field.getModifiers())){
                        continue;
                    }
                    Field ff = clas.getDeclaredField(field.getName()); //NoSuchFieldException
                    ff.setAccessible(true);
                    field.setAccessible(true);
                    Object value=field.get(o);
                    ff.set(res,value);
                } catch (Exception e) {
                    log.error ( e );
                    e.printStackTrace();
                }
            }
        }else {
            recursionType(o,res,clas.getSuperclass());
        }
    }

    public static <T> void serializerE(T t,Class<T>  type) throws IOException {

        synchronized (lock){
            String filename ="";
            if(type==null){
                filename=((IFileStorage)t).getFileName();
            }else {
                try {
                    filename=((IFileStorage)type.newInstance()).getFileName();
                } catch (Exception e) {
                    log.error ( e );
                    e.printStackTrace();
                }
            }
            List<Class> classes=new ArrayList<>();
            if(type==null){
                recursion(classes,t.getClass());
            }else {
                recursion(classes,type);
            }
            Gson gson=UtilsOmsk.getGson();
            if(classes.size()>0){
                GsonBuilder s = new GsonBuilder();
                for (Class aClass : classes) {
                    try {
                        s.setExclusionStrategies((ExclusionStrategy) aClass.newInstance());
                    } catch (Exception e) {
                        log.error ( e );
                        e.printStackTrace();
                    }
                }
               gson=s.create();
            }
            String res="";
            if(type==null){
                res= gson.toJson(t);
            }else {
                res= gson.toJson(t,type);
            }
            writeToFile(filename,res);
        }
    }

    public   static void recursion(List<Class> classes, Class aClass){
        Class c=aClass.getSuperclass();
        if(c!=null){
            List<Class> sd=new ArrayList<Class>(Arrays.asList(c.getInterfaces()));
            if(sd.contains(ExclusionStrategy.class)){
                classes.add(c);
                recursion(classes,c);
            }
        }
    }
}
