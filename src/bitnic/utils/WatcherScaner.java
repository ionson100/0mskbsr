package bitnic.utils;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MBarcode;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.pagesale.ICleatTextField;
import bitnic.pagesale.stacksearcher.FStackSearcher;
import bitnic.pagetableproduct.Finder;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import javafx.application.Platform;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.util.*;

class WrapperFilter {

    public WrapperFilter(Field field, Finder filter) {

        this.field = field;
        this.filter = filter;
    }



    public Field field;
    public Finder filter;
}

public class WatcherScaner {
    static List<WrapperFilter> wrapperFilters = null;
    private static final Logger log = Logger.getLogger(WatcherScaner.class);

    private List<MProduct> curlist;
    private StackPane stackPane;
    private TextField textField;
    private FStackSearcher searcher;
    private IAction addIAction;

    public void refreshList() {
        curlist = Configure.getSession().getList(MProduct.class, " isGroup <> 0 ");
    }

    public WatcherScaner(StackPane stackPane, IAction addIAction) {
        this.addIAction = addIAction;

        curlist = Configure.getSession().getList(MProduct.class, " isGroup <> 0 ");
        Collections.sort(curlist, Comparator.comparing(o -> o.name));
        this.stackPane = stackPane;
        if (wrapperFilters == null) {
            wrapperFilters = new ArrayList<>();
            for (Field field : MProduct.class.getFields()) {

                Finder f = field.getAnnotation(Finder.class);
                if (f != null) {

                    if (field.getName().contains("name")) {
                        if (SettingAppE.getInstance().isSearchProductName) {
                            wrapperFilters.add(new WrapperFilter(field, f));
                        }
                    } else {
                        wrapperFilters.add(new WrapperFilter(field, f));
                    }

                }
            }
        }

    }

    public void clear() {
        textField.setText("");

    }

    private void close() {
        if (stackPane.getChildren().size() > 1)
            stackPane.getChildren().remove(1);
        searcher = null;
        Controller.writeMessage(Support.str("name310"));
    }

    long first = 0, stop = 0;
    private boolean isScaner = true;

    public void run(TextField textField) {
        this.textField = textField;

        textField.textProperty().addListener((observable, oldValue, newValue) -> {

            if (textField.getText().trim().length() > 0) {
                close();

                if (textField.getText().trim().length() == 1) {
                    first = System.nanoTime();
                    isScaner = false;
                }
                if (textField.getText().trim().length() == 2) {
                    stop = System.nanoTime();
                    isScaner = (stop - first) < SettingAppE.getInstance().scanerDelau;
                    //log.info(stop - first);
                }

                if (isScaner) return;
                //startsWith(String prefix, int toffset) (String prefix, int toffset)


                if (textField.getText().trim().length() == 1) {
                    if (NumberUtils.isNumber(textField.getText().trim())) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(SettingAppE.getInstance().sleepScaner);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                if (isScaner == false) {
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (textField.getText().trim().length() == 1) {
                                                addin(textField, newValue);
                                            }

                                        }
                                    });
                                }


                            }
                        }).start();
                    } else {
                        addin(textField, newValue);
                    }

                } else {
                    addin(textField, newValue);
                }


            } else {
                isScaner = true;
                close();
            }
        });

        textField.setOnKeyPressed(event -> {
            String text = event.getText();
            if (text.equals("\r") && textField.getText().length() > 6) {
                addProduct(textField.getText());
            }
        });
    }

    private void addin(TextField textField, String newValue) {
        searcher = new FStackSearcher(addIAction);
        searcher.setPrefHeight(stackPane.getHeight());
        searcher.setPrefWidth(stackPane.getWidth());
        stackPane.getChildren().add(searcher);
        List<MProduct> productList = new ArrayList<>();
        if (textField.getText().trim().length() >= 6 && isNimberAll(textField.getText().trim())) {
            getListProductBarcodesContext(textField.getText(), productList, curlist);
        } else {
            for (MProduct mProduct : curlist) {
                for (WrapperFilter wrapper : wrapperFilters) {
                    String f = null;
                    try {
                        f = wrapper.field.get(mProduct).toString();
                    } catch (IllegalAccessException e) {
                        log.error(e);
                        log.error(wrapper.field.getName());
                        continue;
                    }
                    if (wrapper.filter.useStartsWith()) {
                        if (f.toUpperCase().startsWith(newValue.toUpperCase())) {
                            productList.add(mProduct);
                        }
                    } else {
                        if (f.toLowerCase(Locale.ROOT).indexOf(newValue.toLowerCase(Locale.ROOT)) != -1) {
                            productList.add(mProduct);
                        }
                    }

                }
            }
        }
        if (searcher != null) {
            Collections.sort(productList, new Comparator<MProduct>() {
                @Override
                public int compare(MProduct o1, MProduct o2) {
                    return Double.compare(o1.price, o2.price);
                }
            });
            searcher.setSource(productList);
        }
    }


    private void getListProductBarcodesContext(String text, List<MProduct> list, List<MProduct> total) {
        List<MBarcode> mBarcodes = Configure.getSession().getList(MBarcode.class, " barcode LIKE '%" + text + "%'");
        for (MBarcode mBarcode : mBarcodes) {
            if (SettingAppE.getInstance().isCheckFinishProduct) {
                if (list.size() > 50) {
                    break;
                }
            }
            for (MProduct mProduct : total) {
                if (mProduct.code_prod.equals(mBarcode.id_product)) {
                    list.add(mProduct);
                }
            }
        }

    }

    private boolean isNimberAll(String trim) {
        for (char c : trim.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public void addProduct(String text) {


        StackPane node = Controller.getInstans().stack_panel;
        if (node.getChildren().size() > 1) {
            return;
        }
        if (Controller.ccmainform == null) {
            return;
        }
        List<MBarcode> bargodes = Configure.getSession().getList(MBarcode.class, " barcode = ? ", text.trim());
        if (bargodes.size() == 0) {
            DialogFactory.errorDialog(Support.str("name311") + text, new IAction() {
                @Override
                public boolean action(Object o) {
                    if (Controller.curnode instanceof ICleatTextField) {
                        ((ICleatTextField) Controller.curnode).clearTextField();
                    }
                    return false;
                }
            });
            return;
        }

        List<MProduct> mProducts = new ArrayList<>();
        for (MBarcode barcode : bargodes) {
            mProducts.addAll(Configure.getSession().getList(MProduct.class, " id_core = ? ", barcode.id_product));
        }
       // MBarcode b = bargodes.get(0);



        if (mProducts.size() > 1) {//задвоение штрих кода
            DialogFactory.doubleBarcodeDialog(null, null);
            searcher = new FStackSearcher(addIAction);
            searcher.setPrefHeight(stackPane.getHeight());
            searcher.setPrefWidth(stackPane.getWidth());
            stackPane.getChildren().add(searcher);
            List<MProduct> productList = mProducts;
            Collections.sort(productList, Comparator.comparingDouble(o -> o.price));
            searcher.setSource(productList);
            return;
        } else if (mProducts.size() == 0) {
            DialogFactory.errorDialog("Продукт с кодом:" + text + " не найден!", o -> {
                if (Controller.curnode instanceof ICleatTextField) {
                    ((ICleatTextField) Controller.curnode).clearTextField();
                }
                return false;
            });
            return;
        } else {
            MProduct pr = mProducts.get(0);
            if (pr.price == 0d) {
                DialogFactory.errorDialog("Продукт : " + pr.name_check + " имеет нулевую цену, исключен из продажи !! ", new IAction() {
                    @Override
                    public boolean action(Object o) {
                        if (Controller.curnode instanceof ICleatTextField) {
                            ((ICleatTextField) Controller.curnode).clearTextField();
                        }
                        return false;
                    }
                });
                return;
            }
            if(addIAction!=null){
                addIAction.action(pr);
            }
//            boolean ss=SettingAppE.getInstance().selectProduct.size()>0;
//            boolean es = false;
//            for (MProduct mProduct : SettingAppE.getInstance().selectProduct) {
//                if (mProduct.code_prod.equals(pr.code_prod)) {
//                    mProduct.selectAmount = mProduct.selectAmount + 1;
//                    es = true;
//                }
//            }
//            if (es == false) {
//                SettingAppE.getInstance().selectProduct.add(pr);
//                pr.selectAmount = 1;
//            }
//            if(SettingAppE.getInstance().selectProduct.size()==1&&ss==false){
//                textField.setText("0");
//            }
        }
        SettingAppE.save();
        Controller.ccmainform.refreshCheck();
    }
}
