package bitnic.utils;

import bitnic.model.MTransactions;
import bitnic.orm.Configure;
import bitnic.senders.IAction;
import bitnic.senders.SenderFileBitnic;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.eventfrontol.EventBaseE;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class WorkingFile {


    private static final Logger log = Logger.getLogger(WorkingFile.class);

    public static void appenderReportDate(List<EventBaseE> list) {




        for (EventBaseE eventBaseE : list) {
            if (eventBaseE == null) continue;
            try {
                String s = eventBaseE.join();
                Configure.getSession().insert(new MTransactions(s));
                log.debug(s);
            } catch (Exception e) {

                log.error(e);
                return;
            }
        }

    }

    public static void clearReportDate() {

        Configure.getSession().deleteTable("transactions");

    }


    public static void sendBitnic(IAction iAction) {
        if (SettingAppE.getInstance().getProfile() == 1 || SettingAppE.getInstance().getProfile() == 3) {
            List<String> lines1 = new ArrayList<>();
            for (MTransactions mTransactions : Configure.getSession().getList(MTransactions.class, null)) {
                lines1.add(mTransactions.str);
            }
            if (lines1.size() == 0) {
                log.info("ВНИМАНИЕ: файл отправки данных на сервер, не содержит строк, или нарушен его формат,проверьте выходной файл.");
                if (iAction != null) {
                    iAction.action(null);
                }
                return;
            }
            new SenderFileBitnic().send(lines1, iAction);
        }
    }

}
