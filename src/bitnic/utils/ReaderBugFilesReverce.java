package bitnic.utils;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReaderBugFilesReverce {
    private static final Logger log = Logger.getLogger ( ReaderBugFilesReverce.class );

    public static List <String> getStringList ( File file , int currSession ) {

        boolean start = false;
        List <String> lines = new ArrayList <> ();
        StringBuilder builder = new StringBuilder ();
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile ( file , "r" );
            long fileLength = file.length () - 1;
            randomAccessFile.seek ( fileLength );
            for (long pointer = fileLength; pointer >= 0; pointer--) {
                randomAccessFile.seek ( pointer );
                char c;
                c = (char) randomAccessFile.read ();
                if ( c == '\n' ) {
                    String line = builder.reverse ().toString ();
                    if ( !line.contains ( ";" ) ) continue;
                    String[] ss = line.split ( ";" , -1 );
                    int idsession = Integer.parseInt ( ss[13] );
                    if ( idsession == currSession||idsession==0 ) {
                        lines.add ( line );
                        if ( start == false ) {
                            start = true;
                        }
                        builder.setLength ( 0 );
                    } else {
                        if ( start == true ) {
                            break;
                        }
                    }
                    continue;
                }
                builder.append ( c );
            }
        } catch (Exception e) {
            log.error ( e );
        } finally {
            if ( randomAccessFile != null ) {
                try {
                    randomAccessFile.close ();
                } catch (IOException e) {
                    log.error ( e );
                }
            }
        }
        Collections.reverse ( lines );
        return lines;
    }
}
