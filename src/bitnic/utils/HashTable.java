package bitnic.utils;

import bitnic.orm.Configure;
import org.apache.log4j.Logger;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

public class HashTable {
    private static final Logger log = Logger.getLogger ( HashTable.class );

    public String getHashTable () {
        String comm = Configure.CON_STR;
        Connection connection=null;
        try {
            connection = DriverManager.getConnection ( comm );
            String sql = "Select  printf('%s%s', id_core,amount_self) from product where price <> 0 and amount_self <>0  order by price";
            try(PreparedStatement pstmt = connection.prepareStatement ( sql )){
                try(ResultSet resultSet = pstmt.executeQuery ()){
                    StringBuilder stringBuilder = new StringBuilder ();
                    while (resultSet.next ()) {
                        stringBuilder.append ( resultSet.getString ( 1 ) );
                    }
                    String s = stringBuilder.toString ();
                    MessageDigest m = null;
                    try {
                        m = MessageDigest.getInstance ( "MD5" );
                    } catch (NoSuchAlgorithmException e) {
                        log.error ( e );
                    }
                    m.update ( s.getBytes () , 0 , s.length () );
                    return new BigInteger ( 1 , m.digest () ).toString ( 16 );

                }

            }

        } catch (Exception ex) {
            log.error ( ex );
            return "";
        }finally {
            try {
                if(connection!=null){
                    connection.close ();
                }

            } catch (SQLException e) {
                log.error ( e );
            }

        }

    }
}
