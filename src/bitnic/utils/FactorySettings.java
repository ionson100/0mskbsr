package bitnic.utils;


import bitnic.dialogfactory.DialogFactory;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static bitnic.utils.UtilsSettings.recursion;

public class FactorySettings {
    private static Logger log = Logger.getLogger(FactorySettings.class);
    public static <T> T getSettingsE(Class<T> aClass) {
        T res = null;
        try {
            res = (T) UtilsSettings.deserializerE(aClass);
        } catch (Exception e) {
            log.error ( e );
            e.printStackTrace();
            DialogFactory.errorDialog(e);
        }
        if (res == null) {
            throw new RuntimeException(" не могу получить настройки: " + aClass.getName());
        }
        return res;
    }

    public static <T> void saveE(T t) {

        try {

            UtilsSettings.serializerE(t,null);
            List<Class> classes=new ArrayList<>();
            recursion(classes,t.getClass());
            for (Class aClass : classes) {
                UtilsSettings.serializerE(t,aClass);
            }
        } catch (IOException e) {
            log.error ( e );
            e.printStackTrace();
        }

    }






}
