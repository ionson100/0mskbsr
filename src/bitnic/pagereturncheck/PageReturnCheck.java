package bitnic.pagereturncheck;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.kassa.DeleteCheckConstructor;
import bitnic.kassa.PrintCheckConstructor;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.pagesale.itemsproduct.ItemProductCheck;
import bitnic.pagesale.stacksearcher.itenstacksearcher.FItemStackSearcher;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Support;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.function.Consumer;

public class PageReturnCheck extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger(PageReturnCheck.class);

    List<MProduct> listCheck = new ArrayList<>();

    public RadioButton rb_fiscal, rb_nofiscal, rb_prihod, rb_noprihod, rb_nal, rb_bnal;
    ToggleGroup toggleGroup = new ToggleGroup();
    ToggleGroup toggleGroup2 = new ToggleGroup();
    ToggleGroup toggleGroup3 = new ToggleGroup();


    public TextField tf_id_point, tf_id_user, tf_name_user,text_fider,tf_code_point;


    public Pane pane_base;
    public Button bt_print, bt_clear;
    public ListView list_check, list_product;

    public Label label_amount, label_summa,label_code;
    List<MProduct> mProducts = Configure.getSession().getList(MProduct.class, null);

    ObservableList observableList = FXCollections.observableArrayList();
    ObservableList observableListCheck = FXCollections.observableArrayList();


    public PageReturnCheck() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_page_return_check.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }


    public void setSource(List<MProduct> mProducts, boolean isFirst) {

        if (pane_base.getChildren().size() > 0) {
            pane_base.getChildren().remove(0, 1);
        }

        list_product = new ListView();
        list_product.setCellFactory((Callback<ListView<MProduct>, ListCell<MProduct>>) list -> new CheckCell(o -> {
            addProduct((MProduct) o);
            return false;
        }));

        observableList.clear();

        for (MProduct mProduct : mProducts) {
            if (mProduct.price == 0) continue;
            observableList.add(mProduct);
        }
        list_product.setItems(observableList);

        if (isFirst) {
            new Thread(() -> {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> {
                    pane_base.getChildren().add(list_product);
                    list_product.setPrefHeight(pane_base.getHeight());
                    list_product.setPrefWidth(pane_base.getWidth());
                });
            }).start();
        } else {
            pane_base.getChildren().add(list_product);
            list_product.setPrefHeight(pane_base.getHeight());
            list_product.setPrefWidth(pane_base.getWidth());
        }
        Controller.writeMessage("найдено продуктов - " + list_product.getItems().size());
    }

    List<MProduct> selector = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

//        if (SettingAppE.getInstance().isTypeЫShow3D) {
//            vbox_search.getStyleClass().add("footer_3d");
//        } else {
//            vbox_search.getStyleClass().add("footer");
//        }

        rb_fiscal.setToggleGroup(toggleGroup);
        rb_nofiscal.setToggleGroup(toggleGroup);
        rb_nofiscal.setSelected(true);

        rb_prihod.setToggleGroup(toggleGroup2);
        rb_noprihod.setToggleGroup(toggleGroup2);
        rb_prihod.setSelected(true);

        rb_nal.setToggleGroup(toggleGroup3);
        rb_bnal.setToggleGroup(toggleGroup3);
        rb_nal.setSelected(true);

        tf_id_point.setText("" + SettingAppE.getPointId());
        tf_id_user.setText(SettingAppE.getInstance().user_id);
        tf_name_user.setText(SettingAppE.getInstance().getUserName());

        rb_noprihod.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue&&rb_nofiscal.isSelected()){
                    label_code.setVisible(true);
                    tf_code_point.setVisible(true);
                }else {
                    label_code.setVisible(false);
                    tf_code_point.setVisible(false);
                }
            }
        });

        rb_nofiscal.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue&&rb_noprihod.isSelected()){
                    label_code.setVisible(true);
                    tf_code_point.setVisible(true);
                }else {
                    label_code.setVisible(false);
                    tf_code_point.setVisible(false);
                }
            }
        });


        setSource(mProducts, true);

        UtilsOmsk.painter3d(bt_print, bt_clear);
        text_fider.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                selector.clear();


                for (MProduct mProduct : mProducts) {
                    if (String.valueOf(mProduct.price).startsWith(newValue)) {
                        selector.add(mProduct);
                    }
                }
                Collections.sort(selector, new Comparator<MProduct>() {
                    @Override
                    public int compare(MProduct o1, MProduct o2) {
                        return Double.compare(o1.price, o2.price);
                    }
                });
                setSource(selector, false);
            }
        });

        Controller.writeMessage("Работа с чеками");
        if (SettingAppE.getInstance().isTypeЫShow3D) {
            text_fider.getStyleClass().add("delivery_3d");
        }

        bt_print.setOnAction(event -> {

            if (validate() == false) return;
            StringBuilder sb = new StringBuilder();
            sb.append("Печатаем чек: ");
            if (rb_prihod.isSelected()) {
                sb.append("Приход");
            } else {
                sb.append("Возврат прихода ");
            }

            if (rb_nal.isSelected() == false) {
                sb.append("Безналичный расчет. ").append(System.lineSeparator());
            } else {
                sb.append("Наличный расчет. ").append(System.lineSeparator());
            }
            sb.append("Тип чека: ").append(System.lineSeparator());
            if (rb_fiscal.isSelected()) {
                sb.append("Фискальный чек, с передачей ОФД, в отчет не пишем").append(System.lineSeparator());
            } else {
                sb.append("НЕ фискальный чек, пишем в отчет, в ОФД не передаем").append(System.lineSeparator());
            }


            double amount = 0d, summa = 0d;
            {
                for (MProduct mProduct : listCheck) {
                    amount = amount + mProduct.selectAmount;
                    summa = summa + mProduct.selectAmount * mProduct.price;
                }
            }
            sb.append("Количество товара: ").append(String.valueOf(UtilsOmsk.round(amount, 2))).append(System.lineSeparator());
            sb.append("Сумма чека: ").append(String.valueOf(UtilsOmsk.round(summa, 2))).append(System.lineSeparator());


            DialogFactory.questionDialog("Печать чека", sb.toString(), new ButtonAction("Да", new IAction<Object>() {
                @Override
                public boolean action(Object o) {

                    int id_point, id_user;
                    String userName,code_point;
                    try {

                        id_point = Integer.parseInt(tf_id_point.getText());
                        id_user = Integer.parseInt(tf_id_user.getText());
                        userName = tf_name_user.getText();
                        if (id_point <= 0 || id_user <= 0||userName.trim().length() == 0){

                            throw  new RuntimeException(Support.str("name337"));


                        }
                        code_point=tf_code_point.getText();
                        if(rb_noprihod.isSelected()&&rb_nofiscal.isSelected()){
                            if(code_point.trim().length()==0){

                                throw  new RuntimeException(Support.str("name336"));
                            }
                        }

                    } catch (Exception e) {
                        DialogFactory.errorDialog(e.getMessage());
                        return false;
                    }


                    if (rb_prihod.isSelected()) {
                         new  PrintCheckConstructor().print(id_user,id_point,userName,listCheck,rb_nal.isSelected(),rb_fiscal.isSelected());
                    } else {
                           new DeleteCheckConstructor().deleteReturn(id_user,id_point,userName,listCheck, rb_nal.isSelected(),rb_fiscal.isSelected(),code_point);
                    }

                    return false;
                }
            }));
        });

        bt_clear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                listCheck.clear();
                setSourceCheck();

            }
        });


    }

    private boolean validate() {
        if (listCheck.size() == 0) {
            return false;
        }
        for (MProduct mProduct : listCheck) {
            if (mProduct.price <= 0) {
                DialogFactory.errorDialog(String.format(Support.str("name338"),mProduct.name));
                return false;
            }
            if (mProduct.selectAmount <= 0) {
                DialogFactory.errorDialog(String.format(Support.str("name339"),mProduct.name ));
                return false;
            }
        }

        return true;
    }

    class CheckCell extends ListCell<MProduct> {

        private IAction iAction;

        public CheckCell(IAction iAction) {

            this.iAction = iAction;
        }

        @Override
        public void updateItem(MProduct item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemStackSearcher check = new FItemStackSearcher(item, iAction);
                check.setPrefWidth(list_product.getWidth());
                setGraphic(check);
            }
        }
    }

    private void addProduct(MProduct mProduct) {
        boolean aa = false;
        for (MProduct product : listCheck) {
            if (product.code_prod.equals(mProduct.code_prod)) {
                aa = true;
                product.selectAmount = product.selectAmount + 1;
                break;
            }

        }
        if (aa == false) {
            mProduct.selectAmount = 1;
            listCheck.add(mProduct);
        }
        setSourceCheck();
    }

    private void setSourceCheck() {
        final int[] i = {0};
        observableListCheck.clear();
        List<MProduct> delete = new ArrayList<>();
        listCheck.stream().forEach(new Consumer<MProduct>() {
            @Override
            public void accept(MProduct mProduct) {
                if (mProduct.selectAmount == -1) {
                    delete.add(mProduct);
                } else {
                    mProduct.indexForCheck = ++i[0];
                }
            }
        });
        listCheck.removeAll(delete);

        observableListCheck.clear();
        observableListCheck.addAll(listCheck);
        list_check.setItems(observableListCheck);
        list_check.setCellFactory(param -> new CheckCell2(new IAction() {
            @Override
            public boolean action(Object o) {
                setSourceCheck();
                return false;
            }
        }));

        refreshSumma();

    }

    class CheckCell2 extends ListCell<MProduct> {

        private IAction iAction;

        public CheckCell2(IAction iAction) {

            this.iAction = iAction;
        }

        @Override
        public void updateItem(MProduct item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                ItemProductCheck check = new ItemProductCheck(item, true, o -> {
                    iAction.action(null);
                    return false;
                });
                check.setPrefWidth(list_check.getWidth() - 50);
                setGraphic(check);
            }
        }
    }

    void refreshSumma() {
        double amount = 0d;
        double summ = 0d;
        for (MProduct mProduct : listCheck) {
            amount = amount + mProduct.selectAmount;
            summ = summ + mProduct.selectAmount * mProduct.price;
        }
        label_amount.setText(String.valueOf(UtilsOmsk.round(amount, 2)));
        label_summa.setText(String.valueOf(UtilsOmsk.round(summ, 2)));
    }


}
