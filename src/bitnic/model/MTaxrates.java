package bitnic.model;


import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayColumn;
//import bitnic.table.DisplayViewer;

/**
 * Created by USER on 10.01.2018.
 */
//@DisplayViewer(nameTable = "Таблица пользователей", isShowButton = true)
@Table("taxrates")
@Frontol(command = "ADDTAXRATES")
public class MTaxrates {

    @DisplayColumn(name_column = "id")
    @PrimaryKey("id")
    public int id;

    //1 Да Целое Код налоговой ставки
    @DisplayColumn(name_column = "cod_nalog")
    @IndexFrontol(index = 1)
    @Column("cod_nalog")
    public int cod_nalog;


    // 2 Нет Строка 100 Наименование налоговой ставки
    @DisplayColumn(name_column = "name_nalog")
    @IndexFrontol(index = 2)
    @Column("name_nalog")
    public String name_nalog;


    // 3 Нет Строка 100 Текст
    @DisplayColumn(name_column = "text_")
    @IndexFrontol(index = 3)
    @Column("text_")
    public String text_;


    // 4 Да Целое  Тип налога:    0 – процентный;  1 – суммовой
    @DisplayColumn(name_column = "type_nalog")
    @IndexFrontol(index = 4)
    @Column("type_nalog")
    public int type_nalog = 0;


    //5 Да Дробное* Значение налога
    @DisplayColumn(name_column = "nalog_value")
    @IndexFrontol(index = 5)
    @Column("nalog_value")
    public double nalog_value;


    //    6Нет  Целое
//    Номер налога в ККМ от 0 до 6**
//             0 – использовать налог из секции;
//    1 – НДС 0%;
//    2 – НДС 10%;
//    3 – НДС 18%;
//    4 – НДС не облагается;
//    5 – НДС с расчетной ставкой 10%;
//    6 – НДС с расчетной ставкой 18%.
//    Значение по умолчанию: 0
    @DisplayColumn(name_column = "nalog_number_kkm",width = 200)
    @IndexFrontol(index = 6)
    @Column("nalog_number_kkm")
    public int nalog_number_kkm;


}
