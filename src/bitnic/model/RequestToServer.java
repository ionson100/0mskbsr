package bitnic.model;

import java.util.ArrayList;
import java.util.List;

public class RequestToServer {
    public int id;
    public int point;
    public String trader;
    public boolean status;
    public List<MRequest.Prod> products_core=new ArrayList<>();
}
