package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

/**
 * Created by USER on 10.01.2018.
 */
//удалить весь товар
@Frontol(command = "DELETEALLWARES")
public class MCommandDELETEALLWARES implements IFrontolAction {
    @Override
    public void action() {
        Configure.getSession().deleteTable("product");
    }
}
