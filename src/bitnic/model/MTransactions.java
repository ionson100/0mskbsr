package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;

@Table("transactions")
public class MTransactions {

    public MTransactions(){

    }

    public MTransactions(String str){
        this.str=str;
    }
    @PrimaryKey("id")
    public int id;

    @Column("str")
    public String str;

    @Column("issender")
    public int issender;

}
