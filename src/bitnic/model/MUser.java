package bitnic.model;


import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.pagetableproduct.FinderAdmin;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableTestCenter;
import bitnic.table.celldescriptions.TableTestName;


import java.io.Serializable;


@Frontol ( command = "ADDUSERS" )
@Table ( "user" )
public class MUser implements IInsertValidator, Serializable {


    @PrimaryKey ( "id" )
    public int id;


    // 1 Да Строка 10 Код
    @FinderAdmin
    @DisplayColumn ( name_column = "код", ClassTableCell = TableTestCenter.class )
    @IndexFrontol ( index = 1 )
    @Column ( "cod" )
    public java.lang.String cod;


    //2 Да Строка 100 Наименование
    @FinderAdmin
    @DisplayColumn ( name_column = "имя", width = 500, ClassTableCell = TableTestName.class )
    @IndexFrontol ( index = 2 )
    @Column ( "name" )
    public String name;

    //3 Нет Строка 100 Текст для чека
    @DisplayColumn ( name_column = "имя_чек", ClassTableCell = TableTestName.class )
    @IndexFrontol ( index = 3 )
    @Column ( "name_check" )
    public String name_check;


    // 4 Нет Строка 10 Код профиля пользователя
    @DisplayColumn ( name_column = "профиль", ClassTableCell = TableTestCenter.class )
    @IndexFrontol ( index = 4 )
    @Column ( "profile" )
    public String profile;


    // 5 Нет Строка 50 Пароль
    @FinderAdmin
    @DisplayColumn ( name_column = "pwd", ClassTableCell = TableTestCenter.class )
    @IndexFrontol ( index = 5 )
    @Column ( "password" )
    public String password;


    //6 Нет Строка 255 Набор символов, соответствующих карте, штрих  коду либо мех. ключу, используемому для идентификации пользовател
    @FinderAdmin
    @DisplayColumn ( name_column = "штрихкод pwd", ClassTableCell = TableTestCenter.class )
    @IndexFrontol ( index = 6 )
    @Column ( "map_user" )
    public String map_user;


    @Override
    public java.lang.String getHashKey () {
        return cod == null ? "" : cod;
    }
}

