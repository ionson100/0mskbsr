package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayColumn;

@Table("taxgrouprates")
@Frontol(command = "ADDTAXGROUPRATES")
public class MTaxGroupRates {

    @DisplayColumn(name_column = "id",width = 200)
    @PrimaryKey("id")
    public int id;

    // 1 Да Целое Код налоговой ставки группы
    @DisplayColumn(name_column = "cod_nalog",width = 200)
    @IndexFrontol(index = 1)
    @Column("cod_namlog")
    public int cod_namlog;

    // 2 Да Целое Код налоговой группы
    @DisplayColumn(name_column = "cod_nalog_group",width = 200)
    @IndexFrontol(index = 2)
    @Column("cod_nalog_group")
    public int cod_nalog_group;

    // 3 Да Целое Код налоговой ставки
    @DisplayColumn(name_column = "tax_rate",width = 200)
    @IndexFrontol(index = 3)
    @Column("tax_rate")
    public int tax_rate;

    // 4 Да Целое  Смена базы: 0 – нет;  1 – да
    @DisplayColumn(name_column = "isCheckBase",width = 200)
    @IndexFrontol(index = 4)
    @Column("isCheckBase")
    public int isCheckBase;

}
