package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETEALLTAXGROUPS")
public class MCommandDELETEALLTAXGROUPS implements IFrontolAction {

    @Override
    public void action() {
        Configure.getSession().deleteTable("taxgroup");
    }
}
