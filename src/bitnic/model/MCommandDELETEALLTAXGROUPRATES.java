package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETEALLTAXGROUPRATES")
public class MCommandDELETEALLTAXGROUPRATES implements IFrontolAction {

    @Override
    public void action() {

        Configure.getSession().deleteTable("taxgrouprates");
    }
}
