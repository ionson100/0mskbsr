package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;

@Table("log_time")
public class MLogTime {

    @PrimaryKey("id")
    public int id;

    @Column("timers")
    public int timers;
}
