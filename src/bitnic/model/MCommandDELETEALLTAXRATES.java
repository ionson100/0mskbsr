package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETEALLTAXRATES")
public class MCommandDELETEALLTAXRATES implements IFrontolAction {

    @Override
    public void action() {
        Configure.getSession().deleteTable("taxrates");
    }
}
