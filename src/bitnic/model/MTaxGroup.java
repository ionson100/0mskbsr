package bitnic.model;


import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayColumn;

@Frontol(command = "ADDTAXGROUPS")
@Table("taxgroup")
public class MTaxGroup {

    @DisplayColumn(name_column = "id",width = 200)
    @PrimaryKey("id")
    public int id;

    // 1 Да Целое Код налоговой группы
    @DisplayColumn(name_column = "croup_nalog",width = 200)
    @Column("croup_nalog")
    @IndexFrontol(index = 1)
    public int croup_nalog;

    // 2 Нет Строка 100 Наименование налоговой группы
    @DisplayColumn(name_column = "name",width = 500)
    @Column("name")
    @IndexFrontol(index = 2)
    public String name;

    // 3 Нет Строка 100 Текст
    @DisplayColumn(name_column = "text_",width = 200)
    @Column("text_")
    @IndexFrontol(index = 3)
    public String text_;

}
