package bitnic.model;

import bitnic.checkarchive.AmountSelf;
import bitnic.checkarchive.AmountSelfTotal;
import bitnic.checkarchive.CheckArchiveE;
import bitnic.message.MessageBody;
import bitnic.updateapp.Requests;

import java.util.ArrayList;
import java.util.List;

public class ModelCollection {
   public static final List<Class> classes=new ArrayList<>();
   static {
       classes.add(MTransactions.class);
       classes.add(MBarcode.class);
       classes.add(MCardType.class);
       classes.add(MCheck.class);
       classes.add(MLogTime.class);
       classes.add(MProduct.class);
       classes.add(MTaxGroup.class);
       classes.add(MTaxGroupRates.class);
       classes.add(MTaxrates.class);
       classes.add(MUser.class);
       classes.add(MMarketingAction.class);

       classes.add(MCommandADDTAXGROUPRATES.class);
       classes.add(MCommandDELETEALLASPECTREMAINS.class);
       classes.add(MCommandDELETEALLBARCODES.class);
       classes.add(MCommandDELETEALLUSERS.class);
       classes.add(MCommandDELETEALLWARES.class);
       classes.add(MCommandREPLACEASPECTREMAINS.class);

       classes.add(MCommandDELETEALLTAXGROUPRATES.class);
       classes.add(MCommandDELETEALLTAXGROUPS.class);
       classes.add(MCommandDELETEALLTAXRATES.class);
       classes.add(MCommandDELETEALLCCARDTYPES.class);
       classes.add(MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE.class);


       classes.add(AmountSelf.class);
       classes.add(AmountSelfTotal.class);
       classes.add(CheckArchiveE.class);
       classes.add(MessageBody.class);
       classes.add(Requests.class);

       classes.add(MRequest.class);
       classes.add(MRequestProduct.class);
       classes.add(MTest.class);

   }
}
