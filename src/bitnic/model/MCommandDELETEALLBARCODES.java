package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETEALLBARCODES")
public class MCommandDELETEALLBARCODES implements IFrontolAction {

    @Override
    public void action() {
        Configure.getSession().deleteTable("barcode");
    }
}

