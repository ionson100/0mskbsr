package bitnic.model;

import bitnic.checkarchive.AmountSelf;
import bitnic.orm.Column;
import bitnic.orm.Configure;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.pagetableproduct.Finder;
import bitnic.pagetableproduct.FinderAdmin;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableTestCenter;
import bitnic.table.celldescriptions.TableTestDouble;
import bitnic.table.celldescriptions.TableTestName;
import org.apache.log4j.Logger;

import java.io.Serializable;

@Frontol(command = "REPLACEQUANTITY")
@Table("product")
public class MProduct implements Serializable, IInsertValidator {//

    private static final Logger log = Logger.getLogger(MProduct.class);

    public transient int indexForCheck;

    @PrimaryKey("id")
    public int id;


    @FinderAdmin
    @DisplayColumn(name_column = "id_core", ClassTableCell = TableTestCenter.class)
    //1 Да Строка 10 Код
    @IndexFrontol(index = 1)
    @Column("id_core")
    public String code_prod;


    // 2 Нет Строка Штрихкоды через запятую,максимальная длина  штрихкода не превышает40 символов
    @IndexFrontol(index = 2)
    @Column("shtrihCod")
    public String shtrihCod;

    @FinderAdmin
    @Finder
    @DisplayColumn(name_column = "name",
            index = 2,
            width = 500,
            ClassTableCell = TableTestName.class)
    // 3 Нет Строка 100 Наименование
    @IndexFrontol(index = 3)
    @Column("name")
    public String name;

    //4 Нет Строка 100 Текст для чека
    @IndexFrontol(index = 4)
    @Column("name_check")
    public String name_check;


    @FinderAdmin
    @Finder(useStartsWith = true)
    //5 Нет Дробное 8.*Цена, значение по умолча- нию = 0,00 Не используется
    @DisplayColumn(name_column = "price", index = 2, ClassTableCell = TableTestDouble.class)
    @IndexFrontol(index = 5)
    @Column("price")
    public double price = 0d;


    @FinderAdmin
    //6 Нет Дробное 7.4 Остаток Не используется
    @DisplayColumn(name_column = "amount_self", index = 2, ClassTableCell = TableTestDouble.class)
    @IndexFrontol(index = 6)
    @Column("amount_self")
    public double amount_self;

    public double getAmountSelf() {
        double a = AmountSelf.getAmountCore(this.code_prod);
        return amount_self + a;
    }


    //8 Нет Строка Флаги через запятую: дробное количество (весо-вой); продажа; возврат; отрицательные остатки;без ввода количества;
    //списание остатков;редактирование цены; ввод количества вручную;печатать в чеке;наливаемый товар**;скидки.По умолчанию значение поля = 0 Не используется
    @IndexFrontol(index = 8)
    @Column("flags")
    public String flags;


    //9 Нет Дробное 8.* Минимальная цена, значение по умолчанию = 0,00
    @IndexFrontol(index = 9)
    @Column("min_price")
    public double min_price;


    // 11 Нет Строка 12 Код схемы разрезов.  Значение по умолчанию: ‘’, то есть схема не задана.
    @IndexFrontol(index = 11)
    @Column("cut_map")
    public String cut_map;


    //12 Нет Целое Вариант использования  разрезов: • 0 – полный список;• 1 – заданный список;  • 2 – заданный список с  остатком. Значение по умолчанию = 0
    @IndexFrontol(index = 12)
    @Column("cut_variant")
    public int cut_variant;


    // 14 Нет Дробное10.* Коэффициент штрихкода Не используется
    @IndexFrontol(index = 14)
    @Column("delta_strihcod")
    public double delta_strihcod;


    @DisplayColumn(name_column = "group_product", index = 2, ClassTableCell = TableTestCenter.class)
    //16 Нет Строка 10 Код родительской группы, по умолчанию поле не за- полнено
    @IndexFrontol(index = 16)
    @Column("group_product")
    public String group;


    @DisplayColumn(name_column = "isGroup", index = 2, ClassTableCell = TableTestCenter.class)
    // 17 Нет Целое 1Товар или группа: для това- ра = 1 (значение по умол- чанию) Товар или группа: для группы = 0
    @IndexFrontol(index = 17)
    @Column("isGroup")
    public int isGroup = 1;


    @DisplayColumn(name_column = "series", index = 2, ClassTableCell = TableTestCenter.class)
    // 20 Нет Строка 100 Серия Не используется
    @IndexFrontol(index = 20)
    @Column("series")
    public String series;


    //21 Нет Строка 100 Сертификат Не используется
    @IndexFrontol(index = 21)
    @Column("certificate")
    public String certificate;


    @DisplayColumn(name_column = "cod_nalog_group", index = 2)
    // 23 Нет Целое Код налоговой группы
    @IndexFrontol(index = 23)
    @Column("cod_nalog_group")
    public String cod_nalog_group;


    //26 Нет Строка 20 Артикул
    @DisplayColumn(name_column = "articul", index = 3, ClassTableCell = TableTestCenter.class)
    @IndexFrontol(index = 26)
    @Column("articul")
    public String articul;


    // 29 Нет Дробное*** Максимальная скидка, %
    @IndexFrontol(index = 29)
    @Column("max_discount")
    @DisplayColumn(name_column = "максимальная скидка", index = 4, ClassTableCell = TableTestDouble.class)
    public double max_discount;


    //31 Нет Строка Имя файла с картинкой формата PNG, BMP, JPEG, GIF, TIFF, ICO и путь к нему
    @IndexFrontol(index = 31)
    @Column("image")
    public String image;


    //32 Нет Строка Описание
    @DisplayColumn(name_column = "description", index = 5)
    @IndexFrontol(index = 32)
    @Column("description")
    public String description;


    // 33 Нет Дробное10.3 Кратность количества Не используется
    @IndexFrontol(index = 33)
    @Column("kratnost_count")
    public double kratnost_count;


    // 39 Нет Строка Код ГП товара (загружается в карточку товара) Код ГП группы (загру- жается в карточку группы)
    @IndexFrontol(index = 39)
    @Column("cod_group_product")
    public String cod_group_product;


    // 40 Нет Строка Код ГП копии товара (за- гружается в карточку това- ра) Код ГП копии группы (загружается в карточкугруппы)
    @IndexFrontol(index = 40)
    @Column("cod_copy_group_product")
    public String cod_copy_group_product;


    //53 Нет Целое Код вида продукции. Значение по умолчанию = 100 Не используется
    @IndexFrontol(index = 53)
    @Column("cod_view_product")
    public int cod_view_product = 100;


    // 54 Нет Дробное 10.6 Ёмкость тары, л. Значение по умолчанию = 1,000 Не используется
    @IndexFrontol(index = 54)
    @Column("capacityTare")
    public double capacityTare = 1d;


    //55 Нет Целое Алкогольная продукция:   0-нет;  1-алкогольная продукция. Значение по умолчанию = 0 Не используется
    @IndexFrontol(index = 55)
    @Column("isAlcogol")
    public int isAlcogol = 0;


    //56 Нет Целое Маркировка алкогольной продукции:   0-с маркой;  1-без марки.   Значение по умолчанию = 0Не используется
    @IndexFrontol(index = 56)
    @Column("isSetMarkaAlcogol")
    public int isSetMarkaAlcogol = 0;


    // 57 Нет Дробное Крепость алкогольной про-дукции. Значение по умол-чанию = 0,1. Минимальное значение = 0,01
    @IndexFrontol(index = 57)
    @Column("gradusAlcogol")
    public double gradusAlcogol = 0.01d;


    public double selectAmount;

    public transient Object userObject;
    public int delta = 0;


    public transient int index;

    public transient boolean isOppenGroup;
    public transient boolean firstButton;

    @Override
    public String getHashKey() {

        String ss = String.valueOf(code_prod);
        return ss;
    }

    public String getMysHach() {
        String s = code_prod + String.valueOf(price);
        return s;
    }

    public MProduct clone() {

       MProduct p = Configure.getSession().get(MProduct.class,this.id);
        if (p==null) {
            String msg="null product - id:" + this.id;
            log.error(msg);
            throw new RuntimeException(msg);
        }
        p.selectAmount = this.selectAmount;
        return p;
    }
}

