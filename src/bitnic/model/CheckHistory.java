package bitnic.model;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class CheckHistory {
   public String product_id;
   public String point;
   public double price;
   public int doc_number;
   public double amount;
   public long date;
   public int session;

   private LocalDate _date;
   public LocalDate getDate(){
      if(_date==null){
         _date= Instant.ofEpochMilli(date).atZone(ZoneId.systemDefault()).toLocalDate();
      }
      return _date;
   };
}
