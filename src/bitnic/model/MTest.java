package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;

@Table("test_1")
public class MTest {
    @PrimaryKey("id")
    public int id;

    @Column("datas")
    public int datas;
}
