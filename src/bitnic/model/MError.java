package bitnic.model;

import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableSimpleCell;

public class MError {
    @DisplayColumn(name_column = "данные",width =2000,ClassTableCell = TableSimpleCell.class)
    public String msg;
}
