package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.pagetableproduct.FinderAdmin;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableTestCenter;

import java.io.Serializable;

@Table ( "barcode" )
@Frontol ( command = "ADDBARCODES" )
public class MBarcode implements IInsertValidator, Serializable {


    @PrimaryKey ( "id" )
    public int id;

    // 1 Да Строка 40 Штрихкод
    @FinderAdmin
    @DisplayColumn ( name_column = "код", ClassTableCell = TableTestCenter.class )
    @Column ( "barcode" )
    @IndexFrontol ( index = 1 )
    public String barcode;


    //   2 Да Строка 20/Целое* Идентификатор товара
    @FinderAdmin
    @DisplayColumn ( name_column = "идентификатор товара", ClassTableCell = TableTestCenter.class )
    @Column ( "id_product" )
    @IndexFrontol ( index = 2 )
    public String id_product;


    // 3 Нет Строка Коды значений разрезов через запятую
    @Column ( "codes_cut" )
    @IndexFrontol ( index = 3 )
    public String codes_cut;

    //    4 Нет Дробное 7.4 Коэффициент
    @DisplayColumn ( name_column = "коэффициент", ClassTableCell = TableTestCenter.class )
    @Column ( "coefficient" )
    @IndexFrontol ( index = 4 )
    public String coefficient;


    //    5 Нет Целое
//    Разрешить задавать одинаковый ШК у товара
//    с разными разрезами:
//             0 – создание нескольких ШК у одного
//    товара запрещено, даже, если есть
//    разрезы. Если ШК у товара нет, при
//    загрузке создаётся новый ШК, если ШК
//    есть, он заменится на загружаемый. Если
//    ШК несколько, они все заменятся на
//            загружаемый;
//     1 – создание нескольких ШК у одного
//    товара разрешено. Учитываются разрезы
//    товара, у каждого разреза может быть
//    максимум 1 ШК. Если у разреза уже есть
//    ШК, при загрузке в нем будет изменен
//    коэффициент на тот, который в
//    загружаемом ШК, если у разреза нет ШК,
//    он будет добавлен. У разных разрезов
//    одного товара могут быть одинаковые
//    ШК.
//    Значение по умолчанию = 0.
    @DisplayColumn ( name_column = "isPermit", ClassTableCell = TableTestCenter.class )
    @Column ( "isPermit" )
    @IndexFrontol ( index = 5 )
    public int isPermit = 0;

    @Override
    public String getHashKey () {
        return barcode+id_product;
    }

    // public javafx.beans.property.IntegerProperty isPermitProperty(){return new javafx.beans.property.SimpleIntegerProperty(isPermit); }


}
