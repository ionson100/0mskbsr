package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;

@Table("settings_kkm")
public class KkmSettings{
    @PrimaryKey("id")
    public int id;

    @Column("name")
    public String name;

    public KkmSettings(String settings) {
        name=settings;
    }
    public KkmSettings() {
        name="";
    }
}
