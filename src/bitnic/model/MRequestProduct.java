package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableTestDouble;
import bitnic.table.celldescriptions.TableTestDoubleBlack;
import bitnic.table.celldescriptions.TableTestName;


// продкуты для заявок
@Table(MRequestProduct.TABLE_PRODUCT)
 public class MRequestProduct {


    public static final String TABLE_PRODUCT = "request_product4" ;


    @PrimaryKey( "id" )
    public int id;

    @Column("id_request")
    public int id_request;

    @Column("id_product")
    public String id_product;


   @DisplayColumn(name_column = "Название",
           index = 1,
           width = 900,
           ClassTableCell = TableTestName.class)
   @Column("name")
    public String name;

   @DisplayColumn(name_column = "кол-во",
           index = 2,
           width = 250,
           ClassTableCell = TableTestDoubleBlack.class)
   @Column("amount")
    public double amount;


    @Column("amount_fact")
    public double amount_fact;

   @DisplayColumn(name_column = "Штрих код",
           index = 3,
           width = 300)
   @Column("barcode")
    public String barcode;

   @DisplayColumn(name_column = "Код поставщика",
           index = 4,
           width = 500)
   @Column("codeSeller")
    public String shipper_code;
}


