package bitnic.appender;

import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.URI;
import java.security.cert.X509Certificate;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MyWebSocket2 {


    private static final Logger log = Logger.getLogger ( MyWebSocket2.class );
    private IWsClose iwsclose;
    private IWsMessage iwsmessage;

    private static final int TIMER_RECONNECT_SEC=30;
    private final int WEB_SOCKET_TIMEOUT = 5;


    public interface IWsClose {
        boolean action ( int i , String s , boolean b );
    }

    public interface IWsMessage {
        void action ( String msg );
    }

    public void setIwsClose ( IWsClose iwsclose ) {

        this.iwsclose = iwsclose;
    }

    public void setIwsMessage ( IWsMessage iwsmessage ) {

        this.iwsmessage = iwsmessage;
    }


    private WebSocketClient webSocketClient;
    private URI serverURI;


    public MyWebSocket2 ( URI serverURI ) {

        this.serverURI = serverURI;



    }


    private void innerStart () throws Exception {
        webSocketClient = new WebSocketClient ( this.serverURI ) {
            @Override
            public void onOpen ( ServerHandshake serverHandshake ) {
                log.info ( " wss open - " + serverURI.toString () );
            }

            @Override
            public void onMessage ( String s ) {

                log.info ( s );
                if ( iwsmessage != null ) {
                    iwsmessage.action ( s );
                }
            }

            @Override
            public void onClose ( int i , String s , boolean b ) {
                log.error ( "wss close - " + String.valueOf ( i ) + " - " + s );

                if ( iwsclose != null ) {
                    boolean d = iwsclose.action ( i , s , b );
                    if ( d == true ) {
                        reconnerct ();
                    }
                } else {
                    reconnerct ();
                }
            }

            @Override
            public void onError ( Exception e ) {
                log.error ( e );
            }

        };

        SSLContext sslContext = SSLContext.getInstance ( "TLS" );
        sslContext.init ( null , new TrustManager[]{new X509TrustManager () {
            public X509Certificate[] getAcceptedIssuers () {
                return null;
            }

            public void checkClientTrusted ( X509Certificate[] certs , String authType ) {
            }

            public void checkServerTrusted ( X509Certificate[] certs , String authType ) {
            }
        }} , null );


        SSLSocketFactory factory = sslContext.getSocketFactory();// (SSLSocketFactory) SSLSocketFactory.getDefault();

        webSocketClient.setSocket( factory.createSocket() );

        webSocketClient.connect();




    }

    private synchronized void reconnerct () {
        Timer timer = new Timer ();
        timer.schedule ( new RemindTask (timer) ,
                //initial delay
                TIMER_RECONNECT_SEC * 1000 );  //subsequent rate
    }


    public void start () {
        try {
            innerStart ();
        } catch (Exception e) {
            log.error ( e );
        }
    }

    void sendPing () {
        if ( webSocketClient != null && webSocketClient.getConnection ().isOpen () ){
            webSocketClient.sendPing();
            log.info ( "PING" );
        }
    }

    public void stop () {
        if(webSocketClient!=null){
            webSocketClient.getConnection ().close ();
            webSocketClient.close ();
        }

    }

    public void send ( String msg ) {
        try {
            if ( webSocketClient != null && webSocketClient.getConnection ().isOpen () ) {

                webSocketClient.send ( msg );

            }
        } catch (Exception ex) {
            log.info ( ex );

        }
    }


    private class RemindTask extends TimerTask {

        private Timer timer;

        public RemindTask( Timer timer){

            this.timer = timer;
        }
        public synchronized void run () {
            log.info ( "  wss reconnector timer execute" );
            try {
                // webSocketClient.getConnection().close();
                // webSocketClient.close();
                //webSocketClient=null;
                innerStart ();
               // webSocketClient.closeBlocking();
                //Disconnect manually and reconnect
               //webSocketClient.reconnect();
            } catch (Exception d) {
                log.error ( d );
            } finally {
                if ( timer != null ) {
                    timer.cancel ();
                }
            }


        }
    }


}
