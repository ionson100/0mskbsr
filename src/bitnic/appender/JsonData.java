package bitnic.appender;

import bitnic.utils.UtilsOmsk;

public class JsonData{

    public JsonData(int id,String trader,Object data){
        this.trader = trader;
        this.data=data;
        this.id=id;
    }
    public int id;
    public String trader;
    public Object data;
    public String getJson(){
       String res= UtilsOmsk.getGson().toJson(this);
       return res;
    }
}
