package bitnic.appender;

import bitnic.core.Controller;
import bitnic.core.Main;
import bitnic.pageerror.PageError;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

public /*static*/ class MyAppender extends AppenderSkeleton {


    private static final Logger log = Logger.getLogger(MyAppender.class);
    public void close() {
    }

    public boolean requiresLayout() {
        return false;
    }

    @Override
    protected void append(LoggingEvent loggingEvent) {
        //2018-03-14 09:12:46 ERROR StateKassaMoney:60 - bitnic.kassa.BaseKassa$DriverException: [-3] Порт недоступен
        String msg = String.format("%s#  %s %s  %s  -  %s ",
                SettingAppE.getInstance().getPointId(),
                UtilsOmsk.getStringTimeForError(new Date(loggingEvent.timeStamp)),
                loggingEvent.getLevel(),
                loggingEvent.getLocationInformation().fullInfo,
                loggingEvent.getMessage());


        if (loggingEvent.getLevel() == Level.DEBUG){
            try {
                Files.write( Paths.get(Pather.checkStory),  (loggingEvent.getMessage().toString ()+System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
            }catch (IOException e) {
               log.error ( e );
            }
        }


        if (loggingEvent.getLevel() == Level.ERROR){
            Platform.runLater(() -> {

                if (Main.myWebSockerError != null) {
                    JsonData jsonData=new JsonData(MyWebSocket.ERRORMESSAGE,SettingAppE.getInstance().user_id,msg);
                    Main.myWebSockerError.send(jsonData.getJson());
                }

            });
        }

        try{
            if( Controller.curnode instanceof PageError){
                Platform.runLater (() -> ((PageError)Controller.curnode).setLogMessage(msg));
            }
        }catch (Exception ignores){

        }

    }
}
