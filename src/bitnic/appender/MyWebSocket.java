package bitnic.appender;

import bitnic.dialogfactory.DialogFactory;
import bitnic.message.MessageBody;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.net.URI;
import java.util.ArrayList;

public class MyWebSocket {

    private static final Logger log = Logger.getLogger(MyWebSocket.class);

    public static final int ERRORMESSAGE = 1;// сообщения об ошибках
    public static final int WRITEMESSAGE = 2;// подтверждение что сообщение прочитано

    MyWebSocket2 myWebSocket2;

    public MyWebSocket() {
        URI uri=null;
        String ss = String.format("wss://%s/pos_error?point=%s",
                SettingAppE.getInstance().getUrl(),
                SettingAppE.getInstance().getPointId());
        try {
            uri = new URI ( ss );
        }catch (Exception ex){
            log.error ( ex );
        }
        myWebSocket2=new MyWebSocket2 ( uri );
        myWebSocket2.setIwsClose ((i, s, b) -> {

            switch (i){
                case 10008:{
                    String sm="\nВведите правильный индентификатор предприятия.\nПерезагрузите программу.";
                    DialogFactory.errorDialog(s+sm);
                    return false;
                }

                case 1000:{

                    return false;
                }
                default:{
                    return true;
                }
            }

        });
        myWebSocket2.setIwsMessage ( msg -> workerMessage ( msg ) );
        myWebSocket2.start ();

    }

    private void workerMessage(String message) {


        Thread thread = new Thread(() -> {
            try {
                JsonData data = UtilsOmsk.getGson().fromJson(message, JsonData.class);
                if (data.id == WRITEMESSAGE) {
                    ArrayList<Object> d = (ArrayList<Object>) data.data;
                    new MessageBody().updateDate(d);
                }
            } catch (Exception ex) {
                log.info("ОШИБКА СОКЕТА - " + message);
                log.info(ex);
            }
        });
        thread.start();
    }

    public synchronized void send(String error_msg) {
       myWebSocket2.send ( error_msg );

    }

    public synchronized void stop() {
       myWebSocket2.stop ();
    }
    public void sendPing(){
        myWebSocket2.sendPing ();
    }
}
