package bitnic.pageerror;

import bitnic.core.Controller;
import bitnic.model.MError;
import bitnic.pagetableproduct.IFocusable;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class PageError extends GridPane implements Initializable, IFocusable {
    private static final Logger log = Logger.getLogger ( PageError.class );
    public TextField tf_search;

    public HBox vbox_search;
    public CheckBox cb_autoupdate;

    public CheckBox cb_error;


    public TableView table_error;
    public GridPane grid1;
    GridPane pane;
    List <MError> list = new ArrayList <> ();

    public PageError () {
        try {
            InputStream inputStream = getClass ().getClassLoader ().getResource ( "aa_ru_RU.properties" ).openStream ();
            ResourceBundle bundle = new PropertyResourceBundle ( inputStream );
            FXMLLoader fxmlLoader = new FXMLLoader ( getClass ().getResource ( "page_eror.fxml" ) , bundle );
            fxmlLoader.setRoot ( this );
            fxmlLoader.setController ( this );

            pane = fxmlLoader.load ();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException ( exception );
        }
    }

    @Override
    public void initialize ( URL location , ResourceBundle resources ) {

        cb_error.setSelected(SettingAppE.getInstance().isShowErrorPageError);

        cb_error.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                SettingAppE.getInstance().isShowErrorPageError=newValue;
                refresh();
            }
        });


        cb_autoupdate.setSelected ( SettingAppE.getInstance ().isAutoUpdate );
        cb_autoupdate.selectedProperty ().addListener ( ( observable , oldValue , newValue ) -> {
            SettingAppE.getInstance ().isAutoUpdate = newValue;
            SettingAppE.save ();
        } );

        if ( SettingAppE.getInstance ().isTypeЫShow3D) {
            vbox_search.getStyleClass ().add ( "footer_3d" );

        } else {
            vbox_search.getStyleClass ().add ( "footer" );
        }

        UtilsOmsk.resizeNode ( this , grid1 );
        UtilsOmsk.painter3d ( tf_search );


        refresh();

        tf_search.textProperty ().addListener ((observable, oldValue, newValue) -> {
            UtilsOmsk.searchErrorMessage(newValue, list, table_error);
        });
        focus ();
        Controller.writeMessage( "Лог сообщений" );
    }

    private void refresh() {
        list.clear();
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines ( Paths.get ( System.getProperty ( "user.home" ) + File.separator + "omsk.log" ) );
        } catch (IOException e1) {
            log.error ( e1 );
            e1.printStackTrace ();
        }



        for (String line : lines) {

            if(SettingAppE.getInstance().isShowErrorPageError){
                if(line.contains(" ERROR")==false){
                    continue;
                }
            }
            MError er = new MError ();
            er.msg = line;
            list.add ( er );
        }

        log.info ( "Строк ошибок - " + list.size () );
        new BuilderTable().build( list , table_error );
        table_error.scrollTo ( table_error.getItems ().size () - 1 );
    }


    public void setLogMessage ( String msg ) {
        if ( SettingAppE.getInstance ().isAutoUpdate ) {
            MError er = new MError ();
            er.msg = msg.substring ( msg.indexOf ( "#" ) + 3 );
            list.add ( er );
            new BuilderTable ().build( list , table_error );
            table_error.scrollTo ( table_error.getItems ().size () - 1 );
        }


    }

    @Override
    public void focus () {
        Platform.runLater ( new Runnable () {
            @Override
            public void run () {
                tf_search.requestFocus ();
            }
        } );
    }
}
