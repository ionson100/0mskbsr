package bitnic.keyboardsmail;

import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FKeyBoardSmail extends GridPane implements Initializable {

    public IAction<Object>  iAction;
    private static final Logger log = Logger.getLogger(FKeyBoardSmail.class);
    public boolean isFirst = true;
    public Button bb1;
    public Button bb2;
    public Button bb3;
    public Button bb4;
    public Button bb5;
    public Button bb6;
    public Button bb7;
    public Button bb8;
    public Button bb9;
    public Button bb0;
    public Button bb01;
    public Button bb02;

    public TextField mTextField;

    public FKeyBoardSmail(TextField mTextField) {
        this.mTextField = mTextField;
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_key_board_smail.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bb0.setOnAction(event -> action(event));
        bb1.setOnAction(event -> action(event));
        bb2.setOnAction(event -> action(event));
        bb3.setOnAction(event -> action(event));
        bb4.setOnAction(event -> action(event));
        bb5.setOnAction(event -> action(event));
        bb6.setOnAction(event -> action(event));
        bb7.setOnAction(event -> action(event));
        bb8.setOnAction(event -> action(event));
        bb9.setOnAction(event -> action(event));
        bb0.setOnAction(event -> action(event));
        bb01.setOnAction(event -> action(event));
        bb02.setOnAction(event -> action(event));

        String css = "-fx-font-size: 20";

        if (SettingAppE.getInstance().isTypeЫShow3D) {

            bb1.getStyleClass().add("button_key_big_3d");
            bb2.getStyleClass().add("button_key_big_3d");
            bb3.getStyleClass().add("button_key_big_3d");
            bb4.getStyleClass().add("button_key_big_3d");
            bb5.getStyleClass().add("button_key_big_3d");
            bb6.getStyleClass().add("button_key_big_3d");
            bb7.getStyleClass().add("button_key_big_3d");
            bb8.getStyleClass().add("button_key_big_3d");
            bb9.getStyleClass().add("button_key_big_3d");
            bb0.getStyleClass().add("button_key_big_3d");
            bb01.getStyleClass().add("button_key_big_3d");
            bb02.getStyleClass().add("button_key_big_3d");


        } else {
            bb1.getStyleClass().add("button_key_big");
            bb1.getStyleClass().add("button_key_big");
            bb2.getStyleClass().add("button_key_big");
            bb3.getStyleClass().add("button_key_big");
            bb4.getStyleClass().add("button_key_big");
            bb5.getStyleClass().add("button_key_big");
            bb6.getStyleClass().add("button_key_big");
            bb7.getStyleClass().add("button_key_big");
            bb8.getStyleClass().add("button_key_big");
            bb9.getStyleClass().add("button_key_big");
            bb0.getStyleClass().add("button_key_big");
            bb01.getStyleClass().add("button_key_big");
            bb02.getStyleClass().add("button_key_big");


        }
        bb1.setStyle(css);
        bb2.setStyle(css);
        bb3.setStyle(css);
        bb4.setStyle(css);
        bb5.setStyle(css);
        bb6.setStyle(css);
        bb7.setStyle(css);
        bb8.setStyle(css);
        bb9.setStyle(css);
        bb0.setStyle(css);
        bb01.setStyle(css);
        bb02.setStyle(css);

    }

    void print(String value) {
        if (isFirst) {
            mTextField.setText(value);
        } else {
            mTextField.setText(mTextField.getText().trim() + value);
        }
        if(iAction!=null){
            iAction.action(null);
        }
    }

    private void action(ActionEvent event) {


        String id = ((Button) event.getSource()).getId();
        switch (id) {
            case "bb1": {
                print("1");
                break;
            }
            case "bb2": {
                print("2");
                break;
            }
            case "bb3": {
                print("3");
                break;
            }
            case "bb4": {
                print("4");
                break;
            }
            case "bb5": {
                print("5");
                break;
            }
            case "bb6": {
                print("6");
                break;
            }
            case "bb7": {
                print("7");
                break;
            }
            case "bb8": {
                print("8");
                break;
            }
            case "bb9": {
                print("9");
                break;
            }
            case "bb0": {
                print("0");
                break;
            }
            case "bb01": {

                String s = mTextField.getText().trim();
                if(iAction!=null){
                    iAction.action(null);
                }
                if (s.length() == 1) {
                    mTextField.setText("");
                } else {
                    if(s.length ()>1){
                        mTextField.setText(s.substring(0, s.length() - 1));
                    }
                    else {
                        mTextField.setText("");
                    }

                }
                break;
            }
            case "bb02": {
                mTextField.setText("");
                if(iAction!=null){
                    iAction.action(null);
                }
                break;
            }
            default:{
                log.error("not find button");
                throw  new RuntimeException("not find button");
            }

        }
        isFirst = false;
    }
}
