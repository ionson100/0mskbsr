package bitnic.settings;

import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class SqliteStorage {
    private static Logger log = Logger.getLogger(SqliteStorage.class);
    private static Object lock = new Object();

    private static String getTableName(Class aClass) {
        return aClass.getName().replace('.', '_') + SettingAppE.getInstance().user_id;
    }

    private static boolean existTable(Class aClass) {//;
        List<String> masters = new ArrayList<>();
        try {
            try (Connection connection = DriverManager.getConnection(Reanimation.GetBaseSettingsPath())) {
                try (PreparedStatement statement = connection.prepareStatement("SELECT name FROM sqlite_master")) {
                    try (ResultSet resultSet = statement.executeQuery()) {
                        while (resultSet.next()) {
                            String ss = resultSet.getString(1);
                            masters.add(ss);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            log.error(e);
            e.printStackTrace();
        }
        String name = getTableName(aClass);
        return masters.contains(name);
    }

    static Object getObject(Class aClass) throws SQLException {//synchronized

        synchronized (lock) {
            Connection connection;
            Object resO = null;
            connection = DriverManager.getConnection(Reanimation.GetBaseSettingsPath());
            PreparedStatement statement = null;
            if (!existTable(aClass)) {
                try {
                    connection.setAutoCommit(false);
                    String sql = "create table " + getTableName(aClass) + " ( _id integer primary key autoincrement, ass text not null);";
                    statement = connection.prepareStatement(sql);
                    statement.execute();
                    statement.close();

                    resO = aClass.newInstance();
                    Gson sd3 = new Gson();
                    String str = sd3.toJson(resO);
                    sql = "INSERT INTO " + getTableName(aClass) + " (ass) VALUES ('" + str + "');";
                    statement = connection.prepareStatement(sql);
                    statement.execute();
                    connection.setAutoCommit(true);
                } catch (Exception ex) {

                    log.error(ex);
                    if (connection != null) {
                        try {
                            connection.rollback();
                        } catch (SQLException e) {
                            log.error(e);
                            e.printStackTrace();
                        }
                    }
                    throw new RuntimeException("reanimator create-insert: " + ex.getMessage());
                } finally {
                    if (connection != null)
                        connection.close();
                    if (statement != null) {
                        statement.close();
                    }
                }
            } else {
                String sql = "select ass from " + getTableName(aClass) + " where _id=1";
                try (PreparedStatement statement2 = connection.prepareStatement(sql)) {
                    try (ResultSet resultSet = statement2.executeQuery()) {
                        resultSet.next();
                        String str = resultSet.getString(1);
                        if (str == null) {
                            resO = aClass.newInstance();
                        } else {
                            Gson ss = UtilsOmsk.getGson();
                            resO = ss.fromJson(str, aClass);
                        }
                    }

                } catch (Exception ex) {
                    log.error(ex);
                    throw new RuntimeException("reanimator select  as get object: " + ex.getMessage() + " class - " + aClass.getName());
                } finally {
                    if (connection != null)
                        connection.close();
                }
            }
            return resO;
        }

    }

    static void saveObject(Object o, Class aClass) throws SQLException {//synchronized

        synchronized (lock) {
            try (Connection connection = DriverManager.getConnection(Reanimation.GetBaseSettingsPath())) {
                Gson sd3 = new Gson();
                String str = sd3.toJson(o);
                String sql = "UPDATE " + getTableName(aClass) + " SET ass = '" + str + "' WHERE _id = 1";
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.execute();
                }

            } catch (Exception ex) {
                log.error(ex);
            }

        }

    }


}


