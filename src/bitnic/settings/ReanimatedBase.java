package bitnic.settings;


import bitnic.preloderblender.FPreloderBlender;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ReanimatedBase {
    private static final Logger log = Logger.getLogger ( ReanimatedBase.class );
    private static ReanimatedBase reanimator;

    private static final Map<Class, Object> map = new HashMap<>();

    public static Object get(Class aClass) {
        return innerGetSave(aClass, ActionBase.get);
    }

    public static synchronized void save(Class aClass) {
        innerGetSave(aClass, ActionBase.save);
    }


    private static Object innerGetSave(Class aClass, ActionBase actionBase) {//synchronized

        Object res = null;
        if (actionBase == ActionBase.get) {
            if (map.containsKey(aClass)) {
                Object o = map.get(aClass);
                if (o == null) {
                    try {
                        o = aClass.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        log.error ( e );
                        e.printStackTrace();
                    }
                }
                res = o;
            } else {
                Object o = null;
                try {
                    o = SqliteStorage.getObject(aClass);
                } catch (SQLException e) {
                    log.error ( e );
                    e.printStackTrace();
                }
                if (o == null) {
                    try {
                        o = aClass.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                        log.error ( e );
                    }
                }
                map.put(aClass, o);
                res = o;
            }
        }
        if (actionBase == ActionBase.save) {

            Object o = map.get(aClass);
            if (o == null) {
                try {
                    o = aClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                    log.error ( e );
                }
            }
            try {
                SqliteStorage.saveObject(o, aClass);
            } catch (SQLException e) {
                log.error ( e );
                e.printStackTrace();
            }
        }
        return res;
    }

    private enum ActionBase {
        get, close, save
    }
}
