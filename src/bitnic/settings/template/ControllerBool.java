//package bitnic.settings.template;
//
//import bitnic.dialogfactory.DialogFactory;
//import bitnic.settings.InnerAttribute;
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.fxml.FXML;
//import javafx.fxml.FXMLLoader;
//import javafx.fxml.Initializable;
//import javafx.scene.control.*;
//import javafx.scene.layout.Pane;
//
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.lang.reflect.Field;
//import java.net.URL;
//import java.util.PropertyResourceBundle;
//import java.util.ResourceBundle;
//
///**
// * Created by ion on 13.01.2018.
// */
//public class ControllerBool extends Pane implements Initializable {
//
//    @FXML
//    CheckBox check;
//
//    @FXML
//    Label label1, label2;
//
//    private final Object o;
//    private final InnerAttribute ia;
//    Pane box;
//
//    public ControllerBool(Object o, InnerAttribute ia) {
//        this.o = o;
//        this.ia = ia;
//        try {
//        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
//        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
//        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("booll_settings.fxml"),bundle);
//        fxmlLoader.setRoot(this);
//        fxmlLoader.setController(this);
//
//            box = fxmlLoader.load();
//        } catch (IOException exception) {
//            throw new RuntimeException(exception);
//        }
//    }
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//
//        label1.setText(ia.settingField.title());
//        Field field;
//        try {
//            field = o.getClass().getDeclaredField(ia.fieldName);
//            field.setAccessible(true);
//            Object val = field.get(o);
//            check.setSelected((boolean) val);
//            check.selectedProperty().addListener(new ChangeListener<Boolean>() {
//                @Override
//                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    try {
//                        field.set(o, check.isSelected());
//                    } catch (IllegalAccessException e) {
//                        e.printStackTrace();
//                        DialogFactory.errorDialog(e);
//                    }
//                }
//            });
//            label1.setOnMouseClicked(e -> {
//                check.setSelected(!check.isSelected());
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//            DialogFactory.errorDialog(e);
//        }
//    }
//}
