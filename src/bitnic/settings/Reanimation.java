package bitnic.settings;

public class Reanimation {

    public interface IErrornic {
        void SaveError(String msg);
    }

    public static void setErrorrnic(IErrornic iErrornic) {
        Reanimation.iErrornic = iErrornic;
    }

    public static IErrornic iErrornic;

    private final static boolean isBase = true;

    private static  String filePath = "jdbc:sqlite:omsk24.sqlite";

    public static Object Get(Class aClass) {
        return ReanimatedBase.get(aClass);
    }

    public static void Save(Class aClass) {
        ReanimatedBase.save(aClass);
    }

    public static String GetBaseSettingsPath() {
        return filePath;
    }

    public static void SetBaseSettingsPath(String value) {
        filePath="jdbc:sqlite:"+value;
    }

//    public static void dialogSettinsShow( Class aClass){
//       new Showmen().show(aClass);
//
//
//    }

}
