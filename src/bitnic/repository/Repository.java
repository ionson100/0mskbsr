//package bitnic.repository;
//
//
//import org.apache.log4j.Logger;
//
//import java.lang.reflect.Field;
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.List;
//
//public abstract class Repository<T> implements IClass {
//
//    private static final Logger log = Logger.getLogger(Repository.class);
//    private RepoData repoData;
//
//
//    private static void close(Connection connection) {
//        if (connection != null) {
//            try {
//                connection.close();
//            } catch (SQLException e) {
//                log.error(e);
//            }
//        }
//    }
//
//    public void init() {
//        repoData = RepoReflection.getRepoDate(getClassE());
//        String sql = RepoReflection.getSqlCreatetable(getClass());
//        Connection conn = null;
//        try {
//            conn = DriverManager.getConnection(repoData.basename);
//            conn.createStatement().execute(sql);
//        } catch (SQLException e) {
//            log.error(e);
//        } finally {
//            close(conn);
//        }
//    }
//
//    public Repository() {
//        repoData = RepoReflection.getRepoDate(getClassE());
//
//    }
//
//    public synchronized java.sql.Connection getConnection() {
//        try {
//            return DriverManager.getConnection(repoData.basename);
//        } catch (SQLException e) {
//            log.error(e);
//            return null;
//        }
//    }
//
//
//    public void insert(T t) {
//        Connection connection = getConnection();
//        try {
//            String sql = RepoReflection.getSqlInsert(t.getClass());
//            PreparedStatement pstmt = connection.prepareStatement(sql);
//            doublicate(t, pstmt);
//            pstmt.executeUpdate();
//            pstmt.close();
//        } catch (Exception ex) {
//
//            log.error(ex);
//        } finally {
//            close(connection);
//        }
//    }
//
//    public List<T> getList(String where, Object... params) {
//        Connection connection = getConnection();
//        try {
//            String sql = " select * from '" + repoData.tablename + "' ";
//            if (where == null) {
//
//            } else if (where.trim().length() > 0) {
//                sql = sql + " where " + where + " ";
//            }
//            String ss = sql;
//            PreparedStatement pstmt = connection.prepareStatement(sql);
//            //  pstmt.setInt ( 1,1 );
//            doublicate2(params, pstmt);
//
//            ResultSet resultSet = pstmt.executeQuery();
//            List<T> res = new ArrayList<>();
//            while (resultSet.next()) {
//                T tt = (T) repoData.aClass.newInstance();
//
//                for (Field field : repoData.fieldColunn.keySet()) {
//
//                    repoData.primaryKey.left.set(tt, resultSet.getInt(repoData.primaryKey.right));
//
//                    if (field.getType() == int.class || field.getType() == Integer.class) {
//                        field.set(tt, resultSet.getInt(repoData.fieldColunn.get(field)));
//                    } else if (field.getType() == double.class || field.getType() == Double.class) {
//                        field.set(tt, resultSet.getDouble(repoData.fieldColunn.get(field)));
//                    } else if (field.getType() == float.class || field.getType() == Float.class) {
//                        field.set(tt, resultSet.getFloat(repoData.fieldColunn.get(field)));
//                    } else if (field.getType() == boolean.class || field.getType() == Boolean.class) {
//                        field.set(tt, resultSet.getBoolean(repoData.fieldColunn.get(field)));
//                    } else if (field.getType() == String.class) {
//                        field.set(tt, resultSet.getString(repoData.fieldColunn.get(field)));
//                    } else if (field.getType() == java.util.Date.class) {
//                        //
//                        java.sql.Date resultdate = resultSet.getDate(repoData.fieldColunn.get(field));
//                        if (resultdate == null) {
//                            field.set(tt, null);
//                        } else {
//                            java.util.Date newDate = new java.util.Date(resultdate.getTime());
//                            field.set(tt, newDate);
//                        }
//
//                    }
//
//                }
//
//
//                res.add(tt);
//            }
//            resultSet.close();
//            return res;
//
//
//        } catch (Exception ex) {
//            log.error(ex);
//            return null;
//        } finally {
//            close(connection);
//        }
//
//
//    }
//
//    private void doublicate(T t, PreparedStatement pstmt) throws Exception {
//        int paraams = 1;
//        for (Field field : repoData.fieldColunn.keySet()) {
//            if (field.getType() == int.class || field.getType() == Integer.class) {
//                pstmt.setInt(paraams, field.getInt(t));
//            } else if (field.getType() == double.class || field.getType() == Double.class) {
//                pstmt.setDouble(paraams, field.getDouble(t));
//            } else if (field.getType() == float.class || field.getType() == Float.class) {
//                pstmt.setFloat(paraams, field.getFloat(t));
//            } else if (field.getType() == boolean.class || field.getType() == Boolean.class) {
//                pstmt.setBoolean(paraams, field.getBoolean(t));
//            } else if (field.getType() == String.class) {
//                pstmt.setString(paraams, String.valueOf(field.get(t)));
//            } else if (field.getType() == java.util.Date.class) {
//                Object o = field.get(t);
//                if (o == null) {
//                    pstmt.setDate(paraams, null);
//                } else {
//                    long l = ((java.util.Date) o).getTime();
//                    java.sql.Date d = new java.sql.Date(l);
//                    pstmt.setDate(paraams, d);
//                }
//            }
//            paraams = paraams + 1;
//        }
//    }
//
//    public void update(T t) {
//        Connection connection = getConnection();
//        try {
//            String sql = RepoReflection.getSqlUpdate(t.getClass(), repoData.primaryKey.left.get(t));
//            PreparedStatement pstmt = connection.prepareStatement(sql);
//
//            doublicate(t, pstmt);
//            pstmt.executeUpdate();
//            pstmt.close();
//
//        } catch (Exception ex) {
//            log.error(ex);
//        } finally {
//            close(connection);
//        }
//    }
//
//    public void doublicate2(Object[] params, PreparedStatement pstmt) throws Exception {
//        int i = 0;
//        if (params != null) {
//            for (Object object : params) {
//                if (object instanceof java.util.Date) {
//                    if (object == null) {
//                        pstmt.setObject(++i, null);
//                    } else {
//                        java.sql.Date date = new java.sql.Date(((java.util.Date) object).getTime());
//                        pstmt.setDate(i++, date);
//                    }
//                } else {
//                    pstmt.setObject(++i, object);
//                }
//
//            }
//        }
//    }
//
//    public void freeSql(String sql, Object... params) {
//        Connection connection = getConnection();
//        try {
//
//            PreparedStatement pstmt = connection.prepareStatement(sql);
//
//            doublicate2(params, pstmt);
//
//            pstmt.execute();
//            pstmt.close();
//
//        } catch (Exception ex) {
//            log.error(ex);
//        } finally {
//            close(connection);
//        }
//    }
//
//    public void deleteAllRows() {
//        Connection connection = getConnection();
//        try {
//            PreparedStatement pstmt = connection.prepareStatement("DELETE  FROM '" + repoData.tablename + "'");
//            pstmt.execute();
//            pstmt.close();
//
//        } catch (Exception ex) {
//            log.error(ex);
//        } finally {
//            close(connection);
//        }
//    }
//
//
//    public Object getExecuteScalar(String sql, Object... params) {
//        Connection connection = getConnection();
//        try {
//
//            PreparedStatement pstmt = connection.prepareStatement(sql);
//            doublicate2(params, pstmt);
//            ResultSet resultSet = pstmt.executeQuery();
//            Object res = null;
//            while (resultSet.next()) {
//                res = resultSet.getObject(1);
//            }
//            resultSet.close();
//            pstmt.close();
//            return res;
//
//        } catch (Exception ex) {
//            log.error(ex);
//            return null;
//        } finally {
//            close(connection);
//        }
//    }
//
//    public void insertBulk(List<T> tList) {
//        Connection connection = getConnection();
//        try {
//            String sql = RepoReflection.getSqlBulk((Class<T>) getClass(), tList);
//            log.info(sql);
//            PreparedStatement pstmt = connection.prepareStatement(sql);
//            pstmt.execute();
//            pstmt.close();
//        } catch (Exception ex) {
//
//            log.error(ex);
//        } finally {
//            close(connection);
//        }
//
//    }
//}
//
