package bitnic.table;

import bitnic.table.celldescriptions.ITableColumnCell;

import java.lang.reflect.Field;

/**
 * Created by ion on 13.01.2018.
 */
public class InnerFieldData {


    public ITableColumnCell columnCell;

    public Field field;

    public Field fieldProxy;

    public String name;

    public int index;

    public int width;

    public String dateFormat;


}
