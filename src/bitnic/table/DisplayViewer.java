//package bitnic.table;
//
//import java.lang.annotation.ElementType;
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.lang.annotation.Target;
//
//@Target(value = ElementType.TYPE)
//@Retention(value = RetentionPolicy.RUNTIME)
//public @interface DisplayViewer {
//    String nameTable();
//    int maxWidth() default 800;
//    boolean isShowButton() default false;
//}
