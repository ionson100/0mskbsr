package bitnic.table.celldescriptions;

import bitnic.pagesale.CustomScrollEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

public class TableTestCenter implements ITableColumnCell<Object,Object> {
    private double lastYposition=0d;
   @Override
   public Callback<TableColumn<Object, Object>, TableCell<Object, Object>> getCell () {
       return param ->   new TableCell <Object, Object> (){

           @Override
           protected void updateItem ( Object item , boolean empty ) {
               if(item!=null){

                   setAlignment( Pos.CENTER);
                   setText ( String.valueOf ( item ) );
                   this.setOnMousePressed(new EventHandler<MouseEvent>() {
                       @Override
                       public void handle(MouseEvent event) {
                           lastYposition = event.getSceneY();
                       }
                   });

                   this.setOnMouseDragged(new EventHandler<MouseEvent>() {
                       @Override
                       public void handle(MouseEvent event) {
                           double newYposition = event.getSceneY();
                           double diff = newYposition - lastYposition;
                           lastYposition = newYposition;
                           CustomScrollEvent cse = new CustomScrollEvent();
                           cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
                       }
                   });
               }
           }
       };
   }
}

