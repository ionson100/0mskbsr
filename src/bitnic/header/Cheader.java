package bitnic.header;

import bitnic.core.Controller;
import bitnic.pagehelp.FPageHelp;
import bitnic.utils.Support;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import javafx.util.Duration;
import org.apache.log4j.Logger;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by ion on 20.01.2018.
 */
public class Cheader implements Initializable {
    private static final Logger log = Logger.getLogger(Cheader.class);

    private static Timeline timerPing;
    public static volatile boolean isLiveInternet =true;
    private static final int TIMER_SEC =30;
    public Label label_time;
    public TextField status_app;
    public Label label_user;
    public Button bt_menu_help;
    private static Cheader instance;


    public static void timmerPingStop(){
        timerPing.stop();
    }

    public static void refreshUser(){
        instance.label_user.setText(SettingAppE.getInstance().getUserName());
    }

    public void onAction(ActionEvent actionEvent) {
        String id = "";
        if (actionEvent.getSource() instanceof Node) {
            id = ((Node) actionEvent.getSource()).getId();
        } else if (actionEvent.getSource() instanceof MenuItem) {
            id = ((MenuItem) actionEvent.getSource()).getId();
        }
        if (id.equals("bt_menu_keyboard")) {
           // DialogKeyBoard.show();
            UtilsOmsk.openKeyBoard();
        } else if (id.equals("bt_menu_help")) {

        }else if(id.equals("bt_menu_chaat")){
            Controller.openChat();
        }
    }
   static Date date;
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        SimpleDateFormat formatForTimeNow = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        instance=this;
        bt_menu_help.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Controller.getInstans().factoryPage(new FPageHelp());
               // StarterPage.start();
                //DialogFactory.infoDialog("Страница помощи","Не имеет реализации.");
            }
        });
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    date = new Date();
                    Platform.runLater(() -> label_time.setText(formatForTimeNow.format(date)));
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
            }
        });
        thread.start();
        timerPing = new Timeline(new KeyFrame(Duration.seconds(TIMER_SEC), event -> ping()));
        timerPing.setCycleCount(Timeline.INDEFINITE);
        timerPing.play();
    }

    private synchronized void ping() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Process myProcess = null;
                try{
                    String cmd = "";
                    if(System.getProperty("os.name").startsWith("Windows")) {
                        cmd = "ping -n 5 www.bsr000.net";
                    } else {
                        cmd = "ping -c 5 www.bsr000.net";
                    }

                    myProcess = Runtime.getRuntime().exec(cmd);
                    myProcess.waitFor();

                    if(myProcess.exitValue() == 0) {

                        isLiveInternet =true;
                        Controller.writeMessageEntrnetOk();
                    } else {

                        isLiveInternet =false;
                        Controller.writeMessageEror(Support.str("name301"));
                    }

                } catch( Exception e ) {

                    log.error(e);
                }finally {
                    if(myProcess!=null){
                        myProcess.destroyForcibly();
                    }
                }
            }
        }).start();


    }
}
