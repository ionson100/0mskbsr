package bitnic.pagemessage;

import bitnic.appender.JsonData;
import bitnic.appender.MyWebSocket;
import bitnic.core.Controller;
import bitnic.core.Main;
import bitnic.dialogfactory.DialogFactory;
import bitnic.message.MessageBody;
import bitnic.pagemessage.itemmenu.FItemMenuMessage;
import bitnic.pagesale.MainForm;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.List;

public class PageMessage extends GridPane implements Initializable {

    private static final Logger log = Logger.getLogger(PageMessage.class);
    private boolean isShowAll;
    public ListView<FItemMenuMessage> list_menu;
    public WebView web_view;
    public Button bt_1, bt_2,bt_3;
    public Label label_title;
    List<MessageBody> bodyList;

    private  FItemMenuMessage currentMenuMessage;

    public PageMessage(boolean isShowAll) {
        this.isShowAll = isShowAll;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_page_message.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        UtilsOmsk.painter3d(bt_1,bt_2,bt_3);

        initCore();

        bt_3.setOnAction(event -> {
            Object[] objects=new Object[2];
            objects[0]=currentMenuMessage.getMessageBody().id;
            objects[1]=new Date().getTime();
            JsonData jsonData=new JsonData(MyWebSocket.WRITEMESSAGE, SettingAppE.getInstance().user_id,objects);

            if (Main.myWebSockerError != null) {


                        String json=jsonData.getJson();
                        Main.myWebSockerError.send(json);


            }
        });

        bt_1.setOnAction( event -> {
            isShowAll=true;
            initCore();
        } );

        bt_2.setOnAction( event -> {
            isShowAll=false;
            initCore();
        } );
        //////////////////////////////////////////////////////



        web_view.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>()
        {
            @Override
            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue,
                                Worker.State newValue)
            {
                String toBeopen =web_view.getEngine().getLoadWorker().getMessage().trim();

                log.info(toBeopen);
                if (toBeopen.contains("http://") || toBeopen.contains("https://")) {
                    web_view.getEngine().loadContent(currentMenuMessage.getMessageBody().text, "text/html");
                   // web_view.getEngine().getLoadWorker().cancel();
                    toBeopen=toBeopen.substring(toBeopen.indexOf("http"));
                    try {
                        UtilsOmsk.openKeyBoard();
                        new ProcessBuilder("x-www-browser", toBeopen).start();
                    }
                    catch (Exception e) {
                       log.error(e);
                    }

                }
            }
        });
    }

    public void initCore() {

        currentMenuMessage=null;
        label_title.setText("");
        web_view.getEngine().loadContent("", "text/html");
        list_menu.getItems().clear();
        ObservableList observableList = FXCollections.observableArrayList();

        if(isShowAll){
            bodyList = MessageBody.getMessageBodyList();
        }else {
            bodyList = MessageBody.getMessageBodyNoReadList();
        }
        if(bodyList.size()==0){
 //          bt_1.setVisible(false);
           bt_2.setVisible(false);
            bt_3.setVisible(false);
//            list_menu.setVisible(false);
//            web_view.setVisible(false);

        }else {
            bt_2.setVisible(true);
        }


        Collections.sort(bodyList, new Comparator<MessageBody>() {
            @Override
            public int compare(MessageBody o1, MessageBody o2) {
                if(o1.read==0)return 0;

                return Long.compare(o1.read,o2.read);
            }
        });
        Collections.reverse(bodyList);

        for (MessageBody messageBody : bodyList) {
            FItemMenuMessage message = new FItemMenuMessage(messageBody, new IAction<FItemMenuMessage>() {
                @Override
                public boolean action(FItemMenuMessage fItemMenuMessage) {
                    updateWeb(fItemMenuMessage);
                        return false;
                }
            });
            observableList.add(message);
        }
        list_menu.setItems(observableList);
        list_menu.setCellFactory(list -> new CheckCell());
        if(observableList.size()>0){
            updateWeb((FItemMenuMessage) observableList.get(0));
        }

    }

    void updateWeb(FItemMenuMessage message ){
        currentMenuMessage= message;
        label_title.setText(currentMenuMessage.getMessageBody().title);
        web_view.getEngine().loadContent(currentMenuMessage.getMessageBody().text, "text/html");
        for (FItemMenuMessage fItemMenuMessage : list_menu.getItems()) {
            if(fItemMenuMessage.getMessageBody().id==currentMenuMessage.getMessageBody().id){
                fItemMenuMessage.getStyleClass().clear();
                fItemMenuMessage.setSelect();
            }else {
                fItemMenuMessage.getStyleClass().clear();
                fItemMenuMessage.setNoSelect();
            }
        }
        if(currentMenuMessage.getMessageBody().read==0){
            bt_3.setVisible(true);
        }else {
            bt_3.setVisible(false);
        }
    }

    public void updateRead(double i, long time) {
        for (FItemMenuMessage mes : list_menu.getItems()) {
            if(mes.getMessageBody().id==i){
                mes.updateDate(time);
            }
        }
        Controller.getInstans().factoryPage(new MainForm());
    }

    class CheckCell extends ListCell<FItemMenuMessage> {
        @Override
        public void updateItem(FItemMenuMessage item, boolean empty) {
            super.updateItem(item, empty);
            if(item!=null)
            item.setPrefWidth(list_menu.getWidth()-20);
            setGraphic(item);
        }
    }
}
