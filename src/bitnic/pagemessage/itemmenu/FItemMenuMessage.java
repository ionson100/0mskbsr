package bitnic.pagemessage.itemmenu;

import bitnic.message.MessageBody;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.senders.IAction;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemMenuMessage extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger(FItemMenuMessage.class);

    private Double lastYposition=0d;
    public Label label_title;
    public Label label_read_date;
    private MessageBody messageBody;
    private IAction<FItemMenuMessage> iAction;

    public FItemMenuMessage(MessageBody messageBody, IAction<FItemMenuMessage> iAction) {
        this.messageBody = messageBody;
        this.iAction = iAction;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_menu_message.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        label_title.setText(messageBody.title);
        if (messageBody.read == 0) {
            label_read_date.setText(Support.str("name158"));
        } else {
            String str = UtilsOmsk.simpleDateFormatE(new Date(messageBody.read));
            label_read_date.setText(Support.str("name159") + str);
        }
        this.setOnMouseClicked(event -> {
            if (iAction != null) {
                iAction.action(FItemMenuMessage.this);
            }
        });

        this.setOnMouseEntered(event -> {
            label_title.setStyle("-fx-text-fill: #86d3e3;");
            label_read_date.setStyle("-fx-text-fill: #86d3e3;");
        });

        this.setOnMouseExited(event -> {
            if (this.getStyleClass().toString().equals("menu_message_select")) {
                label_title.setStyle("-fx-text-fill: #86d3e3;");
                label_read_date.setStyle("-fx-text-fill: #86d3e3;");
            } else {
                label_title.setStyle("-fx-text-fill: #253238");
                label_read_date.setStyle("-fx-text-fill: #253238");
            }
        });

        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });
    }

    public MessageBody getMessageBody() {
        return messageBody;
    }

    public void setSelect() {
        this.getStyleClass().clear();
        this.getStyleClass().add("menu_message_select");
        label_title.setStyle("-fx-text-fill: #86d3e3;");
        label_read_date.setStyle("-fx-text-fill: #86d3e3;");
    }

    public void setNoSelect() {
        this.getStyleClass().clear();
        this.getStyleClass().add("menu_message");
        label_title.setStyle("-fx-text-fill: #253238");
        label_read_date.setStyle("-fx-text-fill: #253238");

    }

    public void updateDate(long time) {
        String str = UtilsOmsk.simpleDateFormatE(new Date(time));
        messageBody.read=time;
        label_read_date.setText(Support.str("name159") + str);
    }
}
