package bitnic.sendertoemail;


import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MLogTime;
import bitnic.orm.Configure;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import javafx.concurrent.Task;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SenderToEmail extends Task {

    private static final Logger log = Logger.getLogger(SenderToEmail.class);

    long aLong;
    String path, pathgz;
    private String text;
    private boolean isReal;
    private int log_time;

    public SenderToEmail(String text, boolean isReal, int log_time) {
        this.text = text;
        this.isReal = isReal;
        this.log_time = log_time;
        aLong = new Date().getTime();
        String point = String.valueOf(SettingAppE.getInstance().getPointId());
        String version = SettingAppE.getInstance().getVersion();
        path = System.getProperty("user.home") + File.separator + "point_" + point + "_version_" + version + "_date_" + aLong;
        if (new File(path).mkdir() == false) {
            log.info("not mkdir " + path);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");

        String stime = dateFormat.format(new Date()) + "_logtime_" + String.valueOf(log_time);
        pathgz = System.getProperty("user.home") + File.separator + "point_" + point + "_date_" + stime + ".tar.gz";

        this.setOnSucceeded((e) -> {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        });
    }

    public void stat() {

        int error = 0;
        try {

//            {
//                String p1 = path + File.separator + "settings";
//                File file = new File(p1);
//                file.mkdir();
//                //copyFolder(new File ( Pather.settingsFolder),file );
//                FileUtils.copyDirectory(new File(Pather.settingsFolder), file);
//                log.info("settings ok");
//            }
//            {
//                String p1 = path + File.separator + "omsk";
//                File file = new File(p1);
//                file.mkdir();
//                FileUtils.copyDirectory(new File(System.getProperty("user.home") + File.separator + "omsk"), file);
//
//                log.info("omsk ok");
//            }

//            {
//                String p1 = path + File.separator + "omskdata";
//                File file = new File(p1);
//                file.mkdir();
//                FileUtils.copyDirectory(new File(Pather.curdirdata), file);
//                log.info("omskdata ok");
//            }

//            {
//                File file1 = new File(System.getProperty("user.home") + File.separator + "static");
//                if (file1.exists()) {
//                    String p1 = path + File.separator + "static";
//                    File file = new File(p1);
//                    file.mkdir();
//                    FileUtils.copyDirectory(file1, file);
//                    log.info("static ok");
//                }
//            }
            {
                File file = new File(System.getProperty("user.home") + File.separator + "omsk.log");
                if (file.exists()) {
                    FileUtils.copyFile(file, new File(path + File.separator + "omsk.log"));
                    log.info("omsk.log ok");
                }
            }
            {
                if (DesktopApi.getOs() == DesktopApi.EnumOS.linux) {
                    try {
                        File file = new File("/var/log/syslog");
                        if (file.exists()) {
                            FileUtils.copyFile(file, new File(path + File.separator + "syslog"));
                            log.info("omsk.log ok");
                        }
                    } catch (Exception ex) {
                        log.error(ex);
                    }

                    try {
                        File file = new File(System.getProperty("user.home") + "/.atol/drivers9/logs/fptr_log.txt");
                        if (file.exists()) {
                            FileUtils.copyFile(file, new File(path + File.separator + "fptr_log.txt"));
                            log.info("omsk.log ok");
                        }
                    } catch (Exception ex) {
                        log.error(ex);
                    }
                }


            }
//            {
//                File file = new File(System.getProperty("user.home") + File.separator + Pather.basenameArchive + ".sqlite");
//                if (file.exists()) {
//                    FileUtils.copyFile(file, new File(path + File.separator + "checkarchive.sqlite"));
//                    log.info("checkarchive.sqlite ok");
//                }
//
//            }
//            {
//                File file = new File(System.getProperty("user.home") + File.separator + ".ftp");
//                if (file.exists()) {
//                    FileUtils.copyFile(file, new File(path + File.separator + ".ftp"));
//                    log.info(".ftp ok");
//                }
//            }

//            {
//                File file = new File(Pather.checkStory);
//                if (file.exists()) {
//                    FileUtils.copyFile(file, new File(path + File.separator + "storycheck.txt"));
//                    log.info("storycheck ok");
//                }
//            }

            UtilsOmsk.createTarGZFolder(path, pathgz);

            log.info("start send.");
            String host = "smtp.mail.ru";
            String port = "587";
            String mailFrom = "bitnic.development@mail.ru";
            String password = "312873312873ion100";

            // message info
            String mailTo = "bitnic.development@mail.ru";
            String subject = "точка " + SettingAppE.getInstance().getPointId();
            String message = text;

            String[] attachFiles = new String[1];
            attachFiles[0] = pathgz;


            EmailAttachmentSender.sendEmailWithAttachments(host, port, mailFrom, password, mailTo, subject, message, attachFiles);
            log.info("Email sent.");


        } catch (Exception e) {
            error = 1;
            if (isReal) {
                DialogFactory.errorDialog(e.getMessage());
            }

            log.error(e);
        }
        if (error == 0) {
            if (isReal) {
                DialogFactory.infoDialog("Отправка ошибок", "Ошибки отправлены.");
            } else {
                if (log_time > 0) {
                    Configure.getSession().deleteTable("log_time");
                    MLogTime mLogTime = new MLogTime();
                    mLogTime.timers = log_time;
                    Configure.getSession().insert(mLogTime);
                }
            }
            // удаление не нужных файлов
            try {
                FileUtils.deleteDirectory(new File(path));
                new File(pathgz).delete();
            } catch (Exception ignored) {

            }

        }

    }


    @Override
    public Object call() {
        stat();
        return null;
    }
}
