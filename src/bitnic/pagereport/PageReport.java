package bitnic.pagereport;

import bitnic.core.Controller;
import bitnic.model.MError;
import bitnic.model.MTransactions;
import bitnic.orm.Configure;
import bitnic.pagetableproduct.IFocusable;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class PageReport extends GridPane implements Initializable ,IFocusable,IRefreshLoader{
    private static final Logger log = Logger.getLogger(PageReport.class);
    public TextField tf_search;

    public HBox vbox_search;
    public CheckBox cb_autoupdate;


    public TableView table_error;
    public GridPane grid1;

    List <MError> list = new ArrayList <> ();

    public PageReport() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_page_report.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if ( SettingAppE.getInstance ().isTypeЫShow3D) {
            vbox_search.getStyleClass ().add ( "footer_3d" );
        } else {
            vbox_search.getStyleClass ().add ( "footer" );
        }
        UtilsOmsk.resizeNode ( this , grid1 );
        UtilsOmsk.painter3d ( tf_search );
        refresh();

    }

    @Override
    public void focus() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tf_search.requestFocus();
            }
        });
    }

    @Override
    public void refresh() {
        list.clear();
        table_error.getItems().clear();

        for (MTransactions tt : Configure.getSession().getList(MTransactions.class, null)) {
            MError er = new MError ();
            er.msg = tt.str;
            list.add ( er );
        }

        log.info ( "Строк  - "+list.size () );
        new BuilderTable().build( list , table_error );
        table_error.scrollTo(table_error.getItems ().size()-1);
        Controller.writeMessage("Файл отчета");
        tf_search.textProperty ().addListener ((observable, oldValue, newValue) -> {
            UtilsOmsk.searchErrorMessage(newValue, list, table_error);
        });
        focus ();
    }
}
