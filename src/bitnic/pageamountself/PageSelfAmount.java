package bitnic.pageamountself;


import bitnic.checkarchive.AmountSelfTotal;
import bitnic.core.Controller;
import bitnic.orm.Configure;
import bitnic.pageerror.PageError;
import bitnic.pagetableproduct.IFocusable;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.utils.UtilsOmsk;
import com.sun.javafx.scene.control.skin.TableViewSkin;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class PageSelfAmount extends GridPane implements Initializable, IRefreshLoader,IFocusable {

    private static final Logger log = Logger.getLogger(PageError.class);

    private List<AmountSelfTotal> list;
    public HBox hbox;
    public TextField tf_search;
    public Button bt_top, bt_down;
    private  final AtomicInteger  i = new AtomicInteger(0);
    private ScrollBar verticalBar;
    private ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
    private ScheduledExecutorService serviceapp = Executors.newSingleThreadScheduledExecutor();


    public TableView table_view;

    public PageSelfAmount() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_page_self_amount.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (SettingAppE.getInstance().isTypeЫShow3D) {
            hbox.getStyleClass().add("footer_3d");
        } else {
            hbox.getStyleClass().add("footer");
        }

        UtilsOmsk.painter3d ( tf_search );

        refresh();

        Controller.writeMessage("Продажи ");

        tf_search.textProperty().addListener((observable, oldValue, newValue) -> {
            List<AmountSelfTotal> cores = new ArrayList<>();
            for (AmountSelfTotal core : list) {
                if(core.name_prod==null) continue;
                if (core.name_prod.toLowerCase(Locale.ROOT).contains(newValue.toLowerCase(Locale.ROOT))) {
                    cores.add(core);
                    continue;
                }
                if (String.valueOf(core.prise).contains(newValue)) {
                    cores.add(core);
                    continue;
                }
            }
            table_view.getItems().clear();
            Controller.writeMessage("Продажи " + String.valueOf(cores.size()));
            new BuilderTable().build(cores, table_view);
            serviceapp.schedule(new RunnerApp(), 2, TimeUnit.SECONDS);
        });
        focus();



        bt_down.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {

            ScheduledFuture<?> h = null;

            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    h = service.scheduleAtFixedRate(new Runner(true), 0, UtilsOmsk.millsecButtonScroll, TimeUnit.MILLISECONDS);
                } else if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if(h!=null){
                        h.cancel(false);
                    }

                }
            }
        });


        bt_top.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {

            ScheduledFuture<?> h = null;

            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    h = service.scheduleAtFixedRate(new Runner(false), 0, UtilsOmsk.millsecButtonScroll, TimeUnit.MILLISECONDS);
                } else if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if(h!=null){
                        h.cancel(false);
                    }

                }
            }
        });
    }


    @Override
    public void refresh() {
        tf_search.setText("");
        list = Configure.getSession().getList(AmountSelfTotal.class," session_id = ?",SettingAppE.getSession());
        if(list==null)return;
        for (AmountSelfTotal as : list) {
            as.summ=as.amount*as.prise*-1;
        }
        new BuilderTable().build(list, table_view);
        Controller.writeMessage("Продажи ");
        serviceapp.schedule(new RunnerApp(), 2, TimeUnit.SECONDS);
    }

    @Override
    public void focus() {
        Platform.runLater(() -> tf_search.requestFocus());
    }




    void app() {
        if(verticalBar==null){
            verticalBar = (ScrollBar) table_view.lookup(".scroll-bar:vertical");
            if (verticalBar != null) {
                verticalBar.valueProperty().addListener((ObservableValue<? extends Number> obs, Number oldValue, Number newValue) -> {
                    VirtualFlow<?> vf = (VirtualFlow<?>) ((TableViewSkin<?>) table_view.getSkin()).getChildren().get(1);
                    i.set(vf.getFirstVisibleCell().getIndex());
                    System.out.println("event " + i);
                });
            }

        }



    }
    class Runner implements Runnable {

        private boolean isAdd;

        public Runner(boolean isAdd) {

            this.isAdd = isAdd;
        }

        @Override
        public void run() {
            if (isAdd) {
                if (i.get() < table_view.getItems().size())
                    i .getAndIncrement();
            } else {
                if (i.get() > 0)
                    i .decrementAndGet();
            }

            Platform.runLater(() -> {
                table_view.scrollTo(i);
            });

        }
    }

    class RunnerApp implements Runnable {

        @Override
        public void run() {

            app();
        }
    }
}

