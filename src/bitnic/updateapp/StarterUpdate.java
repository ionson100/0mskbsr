package bitnic.updateapp;

import bitnic.core.Controller;
import bitnic.core.Main;
import bitnic.partfilesender.Downloader;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import javafx.scene.control.Button;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StarterUpdate {
    private static final Logger log = Logger.getLogger(StarterUpdate.class);


    public static void checkUpdate(Controller controller) {


        //controller.addButtonMessage();


        String patch = Pather.curdir + File.separator + "static/share/app";
        File file = new File(patch);
        if (file.exists() == false) {
            log.info("Обновление Папки: static/share/app не обнаружено");
            return;
        }

        File[] files = file.listFiles();
        if(files==null)
            return;
        List<File> filesApp = new ArrayList<>();
        for (File f : files) {
            int i = f.getName().lastIndexOf('.');
            if (i > 0) {

                if (DesktopApi.getOs().isLinux()) {
                    if (f.getName().indexOf("tar.gz") != -1) {
                        filesApp.add(f);
                    }
                } else {
                    if (f.getName().indexOf("rar") != -1) {
                        filesApp.add(f);
                    }
                }
            }
        }
        if (filesApp.size() == 0) {
           // log.info("Обновление Файлы для обновления отсутствуют");
            return;
        }
        if(SettingAppE.getInstance().user_id==null){
            return;
        }
        if(SettingAppE.getInstance().user_id==null){
            return;
        }

            for (File f : filesApp) {
                if(f.getName().contains(Downloader.HLAM)){
                    return;
                }
                String name = f.getName().replace(".rar", "").replace(".tar.gz", "");
                try {

                    int cur = Integer.parseInt(SettingAppE.getInstance().getVersion().replace(".", ""));
                    int app = Integer.parseInt(name.replace(".", ""));
                    if (cur < app) {
                        Button button = new Button("Обновление");
                        button.getStyleClass().add("update_button");
                        button.setUserData(Main.BUTTONUPDATE);
                        button.setOnAction(event -> controller.onUpdateApp());
                        controller.panel_buttons.getChildren().add(button);
                        button.setPrefHeight(controller.panel_buttons.getPrefHeight());
                        if (SettingAppE.getInstance().user_id.trim().equals("1")||SettingAppE.getShowButtonUpdate()==1) {
                            button.setVisible(true);
                        }else {
                            button.setVisible(false);
                        }

                        break;
                    }
                } catch (Exception e) {
                    log.error(e);
                }
            }



    }


}
