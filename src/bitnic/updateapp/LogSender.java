package bitnic.updateapp;

import bitnic.model.MLogTime;
import bitnic.orm.Configure;
import bitnic.sendertoemail.SenderToEmail;
import org.apache.log4j.Logger;

import java.util.List;

public class LogSender {

    private static final Logger log = Logger.getLogger(LogSender.class);

    public static void logsender( int log_time){

        log.info("log_time *********************** "+String.valueOf(log_time));




        if(log_time==0) return;
        List<MLogTime>  logTimes= Configure.getSession().getList(MLogTime.class,null);
        if(logTimes.size()>0){
            if(logTimes.get(0).timers==log_time){
                return;
            }else {
                sender(log_time);
            }
        }else {
            sender(log_time);
        }
    }
    private static void sender(int t){

        new Thread(new SenderToEmail("Фоновый запрос",false,t)).start();
    }
}
