package bitnic.updateapp;

import com.github.junrar.Archive;
import com.github.junrar.rarfile.FileHeader;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ExtractArchive {

    private static final Logger log = Logger.getLogger ( ExtractArchive.class );

    public static void extractArchive(String archive , String destination ) {
        if ( archive == null || destination == null ) {
            throw new RuntimeException ( "archive and destination must me set" );
        }
        File arch = new File ( archive );
        if ( !arch.exists () ) {
            throw new RuntimeException ( "the archive does not exit: " + archive );
        }
        File dest = new File ( destination );
        if ( dest.exists () == false ) {
            if(dest.mkdir ()==false){
                log.info("not mkdir "+destination);
            }
        }
        if ( !dest.exists () || !dest.isDirectory () ) {
            throw new RuntimeException (
                    "the destination must exist and point to a directory: "
                            + destination );
        }
        extractArchive( arch , dest );
    }



    public static void extractArchive(File archive , File destination ) {
        Archive arch = null;
        try {
            arch = new Archive ( archive );
        } catch (Exception e) {

            log.error ( e );
        }
        if ( arch != null ) {
            if ( arch.isEncrypted () ) {

                return;
            }
            FileHeader fh = null;
            while (true) {
                fh = arch.nextFileHeader ();
                if ( fh == null ) {
                    break;
                }
                if ( fh.isEncrypted () ) {

                    continue;
                }

                try {
                    if ( fh.isDirectory () ) {
                        createDirectory ( fh , destination );
                    } else {
                        File f = createFile ( fh , destination );
                        try(OutputStream stream = new FileOutputStream ( f )){
                            arch.extractFile ( fh , stream );
                            stream.close ();
                        }

                    }
                } catch (Exception e) {

                    log.error ( e );
                }
            }
        }
    }

    private static File createFile ( FileHeader fh , File destination ) {
        File f = null;
        String name = null;
        if ( fh.isFileHeader () && fh.isUnicode () ) {
            name = fh.getFileNameW ();
        } else {
            name = fh.getFileNameString ();
        }
        f = new File ( destination , name );
        if ( !f.exists () ) {
            try {
                f = makeFile ( destination , name );
            } catch (IOException e) {
                log.error ( e );
            }
        }
        return f;
    }

    private static File makeFile ( File destination , String name )
            throws IOException {
        String[] dirs = name.split ( "\\\\" );
//        if ( dirs == null ) {
//            return null;
//        }
        StringBuilder path = new StringBuilder();
        int size = dirs.length;
        if ( size == 1 ) {
            return new File ( destination , name );
        } else if ( size > 1 ) {
            for (int i = 0; i < dirs.length - 1; i++) {
               path.append(File.separator).append(dirs[i]);
                new File ( destination , path.toString() ).mkdir ();
            }
            path.append(File.separator).append(dirs[dirs.length - 1]);
            File f = new File ( destination , path.toString() );
            f.createNewFile ();
            return f;
        } else {
            return null;
        }
    }

    private static void createDirectory ( FileHeader fh , File destination ) {
        File f;
        if ( fh.isDirectory () && fh.isUnicode () ) {
            f = new File ( destination , fh.getFileNameW () );
            if ( !f.exists () ) {
                makeDirectory ( destination , fh.getFileNameW () );
            }
        } else if ( fh.isDirectory () && !fh.isUnicode () ) {
            f = new File ( destination , fh.getFileNameString () );
            if ( !f.exists () ) {
                makeDirectory ( destination , fh.getFileNameString () );
            }
        }
    }

    private static void makeDirectory ( File destination , String fileName ) {
        String[] dirs = fileName.split ( "\\\\" );
//        if ( dirs == null ) {
//            return;
//        }
        StringBuilder path = new StringBuilder();
        for (String dir : dirs) {
            path.append(File.separator).append(dir);
            new File ( destination , path.toString() ).mkdir ();
        }

    }
}
