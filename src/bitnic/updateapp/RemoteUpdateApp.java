package bitnic.updateapp;

import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import org.apache.log4j.Logger;

import java.io.File;

public class RemoteUpdateApp {

    private static final Logger log = Logger.getLogger(RemoteUpdateApp.class);

    public static synchronized void startUpdateApp(String name,int size){
        log.info("Удаленное обновление программы  ->>>>>>>>>>> name: "+String.valueOf(name)+" размер: "+String.valueOf(size));
        if(name==null||name.trim().length()==0) return;
        if(size<=0) return;
        if(DesktopApi.getOs()!=DesktopApi.EnumOS.linux)return;
        File file=new File("/home/bsr/static/share/app/"+name);
        if(file.exists()==false) return;
        if(file.length()!=size) {

            log.error("Обновление удаленное, файл не соответствует размеру, будет удален: "+String.valueOf(file.length())+" "+String.valueOf(size));
            file.delete();
            return;

        }
        String cur_version= SettingAppE.getVersion();
        String ver=name.replace(".tar.gz","");
        if(cur_version.trim().equals(ver.trim())==true) {
            log.error("Обновление удаленное, текущая версия программы совпадаетс загружаемой : "+ cur_version+" = "+ver);
            return;
        }
        log.error("Инфо: Запускаем скрипт обновления программы , текущая версия: "+ver+" рекомендованная: "+name+" размер: то"+size);
        RollBackApp.updateCoreLinux2(file,log);
    }
}
