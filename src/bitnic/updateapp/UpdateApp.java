package bitnic.updateapp;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.partfilesender.Downloader;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UpdateApp {
    private static final Logger log = Logger.getLogger(UpdateApp.class);

    public void run(Controller controller) {
        String patch = Pather.curdir + File.separator + "static/share/app";
        File file = new File(patch);
        if (file.exists() == false) {
            DialogFactory.infoDialog("Обновление", "Папки: static/share/app не обнаружено\nОбновление прервано!");
            return;
        }

        File[] files = file.listFiles();
        if(files==null){
            log.error(" not files");
            return;
        }
        List<File> filesApp = new ArrayList<>();
        int curVersion = 100000;

        curVersion = Integer.parseInt(SettingAppE.getInstance().getVersion().replace(".", ""));

        for (File f : files) {
            if (f.getName().contains(Downloader.HLAM)) continue;
            int i = f.getName().lastIndexOf('.');
            if (i > 0) {
                String ext = f.getName();
                System.out.println(ext);
                if (DesktopApi.getOs().isLinux()) {
                    if (ext.indexOf(".tar.gz") != -1) {

                        String ss = f.getName().replace(".tar.gz", "").replace(".", "");
                        int ver = Integer.parseInt(ss);
                        if (curVersion < ver) {
                            filesApp.add(f);
                        }
                    }
                } else {
                    if (ext.indexOf(".rar") != -1) {
                        String ss = f.getName().replace(".rar", "").replace(".", "");
                        int ver = Integer.parseInt(ss);
                        if (curVersion < ver) {
                            filesApp.add(f);
                        }
                    }

                }
            }
        }
        if (filesApp.size() == 0) {
            DialogFactory.infoDialog("Обновление", "Файлы для обновления отсутствуют");
            return;
        }
        DialogFactory.rollBackDialog(filesApp, o -> {

            if (DesktopApi.getOs().isLinux()) {
                RollBackApp.updateCoreLinux((File) o, log);
            } else {
                RollBackApp.updateCoreWindows((File) o, log);
            }

            return false;
        });
    }
}
