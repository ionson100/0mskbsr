package bitnic.updateapp;

import bitnic.core.Controller;
import bitnic.core.Main;
import bitnic.message.MessageBody;
import bitnic.partfilesender.Downloader;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.WorkingFile;
import javafx.application.Platform;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class TimerAppUpdate {


    public final static String hlam = ".hlam";
    private static final Logger log = Logger.getLogger(TimerAppUpdate.class);
    Timer timer;
    private ExecutorService service = Executors.newFixedThreadPool(10);
    private Controller controller;

    public void run(int timerSec, Controller controller) {

        //timerSec=5;
        this.controller = controller;

        timer = new Timer();

        timer.schedule(new RemindTask(),
                5000,
                timerSec * 1000);

    }

    public void stop() {
        timer.cancel();
        service.shutdown();
    }

    public synchronized void activate() {

        Main.myWebSockerError.sendPing ();
        WorkingFile.sendBitnic(null);

        //if (SettingAppE.getInstance().user_id.equals(SettingAppE.UniversalUserKey)) return;
        if (SettingAppE.getInstance().getPointId() <= 0) return;

        //log.error("ИНФО: запрос на файлы для точки");
        InputStreamReader ee = null;
        BufferedReader reader = null;
        HttpsURLConnection conn = null;

        try {
            String user_id= SettingAppE.getInstance().user_id;
            if (SettingAppE.getInstance().user_id == null) {
                user_id="1";
            };
            String idpoint = String.valueOf(SettingAppE.getInstance().getPointId());
            String version = SettingAppE.getInstance().getVersion();
            URL url = new URL("https://" + SettingAppE.getInstance().getUrl() + "/pos_info?point=" + idpoint + "&ver=" + version + "&trader=" + user_id);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(15000 /*milliseconds*/);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            conn.connect();
            final int status = conn.getResponseCode();
            if (status != 200) {
                log.error("Запрос файлов: ответ " + String.valueOf(status) + " " + url.toString());
                return;
            }
            log.info("Запрос файлов для точки: ответ " + String.valueOf(status) + " " + url.toString());
            int ch;

            StringBuilder sb = new StringBuilder();

            ee = new InputStreamReader(conn.getInputStream(), "UTF-8");
            reader = new BufferedReader(ee);
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }

            String ssres = sb.toString();
            if(ssres.trim().length()==0) return;

            BodyFiles bodyFiles = UtilsOmsk.getGson().fromJson(ssres, BodyFiles.class);

            for (String file : bodyFiles.files) {
                String s = file.replace("https://", "");
                s = s.substring(s.indexOf("/"));
                if (DesktopApi.getOs() == DesktopApi.EnumOS.linux) {
                    s = Pather.curdir + File.separator + s;
                } else {
                    s = Pather.curdir + s;
                }
                if (service.isShutdown() == false) {
                    service.submit(new Downloader().setUrl(file).setPath(s));//.execute ();
                }
            }

            try {
                for (MessageBody directive : bodyFiles.directives) {
                    directive.user_id = SettingAppE.getInstance().user_id;
                    directive.insert(directive);
                }

                Requests.add(bodyFiles.requests);
                Platform.runLater(() -> Controller.getInstans().addButtonMessage());
            }catch (Exception ex){
                log.error(ex);
            }


            try{
                RemoteUpdateApp.startUpdateApp(bodyFiles.new_ver,bodyFiles.new_ver_size);//
            }catch (Exception ex){
                log.error(ex);
            }
            try{
                LogSender.logsender(bodyFiles.log_time);
            }catch (Exception ex){
                log.error(ex);
            }


        } catch (Exception ex) {
            log.error(ex);
            // Controller.writeMessage(ex.getMessage());

        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            try {
                if (ee != null) {
                    ee.close();
                }
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception ex) {
                log.error(ex);
            }

        }
    }


    public class BodyFiles {
        // команда отрпавить лог на сервер
        public int log_time;

        // название файла архива обновления
        public String new_ver;

        // размер архива
        public int new_ver_size;

        public List<String> files = new ArrayList<>();

        public List<MessageBody> directives = new ArrayList<>();

        public List<Requests> requests = new ArrayList<>();//электрики
    }

    private class RemindTask extends TimerTask {
        public void run() {
            activate();
        }
    }


}