package bitnic.updateapp;

import bitnic.orm.Column;
import bitnic.orm.Configure;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;


import java.util.List;

// сообшение электику


@Table( "userto" )
public class Requests  {
    @PrimaryKey("_id")
    public int _id;// primary key
    @Column("id")
    public int id;
    @Column("name")
    public String name;
    public static void add(List<Requests> req) {
        Configure.getSession().deleteTable("userto");
        Configure.bulk(Requests.class,req,Configure.getSession());
    }

}
