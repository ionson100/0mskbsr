//package bitnic.blenderloader;
//
//import bitnic.core.Controller;
//import bitnic.dialogfactory.DialogBase;
//import bitnic.senders.IAction;
//import javafx.application.Platform;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Node;
//import javafx.scene.control.Button;
//import org.apache.log4j.Logger;
//
//import java.io.IOException;
//
//public class FactoryBlenderBank {
//
//    public Button bt_close;
//    private static final Logger log = Logger.getLogger(FactoryBlender.class);
//    static FBlenderLoader cur_node2;
//    public static void AddMessage(String msg){
//        Platform.runLater(new Runnable() {
//            @Override
//            public void run() {
//                if(cur_node2!=null){
//                    cur_node2.label_message.setText(cur_node2.label_message.getText()+System.lineSeparator()+msg);
//                }
//            }
//        });
//
//    }
//
//    public static void Run(IAction actionButton) {
//        if(cur_node2!=null){
//            stop();
//        }
//        FXMLLoader loader = new FXMLLoader(FactoryBlenderBank.class.getResource("f_loader_bank.fxml"));
//        Node pane = null;
//        try {
//            pane = loader.load();
//        } catch (IOException e) {
//            e.printStackTrace();
//            log.error(e);
//        }
//        FBlenderLoader loader2 = loader.getController();
//        cur_node2 = loader2;
//        cur_node2.bt_close.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent event) {
//                if(actionButton!=null){
//                    actionButton.action(event.getSource());
//                }
//            }
//        });
//
//        Controller.Instans.stack_panel.getChildren().add(pane);
//    }
//
//
//
//    public static void stop() {
//        try {
//            Platform.runLater(() -> {
//                if (cur_node2 != null) {
//                    cur_node2.close();
//                    cur_node2 = null;
//                }
//            });
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            log.error(ex);
//        }
//    }
//
//
//}
//
