package bitnic.blenderloader;

import bitnic.dialogfactory.DialogBase;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class FBlenderLoader implements Initializable {


    private static final Logger log = Logger.getLogger(FBlenderLoader.class);
    public Button bt_close;
    public Label label_message,label_timer;
    private Timer timer;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
       // String dd=SettingAppE.getInstance().user_id;
        if(SettingAppE.getInstance().user_id.equals("1")){
            bt_close.setVisible(true);
            bt_close.setOnAction(e -> {
                log.error("Продавец воспользовалась кнопкой аварийной отмены ожидания конца операции");
                close();
            });
        }else {
            bt_close.setVisible(false);
        }

        timer = new Timer ();
        timer.schedule(new TimerTask() {
            int id= UtilsOmsk.START_TIMER_BLENDER_VALUE;
                           @Override
                           public void run() {
                               id=id-1;
                               if(id<1){
                                   Platform.runLater(() -> close());
                                   log.error("Сработка таймера блендора");
                               }
                               Platform.runLater(() -> label_timer.setText(String.valueOf(id)));
                           }
                       }, 0,1000);
    }

    public void close() {

        if(timer!=null){
            timer.cancel();
        }
        DialogBase.deleteBlenda();
    }

}
