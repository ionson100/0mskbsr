package bitnic.blenderloader;

import bitnic.core.Controller;
import bitnic.core.Main;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class FactoryBlender {



    private static final Logger log = Logger.getLogger(FactoryBlender.class);
    static FBlenderLoader cur_node2;
    public static void addMessage(String msg){
        try{
            Platform.runLater(() -> {
                if(cur_node2!=null){
                    cur_node2.label_message.setText(cur_node2.label_message.getText()+System.lineSeparator()+msg);
                }
            });
        }catch (Exception ex){
            log.error(ex);
        }
    }

    public static void run() {
        if(cur_node2!=null){
            stop();
        }
        FXMLLoader loader = new FXMLLoader(FBlenderLoader.class.getResource("f_loader.fxml"), Main.myBundle);
        Node pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e);
        }
        FBlenderLoader loader2 = loader.getController();
        cur_node2 = loader2;
        if(Controller.getInstans()!=null&&Controller.getInstans().stack_panel!=null&&pane!=null){
            Controller.getInstans().stack_panel.getChildren().add(pane);
        }

    }

    public static void stop() {
        try {
            Platform.runLater(() -> {
                if (cur_node2 != null) {
                    cur_node2.close();
                    cur_node2 = null;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(ex);
        }
    }


}

