package bitnic.pagesale.stacksearcher;

import bitnic.core.Controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import bitnic.model.MProduct;
import bitnic.pagesale.stacksearcher.itenstacksearcher.FItemStackSearcher;
import bitnic.senders.IAction;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FStackSearcher extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger ( FStackSearcher.class );
    public ListView list_21;
    GridPane pane;
    private IAction addIAction;

    public FStackSearcher(IAction addIAction) {
        this.addIAction = addIAction;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_stack_searcher.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException(exception);
        }
    }

    ObservableList observableList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        list_21.setCellFactory((Callback<ListView<MProduct>, ListCell<MProduct>>) list -> new CheckCell());
        list_21.setItems(observableList);
    }

    public void setSource(List<MProduct> mProducts) {
        observableList.clear();
        for (MProduct mProduct : mProducts) {
            if (mProduct.price == 0) continue;
            observableList.add(mProduct);
        }

        Controller.writeMessage("найдено продуктов - " + list_21.getItems().size());
        //System.out.println("найдено продуктов - " + list_21.getItems().size());
    }

    class CheckCell extends ListCell<MProduct> {


        @Override
        public void updateItem(MProduct item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemStackSearcher check = new FItemStackSearcher(item, addIAction);
                check.setPrefWidth(list_21.getWidth());
                setGraphic(check);
            }
        }
    }
}
