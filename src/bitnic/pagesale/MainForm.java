package bitnic.pagesale;


import bitnic.core.Controller;
import bitnic.core.IDestroy;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.keyboard.KeyKeyB;
import bitnic.model.MBarcode;
import bitnic.model.MProduct;
import bitnic.model.MTaxrates;
import bitnic.orm.Configure;
import bitnic.pagesale.blender.FUserBlender;
import bitnic.pagetableproduct.IFocusable;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Transaction;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.WatcherScaner;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class MainForm extends GridPane implements Initializable, IDestroy, IFocusable, IRefreshUser, ICleatTextField {

    private final String err = "Товар: %s \nимеет нулевое или отрицательное значение количества товара.\nПечать отменена";
    private ResourceBundle bundle;
    private static final Logger log = Logger.getLogger(MainForm.class);
    public StackPane stack21, stack22;
    public Label label_itogo_amount, label_itogo_summ;

    public WatcherScaner watcherScaner;
    public static KeyKeyB keyKeyB;
    private TextField currentTextField;
    public GridPane grid_delivery_host, grid_buttons_host, grid_blue_top, grid_blue_bottom, grid_pane_itogo;
    public static MainForm cmainform;
    public TextField kb_text_field;
    public Button bt_clear, bt_50, bt_100, bt_200, bt_500, bt_1000, bt_2000, bt_5000;
    private Button bt_print_check, bt_print_check2;
    public TextField tb_get_many, tb_delivery, tb_check_summ;
    public GridPane pane;
    public GridPane grid1;
    public ListView box;
    public Button bt_2;
    public Label label_nds;
    public Label label_nds2;
    public GridPane keyboard;

    public MainForm() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_sale.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
        cmainform = this;
    }

    List<MBarcode> mBarcodes = new ArrayList<>();
    StringBuilder s = new StringBuilder("");


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Controller.writeMessage(Support.str("name1"));

        this.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                refreshKeyBoard(event.getText());
            }
        });
//                .addEventFilter(KeyEvent.KEY_PRESSED,
//                event -> {
//                    System.out.println("Pressed: " + event.getCode());
//                    if (Controller.Ccmainform != null) {
//                        Controller.Ccmainform.refreshKeyBoard(event.getText());
//                    }
//                });


        bundle = resources;
        String css = "-fx-font-size: " + SettingAppE.getInstance().fontSizeDelivery;

        tb_get_many.setStyle(css);
        tb_delivery.setStyle(css);
        tb_check_summ.setStyle(css);

        if (SettingAppE.getInstance().isTypeЫShow3D) {//button_delivery_clear,button_key_big
            tb_get_many.getStyleClass().add("delivery_3d");
            tb_check_summ.getStyleClass().add("delivery_3d");
            tb_delivery.getStyleClass().add("delivery_3d");
            bt_clear.getStyleClass().add("button_delivery_clear_3d");
            bt_clear.getStyleClass().add("button_key_big_3d");
            grid_blue_top.getStyleClass().add("left_grid_head_3d");
            grid_blue_bottom.getStyleClass().add("left_grid_head_3d");
            grid_pane_itogo.getStyleClass().add("left_grid_head_3d");

            bt_clear.getStyleClass().add("button_key_big_3d");


            bt_50.getStyleClass().add("button_key_big_3d");
            bt_100.getStyleClass().add("button_key_big_3d");
            bt_200.getStyleClass().add("button_key_big_3d");
            bt_500.getStyleClass().add("button_key_big_3d");
            bt_1000.getStyleClass().add("button_key_big_3d");
            bt_2000.getStyleClass().add("button_key_big_3d");
            bt_5000.getStyleClass().add("button_key_big_3d");

            bt_50.setText("50");
            bt_100.setText("100");
            bt_200.setText("200");
            bt_500.setText("500");
            bt_1000.setText("1000");
            bt_2000.setText("2000");
            bt_5000.setText("5000");


        } else {
            bt_50.getStyleClass().add("button_money_50");
            bt_100.getStyleClass().add("button_money_100");
            bt_200.getStyleClass().add("button_money_200");
            bt_500.getStyleClass().add("button_money_500");
            bt_1000.getStyleClass().add("button_money_1000");
            bt_2000.getStyleClass().add("button_money_2000");
            bt_5000.getStyleClass().add("button_money_5000");

            bt_clear.getStyleClass().add("button_delivery_clear");
            grid_blue_top.getStyleClass().add("left_grid_head");
            grid_blue_bottom.getStyleClass().add("left_grid_head");
            grid_pane_itogo.getStyleClass().add("ritch_grid_hed");
        }

        bt_50.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButton);
        bt_100.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButton);
        bt_200.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButton);
        bt_500.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButton);
        bt_1000.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButton);
        bt_2000.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButton);
        bt_5000.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButton);

        label_itogo_amount.setText("");
        label_itogo_summ.setText("");

        keyKeyB.iActionDeletecheck = s -> {
            if (SettingAppE.getInstance().selectProduct.size() > 0) {
                DialogFactory.questionDialog(bundle.getString("name114"),
                        bundle.getString("name115"), new ButtonAction("Да", new IAction<Object>() {
                            @Override
                            public boolean action(Object o) {
                                SettingAppE.getInstance().selectProduct.clear();
                                SettingAppE.save();
                                refreshCheck();
                                return false;
                            }
                        }));
            }


            return false;
        };

        keyKeyB.iActionKeyPress = s -> {
            switch (s) {
                case "0": {
                    printableDelivery("0");
                    break;
                }
                case "1": {

                    printableDelivery("1");

                    break;
                }
                case "2": {
                    printableDelivery("2");
                    break;
                }
                case "3": {
                    printableDelivery("3");
                    break;
                }
                case "4": {
                    printableDelivery("4");
                    break;
                }
                case "5": {
                    printableDelivery("5");
                    break;
                }
                case "6": {
                    printableDelivery("6");
                    break;
                }
                case "7": {
                    printableDelivery("7");
                    break;
                }
                case "8": {
                    printableDelivery("8");
                    break;
                }
                case "9": {
                    printableDelivery("9");
                    break;
                }
                case "88": {
                    if (currentTextField.getText().length() > 0) {
                        currentTextField.setText(currentTextField.getText().substring(0, currentTextField.getText().length() - 1));
                    } else {
                        watcherScaner.clear();
                    }

                    break;
                }

                case "888": {
                    if (currentTextField == kb_text_field) {
                        watcherScaner.addProduct(kb_text_field.getText());
                    }

                    break;
                }
                case "8888": {
                    currentTextField.setText("");
                    watcherScaner.clear();
                    break;
                }

            }
            return false;
        };


        if (kb_text_field == null) {
            kb_text_field = keyKeyB.kb_text_field;
        }


        String css2 = "-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliverySearch;
        kb_text_field.setStyle(css2);


        if (bt_print_check == null) {
            bt_print_check = (Button) keyboard.lookup("#bt_print_check");
        }

        if (bt_print_check2 == null) {
            bt_print_check2 = (Button) keyboard.lookup("#bt_print_check2");
        }

        bt_print_check.setOnAction(e -> onPrintCheck());
        bt_print_check2.setOnAction(e -> onPrintCheckCard());


        currentTextField = kb_text_field;

        kb_text_field.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                textFieldFocusable(kb_text_field);
            }
        });

        tb_get_many.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                textFieldFocusable(tb_get_many);
            }
        });


        if (SettingAppE.getInstance().isTypeЫShow3D) {
            kb_text_field.getStyleClass().add("delivery_3d");
        }


        grid_delivery_host.setOnMouseClicked(event -> {

            if (tb_get_many == currentTextField) return;
            textFieldFocusable(tb_get_many);

        });
        grid_buttons_host.setOnMouseClicked(event -> {

            if (kb_text_field == currentTextField) return;
            textFieldFocusable(kb_text_field);

        });


        UtilsOmsk.resizeNode(this, grid1);


        mBarcodes = Configure.getSession().getList(MBarcode.class, null);


        // box.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) ->
        //          widthHost = (Double) newSceneWidth);
        ////////////////////////////////

        tb_check_summ.textProperty().addListener((observable, oldValue, newValue) -> recalculation());
        tb_get_many.textProperty().addListener((observable, oldValue, newValue) -> recalculation());

        bt_50.setOnAction(this::onManyAction);
        bt_200.setOnAction(this::onManyAction);
        bt_100.setOnAction(this::onManyAction);
        bt_1000.setOnAction(this::onManyAction);
        bt_500.setOnAction(this::onManyAction);
        bt_2000.setOnAction(this::onManyAction);
        bt_5000.setOnAction(this::onManyAction);
        bt_clear.setOnAction(event -> tb_get_many.setText(String.valueOf(0)));

        refreshCheck();
        textFieldFocusable(kb_text_field);
        tb_get_many.setDisable(true);


        //box.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        refreshUser();
        refreshButtonPrintCardCheck();

    }

    public void refreshButtonPrintCardCheck() {

        if (SettingAppE.getInstance().getAcquire() == 0) {
            Controller.getInstans().bt_menu_aquering.setDisable(true);
            bt_print_check2.setVisible(false);
        } else {
            bt_print_check2.setVisible(true);
            Controller.getInstans().bt_menu_aquering.setDisable(false);
        }

        Controller.getInstans().label_acquery.setText(String.format("Эквайринг: %s", SettingAppE.getInstance().getNameAcquaring()));

    }

    private void onPrintCheckCard() {

        List<MProduct> mProducts = SettingAppE.getInstance().selectProduct;
        if (mProducts.size() == 0) return;
        for (MProduct mProduct : mProducts) {
            if (mProduct.selectAmount <= 0) {
                DialogFactory.infoDialog("Печать чека", String.format(err, mProduct.name));
                return;
            }
        }

        if (kb_text_field.getText().length() > 0) {
            DialogFactory.infoDialog("Печать чека", Support.str("name156"));
        } else {

            if (!SettingAppE.getInstance().isQuestionPrint) {
                DialogFactory.printCheck("Чек продажи", o -> {
                    List<MProduct> selectProduct = new ArrayList<>(mProducts);
                    new Transaction().printCheckBank(selectProduct, "0000000000000000");
                    return false;
                });
            } else {
                List<MProduct> selectProduct = new ArrayList<>(mProducts);
                new Transaction().printCheckBank(selectProduct, "0000000000000000");
            }
        }


    }

    private void printableDelivery(String d) {
        if (currentTextField == tb_get_many && tb_get_many.getText().equals("0")) {
            currentTextField.setText(d);
        } else {
            currentTextField.setText(currentTextField.getText() + d);
        }
    }


    void textFieldFocusable(TextField textFocus) {

        currentTextField = textFocus;
        Platform.runLater(textFocus::requestFocus);

//        String css = ";-fx-font-size: " + SettingAppE.instance.fontSizeDeliverySearch;
//        if (currentTextField == kb_text_field) {
//            currentTextField.setStyle("-fx-border-color: chartreuse;-fx-border-width: 0" + css);
//            if (SettingAppE.instance.isTypeЫShow3D)
//                currentTextField.getStyleClass().add("delivery_3d");
//        } else {
//            currentTextField.setStyle("-fx-border-color: chartreuse;-fx-border-width: 0;");
//        }
//        if (textFocus == kb_text_field) {
//            textFocus.setStyle("-fx-border-color: chartreuse;-fx-border-width: 3" + css);
//            if (SettingAppE.instance.isTypeЫShow3D)
//                textFocus.getStyleClass().add("delivery_3d");
//        } else {
//            textFocus.setStyle("-fx-border-color: chartreuse;-fx-border-width: 3;");
//        }
//
//        currentTextField = textFocus;
//        Platform.runLater(new Runnable() {
//            @Override
//            public void run() {
//                textFocus.requestFocus();
//            }
//        });
    }

    private void onManyAction(ActionEvent actionEvent) {
        Button b = (Button) actionEvent.getSource();
        double getmany = 0d;
        try {
            getmany = Double.parseDouble(tb_get_many.getText().trim());
        } catch (Exception ignored) {
            getmany = 0;
            tb_get_many.setText(String.valueOf(0));
        }

        switch (b.getId()) {
            case "bt_50": {
                getmany = getmany + 50;
                break;
            }
            case "bt_100": {
                getmany = getmany + 100;
                break;
            }

            case "bt_200": {
                getmany = getmany + 200;
                break;
            }
            case "bt_500": {
                getmany = getmany + 500;
                break;
            }
            case "bt_1000": {
                getmany = getmany + 1000;
                break;
            }

            case "bt_2000": {
                getmany = getmany + 2000;
                break;
            }
            case "bt_5000": {
                getmany = getmany + 5000;
                break;
            }
            default: {
                log.error("not find button");
                throw new RuntimeException("not find button");
            }
        }
        tb_get_many.setText(String.valueOf(getmany));

    }

    private void recalculation() {
        double sum;
        try {
            sum = Double.parseDouble(tb_check_summ.getText().trim());
        } catch (Exception ignored) {
            sum = 0;
            tb_check_summ.setText(String.valueOf(0d));
        }
        double getmany = 0d;
        try {
            getmany = Double.parseDouble(tb_get_many.getText().trim());
        } catch (Exception ignored) {
            getmany = 0;
            //tb_get_many.setText(String.valueOf(0));
        }
        double delivery;
        delivery = getmany - sum;
        tb_delivery.setText(String.valueOf(UtilsOmsk.round(delivery, 2)));

    }

    public static boolean isNumeric(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    private void onPrintCheck() {
        List<MProduct> mProducts = SettingAppE.getInstance().selectProduct;
        if (mProducts.size() == 0) return;
        for (MProduct mProduct : mProducts) {
            if (mProduct.selectAmount <= 0) {
                DialogFactory.infoDialog("Печать чека", String.format(err, mProduct.name));
                return;
            }
        }


        if (kb_text_field.getText().length() > 0) {
            DialogFactory.infoDialog("Печать чека", Support.str("name156"));
        } else {

            if (SettingAppE.getInstance().isQuestionPrint == false) {
                DialogFactory.printCheck("Чек продажи", o -> {
                    List<MProduct> selectProduct = new ArrayList<>(mProducts);
                    new Transaction().printCheck(selectProduct, new IAction<Double>() {
                        @Override
                        public boolean action(Double aDouble) {
                            tb_check_summ.setText(String.valueOf(aDouble));
                            return false;
                        }
                    });
                    return false;
                });
            } else {
                List<MProduct> selectProduct = new ArrayList<>(mProducts);
                new Transaction().printCheck(selectProduct, new IAction<Double>() {
                    @Override
                    public boolean action(Double aDouble) {
                        tb_check_summ.setText(String.valueOf(aDouble));
                        return false;
                    }
                });
            }


        }

    }

    @Override
    public void destroy() {

        grid1.setOnKeyTyped(null);
    }

    public void refreshCheck() {
        textFieldFocusable(kb_text_field);
        new Additor().refresh(box, o -> {
            refreshCheck();
            return false;
        });
        kb_text_field.setText("");
        double total = 0;
        double amount = 0d;

        double nds = 0;
        for (MProduct p : SettingAppE.getInstance().selectProduct) {

            List<MTaxrates> mTaxrates = Configure.getSession().getList(MTaxrates.class, "cod_nalog = ?", p.cod_nalog_group);
            if (mTaxrates.size() == 0) {
                nds = nds + (p.price * p.selectAmount) / 1.18;
            } else {

                if (mTaxrates.get(0).nalog_value == 18.0) {
                    nds = nds + (p.price * p.selectAmount) / 1.18;
                }
                if (mTaxrates.get(0).nalog_value == 20.0) {
                    nds = nds + (p.price * p.selectAmount) / 1.20;
                }
                if (mTaxrates.get(0).nalog_value == 10.0) {
                    nds = nds + (p.price * p.selectAmount) / 1.10;
                }

            }
            total = total + p.price * p.selectAmount;
            amount = amount + p.selectAmount;

        }
        label_nds.setText(String.valueOf(UtilsOmsk.round(total, 2)));
        if (total == 0) {
            label_nds2.setText(String.valueOf(0d));
        }
        double s1 = nds;// (total / 1.18);
        double s = (total - s1);
        label_nds2.setText(String.valueOf(UtilsOmsk.round(s, 2)));
        tb_check_summ.setText(String.valueOf(UtilsOmsk.round(total, 2)));
        label_itogo_amount.setText(String.valueOf(amount));
        label_itogo_summ.setText(String.valueOf(UtilsOmsk.round(total, 2)));
    }

    public void refreshKeyBoard(String s) {
        if (kb_text_field.isFocused() == false) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    kb_text_field.requestFocus();
                    kb_text_field.setText(s);
                    kb_text_field.selectBackward();
                    kb_text_field.positionCaret(1);

                }
            });
        }
    }

    @Override
    public void focus() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                kb_text_field.requestFocus();
            }
        });
    }

    @Override
    public void refreshUser() {
        String userid = SettingAppE.getInstance().user_id;
        if (userid == null || userid.equals(SettingAppE.UniversalUserKey)) {
            FUserBlender fUserBlender = new FUserBlender();
            stack22.getChildren().add(fUserBlender);
            kb_text_field.setDisable(true);
            watcherScaner = null;
        } else {

            if (stack22.getChildren().size() > 2) {
                stack22.getChildren().remove(1);
            }
            kb_text_field.setDisable(false);

            watcherScaner = new WatcherScaner(stack21, o -> {
                kb_text_field.setText("");
                MProduct prod = (MProduct) o;
                kb_text_field.setText("");
                boolean f = false;
                for (int i = 0; i < SettingAppE.getInstance().selectProduct.size(); i++) {
                    if (SettingAppE.getInstance().selectProduct.get(i).code_prod.equals(prod.code_prod)) {
                        ++SettingAppE.getInstance().selectProduct.get(i).selectAmount;
                        f = true;
                    }
                }
                if (f == false) {
                    prod.selectAmount = 1;
                    SettingAppE.getInstance().selectProduct.add(prod);
                    if (SettingAppE.getInstance().selectProduct.size() == 1) {
                        tb_get_many.setText("0");
                    }
                }

                SettingAppE.save();
                refreshCheck();
                return false;
            });

            watcherScaner.run(kb_text_field);
        }
    }

    @Override
    public void clearTextField() {
        kb_text_field.setText("");
    }
}



