package bitnic.pagesale.blender;

import bitnic.pageconstrictor.FConsdtructorCheck;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FUserBlender extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger ( FUserBlender.class );
    public FUserBlender() {
        try {

            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_user_blender.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException(exception);
        }

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
