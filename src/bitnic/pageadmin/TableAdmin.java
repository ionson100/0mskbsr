package bitnic.pageadmin;

import bitnic.core.Controller;
import bitnic.pagetableproduct.FinderAdmin;
import bitnic.pagetableproduct.IFocusable;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.table.BuilderTable;
import bitnic.table.BuilderTableExecuter;
import bitnic.core.Main;
import bitnic.dialogfactory.DialogFactory;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import bitnic.model.*;
import bitnic.orm.*;
import bitnic.utils.UtilsOmsk;
//import bitnic.table.DisplayViewer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.*;
import java.util.*;

public class TableAdmin extends GridPane implements Initializable, IRefreshLoader, IFocusable {

    private static final Logger log = Logger.getLogger(TableAdmin.class);

    BuilderTable builderTable = new BuilderTable();


    ArrayList<Class> classes = new ArrayList<Class>() {{
        add(MProduct.class);
        add(MUser.class);
        add(MTaxGroupRates.class);
        add(MTaxrates.class);
        add(MTaxGroup.class);
        add(MBarcode.class);
        add(MMarketingAction.class);
    }};

    public TextField tf_search;
    @FXML
    public ComboBox com_box;
    @FXML
    public TextField text_sql;
    @FXML
    public Button bt_execute;
    @FXML
    public javafx.scene.control.TableView table;
    @FXML
    public GridPane grid1;
    GridPane pane;

    private Class currentClass;
    private List<Object> list;
    private static Map<Class, List<Field>> listMap = new HashMap<>();

    public TableAdmin() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("table_admin.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        com_box.getSelectionModel().selectedItemProperty()
                .addListener((ChangeListener<Class>) (observable, oldValue, newValue) -> {
                    currentClass = newValue;
                    System.out.println("Value is: " + newValue);

                    list = Configure.getSession().getList(newValue, null);
                    builderTable.build(list, table);
                    tf_search.setText("");
                    Platform.runLater(() -> {
                        tf_search.focusedProperty();
                        Controller.writeMessage("Таблицы администратора");
                    });


                });

        ObservableList<Class> list1 = FXCollections.observableArrayList();
        for (Class<?> aClass : classes) {


            list1.add(aClass);

        }
        com_box.setItems(list1);
        com_box.getSelectionModel().select(0);
        UtilsOmsk.resizeNode(this, grid1);
        bt_execute.setOnAction(event -> {
            table.getItems().clear();
            if (builderTable.listener != null) {
                table.getColumns().removeListener(builderTable.listener);
            }
            if (text_sql.getText().trim().length() == 0) return;
            Connection c = null;
            ResultSet rs = null;
            Statement stmt = null;
            try {
                currentClass = null;
                c = DriverManager.getConnection(Configure.CON_STR);
                c.setAutoCommit(false);
                stmt = c.createStatement();
                rs = stmt.executeQuery(text_sql.getText());//"select * from product"text_sql.getText()
                List<Object> list = new ArrayList<>();
                ResultSetMetaData rsmd = rs.getMetaData();
                List<BuilderTableExecuter.MyField> fields = new ArrayList<>(rsmd.getColumnCount());
                for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                    String s1 = rsmd.getColumnName(i);
                    String s3 = rsmd.getColumnTypeName(i);
                    if (s3.equals("INTEGER")) {
                        fields.add(new BuilderTableExecuter.MyField(s1, Integer.class));
                    } else if (s3.equals("TEXT")) {
                        fields.add(new BuilderTableExecuter.MyField(s1, String.class));
                    } else if (s3.equals("REAL")) {
                        fields.add(new BuilderTableExecuter.MyField(s1, Double.class));
                    }
                }

                BuilderTableExecuter sbt = new BuilderTableExecuter();

                if (currentClass == null) {
                    currentClass = sbt.createClasse(fields, Main.class);
                }
                Field[] fieldsser = currentClass.getDeclaredFields();
                while (rs.next()) {
                    Object o = currentClass.newInstance();
                    for (Field field : fieldsser) {
                        Object val = rs.getObject(field.getName());
                        field.set(o, val);
                    }
                    list.add(o);
                }

                sbt.build(list, table);
                // new BuilderTable ().build ( list,table );
            } catch (Exception e) {
                log.error(e);
                DialogFactory.errorDialog(e.getMessage());
            } finally {
                try {
                    if (c != null) {
                        c.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (stmt != null) {
                        stmt.close();
                    }

                } catch (SQLException e) {
                    log.error(e);
                }


            }
        });
        focus();
        UtilsOmsk.painter3d(tf_search);
        tf_search.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                List<Field> fields = null;
                if (listMap.containsKey(currentClass)) {

                    fields = listMap.get(currentClass);
                } else {
                    fields = new ArrayList<>();
                    for (Field field : currentClass.getDeclaredFields()) {
                        if (field.getAnnotation(FinderAdmin.class) != null) {
                            fields.add(field);
                        }
                    }
                    listMap.put(currentClass, fields);
                }
                Set<Object> curList = new HashSet<>();
                for (Object o : list) {
                    for (Field field : fields) {
                        try {
                            Object oo = field.get(o);
                            if (oo.toString().toLowerCase(Locale.ROOT).indexOf(newValue.toLowerCase(Locale.ROOT)) != -1) {
                                curList.add(o);
                            }
                        } catch (Exception e) {
                            log.error(e);
                        }
                    }
                }
                if (currentClass == MProduct.class) {
                    if (newValue.length() > 6) {
                        List<MBarcode> list = Configure.getSession().getList(MBarcode.class, "barcode =? ", newValue);
                       // List<MProduct> mProducts = new ArrayList<>();
                        for (MBarcode mBarcodes : list) {
                            List<MProduct> ll = Configure.getSession().getList(MProduct.class, "id_core =? ", mBarcodes.id_product);
                            curList.addAll(ll);
                        }
                    }
                }
                table.getItems().clear();
                builderTable.build(new ArrayList<>(curList), table);

            }
        });
    }

    @Override
    public void refresh() {
        int i = com_box.getSelectionModel().getSelectedIndex();
        com_box.getSelectionModel().select(i);
        System.out.println("select combo- " + i);
    }

    @Override
    public void focus() {
        Platform.runLater(() -> tf_search.requestFocus());
    }
}
