package bitnic.settingscore;

import bitnic.pagesettings.SettingItem;
import bitnic.pagesettings.TypeSettings;
import bitnic.utils.FactorySettings;
import bitnic.utils.IFileStorage;
import bitnic.utils.Pather;
import bitnic.utils.Starter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class SettingAppE extends SettingsE implements IFileStorage {

    private static Logger log = Logger.getLogger(SettingAppE.class);
    private static transient SettingAppE instance;
    // не удачный выход из сменыж
    public boolean isErrorOut;

    @SettingItem(index = 26, type = TypeSettings.integer, key = "name292",isAdmin = true)
    public int sleepScaner =200;

    public static SettingAppE getInstance() {
        if (instance == null) {
            instance = FactorySettings.getSettingsE(SettingAppE.class);
        }
        return instance;
    }

    public static void setInstance(SettingAppE instance) {
        SettingAppE.instance = instance;
    }

    static {
        instance = FactorySettings.getSettingsE(SettingAppE.class);
    }

    private static transient String _version="";
    public static synchronized String getVersion()  {
        if(_version.length()==0){
            try {
                _version=Starter.getStringVersion();
            } catch (IOException e) {
                log.error(e);
            }
        }
        return String.valueOf(_version);
    }


    // интервал таймера для опроса списка фалов для меня. sec
    @SettingItem(index = 30,type = TypeSettings.integer,key = "name295",isAdmin = true)
    public static final int tumerIntervalUpdate = 300;

    public static void refreshInstance() {
        instance = FactorySettings.getSettingsE(SettingAppE.class);
    }


    //пароль сиса по умолчанию
    public static final String defUserAdmin = "312873";
    public static final String defUserAdmin2 = "7117117";

    public static final String defUser = "911911";

    public static final String UniversalUserKey = "1000000000";
    //максимальная сумма чека
    public static final double maxSumm = 100000d;


    // число копий при печати штрихкодов
    public int copy = 1;



    public static void save() {
        FactorySettings.saveE(instance);
    }

    public int ftp_port = 21;


    public String ftp_remote_file = "goods.txt";


    public String ftp_user;


    public String ftp_password;


    public String ftp_cahrset;

    //настройки кассы
    public String settings_kassa;


    @Override
    public String getFileName() {
        SettingAppMode d = FactorySettings.getSettingsE(SettingAppMode.class);
        return Pather.settingsFolder + File.separator + "settingsApp" + d.getProfile() + ".txt";
    }

    private  volatile int session = -1;

    public static int getSession() {

        return instance.session;

    }

    public static void setSession(int ses) {
        instance.session = ses;
    }

}

