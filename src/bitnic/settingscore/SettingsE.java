package bitnic.settingscore;

import bitnic.utils.Pather;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import bitnic.model.MProduct;
import bitnic.pagesettings.SettingItem;
import bitnic.pagesettings.TypeSettings;
import bitnic.table.ColumnUserObject;
import bitnic.utils.FactorySettings;
import bitnic.utils.IFileStorage;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by USER on 10.01.2018.
 */
public class SettingsE  extends SettingAppMode implements ExclusionStrategy, IFileStorage {

   // public int[] procedureIn={0,0,0,0};
   // public int[] procedureOut={0,0,0,0};

    public boolean isAutoUpdate = true;

    transient public double summNallKmm;//сумма наличности вкк

    // показывать только ошибки на тсранице ошибок
    public boolean isShowErrorPageError;

    public int typeHistory=1;//1 6 ,1 last, 3 current

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return SettingsE.class.equals(fieldAttributes.getDeclaringClass());
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return SettingsE.class.equals(aClass);
    }


    // выбранный продукт
    public List<MProduct> selectProduct=new ArrayList<>();
    //словарь ширины колонак таблицадмина
    public Map<String,List<ColumnUserObject>> map=new HashMap<>();
    // максимальный показ количества строк для поиска продукта в странице продуктов
    public boolean isCheckFinishProduct =false;

    public List<ColumnUserObject> getColumnUserObjects(String tableName){
        if(map.containsKey(tableName)==false){
            List<ColumnUserObject> objectList=new ArrayList<>();
            map.put(tableName,objectList);
        }
        return map.get(tableName);
    }

    @Override
    public String getFileName() {
        SettingAppMode mode= FactorySettings.getSettingsE(SettingAppMode.class);
        return Pather.settingsFolder+ File.separator+"settingsE_"+mode.user_id+".txt";
    }

    @SettingItem(index = 101,type = TypeSettings.booleans,key = "name122")
    public boolean isTypeЫShow3D=true;

    @SettingItem(index = 1,type = TypeSettings.integer,key = "name123",isFontSize = true)
    public int fontSizeBigButton=30;

    @SettingItem(index = 2,type = TypeSettings.integer,key = "name124",isFontSize = true)
    public int fontSizeProduct=20;

    @SettingItem(index = 3,type = TypeSettings.integer,key = "name125",isFontSize = true)
    public int fontSizeSelectProduct=20;

    @SettingItem(index = 4,type = TypeSettings.integer,key = "name126",isFontSize = true)
    public int fontSizeDelivery=30;

    @SettingItem(index = 5,type = TypeSettings.integer,key = "name127",isFontSize = true)
    public int fontSizeDeliverySearch =35;

    @SettingItem(index = 7,type = TypeSettings.integer,key = "name128",isFontSize = true)
    public int fontSizeDeliveryButton =20;

    @SettingItem(index = 8,type = TypeSettings.integer,key = "name129",isFontSize = true)
    public int fontSizeDeliveryButtonCountProduct =20;

    @SettingItem(index = 20,type = TypeSettings.booleans,key = "name132")
    public boolean isSearchProductName;


    @SettingItem(index = 21,type = TypeSettings.integer,key = "name133",isFontSize = true)
    public int fontSizeMenuProduct=20;

    @SettingItem(index = 23,type = TypeSettings.booleans,key = "name171")
    public boolean isQuestionPrint;

    @SettingItem(index = 27,type = TypeSettings.booleans,key = "name294",isAdmin = true)
    public boolean isShowError205 =true;

    @SettingItem(index = 28,type = TypeSettings.integer,key = "name296",isFontSize = true)
    public int fontSizeAllTables=20;

}
