package bitnic.settingscore;

import bitnic.model.MUser;
import bitnic.orm.Configure;
import bitnic.pagesettings.SettingItem;
import bitnic.pagesettings.TypeSettings;
import bitnic.utils.*;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import org.apache.log4j.Logger;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class SettingAppMode implements ExclusionStrategy, IFileStorage {
    private transient static volatile int pointid = -1;
    private transient static volatile int showbutton = -1;

    private transient static final Logger log = Logger.getLogger(SettingAppMode.class);

    @SettingItem(index = 6, type = TypeSettings.integer, key = "name121")
    public int scanerDelau = 100000000;
    @SettingItem(index = 30,type = TypeSettings.integer,key = "name293",isAdmin = true)
    public int sleepCheckBank=5000;

//    public String getOfdUrl() {
//        return ofd_url[profile];
//    }
//
//    public int getPortOfd() {
//        return ofd_port[profile];
//    }

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return SettingAppMode.class.equals(fieldAttributes.getDeclaringClass());
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return SettingAppMode.class.equals(aClass);
    }


    // профиль приложения
    private transient volatile static String uuid;
    private transient volatile static int profile = -1;
    private transient volatile static int acquire =-1;
    //private static final Integer[] ofd_port = {21101, 7790, 7790,7790};
    //private static final String[] ofd_url = {"185.170.204.91", "91.107.67.212",  "91.107.67.212",  "91.107.67.212"};
    //private static final String[] url =     {"91.144.170.205", "174.138.11.176", "178.62.249.118", "174.138.11.176"};
    private static final String[] names =   {"1C", "bsr000.net", "Bitnic Test 1C","bsr000.net test"};
    private static final String[] acquires={"noAcquire","Sberbank"};

    //индентификатор торговой точки
    public static synchronized int getPointId() {
        if (pointid == -1) {
            String dd = UtilsOmsk.readFile(Pather.pointid);
            if (dd == null) {
                pointid = 0;
            }
            try {
                if(dd!=null){
                    dd = dd.replace("\n", "");
                    pointid = Integer.parseInt(dd);
                }

            } catch (Exception ex) {
                log.error(ex);
                pointid = 0;
            }
        }
        return pointid;

    }
    public static synchronized int getShowButtonUpdate(){
        if(showbutton==-1){
            String dd = UtilsOmsk.readFile(Pather.showButtonUpdate);
            if (dd == null) {
            }
            try {
                dd = dd.replace("\n", "");
                showbutton = Integer.parseInt(dd);
            } catch (Exception ex) {
                log.error(ex);
            }
        }
        return showbutton;
    }

    public static void setShowButtonUpdate(int id) {
        try {
            showbutton = id;
            UtilsOmsk.rewriteFile(Pather.showButtonUpdate, String.valueOf(id));
        } catch (Exception e) {

            log.error(e);
        }
    }


    public static synchronized void setPointId(int id) {
        try {
            pointid = id;
            UtilsOmsk.rewriteFile(Pather.pointid, String.valueOf(id));
        } catch (Exception e) {

            log.error(e);
        }
    }

    public static synchronized int getProfile() {
        if (profile == -1) {
            String dd = UtilsOmsk.readFile(Pather.patchprofile);
            if (dd == null) {
                profile = 0;
            }
            try {
                if(dd!=null){
                    dd = dd.replace("\n", "");
                    profile = Integer.parseInt(dd);
                }

            } catch (Exception ex) {
                log.error(ex);
                profile = 0;
            }
        }
        return profile;
    }

    public static synchronized String getUuid(){
        if(uuid==null){
            uuid = UtilsOmsk.readFile(Pather.uuidFile);
        }
        return uuid;
    }

    public static synchronized int getAcquire() {
        if (acquire == -1) {
            String dd = UtilsOmsk.readFile(Pather.pathAcquire);
            if (dd == null) {
                acquire = 0;

            }
            try {
                if(dd!=null){
                    dd = dd.replace("\n", "");
                    acquire = Integer.parseInt(dd);
                }

            } catch (Exception ex) {
                log.error(ex);
                try {
                    UtilsOmsk.rewriteFile(Pather.pathAcquire, "0");
                } catch (Exception e) {
                   log.error ( e );
                }
                acquire = 0;
            }
        }
        return acquire;
    }

    public static synchronized void setProfile(int id) {
        try {
            profile = id;
            UtilsOmsk.rewriteFile(Pather.patchprofile, String.valueOf(id));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    public static void setAcquery(int id){
        try {
            acquire = id;
            UtilsOmsk.rewriteFile(Pather.pathAcquire, String.valueOf(id));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    // число дней для проверки срока годности продукта, меньше нее - алерт
    public int maxDatecheckProduct = 20;


    //индентификатор пользователя  его табличный код
    public volatile String user_id;

    //наименование профиля
    public static String[] getNamesProfile() {
        //todo не менять
        return names;
    }

    public static String[] getNamesAcquire() {
        //todo не менять
        return acquires;
    }

    public static synchronized String getUrl() {

        try {

            List<String> strings=  Files.readAllLines(Paths.get(Pather.patchUrlFile));
            return strings.get(profile).trim ();
        }catch (Exception ex){
            log.error(ex);
            return null;
        }

    }

    public static String getNameProfile() {
        return names[getProfile()];
    }

    public static String getNameAcquaring() {

        return acquires[getAcquire()];
    }

    public String getUserName() {

        if (user_id == null) {
            return Support.str("name131");
        }

        if (user_id.equals(SettingAppE.UniversalUserKey)) {
            return Support.str("name166");
        }

        if (user_id.equals("1")) {
            return Support.str("name130");
        }

        List<MUser> mUsers = Configure.getSession().getList(MUser.class, " cod = ?", user_id);

        if (mUsers.size() > 0) {
            return mUsers.get(0).name;
        } else {
            return Support.str("name131");
        }
    }



    public synchronized String getFileName() {
        return Pather.settingsFolder + File.separator + "settingsAppMode.txt";
    }

    public  String getUserCheck() {

        if (user_id == null) {
            return Support.str("name131");
        }
        if (user_id.equals("1")) {
            return Support.str("name130");
        }

        List<MUser> mUsers = Configure.getSession().getList(MUser.class, " cod = ?", user_id);
        if (mUsers.size() > 0) {
            return mUsers.get(0).name_check;
        } else {
            return Support.str("name131");
        }
    }
}
