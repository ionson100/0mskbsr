package bitnic.pagesettingskkm;

import bitnic.core.Controller;
import bitnic.utils.Support;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import bitnic.kassa.ItemSettingKkm;
import bitnic.kassa.SettingsKkmTable;
import bitnic.pagesettingskkm.itemPageSettingKmm.FItemSettingKkm;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

public class FSettingsKkm extends GridPane implements Initializable {

    Logger log=Logger.getLogger(getClass());

    public ListView list_kkm;



    public FSettingsKkm() {
        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_settings_kkm.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
        Controller.writeMessage(Support.str("name35"));
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new SettingsKkmTable().getSettings(itemSettingKkms -> {
            activateList(itemSettingKkms);
            return false;
        });
        list_kkm.setCellFactory(param -> new KkmCellFactory());


    }

//    static String readFile(String path, Charset encoding)
//            throws IOException {
//        byte[] encoded = Files.readAllBytes(Paths.Get(path));
//        return new String(encoded, encoding);
//    }

  static   class KkmCellFactory extends ListCell<ItemSettingKkm> {
        @Override
        public void updateItem(ItemSettingKkm item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemSettingKkm fItemCheck = new FItemSettingKkm(item);
                setGraphic(fItemCheck);
            }
        }
    }

    void activateList(List<ItemSettingKkm> itemSettingKkms) {

        ObservableList<ItemSettingKkm> fruitList = FXCollections.<ItemSettingKkm>observableArrayList(itemSettingKkms);
        list_kkm.getItems().setAll(fruitList);
    }
}
