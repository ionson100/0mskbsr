package bitnic.pagesettingskkm.itemPageSettingKmm;

import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.UtilsOmsk;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import bitnic.kassa.ItemSettingKkm;
import bitnic.kassa.UpdateItemKkm;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemSettingKkm extends GridPane implements Initializable {

    Logger log=Logger.getLogger(getClass());
    public Label label_table;
    public Label label_cell;
    public Label label_des;
    public Label label_des2;
    public Label label_value;
    public Button bt_update;

    private ItemSettingKkm item;

    public FItemSettingKkm(ItemSettingKkm item) {

        this.item = item;
        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_setting_kkm.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        UtilsOmsk.painter3d(bt_update);
        label_table.setText(item.param);
        label_cell.setText(String.valueOf(item.index));
        label_des2.setWrapText(true);
        label_des.setText(item.name);
        label_value.setText(item.value == null ? "" : String.valueOf(item.value));
        label_des2.setText(item.description);
        bt_update.setOnAction(event -> DialogFactory.editItemKkmDialog(item, o -> {
            label_value.setText(o.toString());
            new UpdateItemKkm().update(item,o);
            return false;
        }));

    }
}
