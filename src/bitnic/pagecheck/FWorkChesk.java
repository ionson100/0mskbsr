package bitnic.pagecheck;


import bitnic.checkarchive.CheckArchiveE;
import bitnic.core.Controller;
import bitnic.orm.Configure;
import bitnic.pagecheck.itemcheck.FItemCheck;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class FWorkChesk extends GridPane implements Initializable, IRefreshLoader {
    private double lastYposition=0d;
    private static final Logger log = Logger.getLogger(FWorkChesk.class);
    public ListView list_check;
    public GridPane grid1;
    public HBox hbox;
    public ComboBox<String>     com_box;
    public Button bt_cur_session;
    public DatePicker datePicker;


    GridPane pane;

    List<CheckArchiveE> liste = new ArrayList<>();

    public FWorkChesk() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_chesk.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // liste = new CheckArchiveE().getList(null);
        UtilsOmsk.resizeNode(this, grid1);


        ObservableList<String> list = FXCollections.observableArrayList();
        list.add("Текущая смена");
        list.add("Текущая смена - банк");
        list.add("Не фиксированные в банке");
        com_box.setItems(list);


        if (SettingAppE.getInstance().isTypeЫShow3D) {
            hbox.getStyleClass().add("footer_3d");

        } else {
            hbox.getStyleClass().add("footer");
        }
        com_box.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                refresh();
            }
        });
        com_box.getSelectionModel().select(0);


        this.setOnMousePressed(event -> lastYposition = event.getSceneY());

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });

        Controller.writeMessage( "Таблица чеков" );
        bt_cur_session.setOnAction(event -> refreshCheck());
        datePicker.setOnAction(event -> {
            LocalDate date = datePicker.getValue();
            refreshCheckDate(date);
            System.err.println("Selected date: " + date);
        });

        UtilsOmsk.painter3d(bt_cur_session);

    }


    private void refreshCheckDate(LocalDate localDate) {


        Date d = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        long dateStart=UtilsOmsk.atStartOfDay(d).getTime();
        long dateFinish=UtilsOmsk.atEndOfDay(d).getTime();
        list_check.getItems().clear();
        list_check.setItems(null);
        if(com_box.getSelectionModel().getSelectedIndex()==0){
            liste = Configure.getSession().getList(CheckArchiveE.class,
                    " date BETWEEN ? AND ? ", dateStart,dateFinish);
        }
        ObservableList<CheckArchiveE> data = FXCollections.observableArrayList(liste);
        list_check.setItems(data);
        list_check.setCellFactory((Callback<ListView<CheckArchiveE>, ListCell<CheckArchiveE>>) list -> new CheckCell());
    }



    private void refreshCheck() {


        int  session=SettingAppE.getSession();
        System.out.println(">>>>>>>>>>>>>>"+session);
        list_check.getItems().clear();
        list_check.setItems(null);
        if(com_box.getSelectionModel().getSelectedIndex()==0){
            liste = Configure.getSession().getList(CheckArchiveE.class,
                    " session_id = ? ", session);
        }

        if(com_box.getSelectionModel().getSelectedIndex()==1){
            liste = Configure.getSession().getList(CheckArchiveE.class," session_id = ? and is_bank = ? ", session,true);
        }
        if(com_box.getSelectionModel().getSelectedIndex()==2){
            liste = Configure.getSession().getList(CheckArchiveE.class," session_id = ? and is_bank = ? and is_send_bank = ? ", session,true,false);
        }


        ObservableList<CheckArchiveE> data = FXCollections.observableArrayList(liste);
        list_check.setItems(data);
        list_check.setCellFactory((Callback<ListView<CheckArchiveE>, ListCell<CheckArchiveE>>) list -> new CheckCell());
    }

    @Override
    public void refresh() {
        refreshCheck();
    }

    static class CheckCell extends ListCell<CheckArchiveE> {
        @Override
        public void updateItem(CheckArchiveE item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {

                if (empty ) {
                    setGraphic(null);
                } else {

                    FItemCheck fItemCheck = new FItemCheck(item);

                    setGraphic(fItemCheck);
                }

            }
        }
    }



}
