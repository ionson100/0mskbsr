package bitnic.pagecheck.itemcheck;


import bitnic.checkarchive.CheckArchiveE;
import bitnic.dialogfactory.DialogFactory;
import bitnic.kassa.PrintFreeText;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Transaction;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemCheck extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger(FItemCheck.class);

    private double lastYposition=0d;
    public Label check_number;
    public Label check_type;
    public Label check_date;
    public Label check_summ;
    public Label check_amount;
    public Label check_status, label_bank,label_session,label_code_doc,label_is_fix_bank;
    public Button bt_show_prod, bt_print_double, bt_cancellation,bt_bank;
    GridPane pane;
    private CheckArchiveE checkBody;


    public FItemCheck(CheckArchiveE checkBody) {
        this.checkBody = checkBody;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_check.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UtilsOmsk.painter3d(bt_show_prod, bt_print_double, bt_cancellation,bt_bank);
        check_number.setText(String.valueOf(checkBody.doc_id));
        check_type.setText("Продажа");
        check_date.setText(UtilsOmsk.simpleDateFormatE(checkBody.date));

        check_summ.setText(String.valueOf(checkBody.summ));
        check_amount.setText(String.valueOf(checkBody.amount_product));
        label_session.setText(String.valueOf(checkBody.session_id));
        label_code_doc.setText(String.valueOf(checkBody.cod_doc));

        if(checkBody.is_bank==false){
            label_is_fix_bank.setText("");
        }else {
            if(checkBody.is_send_bank){
                label_is_fix_bank.setText("Чек зафиксирован");
            }else {
                label_is_fix_bank.setText("Чек не зафиксирован");
            }
        }


        if (checkBody.date_return .getTime()!=0) {
            check_status.setText(Support.str("name136")+"  "+UtilsOmsk.simpleDateFormatE(checkBody.date_return));
            bt_cancellation.setVisible(false);
        } else {
            if (checkBody.is_active == false) {
                check_status.setText(Support.str("name173"));
                bt_cancellation.setVisible(false);
            } else {
                check_status.setText(Support.str("name137"));
            }

        }
        if(checkBody.is_bank==false){
            bt_bank.setVisible(false);
        }

        if(checkBody.is_bank==true&&checkBody.is_send_bank==true){
            bt_cancellation.setVisible(false);
        }

        bt_show_prod.setOnAction(event -> DialogFactory.productForCheckDialog(checkBody));
        bt_print_double.setOnAction(event -> bitnic.kassa.PrintMyLastCheck.printLast(checkBody));

        bt_cancellation.setOnAction(event -> {

            if ( SettingAppE.getInstance ().isQuestionPrint == false ){
                DialogFactory.printCheck("Чек возврата", o -> {
                    new Transaction().deleteCheck(checkBody, checkBody.doc_id, integer -> false);
                    return false;
                });
            }else {
                new Transaction().deleteCheck(checkBody, checkBody.doc_id, integer -> false);
            }

        });

        bt_bank.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(checkBody.check_body_bank!=null){
                    new PrintFreeText().print(checkBody.check_body_bank);
                }else {
                    DialogFactory.infoDialog(Support.str("name175"), Support.str("name176"));
                }

            }
        });

        if (checkBody.link_bank != null) {
            label_bank.setText("Банковская проводка. Сcылка: " + checkBody.link_bank);
        } else {
            label_bank.setText("Нет");
        }

        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });
    }
}
