package bitnic.message;

import bitnic.core.Controller;
import bitnic.pagemessage.PageMessage;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Pather;
import javafx.application.Platform;
import org.apache.log4j.Logger;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RepositoryMessage {


    public static final String baseMessage="jdbc:sqlite:"+ Pather.settingsFolder+ File.separator+"message";

    private static final Logger log = Logger.getLogger(RepositoryMessage.class);
    private static java.sql.Connection conn;

    private static synchronized java.sql.Connection getConnection() {
        try {
            if(conn==null){
                String core=baseMessage+"_"+String.valueOf(SettingAppE.getInstance().user_id)+".sqlite";
                conn = DriverManager.getConnection(core);
                {
                    String sql = "CREATE TABLE IF NOT EXISTS messages (\n" +
                            "_id integer PRIMARY KEY AUTOINCREMENT,\n" +
                            "id  INTEGER,\n" +
                            "read  INTEGER,\n" +
                            "text TEXT,\n" +
                            "title TEXT\n" +
                            ");";
                    if (conn != null) {
                        conn.createStatement().execute(sql);
                        conn.getMetaData();

                    }
                }
            }

        } catch (SQLException e) {
            log.error(e);
        }
        return conn;
    }

    public static synchronized void insert(MessageBody messageBody) {
        ResultSet resultSet=null;
        try {
             resultSet = getConnection().createStatement().
                    executeQuery("select * from messages where id = " + messageBody.id);
            int i = 0;
            while (resultSet.next()) {
                i++;
            }
            if (i == 0) {
                String sql = "INSERT INTO messages (id ,read,text,title) VALUES(?,?,?,?)";
                PreparedStatement pstmt = getConnection().prepareStatement(sql);
                pstmt.setInt(1, messageBody.id);
                pstmt.setLong(2, messageBody.read);
                pstmt.setString(3, messageBody.text);
                pstmt.setString(4, messageBody.title);
                pstmt.executeUpdate();
                pstmt.close ();
            }
        } catch (Exception e) {
            log.error(e);
        }finally {
            if(resultSet!=null){
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    log.error(e);
                }
            }
        }
    }

//    public static synchronized List<MessageBody> getMessageBodyList() {
//        List<MessageBody> list = new ArrayList<>();
//        try {
//            ResultSet resultSet = getConnection().createStatement().executeQuery("select * from messages ");
//            addMessage(list, resultSet);
//            resultSet.close ();
//            return list;
//        } catch (SQLException e) {
//            log.error(e);
//            return null;
//        }
//
//    }

    public static void addMessage(List<MessageBody> list, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            MessageBody er = new MessageBody();
            er.id = resultSet.getInt("id");
            er.read = resultSet.getLong("read");
            er.text = resultSet.getString("text");
            er.title = resultSet.getString("title");
            list.add(er);

        }
    }

    public static synchronized List<MessageBody> getMessageBodyNoReadList() {
        List<MessageBody> list = new ArrayList<>();
        try {
            try(ResultSet resultSet = getConnection().createStatement().executeQuery("select * from messages WHERE read = 0")){
                addMessage(list, resultSet);
            }
            return list;
        } catch (SQLException e) {
            log.error(e);
            return null;
        }

    }

//    public synchronized static void setConnectionNull() {
//        conn=null;
//    }
//
//
//    public static synchronized void updateDate(ArrayList<Object> d)  {
//        try{
//            java.util.Date date=new Date();
//            log.info(" подтвережденеи о прочтении с сервера "+date.toString());
//
//            int i= ((Double) d.get(0)).intValue();
//            String sql = "UPDATE messages SET read = ? WHERE id = "+String.valueOf(i);
//            log.info(sql);
//            PreparedStatement pstmt = getConnection().prepareStatement(sql);
//            pstmt.setLong(1, date.getTime());
//            pstmt.executeUpdate();
//            pstmt.close ();
//            Platform.runLater(() -> {
//                Controller.getInstans().refreshMenu();
//                if(Controller.curnode instanceof PageMessage){
//                    ((PageMessage)Controller.curnode).updateRead((double) d.get(0),date.getTime());
//                }
//            });
//        }catch (Exception ex){
//            log.error(ex);
//        }
//
//    }
}


