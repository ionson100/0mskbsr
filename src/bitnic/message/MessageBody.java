package bitnic.message;


import bitnic.core.Controller;
import bitnic.orm.Column;
import bitnic.orm.Configure;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.pagemessage.PageMessage;
import bitnic.settingscore.SettingAppE;
import javafx.application.Platform;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table("mymessages")
public class MessageBody {//extends RepositoryMessage

    private static final Logger log = Logger.getLogger(MessageBody.class);

    @PrimaryKey("_id")
    public int _id;

    @Column("id")
    public int id;
    @Column("user_id")
    public String user_id;
    @Column("read")
    public long read;
    @Column("text")
    public String text;
    @Column("title")
    public String title;

    public static List<MessageBody> getMessageBodyNoReadList() {
        List<MessageBody> res = Configure.getSession().getList(MessageBody.class, " read = 0 and user_id = ?", SettingAppE.getInstance().user_id);
        return res;
    }

    public void updateDate(ArrayList<Object> d) {
        java.util.Date date = new Date();
        log.info(" подтверждение о прочтении с сервера " + date.toString());
        int i = ((Double) d.get(0)).intValue();
        List<MessageBody> bodyList=Configure.getSession().getList(MessageBody.class," id = ? ",i);
        if(bodyList.size()==0){
            log.error("lisst = 0 id="+String.valueOf(i));
            return;
        }
        long datel =new Date().getTime();
        for (MessageBody messageBody : bodyList) {
            messageBody.read=datel;
            Configure.getSession().update(messageBody);
        }
        Platform.runLater(() -> {
            Controller.getInstans().refreshMenu();
            if (Controller.curnode instanceof PageMessage) {
                ((PageMessage) Controller.curnode).updateRead((double) d.get(0), date.getTime());
            }

        });
    }

    public static List<MessageBody> getMessageBodyList() {

        List<MessageBody> res = Configure.getSession().getList(MessageBody.class,
                " user_id = ?", SettingAppE.getInstance().user_id);
        return res;
    }

    public  synchronized void  insert(MessageBody directive) {

        int i = (int) Configure.getSession().executeScalar("select count() from mymessages where  id= ?  and user_id = ? ", directive.id,SettingAppE.getInstance().user_id);

        if (i == 0) {
            directive.user_id=SettingAppE.getInstance().user_id;
            Configure.getSession().insert(directive);
        }


    }
}
