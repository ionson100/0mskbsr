package bitnic.pagehelp;

import bitnic.pagereport.PageReport;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FPageHelp  extends GridPane implements Initializable {

    private static final Logger log = Logger.getLogger(FPageHelp.class);

    public WebView web_viewer;


    public FPageHelp() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_page_help.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

       // URL url = this.getClass().getResource("");

        File f = new File("/home/bsr/static/share/help/index.html");
        web_viewer.getEngine().load(f.toURI().toString());

    }
}
