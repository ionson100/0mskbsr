package bitnic.pagehistory;


import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.CheckHistory;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.pagehistory.itemday.ItemDay;
import bitnic.pagehistory.itemmount.ItemMount;
import bitnic.pagehistory.itemproduct.ItemProduct;
import bitnic.senders.SenderHistory;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class FPageHistory extends GridPane implements Initializable {


    private static final Logger log = Logger.getLogger(FPageHistory.class);
    public Button bt_expander;
    public TreeView tree_view;
    public CheckBox cb_1, cb_2, cb_3;
    private Map<String, String> stringMap = new HashMap<>();
    private List<CheckHistory> list = null;

    public FPageHistory() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_my_history.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
        List<MProduct> mProducts = Configure.getSession().getList(MProduct.class, null);
        for (MProduct mProduct : mProducts) {
            if (stringMap.containsKey(mProduct.code_prod)) {

            } else {
                stringMap.put(mProduct.code_prod, mProduct.name);
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (SettingAppE.getInstance().typeHistory == 1) {
            cb_1.setSelected(true);
        }
        if (SettingAppE.getInstance().typeHistory == 2) {
            cb_2.setSelected(true);
        }
        if (SettingAppE.getInstance().typeHistory == 3) {
            cb_3.setSelected(true);
        }

        cb_1.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                cb_2.setSelected(false);
                cb_3.setSelected(false);
                SettingAppE.getInstance().typeHistory = 1;
                SettingAppE.save();
                activate(list, SettingAppE.getInstance().typeHistory);

            }

        });
        cb_2.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    cb_1.setSelected(false);
                    cb_3.setSelected(false);
                    SettingAppE.getInstance().typeHistory = 2;
                    SettingAppE.save();
                    activate(list, SettingAppE.getInstance().typeHistory);
                }
            }
        });
        cb_3.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    cb_1.setSelected(false);
                    cb_2.setSelected(false);
                    SettingAppE.getInstance().typeHistory = 3;
                    SettingAppE.save();
                    activate(list, SettingAppE.getInstance().typeHistory);
                }
            }
        });

        bt_expander.setOnAction(event -> activate(list, SettingAppE.getInstance().typeHistory));

        SenderHistory history = new SenderHistory();
        history.send(checkHistories -> {
            if (checkHistories == null) {
                DialogFactory.errorDialog(history.exception.getMessage());
            } else {
                list = checkHistories;
                activate(list, SettingAppE.getInstance().typeHistory);
            }
            return false;
        });
        Controller.writeMessage(Support.str("name309"));
    }

    private double mediumCheck(WrapperTree list) {
        Set<String> set = new HashSet<>();
        double summ = 0;
        for (List<CheckHistory> checkHistories : list.map.values()) {
            for (CheckHistory ss : checkHistories) {
                String key = String.valueOf(ss.date) + String.valueOf(ss.doc_number) + String.valueOf(ss.point);
                set.add(key);
                summ = summ + (ss.amount * ss.price);
            }
        }
        return summ / set.size();
    }

    void activate(List<CheckHistory> list, int typeHistory) {

//        list.get(0).date=  LocalDate.of(2017,12,2).atStartOfDay().
//                atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
//        list.get(1).date=  LocalDate.of(2017,10,2).atStartOfDay().
//                atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

        int is = LocalDate.now().getMonth().getValue();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        Map<Integer, WrapperTree> map = new HashMap<>();
        for (CheckHistory checkHistory : list) {
            LocalDate date = checkHistory.getDate();

            int i = date.getMonth().getValue();
            switch (typeHistory) {
                case 1:

                    break;
                case 2:

                    if (is == 1) {
                        if (i != 12) {
                            continue;
                        }
                    } else {
                        if (i != (is - 1)) {
                            continue;
                        }
                    }
                    break;
                case 3:

                    if (i != is) continue;
                    break;
            }
            if (map.containsKey(i)) {
                map.get(i).add(checkHistory);
            } else {

                WrapperTree tree = new WrapperTree();
                tree.year = checkHistory.getDate().getYear();
                tree.mount = i;
                tree.time = date.atStartOfDay().
                        atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                tree.add(checkHistory);
                map.put(i, tree);

            }
        }

        TreeItem<String> rootItem = new TreeItem<String>();


        rootItem.setExpanded(true);


        List<WrapperTree> wrapperTrees = new ArrayList<>(map.values());
        Collections.sort(wrapperTrees, Comparator.comparingLong(o -> o.time));

        for (WrapperTree tree : wrapperTrees) {

            double m = mediumCheck(map.get(tree.mount));

            //%-5s%-11s%-25s%-11s%n
            String str1 = String.format("1;%s;%s;%s;%s",//
                    getMont(tree.mount) + " " + tree.year,
                    itogerMount(tree),
                    String.valueOf(UtilsOmsk.round(m, 2)),
                    itogerMountSession(tree));
            TreeItem mont = new TreeItem<>(str1);


            List<Integer> integers = new ArrayList<>(tree.map.keySet());
            Collections.sort(integers, Integer::compare);

            for (Integer day : integers) {
                String result = "";
                try {
                    result = formatter.format(tree.map.get(day).get(0).getDate());


                } catch (Exception d) {
                    log.error(d);
                }
                String sqt = String.format("2;%s;%s", result, itogerDay(tree.map.get(day)));

                TreeItem daytree = new TreeItem<>(sqt);
                mont.getChildren().add(daytree);
                List<CheckHistory> list1 = tree.map.get(day);
                for (MiniProd checkHistory : getProduct(list1)) {

                    double sum = checkHistory.price * checkHistory.amount;
                    String str = String.format("3;222;%s;%s x %s=%s",
                            checkHistory.name,
                            String.valueOf(checkHistory.amount),
                            String.valueOf(checkHistory.price),
                            String.valueOf(UtilsOmsk.round(sum, 2)));

                    TreeItem itemtree = new TreeItem<>(str);
                    daytree.getChildren().add(itemtree);
                }
            }

            rootItem.getChildren().add(mont);
        }
        tree_view.setRoot(rootItem);
        tree_view.setCellFactory(param -> new MyTreeCell());
        tree_view.setShowRoot(false);

    }

    Collection<MiniProd> getProduct(Collection<CheckHistory> list) {
        Map<String, MiniProd> map = new HashMap<>();
        for (CheckHistory checkHistory : list) {
            String key = checkHistory.product_id + String.valueOf(checkHistory.price);
            if (map.containsKey(key)) {
                double amount = map.get(key).amount;
                map.get(key).amount = amount + checkHistory.amount;
            } else {
                MiniProd prod = new MiniProd();
                prod.amount = checkHistory.amount;
                prod.code = checkHistory.product_id;
                prod.price = checkHistory.price;
                prod.name = stringMap.getOrDefault(checkHistory.product_id, "Продукт не найден  Код: "+prod.code);
                map.put(key, prod);
            }
        }
        List<MiniProd> res = new ArrayList<>(map.values());
        Collections.sort(res, Comparator.comparing(o -> o.name));
        return res;
    }

    private String getMont(int i) {
        switch (i) {
            case 1:
                return "Январь";
            case 2:
                return "Февраль";
            case 3:
                return "Март";
            case 4:
                return "Апрель";
            case 5:
                return "Май";
            case 6:
                return "Июнь";
            case 7:
                return "Июль";
            case 8:
                return "Август";
            case 9:
                return "Сентябрь";
            case 10:
                return "Октябрь";
            case 11:
                return "Ноябрь";
            case 12:
                return "Декабрь";

        }
        return "";
    }

    private String itogerMount(WrapperTree wrapperTree) {
        double sum = 0;
        for (List<CheckHistory> checkHistories : wrapperTree.map.values()) {
            for (CheckHistory checkHistory : checkHistories) {
                sum = sum + (checkHistory.amount * checkHistory.price);
            }
        }
        return String.valueOf(UtilsOmsk.round(sum, 2));
    }

    private String itogerMountSession(WrapperTree wrapperTree) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        Set<String> stringSet=new HashSet<>();
        for (List<CheckHistory> checkHistories : wrapperTree.map.values()) {
            for (CheckHistory checkHistory : checkHistories) {
                String x=dateFormat.format(new Date(checkHistory.date));
                String res=x+checkHistory.point+checkHistory.session;
                stringSet.add(res);
            }
        }
        return  String.valueOf(stringSet.size());
    }

    private String itogerDay(List<CheckHistory> list) {
        double sum = 0;

        for (CheckHistory checkHistory : list) {
            sum = sum + (checkHistory.amount * checkHistory.price);
        }

        return String.valueOf(UtilsOmsk.round(sum, 2));
    }

   static class WrapperTree {
        public int year;
        public int mount;
        public long time;
        public Map<Integer, List<CheckHistory>> map = new HashMap<>();

        public void add(CheckHistory checkHistory) {

            int i = checkHistory.getDate().getDayOfMonth();

            if (map.containsKey(i)) {
                map.get(i).add(checkHistory);
            } else {
                List<CheckHistory> list = new ArrayList<>();
                list.add(checkHistory);
                map.put(i, list);
            }
        }
    }

   static class MiniProd {
        public String code;
        public String name;
        public Double price;
        public Double amount;
    }

}

class MyTreeCell extends TextFieldTreeCell<String> {



    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {

            setDisclosureNode(null);
           // MyTreeCell.this.getTreeItem().setExpanded(true);
            String[] d = item.split(";", -1);

            if (d.length > 1) {
                if (d[0].equals("1")) {
                    setGraphic(new ItemMount(d[1], d[2], d[3],d[4],MyTreeCell.this.getTreeItem()));
                    setText(null);
                    return;
                }
                if (d[0].equals("2")) {
                    setGraphic(new ItemDay(d[1], d[2],MyTreeCell.this.getTreeItem()));
                    setText(null);
                    return;
                }
                if (d[0].equals("3")) {
                    setGraphic(new ItemProduct(d[1], d[2], d[3]));
                    setText(null);
                }
            } else {
                setText(null);
                setGraphic(null);
            }


        }

    }

}