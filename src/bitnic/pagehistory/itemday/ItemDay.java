package bitnic.pagehistory.itemday;

import bitnic.pagehistory.itemmount.ItemMount;
import bitnic.pagesale.CustomScrollEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class ItemDay extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger(ItemMount.class);

    double lastYposition=0d;
    private String date;
    private final String summ;
    private TreeItem treeItem;
    public Label label_day;
    public Label label_summ;
    public Button bt_action;

    public ItemDay(String date,String  summ,TreeItem treeItem) {
        this.date = date;

        this.summ = summ;
        this.treeItem = treeItem;


        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("item_day.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bt_action.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(treeItem.isExpanded()){
                    treeItem.setExpanded(false);
                }else {
                    treeItem.setExpanded(true);
                }
            }
        });

        if(treeItem.isExpanded()){
            bt_action.getStyleClass().add("button_tree_open");
        }else {
            bt_action.getStyleClass().add("button_tree_close");
        }
        label_day.setText(date);
        label_summ.setText(summ);
        this.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });
    }
}
