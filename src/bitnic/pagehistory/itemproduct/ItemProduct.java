package bitnic.pagehistory.itemproduct;

import bitnic.pagehistory.itemmount.ItemMount;
import bitnic.pagesale.CustomScrollEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class ItemProduct extends GridPane implements Initializable {
    double lastYposition=0d;
    private static final Logger log = Logger.getLogger(ItemMount.class);


    private final String name;
    private final String itogo;
    public Label label_date, label_name, label_itogo;


    public ItemProduct(String date, String name,String itogo) {

        this.name = name;
        this.itogo = itogo;


        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("item_product.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        label_name.setText(name);
        label_itogo.setText(itogo);
        this.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });
    }
}
