package bitnic.pagehistory.itemmount;

import bitnic.pagesale.CustomScrollEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class ItemMount extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger(ItemMount.class);

    public Button bt_action;
    double lastYposition=0d;
    private String date;
    private final String summ;
    private String mcheck;
    private String session;
    private TreeItem treeItem;

     public Label label_day,label_summ,label_mcheck,label_session;

    public ItemMount(String date, String  summ, String mcheck, String session, TreeItem treeItem) {
        this.date = date;

        this.summ = summ;
        this.mcheck = mcheck;
        this.session = session;
        this.treeItem = treeItem;


        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("item_mount.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bt_action.setOnAction(event -> {
            if(treeItem.isExpanded()){
                treeItem.setExpanded(false);
            }else {
                treeItem.setExpanded(true);
            }
        });

        if(treeItem.isExpanded()){

            bt_action.getStyleClass().add("button_tree_open");
        }else {
            bt_action.getStyleClass().add("button_tree_close");
        }

        label_day.setText(date);
        label_summ.setText(summ);
        label_mcheck.setText(mcheck);
        label_session.setText(session);
        this.setOnMousePressed(event -> lastYposition = event.getSceneY());

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });
    }
}
