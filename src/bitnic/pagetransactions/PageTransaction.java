package bitnic.pagetransactions;


import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MError;
import bitnic.pagetableproduct.IFocusable;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.settingscore.SettingAppE;
import bitnic.table.BuilderTable;
import bitnic.transaction.eventfrontol.*;
import bitnic.utils.Pather;
import bitnic.utils.ReaderBugFilesReverce;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class PageTransaction extends GridPane implements Initializable, IFocusable, IRefreshLoader {

    private static final Logger log = Logger.getLogger ( PageTransaction.class );
    public TextField tf_search;
    public HBox vbox_search;
    public TableView table_error;
    public GridPane grid1;

    List <MError> list = new ArrayList <> ();

    public PageTransaction () {
        try {
            InputStream inputStream = getClass ().getClassLoader ().getResource ( "aa_ru_RU.properties" ).openStream ();
            ResourceBundle bundle = new PropertyResourceBundle ( inputStream );
            FXMLLoader fxmlLoader = new FXMLLoader ( getClass ().getResource ( "f_page_pransaction.fxml" ) , bundle );
            fxmlLoader.setRoot ( this );
            fxmlLoader.setController ( this );
            fxmlLoader.load ();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException ( exception );
        }
    }

    @Override
    public void initialize ( URL location , ResourceBundle resources ) {
        if ( SettingAppE.getInstance ().isTypeЫShow3D) {
            vbox_search.getStyleClass ().add ( "footer_3d" );
        } else {
            vbox_search.getStyleClass ().add ( "footer" );
        }

        UtilsOmsk.resizeNode ( this , grid1 );
        UtilsOmsk.painter3d ( tf_search );
        refresh ();
    }

    public static String reader ( String cvs ) {
        String[] strings = cvs.split ( ";" , -1 );

        String msg = "";
        switch (strings[3]) {
            case "11": {
                Event_1_11_2_12_4_14 f = new Event_1_11_2_12_4_14 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 +
                        "  Регистрация товара из справочника   товар: " + f.codeProd_8 + "   Количество: " +
                        f.amount_prod_11 + "   Цена: " + f.price_not_discount_10 + " руб.   Итого; " +
                        f.summ_itogo_20 + " руб.  doc: " + f.info_doc_26;
                ;
                break;
            }
            case "12": {
                msg = "не реализован";
                break;
            }
//            case "14": {
//                eList.add(new Event_1_11_2_12_4_14(strings));
//                break;
//            }
            case "40": {
                Event_40_41 f = new Event_40_41 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "  Оплата с вводом суммы клиента. doc: " + f.info_doc_26;
                break;
            }
            case "41": {
                Event_40_41 f = new Event_40_41 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + " " +
                        " Смена: " + f.number_smena_14 + "  " +
                        "Оплата без ввода суммы клиента Cумма: "
                        + f.summ_doc_11 + " руб. doc: " + f.info_doc_26;
                ;
                break;
            }
            case "42": {
                Event_42_55 f = new Event_42_55 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "  Открытие документа doc: " + f.info_doc_26;
                ;
                break;
            }
            case "55": {
                Event_42_55 f = new Event_42_55 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "  Закрытие документа doc: " + f.info_doc_26;
                ;
                break;
            }
            case "50": {
                Event_50_51 f = new Event_50_51 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Внесение Cумма: " + f.double_3_12 + " doc: " + f.info_doc_26;
                break;
            }
            case "51": {
                Event_50_51 f = new Event_50_51 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Выплата Сумма: " + f.double_3_12 + " doc: " + f.info_doc_26;
                break;
            }
            case "60": {
                Event_60_61_62_63_64 f = new Event_60_61_62_63_64 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Отчёт без гашения  doc: " + f.info_doc_26;
                break;
            }
            case "61": {
                Event_60_61_62_63_64 f = new Event_60_61_62_63_64 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Закрытие смены doc: " + f.info_doc_26;
                ;
                break;
            }
            case "62": {
                Event_60_61_62_63_64 f = new Event_60_61_62_63_64 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Открытие смены doc: " + f.info_doc_26;
                ;
                break;
            }
            case "63": {
                Event_60_61_62_63_64 f = new Event_60_61_62_63_64 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Отчёт с гашением doc: " + f.info_doc_26;
                ;
                break;
            }
            case "64": {
                Event_60_61_62_63_64 f = new Event_60_61_62_63_64 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Документ открытия смены doc: " + f.info_doc_26;
                ;
                break;
            }
            case "56": {
                Event_56 f = new Event_56 ( strings );
                msg = String.valueOf ( f.numberAction_1 ) + "  Смена: " + f.number_smena_14 + "   Возрат по чеку код чека возврата: doc - " + f.info_doc_26;
                break;
            }
            case "300": {
                break;
            }
            default: {
                msg = (" не могу определить транзакцию - " + strings[3]);
                //throw new RuntimeException(" не могу определить транзакцию - " + strings[3]);
            }
        }
        return msg;


    }

    @Override
    public void focus () {
        Platform.runLater ( () -> tf_search.requestFocus () );
    }

    @Override
    public void refresh () {

        list.clear ();
        table_error.getItems ().clear ();

        if ( SettingAppE.getSession () == -1 ) {
            DialogFactory.errorDialog( "Ошибка поиска текущей сесии" );
            return;
        }


        List <String> lines= ReaderBugFilesReverce.getStringList (new File ( Pather.checkStory ) ,SettingAppE.getSession ()  );


        for (String line : lines) {

            MError mError = new MError ();
            mError.msg = reader ( line );
            list.add ( mError );
        }

        new BuilderTable ().build ( list , table_error );
        table_error.scrollTo ( table_error.getItems ().size () - 1 );

        tf_search.textProperty ().addListener ( new ChangeListener <String> () {
            @Override
            public void changed ( ObservableValue <? extends String> observable , String oldValue , String newValue ) {
                UtilsOmsk.searchErrorMessage( newValue , list , table_error );
            }
        } );
        focus ();
        Controller.writeMessage(Support.str("name29"));
    }
}
