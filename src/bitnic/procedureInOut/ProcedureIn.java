package bitnic.procedureInOut;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.kassa.ProcedureInKassa;
import bitnic.pagetableproduct.IRefreshLoader;
import bitnic.parserfrontol.Testfrontol;
import bitnic.senders.IAction;
import bitnic.senders.SenderRequestFromServer;
import bitnic.utils.Support;
import org.apache.log4j.Logger;

public class ProcedureIn {

    private static final Logger log = Logger.getLogger(ProcedureIn.class);
    StringBuilder stringBuilder = new StringBuilder(Support.str("name185"));
    //1 Обновление данных с сервера
//    2 пароль
//    3 Синхронизация
//    4 Открытие смены
//    5 Внесение денег

 int ii=0;
    public void start() {
        stringBuilder.append(System.lineSeparator()).append(System.lineSeparator());
        updateDate();
    }

    public void updateDate() {
        new Testfrontol().test(o -> {
            if (o == null) {
                stringBuilder.append("Данные с сервера загружены УСПЕШНО.").append(System.lineSeparator());
                if (Controller.curnode != null) {
                    if (Controller.curnode instanceof IRefreshLoader) {
                        ((IRefreshLoader) Controller.curnode).refresh();
                    }
                }
            } else {
                stringBuilder.append("Загрузка данных с сервера ОШИБКА.").append(System.lineSeparator());
                stringBuilder.append(String.valueOf(o)).append(System.lineSeparator());
                stringBuilder.append("загрузку производить  в ручную").append(System.lineSeparator());
            }
            new SenderRequestFromServer().send(new IAction<String>() {
                @Override
                public boolean action(String s) {
                    if(s!=null){
                        stringBuilder.append("Загрузка заявок поставщика с сервера ОШИБКА.").append(System.lineSeparator());
                        stringBuilder.append(s).append(System.lineSeparator());
                        stringBuilder.append("Загрузку заявок поставщика производить вручную.").append(System.lineSeparator());
                        ii=1;

                    }
                    stringBuilder.append("Заявки поставщика загружены УСПЕШНО").append(System.lineSeparator());
                    contributeMoney();
                    return false;
                }
            });

            return false;
        });
    }




    boolean rum = false;

    private void contributeMoney() {



        DialogFactory.contributindMoneyProcedureDialog(o -> {


            ProcedureInKassa.ResultProcedureIn  procedureIn=o;
            if(procedureIn==null){
                stringBuilder.append("Процедура входа прервана пользователем!").append(System.lineSeparator());
                stringBuilder.append("Синхронизацию ККМ производить в ручную").append(System.lineSeparator());
                stringBuilder.append("Открытие смены в ККМ производить в ручную").append(System.lineSeparator());
                stringBuilder.append("Внесение денег в ККМ производить в ручную").append(System.lineSeparator());
                stringBuilder.append("СМЕНА НЕ ОТКРЫТА").append(System.lineSeparator());
            }else {
                if(procedureIn.exception1!=null){
                    stringBuilder.append("Смена не открыта Ошибка.").append(System.lineSeparator());
                    stringBuilder.append(procedureIn.exception1.getMessage()).append(System.lineSeparator());
                    stringBuilder.append("Внесение денег  Не произведено.").append(System.lineSeparator());
                    stringBuilder.append("Синхронизацию ККМ производить в ручную").append(System.lineSeparator());
                    stringBuilder.append("Открытие смены в ККМ производить в ручную").append(System.lineSeparator());
                    stringBuilder.append("Внесение денег в ККМ производить в ручную").append(System.lineSeparator());
                    stringBuilder.append("СМЕНА НЕ ОТКРЫТА").append(System.lineSeparator());
                }
                if(procedureIn.exception2!=null){
                    stringBuilder.append("Смена Открыта .").append(System.lineSeparator());
                    stringBuilder.append("Внесение денег  Ошибка.").append(System.lineSeparator());
                    stringBuilder.append(procedureIn.exception2.getMessage()).append(System.lineSeparator());
                    stringBuilder.append("Внесение денег в ККМ производить в ручную").append(System.lineSeparator());
                    stringBuilder.append("СМЕНА ОТКРЫТА").append(System.lineSeparator());
                }
                if(procedureIn.exception1==null&&procedureIn.exception2==null){
                    stringBuilder.append("Внесение денег  УСПЕШНО.").append(System.lineSeparator());
                    stringBuilder.append("Синхронизаця ККМ УСПЕШНО").append(System.lineSeparator());
                    stringBuilder.append("Открытие смены в ККМ УСПЕШНО").append(System.lineSeparator());
                    stringBuilder.append("Внесение денег в ККМ УСПЕШНО").append(System.lineSeparator());
                }
            }
            close();
            return false;
        });
    }

    private void close() {
        DialogFactory.infoDialog(Support.str("name185"), stringBuilder.toString());

        Controller.getInstans().refreshMenu();
        log.info(stringBuilder.toString());
    }
}
