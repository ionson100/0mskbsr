package bitnic.procedureInOut;

import bitnic.bank.sber.SummatorBankingAction;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.closesessionsber.CloseSession;
import bitnic.header.Cheader;
import bitnic.pagesale.MainForm;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Transaction;
import bitnic.utils.Support;
import org.apache.log4j.Logger;

public class ProcedureOut {


    private static final Logger log = Logger.getLogger(ProcedureOut.class);
    StringBuilder stringBuilder = new StringBuilder(Support.str("name188"));
//    1 закрыть банковскую смену;

//    2 z отчет
//    3 Выход из системы

    public void start() {

        stringBuilder.append(System.lineSeparator()).append(System.lineSeparator());


        CloseSession cs = SummatorBankingAction.getCloseSession();
        log.info(" сбербанк закрытие число чеков: " + cs.amount_transaction + " итого сумм: " + cs.summ_action);

        new Transaction().procedureOut(cs, new IAction<String>() {
            @Override
            public boolean action(String o) {
                stringBuilder.append(o).append(System.lineSeparator());
                systemOut();
                return false;
            }
        });
    }


    private void systemOut() {

        SettingAppE.getInstance().user_id = null;
        SettingAppE.save();
        SettingAppE.refreshInstance();
        Cheader.refreshUser();
        Controller.getInstans().refreshMenu();
        Controller.getInstans().factoryPage(new MainForm());
        close();
    }

    public void close() {
        DialogFactory.infoDialog(Support.str("name188"), stringBuilder.toString());
        log.error(stringBuilder.toString());

        //Sett Controller.getInstans().refreshMenu();
    }


}
