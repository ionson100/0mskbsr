package bitnic.senderstate;

import bitnic.core.Controller;
import bitnic.model.MTransactions;
import bitnic.orm.Configure;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.List;

public class CheckStateImage {
    private static final Logger log = Logger.getLogger(CheckStateImage.class);
    private static Object lock = new Object();

    public static void check() {
        synchronized (lock) {
            try {
                List<String> list = Files.readAllLines(Paths.get(Pather.senderStateFile));
                if (list.size() != 2) return;
                Controller.refreshImageState(list.get(0), list.get(1));
            } catch (IOException e) {
                log.error(e);
            }
        }
    }

    public static void refreshFileForSenderToServer() {
        synchronized (lock) {
            try {
                List<String> list = Files.readAllLines(Paths.get(Pather.senderStateFile));
                rewritefile(list);
                MessageDigest md = MessageDigest.getInstance("MD5");
                StringBuilder sb=new StringBuilder();
                for (MTransactions tt : Configure.getSession().getList(MTransactions.class, null)) {
                    sb.append(tt.str);
                }
                md.update(sb.toString().getBytes());
                byte[] digest = md.digest();
                String s = DatatypeConverter.printHexBinary(digest).toUpperCase();

                UtilsOmsk.rewriteFile(Pather.senderStateFile, list.get(1) + System.lineSeparator() + s);
                Controller.refreshImageState(list.get(1), s);
            } catch (Exception e) {
                log.error(e);
            }
        }
    }
    public static void refreshFileForAddLines(String s) {
        synchronized (lock) {
            try {
                List<String> list = Files.readAllLines(Paths.get(Pather.senderStateFile));
                rewritefile(list);
                UtilsOmsk.rewriteFile(Pather.senderStateFile, list.get(1) + System.lineSeparator() + s);
                Controller.refreshImageState(list.get(1), s);
            } catch (Exception e) {
                log.error(e);
            }
        }
    }

    private static void rewritefile(List<String> list) throws Exception {
        if (list.size() != 2) {
            UtilsOmsk.rewriteFile(Pather.senderStateFile, "0" + System.lineSeparator() + "0");
            list.clear();
            list.add("0");
            list.add("0");
        }
    }
}
