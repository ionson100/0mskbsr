package bitnic.checkarchive;

import bitnic.core.Controller;
import bitnic.model.MProduct;
import bitnic.orm.Column;
import bitnic.orm.Configure;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.table.DisplayColumn;

import bitnic.table.celldescriptions.TableTestCenter;
import bitnic.utils.UtilsOmsk;
import bitnic.table.celldescriptions.TableTestDouble;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table( "archive" )
public class CheckArchiveE  {//extends Repository<CheckArchiveE>
    private static final Logger log = Logger.getLogger(CheckArchiveE.class);




    @DisplayColumn(name_column = "_pk",width = 70,ClassTableCell = TableTestCenter.class)
    @PrimaryKey("_id")
    public int _id;// primary key


    @DisplayColumn(name_column = "session_id")
    @Column("session_id")
    public int session_id;// смена


    @DisplayColumn(name_column = "doc_id")
    @Column("doc_id")
    public int doc_id;//номер кассового документа


    @DisplayColumn(name_column = "date")
    @Column("date")
    public java.util.Date date;// data чека


    @DisplayColumn(name_column = "products")
    @Column("products")
    public String products;

    @DisplayColumn(name_column = "is_active")
    @Column("is_active")
    public boolean is_active=true;// z report action

    @DisplayColumn(name_column = "amount_product",ClassTableCell = TableTestDouble.class)
    @Column("amount_product")
    public double amount_product;

    @DisplayColumn(name_column = "is_bank")
    @Column("is_bank")
    public boolean is_bank;

    @DisplayColumn(name_column = "link_bank")
    @Column("link_bank")
    public String link_bank;

    @DisplayColumn(name_column = "date_return")
    @Column("date_return")
    public Date date_return;

    @DisplayColumn(name_column = "result_bank")
    @Column("result_bank")
    public String result_bank;

    @DisplayColumn(name_column = "check_body_bank")
    @Column("check_body_bank")
    public String check_body_bank;//банковский чек

    @DisplayColumn(name_column = "str_tr")
    @Column("string_cvs")
    public String string_cvs;//


    @DisplayColumn(name_column = "code_doc")
    @Column("cod_doc")
    public String cod_doc;


    @DisplayColumn(name_column = "is_send_bank")
    @Column("is_send_bank")
    public boolean is_send_bank;


    public CheckArchiveE() {

    }
    @DisplayColumn(name_column = "summ")
    @Column("summ")
    public double summ;


    public void insert() {
        Configure.getSession().insert(this );
        Controller.refreshBezNal();

    }





    private List<MProduct> mProductsHiden;

    public List<MProduct> getProductList() {
        if (mProductsHiden == null) {
            Type listOfTestObject = new TypeToken<List<MProductSmail>>() {
            }.getType();
            List<MProductSmail> list2 = UtilsOmsk.getGson().fromJson(products, listOfTestObject);
            List<MProduct> mProducts = new ArrayList<>();
            for (MProductSmail mProductSmail : list2) {
                List<MProduct> list = Configure.getSession().getList(MProduct.class, "id_core = ? ", mProductSmail.code_prod);
                if (list.size() > 0) {
                    MProduct mProduct = list.get(0);
                    mProduct.price = mProductSmail.price;
                    mProduct.selectAmount = mProductSmail.amount;
                    mProducts.add(mProduct);
                } else {
                    MProduct mProduct = new MProduct();
                    mProduct.code_prod = mProductSmail.code_prod;
                    mProduct.price = mProductSmail.price;
                    mProduct.selectAmount = mProductSmail.amount;
                    mProduct.name = "Не найден";
                    mProduct.name_check = "Не найден";
                    mProducts.add(mProduct);
                }
            }
            mProductsHiden = mProducts;
        }
        return mProductsHiden;

    }
    public void setProduct(List<MProduct>  mProducts){
        List<MProductSmail> res=new ArrayList<>();
        for (MProduct mProduct : mProducts) {
            MProductSmail smail=new MProductSmail();
            smail.amount=mProduct.selectAmount;
            smail.code_prod=mProduct.code_prod;
            smail.price=mProduct.price;
            smail.name=mProduct.name_check;
            res.add(smail);
        }

        Type listOfTestObject = new TypeToken<List<MProductSmail>>(){}.getType();
        products = UtilsOmsk.getGson().toJson(res, listOfTestObject);
    }
    public List<MProductSmail> getMProductSmails(){
        Type listOfTestObject = new TypeToken<List<MProductSmail>>() {
        }.getType();
        List<MProductSmail> list2 = UtilsOmsk.getGson().fromJson(products, listOfTestObject);
        return list2;
    }

    public static void updateReturn(int doc_id) {

        System.out.println("update archive set  date = "+new Date().getTime()+" where doc_id = "+doc_id);
        Configure.getSession().execSQL("update archive set  date_return = "+new Date().getTime()+" where doc_id = "+doc_id);

        Controller.getInstans().refreshBezNal();
    }

    public static void zReport(int numberSession) {
        Configure.getSession().execSQL("update archive set is_active = ? ",false);
       log.info("заперли точки Z отчетом");
    }
}

