package bitnic.checkarchive;

import bitnic.model.MProduct;
import bitnic.orm.Column;
import bitnic.orm.Configure;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableTestCenter;
import bitnic.table.celldescriptions.TableTestDouble;
import bitnic.table.celldescriptions.TableTestName;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table( "amountselftotal" )
public class AmountSelfTotal {



    private static final Logger log = Logger.getLogger ( AmountSelf.class );


    public AmountSelfTotal () {
        date_sell = new Date();
    }

   // @DisplayColumn ( name_column = "_pk", ClassTableCell = TableTestCenter.class )
   @PrimaryKey( "_id" )
    public int _id;// primary key

    @DisplayColumn ( name_column = "смена", ClassTableCell = TableTestCenter.class,width = 100)
    @Column( "session_id" )
    public int  session_id;

    @DisplayColumn ( name_column = "ид продукта", ClassTableCell = TableTestCenter.class,width = 150)
    @Column( "code_product" )
    public String code_product;

    @DisplayColumn ( name_column = "док ККМ", ClassTableCell = TableTestCenter.class,width = 100)
    @Column( "doc_id" )
    public int doc_id;

    @DisplayColumn ( name_column = "дата", width = 300 )
    @Column( "date_sell" )
    public Date date_sell;


    @DisplayColumn ( name_column = "наименование", ClassTableCell = TableTestName.class,width = 500)
    @Column( "name_prod" )
    public String name_prod;

    @DisplayColumn ( name_column = "кол-во", ClassTableCell = TableTestDouble.class )
    @Column( "amount" )
    public double amount;


    @DisplayColumn ( name_column = "цена", ClassTableCell = TableTestDouble.class )
    @Column( "prise" )
    public double prise;

    @DisplayColumn ( name_column = "итого", ClassTableCell = TableTestDouble.class )
    public double summ;


    public static void addProductSell (List<MProduct> mProducts , int doc_id ,int session_id) {

        for (MProduct mProduct : mProducts) {
            AmountSelfTotal self = new AmountSelfTotal ();
            self.amount = mProduct.selectAmount * -1;
            self.doc_id = doc_id;

            self.session_id=session_id;
            self.code_product = mProduct.code_prod;
            self.name_prod = mProduct.name;
            self.prise = mProduct.price;
            Configure.getSession().insert ( self );
        }
    }

    public static void addProductReturn ( List <MProduct> mProducts , int doc_id ,int session_id) {

        for (MProduct mProduct : mProducts) {
            AmountSelfTotal self = new AmountSelfTotal ();
            self.amount = mProduct.selectAmount;
            self.doc_id = doc_id;
            self.session_id=session_id;
            self.name_prod = mProduct.name;
            self.code_product = mProduct.code_prod;
            self.prise = mProduct.price;
            Configure.getSession().insert ( self );
        }
    }



    public static void insertBulk ( List <MProduct> mProducts , int doc_id , boolean isReturn ,int session_id) {
        List <AmountSelfTotal> mProductList = new ArrayList<>( mProducts.size () );
        for (MProduct mProduct : mProducts) {
            AmountSelfTotal self = new AmountSelfTotal ();
            if ( isReturn ) {
                self.amount = mProduct.selectAmount * -1;
            } else {
                self.amount = mProduct.selectAmount;
            }
            self.session_id=session_id;
            self.doc_id = doc_id;
            self.code_product = mProduct.code_prod;
            self.name_prod = mProduct.name;
            self.prise = mProduct.price;
            mProductList.add ( self );
        }
        Configure.bulk( AmountSelfTotal.class ,mProductList,Configure.getSession());
    }
}
