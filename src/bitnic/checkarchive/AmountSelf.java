package bitnic.checkarchive;

import bitnic.model.MProduct;
import bitnic.orm.Column;
import bitnic.orm.Configure;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.table.DisplayColumn;
import bitnic.table.celldescriptions.TableTestCenter;
import bitnic.table.celldescriptions.TableTestDouble;
import bitnic.table.celldescriptions.TableTestName;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table( "amountself" )
public class AmountSelf {//extends Repository<AmountSelf>


    private static final Logger log = Logger.getLogger ( AmountSelf.class );


    public AmountSelf () {
        date_sell = new Date ();
    }

    @DisplayColumn ( name_column = "_pk", ClassTableCell = TableTestCenter.class )
    @PrimaryKey( "_id" )
    public int _id;// primary key



    @DisplayColumn ( name_column = "ид продукта", ClassTableCell = TableTestCenter.class )
    @Column( "code_product" )
    public String code_product;

    @DisplayColumn ( name_column = "док ККМ", ClassTableCell = TableTestCenter.class )
    @Column ( "doc_id" )
    public int doc_id;

    @DisplayColumn ( name_column = "дата", width = 300 )
    @Column ( "date_sell" )
    public Date date_sell;


    @DisplayColumn ( name_column = "наименование", ClassTableCell = TableTestName.class )
    @Column ( "name_prod" )
    public String name_prod;

    @DisplayColumn ( name_column = "кол-во", ClassTableCell = TableTestDouble.class )
    @Column ( "amount" )
    public double amount;


    @DisplayColumn ( name_column = "цена", ClassTableCell = TableTestDouble.class )
    @Column ( "prise" )
    public double prise;


    public static void addProductSell ( List <MProduct> mProducts , int doc_id ) {

        for (MProduct mProduct : mProducts) {
            AmountSelf self = new AmountSelf ();
            self.amount = mProduct.selectAmount * -1;
            self.doc_id = doc_id;

            self.code_product = mProduct.code_prod;
            self.name_prod = mProduct.name;
            self.prise = mProduct.price;
            Configure.getSession().insert ( self );
        }
    }

    public static void addProductReturn ( List <MProduct> mProducts , int doc_id ) {

        for (MProduct mProduct : mProducts) {
            AmountSelf self = new AmountSelf ();
            self.amount = mProduct.selectAmount;
            self.doc_id = doc_id;
            self.name_prod = mProduct.name;
            self.code_product = mProduct.code_prod;
            self.prise = mProduct.price;
            Configure.getSession().insert ( self );
        }
    }

    public static double getAmountCore ( String code_product ) {

        Double aDouble = (Double) Configure.getSession().executeScalar("select sum(amount) from amountself where code_product = ? " , code_product);
        if ( aDouble == null ) {
            return 0d;
        } else {
            return aDouble;
        }
    }

    public static void insertBulk ( List <MProduct> mProducts , int doc_id , boolean isReturn ) {
        List <AmountSelf> mProductList = new ArrayList <> ( mProducts.size () );
        for (MProduct mProduct : mProducts) {
            AmountSelf self = new AmountSelf ();
            if ( isReturn ) {
                self.amount = mProduct.selectAmount * -1;
            } else {
                self.amount = mProduct.selectAmount;
            }

            self.doc_id = doc_id;
            self.code_product = mProduct.code_prod;
            self.name_prod = mProduct.name;
            self.prise = mProduct.price;
            mProductList.add ( self );
        }
        Configure.bulk( AmountSelf.class, mProductList ,Configure.getSession());
    }
}
