package bitnic.dialogfactory.ofd;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.kassa.FactoryOfd;
import bitnic.senders.IAction;

import java.net.URL;

public class FOfd extends DialogBase  {

    public TextField ofd_ip,ofd_port;
    public Button bt_open_board;
    public FOfd(URL location, Stage stage) {
        super(location, stage);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        stage.setOnCloseRequest(we -> {
            DialogBase.deleteBlenda();
        });
        ofd_ip.textProperty().addListener((observable, oldValue, newValue) -> validate());
        ofd_port.textProperty().addListener((observable, oldValue, newValue) -> validate());
        Platform.runLater(() -> ofd_ip.requestFocus());
        bt_save.setDisable(true);
        bt_save.setOnAction( event -> new FactoryOfd().saveOfd(new FactoryOfd.Ofd(ofd_ip.getText(), Integer.parseInt(ofd_port.getText())), new IAction<String>() {
            @Override
            public boolean action(String s) {
                DialogFactory.infoDialog("Настройки офд",s);
                return false;
            }
        }) );

        new bitnic.kassa.FactoryOfd().getOfd( ofd -> {
            ofd_ip.setText(ofd.ofd_ip);
            ofd_port.setText(String.valueOf(ofd.ofd_port));
            return false;
        } );

        Platform.runLater( () -> ofd_ip.requestFocus() );

        UtilsOmsk.painter3d(bt_close1,bt_save,ofd_ip,ofd_port,bt_open_board);

        label_title.setText ( Support.str ("name78") );

        bt_open_board.setOnAction ( event -> {
            UtilsOmsk.openKeyBoard ();
            Platform.runLater ( () -> ofd_ip.requestFocus () );
        } );

    }
    private   void validate(){
        int i=ofd_ip.getText().trim().length(),i2=ofd_port.getText().trim().length();
        if(i>0&&i2>0){
            bt_save.setDisable(false);
        }else {
            bt_save.setDisable(true);
        }
    }
}
