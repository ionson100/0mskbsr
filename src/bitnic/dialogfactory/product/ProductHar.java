package bitnic.dialogfactory.product;

import bitnic.barcodegenerate.BarcodePrint;
import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.model.MBarcode;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.settingscore.SettingAppE;

import java.net.URL;
import java.util.List;

public class ProductHar extends DialogBase {

    public Button  bt_print;
    public TextField tf_count_barcode;
    public Label label_name;
    public Label label_name_check;
    public Label label_price;
    public Label label_barcode;
    private String barcode;

    public ProductHar(URL url, MProduct mProduct, Stage stage) {

        super(url, stage);

        UtilsOmsk.painter3d(bt_close1,tf_count_barcode,bt_print);
        label_title.setText ( "Описание товара" );
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        stage.setOnCloseRequest(we -> {
            DialogBase.deleteBlenda();
        });
        label_name.setText(mProduct.name);
        label_name_check.setText(mProduct.name_check);
        label_price.setText(String.valueOf(mProduct.price) + " руб.");
        List<MBarcode> mBarcodes= Configure.getSession().getList(MBarcode.class," id_product = ? ",mProduct.code_prod);
        if(mBarcodes.size()==0){
            label_barcode.setText("Barcode отсутствует");
        }else {
            barcode=mBarcodes.get(0).barcode;
            label_barcode.setText(barcode);
        }
        tf_count_barcode.textProperty().addListener((observable, oldValue, newValue) -> {
            int i;
            try{

                i=Integer.parseInt(newValue);
                if(i>0){
                    SettingAppE.getInstance().copy=i;
                    SettingAppE.save();
                }
            }catch (Exception ex){
                tf_count_barcode.setText("");
            }
        });
        tf_count_barcode.setText(String.valueOf(SettingAppE.getInstance().copy));
        bt_print.setOnAction(event -> {
           if(barcode==null||barcode.trim().length()==0){
               return;
           }
           new BarcodePrint().print(barcode);
        });

        Platform.runLater(() -> tf_count_barcode.requestFocus());



    }
}
