package bitnic.dialogfactory.contributingmoney;

import bitnic.dialogfactory.DialogBase;
import bitnic.keyboardsmail.FKeyBoardSmail;
import bitnic.transaction.Transaction;
import bitnic.utils.TabRoute;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;

public class FContributingMoney extends DialogBase {

    TabRoute tabRoute;

    private double aDouble;
    public TextField tf_summ;
    public Button bt_execute, bt_open_board;
    public AnchorPane anchorbase;


    public FContributingMoney(URL location, Stage stage) {
        super(location, stage);


        tf_summ.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    aDouble = Double.parseDouble(tf_summ.getText().trim().replace(",", "."));
                    bt_execute.setDisable(false);
                } catch (Exception ex) {
                    aDouble = 0;
                    bt_execute.setDisable(true);
                }
            }
        });
        bt_execute.setDisable(true);


        bt_execute.setOnAction(event -> {
           close ();
        });
        bt_close1.setOnAction(event -> FContributingMoney.this.handle(event));

        bt_close2.setOnAction(event -> FContributingMoney.this.handle(event));
        Platform.runLater(() -> tf_summ.requestFocus());

        UtilsOmsk.painter3d(bt_close1, tf_summ, bt_execute,bt_open_board);

        bt_open_board.setOnAction(a -> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(() -> tf_summ.requestFocus());
        });
        label_title.setText(Support.str("name58"));


        tabRoute = new TabRoute();
        tabRoute.addNode(tf_summ, node -> bt_execute.fire ());
        tabRoute.addNode(bt_execute);
        tabRoute.addNode(bt_close1);
        FKeyBoardSmail smail=new FKeyBoardSmail(tf_summ);
        smail.setPrefHeight(250);
        anchorbase.getChildren().add(smail);
    }
    void close(){
        new Transaction().casheIn(aDouble);
        bt_close1.fire();
    }
}
