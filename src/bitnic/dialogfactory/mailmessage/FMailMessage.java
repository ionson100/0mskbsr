package bitnic.dialogfactory.mailmessage;

import bitnic.dialogfactory.DialogBase;
import bitnic.sendertoemail.SenderToEmail;
import bitnic.utils.TabRoute;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.net.URL;

public class FMailMessage extends DialogBase {

    private TabRoute tabRoute;
    @FXML
    public Label label_content;

    //public Node head;
    public Button bt_sender;
    public TextArea text_area;


    public FMailMessage(URL location, Stage stage) {
        super(location, stage);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1);
        Platform.runLater(() -> bt_close1.requestFocus());
        tabRoute = new TabRoute();
        tabRoute.addNode(bt_close1);
        label_title.setText(Support.str("name315"));
        bt_sender.setOnAction(event -> {
            new Thread(new SenderToEmail(text_area.getText(),true,0)).start();
            bt_close2.fire();
        });
        UtilsOmsk.painter3d(bt_sender);

    }
}
