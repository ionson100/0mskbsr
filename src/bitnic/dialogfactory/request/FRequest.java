package bitnic.dialogfactory.request;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.productforcheck.FProductCheck;
import bitnic.dialogfactory.productforcheck.itemProductforcheck.FItemProductForCheck;
import bitnic.dialogfactory.request.itemrequest.FItemRequest;
import bitnic.model.MProduct;
import bitnic.model.MRequest;
import bitnic.orm.Configure;
import bitnic.senders.IAction;

import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.net.URL;

public class FRequest extends DialogBase {


    private final IAction iAction;
    public javafx.scene.control.ListView list_request;

    public FRequest(URL location, Stage stage, IAction iAction) {
        super(location, stage);
        this.iAction = iAction;

        bt_close2.setOnAction ( this );
        UtilsOmsk.painter3d ( bt_close1 );
        Platform.runLater ( () -> bt_close1.requestFocus () );

        bt_close1.setOnAction(event -> {
            if(iAction!=null){
                iAction.action(null);
                bt_close2.fire();
            }
        });

        label_title.setText("Заявки");

        ObservableList observableList= FXCollections.observableList(Configure.getSession().getList(MRequest.class," 1 == 1 order by date_e "));
        list_request.getItems().addAll(observableList);
        list_request.setCellFactory((Callback<javafx.scene.control.ListView<MRequest>, ListCell<MRequest>>) list -> new CheckCell(iAction, new IAction() {
            @Override
            public boolean action(Object o) {
                bt_close2.fire();
                return false;
            }
        }));

    }


    static class CheckCell extends ListCell<MRequest> {
        private IAction iAction;
        private IAction close;

        public CheckCell(IAction iAction,IAction close){

            this.iAction = iAction;
            this.close = close;
        }
        @Override
        public void updateItem(MRequest item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemRequest check=new FItemRequest(item,iAction,close);
                setGraphic(check);
            }
        }
    }

}
