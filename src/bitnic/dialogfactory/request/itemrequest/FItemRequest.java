package bitnic.dialogfactory.request.itemrequest;

import bitnic.dialogfactory.productforcheck.itemProductforcheck.FItemProductForCheck;
import bitnic.model.MRequest;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemRequest extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger ( FItemRequest.class );

    private double lastYposition=0d;
    public Button button_select;
    public Label label_id,label_date,label_name;

    private MRequest mRequest;
    private IAction iAction;
    private IAction actionClose;

    public FItemRequest(MRequest mRequest, IAction iAction, IAction actionClose ){

        this.mRequest = mRequest;
        this.iAction = iAction;
        this.actionClose = actionClose;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_request.fxml"),bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException(exception);
        }
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        label_date.setText(UtilsOmsk.simpleDateFormatE(mRequest.date));
        label_id.setText("№: "+mRequest.id);
        label_name.setText(mRequest.shipper);
        if (SettingAppE.getInstance().isTypeЫShow3D) {
            button_select.getStyleClass().add("button_key_big_3d");
        } else {
            button_select.getStyleClass().add("button_key_big");
        }
        button_select.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                actionClose.action(mRequest);
                iAction.action(mRequest);
            }
        });


        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });


    }


}
