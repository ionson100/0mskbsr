package bitnic.dialogfactory.closesessionsber;

import bitnic.bank.sber.SummatorBankingAction;
import bitnic.dialogfactory.DialogBase;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import java.net.URL;

public class FCloseSessionSber extends DialogBase {
    private static final Logger log = Logger.getLogger(FCloseSessionSber.class);

    //public Button bt_save;

    public Label  label_amount, label_summ,label_session;


    int session;
    CloseSession closeSession ;


    public FCloseSessionSber(URL location, Stage stage, IAction<CloseSession> iAction) {
        super(location, stage);


        session=SettingAppE.getInstance().getSession();
        label_title.setText(Support.str("name177"));

        UtilsOmsk.painter3d(bt_close1, bt_save);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);






        bt_save.setOnAction(event -> {
            if (session > 0) {
                if (iAction != null) {
                    bt_close1.fire();
                    iAction.action(closeSession);
                }
            }
        });

        closeSession = SummatorBankingAction.getCloseSession();
        closeSession.session = session;
        label_amount.setText("Количество чеков продаж: " + String.valueOf(closeSession.amount_transaction));
        label_summ.setText("Сумма продаж: " + String.valueOf(closeSession.summ_action) + " руб.");
        label_session.setText("Смена ККМ: "+closeSession.session);

    }
}

