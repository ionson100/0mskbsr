package bitnic.dialogfactory.reguser;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.header.Cheader;
import bitnic.keyboardsmail.FKeyBoardSmail;
import bitnic.model.MUser;
import bitnic.orm.Configure;
import bitnic.pagesale.MainForm;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import java.net.URL;
import java.util.List;



public class FRegUser extends DialogBase {
    private static final Logger log = Logger.getLogger(FRegUser.class);
    private final IAction iActionProcedure;
    public Button  bt_open_board;
    public TextField tf_pwd;
    public Label label_error;
    public AnchorPane pane;


    public FRegUser(URL location, Stage stage, IAction iActionProcedure) {
        super(location, stage);


        FKeyBoardSmail smail=new FKeyBoardSmail(tf_pwd);
        smail.iAction= o -> {
            label_error.setText("");
            return false;
        };
        smail.setPrefHeight(250);
        pane.getChildren().add(smail);

        this.iActionProcedure = iActionProcedure;

        stage.setOnHidden(event -> {
            String str = SettingAppE.getInstance().user_id;
            List<MUser> mUsers = Configure.getSession().getList(MUser.class, " password = ? ", str);
            if (mUsers.size() == 0) {
                label_error.setText(Support.str("name314"));
            }
        });
        label_title.setText(Support.str("name13"));

        UtilsOmsk.painter3d(bt_close1, bt_save, tf_pwd,bt_open_board);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        bt_save.setOnAction(event -> {

            String s1=SettingAppE.defUserAdmin;
            String s2=SettingAppE.defUserAdmin2;

            String str = tf_pwd.getText();

            if (str.equals(SettingAppE.defUser)) {
                SettingAppE.getInstance().user_id = SettingAppE.UniversalUserKey;
                SettingAppE.save();
                SettingAppE.refreshInstance();

                Controller.getInstans().refreshMenu();
                Cheader.refreshUser();
                Controller.getInstans().factoryPage(new MainForm());
                log.error("Вход пользователя обезличенный продавец");
                bt_close1.fire();
            } else if (str.equals(s1) || str.equals(s2)) {

                SettingAppE.getInstance().user_id = "1";
                SettingAppE.save();
                SettingAppE.refreshInstance();

               //
                Cheader.refreshUser();
                Controller.getInstans().factoryPage(new MainForm());
                log.info("Вход пользователя Admin");
                renameUserKkm(new IAction() {
                    @Override
                    public boolean action(Object o) {
                        Controller.getInstans().refreshMenu();
                        return false;
                    }
                });
                bt_close1.fire();


            }else {
                List<MUser> mUsers = Configure.getSession().getList(MUser.class, " password = ? or map_user = ?", str, str);
                if (mUsers.size() > 0) {
                    MUser user = mUsers.get(0);
                    SettingAppE.getInstance().user_id = user.cod;
                    SettingAppE.save();
                    SettingAppE.refreshInstance();

                    Cheader.refreshUser();
                    Controller.getInstans().factoryPage(new MainForm());

                    log.info("Вход пользователя: " + mUsers.get(0).name + " id: " + mUsers.get(0).cod);
                    renameUserKkm(new IAction() {
                        @Override
                        public boolean action(Object o) {
                            Controller.getInstans().refreshMenu();
                            return false;
                        }
                    });
                    bt_close1.fire();

                }else {
                    label_error.setText(Support.str("name314"));
                }
            }
        });




        tf_pwd.textProperty().addListener((observable, oldValue, newValue) ->

        {
            validate(newValue);
            if (newValue.contains("\r")) {
                bt_save.fire();
            }
        });


        Platform.runLater(() -> tf_pwd.requestFocus());
        tf_pwd.setOnKeyPressed(event ->

        {
            String a = event.getText();
            if (a.equals("\r")) {
                Platform.runLater(() -> bt_save.fire());

            }

        });

        bt_open_board.setOnAction(a ->

        {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(() -> tf_pwd.requestFocus());
        });
    }

    void renameUserKkm(IAction iAction){
        new bitnic.kassa.SetUserSettungsKmm().print(o -> {
            if (iActionProcedure != null) {
                iActionProcedure.action(o);
            }else {
                if(o!=null){
                    DialogFactory.infoDialog(Support.str("name194"), Support.str("name195") );                           }
            }
            if(iAction!=null){
                iAction.action(null);
            }
            return false;
        });
    }


    private void validate(String str) {
        if (str == null || str.length() == 0) {
            bt_save.setDisable(true);
        } else {
            bt_save.setDisable(false);
        }
    }

}
