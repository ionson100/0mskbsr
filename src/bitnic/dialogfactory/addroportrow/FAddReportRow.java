package bitnic.dialogfactory.addroportrow;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MTransactions;
import bitnic.orm.Configure;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;

public class FAddReportRow extends DialogBase {

    public TextField tf_data;
    public Button bt_add;

    public FAddReportRow(URL location, Stage stage) {
        super(location, stage);
        label_title.setText ("Добавление строки в файл отчета");
        bt_close1.setOnAction ( this );
        bt_close2.setOnAction ( this );
        UtilsOmsk.painter3d ( bt_close1,bt_add ,tf_data);
        Platform.runLater ( () -> bt_close1.requestFocus () );
        bt_add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tf_data.getText().trim().length()==0){
                    DialogFactory.errorDialog("Поле ввода пустое");
                }else {
                    Configure.getSession().insert(new MTransactions(tf_data.getText()));
                    bt_close1.fire();
                }
            }
        });
    }
}
