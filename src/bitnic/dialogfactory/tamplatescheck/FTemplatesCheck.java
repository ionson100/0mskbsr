package bitnic.dialogfactory.tamplatescheck;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.kassa.GetTemplateCheck;
import bitnic.kassa.SetTemplateCheck;
import bitnic.senders.IAction;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;

public class FTemplatesCheck extends DialogBase {

    @FXML
    public Label label_content;
   // public Node head;
    public TextField tf_value;

    private int res=1;

    public FTemplatesCheck(URL location, Stage stage) {
        super(location, stage);


        label_title.setText(Support.str("name198"));
        label_content.setText(Support.str("name199"));
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1, bt_save, tf_value);
        bt_save.setDisable(true);

        bt_save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(res>0&&res<12){
                    new SetTemplateCheck().set(res);
                }
                bt_close1.fire();

            }
        });

        tf_value.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                try {
                    res=Integer.parseInt(newValue);
                    if(res>0&&res<12){
                        bt_save.setDisable(false);
                    }else {
                        bt_save.setDisable(true);
                    }
                }catch (Exception ex){
                    bt_save.setDisable(true);
                }


            }
        });

        new GetTemplateCheck().get(new IAction<Integer>() {
            @Override
            public boolean action(Integer integer) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if(integer<=0){
                            DialogFactory.infoDialog("Ошибка","Ошибка закрузки шаблона с ККМ");
                            bt_close1.fire();
                        }else {
                            tf_value.setText(String.valueOf(integer));
                            bt_save.setDisable(false);
                        }
                        tf_value.requestFocus();
                    }
                });

                return false;
            }
        });

    }
}
