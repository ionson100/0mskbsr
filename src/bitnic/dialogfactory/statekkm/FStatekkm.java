package bitnic.dialogfactory.statekkm;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import bitnic.kassa.MStateKKM;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;

public class FStatekkm extends DialogBase {


    public Label port_ofd, kkm_mode, addrres_ofd, date_first, not_send_ofd, kkm_pwd,
            number_kkm, kkm_date, kkm_user_name, label_session, label_is_open_session;


    public FStatekkm(URL location, Stage stage, MStateKKM kkm) {
        super(location, stage);


        label_title.setText(Support.str("name88"));
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        kkm_user_name.setText(kkm.username);
        kkm_date.setText(UtilsOmsk.simpleDateFormatE(kkm.date));
        number_kkm.setText(kkm.serialNumer);
        kkm_pwd.setText(kkm.userPassword);
        not_send_ofd.setText(String.valueOf(kkm.notSenderOfd));
        date_first.setText(UtilsOmsk.simpleDateFormatE(kkm.notSenderFirstDate));
        addrres_ofd.setText(kkm.ofdUrl);
        port_ofd.setText(String.valueOf(kkm.ofdPort));
        kkm_mode.setText(String.valueOf(kkm.mode));
        label_session.setText(String.valueOf(kkm.session));

        if (kkm.sessionOpened) {
            label_is_open_session.setText("Смена открыта");
        } else {
            label_is_open_session.setText("Смена закрыта");
        }

        Controller.writeMessage(Support.str("name302"));

        UtilsOmsk.painter3d(bt_close1);
    }




}
