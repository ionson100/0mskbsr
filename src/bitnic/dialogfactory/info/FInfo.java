package bitnic.dialogfactory.info;

import bitnic.dialogfactory.DialogBase;
import bitnic.senders.IAction;
import bitnic.utils.TabRoute;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;

public class FInfo extends DialogBase {


    private  IAction<Object> objectIAction;
    private TabRoute tabRoute;
    @FXML
    public Label label_content;

    //public Node head;


    public FInfo (URL location , Stage stage , String labelTitle, String labelContent ) {
        super ( location , stage );



        label_title.setText (labelTitle);
        label_content.setText ( labelContent );
        bt_close1.setOnAction ( this );
        bt_close2.setOnAction ( this );
        UtilsOmsk.painter3d ( bt_close1 );
        Platform.runLater ( () -> bt_close1.requestFocus () );
        tabRoute=new TabRoute ();
        tabRoute.addNode ( bt_close1 );

    }

    public FInfo(URL location, Stage stage, String labelTitle, String labelContent, IAction<Object> objectIAction) {
        super(location , stage);

        this.objectIAction = objectIAction;
        label_title.setText (labelTitle);
        label_content.setText ( labelContent );

        bt_close2.setOnAction ( this );
        UtilsOmsk.painter3d ( bt_close1 );
        Platform.runLater ( () -> bt_close1.requestFocus () );
        tabRoute=new TabRoute ();
        tabRoute.addNode ( bt_close1 );
        bt_close1.setOnAction(event -> {
            if(objectIAction!=null){
                objectIAction.action(null);
            }
        });
    }
}
