package bitnic.dialogfactory.checkstory.ietemCheckStory;

import bitnic.dialogfactory.barcodeprint.FBarcodePrint;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import bitnic.transaction.eventfrontol.EventBaseE;
import bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemCheckStory extends GridPane implements Initializable {

    private static final Logger log = Logger.getLogger(FItemCheckStory.class);

    public Label label_deate, label_prod, label_amount1, label_amount, label_id, label_num_check, label_price, label_action_name;

    GridPane pane;
    private EventBaseE eventBaseE;


    public FItemCheckStory(EventBaseE eventBaseE) {
        this.eventBaseE = eventBaseE;
        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_check_story.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        label_deate.setText(eventBaseE.timeAction_3);

        label_amount.setText("");
        label_price.setText("");
        label_prod.setText("");
        label_id.setText(String.valueOf(eventBaseE.numberAction_1));
        label_num_check.setText(String.valueOf(eventBaseE.nimberDoc_6));
        Integer i = eventBaseE.int_1_4;
        switch (i) {
            case 1: {
                label_action_name.setText("Регистрация товара по свободной цене");
                Event_1_11_2_12_4_14 e= (Event_1_11_2_12_4_14) eventBaseE;
                label_prod.setText(e.codeProd_8);
                label_amount.setText(String.valueOf(e.amount_rount_prod_12));
                label_price.setText(String.valueOf(e.price_not_discount_10));
                break;
            }
            case 2: {
                label_action_name.setText("Сторно товара по свободной цене");
                Event_1_11_2_12_4_14 e= (Event_1_11_2_12_4_14) eventBaseE;
                label_prod.setText(e.codeProd_8);
                label_amount.setText(String.valueOf(e.amount_rount_prod_12));
                label_price.setText(String.valueOf(e.price_not_discount_10));
                break;
            }
            case 11: {
                label_action_name.setText("Регистрация товара из справочника");
                Event_1_11_2_12_4_14 e= (Event_1_11_2_12_4_14) eventBaseE;
                label_amount.setText(String.valueOf(e.amount_rount_prod_12));
                label_price.setText(String.valueOf(e.price_not_discount_10));
                label_prod.setText(e.codeProd_8);
                break;
            }
            case 12: {
                label_action_name.setText("Сторно товара из справочника");
                Event_1_11_2_12_4_14 e= (Event_1_11_2_12_4_14) eventBaseE;
                label_amount.setText(String.valueOf(e.amount_rount_prod_12));
                label_price.setText(String.valueOf(e.price_not_discount_10));
                label_prod.setText(e.codeProd_8);
                break;
            }
            case 4: {
                label_action_name.setText("Налог на товар по свободной цене");
                Event_1_11_2_12_4_14 e= (Event_1_11_2_12_4_14) eventBaseE;
                label_amount.setText(String.valueOf(e.amount_rount_prod_12));
                label_price.setText(String.valueOf(e.price_not_discount_10));
                label_prod.setText(e.codeProd_8);
                break;
            }
            case 14: {
                label_action_name.setText("Налог на товар из справочника");
                Event_1_11_2_12_4_14 e= (Event_1_11_2_12_4_14) eventBaseE;
                label_amount.setText(String.valueOf(e.amount_rount_prod_12));
                label_price.setText(String.valueOf(e.price_not_discount_10));
                label_prod.setText(e.codeProd_8);
                break;
            }
            case 40: {
                label_action_name.setText("Оплата с вводом суммы клиента");
                break;
            }
            case 41: {
                label_action_name.setText("Оплата без ввода суммы клиента");
                break;
            }
            case 42: {
                label_action_name.setText("Открытие документа");
                break;
            }
            case 55: {
                label_action_name.setText("Закрытие документа");
                break;
            }
            case 45: {
                label_action_name.setText("Закрытие документа в ККМ ");
                break;
            }
            case 50: {
                label_action_name.setText("Внесение");
                break;
            }
            case 51: {
                label_action_name.setText("Выплата");
                break;
            }

            case 56: {
                label_action_name.setText("Аннулирование чека");
                break;
            }
            case 60: {
                label_action_name.setText("Отчёт без гашения");
                break;
            }
            case 61: {
                label_action_name.setText("Закрытие смены");
                break;
            }
            case 62: {
                label_action_name.setText("Открытие смены");
                break;
            }
            case 63: {
                label_action_name.setText("Отчёт с гашением");
                break;
            }
            case 64:{
                label_action_name.setText("Документ открытия смены");
                break;
            }
            default: {
                log.error("Не могу найти тип: "+i);
                throw new RuntimeException("Не могу найти тип: "+i);
            }
        }
    }
}

