package bitnic.dialogfactory.editpagerequest;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.editcountproduct.FEditCountProduct;
import bitnic.kassa.itoger.Totals;
import bitnic.pagerequest.FPageRequest;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.net.URL;

public class FEditPageRequest extends DialogBase {

    private static final Logger log = Logger.getLogger(FEditPageRequest.class);

    private boolean first;
    private FPageRequest.Tovar tovar;
    private IAction iAction;



    public Button bt_update, bt_open_board;
    public TextField tf_value;

    public Button bt_1, bt_2, bt_3, bt_4, bt_5, bt_6, bt_7, bt_8, bt_9, bt_10, bt_del, bt_del2;


    public FEditPageRequest(URL location, Stage stage, FPageRequest.Tovar tovar, IAction iAction) {
        super(location, stage);
        this.tovar = tovar;
        this.iAction = iAction;
        this.label_title.setText("Фактическое количество");

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);

        tf_value.setText(String.valueOf(tovar.fact.getValue()));


        tf_value.textProperty().addListener((observable, oldValue, newValue) -> {

            try {
                tovar.fact.setValue((Double.parseDouble(newValue.replace(",", "."))));
                bt_update.setDisable(false);
            } catch (Exception ex) {

                bt_update.setDisable(true);
            }

        });

        bt_update.setDisable(true);

        bt_open_board.setOnAction(a -> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(() -> tf_value.requestFocus());
        });

        bt_1.setOnAction(event -> action(event));
        bt_2.setOnAction(event -> action(event));
        bt_3.setOnAction(event -> action(event));
        bt_4.setOnAction(event -> action(event));
        bt_5.setOnAction(event -> action(event));
        bt_6.setOnAction(event -> action(event));
        bt_7.setOnAction(event -> action(event));
        bt_8.setOnAction(event -> action(event));
        bt_8.setOnAction(event -> action(event));
        bt_9.setOnAction(event -> action(event));
        bt_10.setOnAction(event -> action(event));
        bt_del.setOnAction(event -> action(event));
        bt_del2.setOnAction(event -> action(event));

        UtilsOmsk.painter3d(tf_value, bt_close1, bt_update, bt_open_board);

        if (SettingAppE.getInstance().isTypeЫShow3D) {

            bt_1.getStyleClass().add("button_key_big_3d");
            bt_2.getStyleClass().add("button_key_big_3d");
            bt_3.getStyleClass().add("button_key_big_3d");
            bt_4.getStyleClass().add("button_key_big_3d");
            bt_5.getStyleClass().add("button_key_big_3d");
            bt_6.getStyleClass().add("button_key_big_3d");
            bt_7.getStyleClass().add("button_key_big_3d");
            bt_8.getStyleClass().add("button_key_big_3d");
            bt_8.getStyleClass().add("button_key_big_3d");
            bt_9.getStyleClass().add("button_key_big_3d");
            bt_10.getStyleClass().add("button_key_big_3d");
            bt_del.getStyleClass().add("button_key_big_3d");
            bt_del2.getStyleClass().add("button_key_big_3d");


        } else {

            bt_1.getStyleClass().add("button_key_big");
            bt_2.getStyleClass().add("button_key_big");
            bt_3.getStyleClass().add("button_key_big");
            bt_4.getStyleClass().add("button_key_big");
            bt_5.getStyleClass().add("button_key_big");
            bt_6.getStyleClass().add("button_key_big");
            bt_7.getStyleClass().add("button_key_big");
            bt_8.getStyleClass().add("button_key_big");
            bt_8.getStyleClass().add("button_key_big");
            bt_9.getStyleClass().add("button_key_big");
            bt_10.getStyleClass().add("button_key_big");
            bt_del.getStyleClass().add("button_key_big");
            bt_del2.getStyleClass().add("button_key_big");


        }
        bt_1.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_2.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_3.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_4.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_5.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_6.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_7.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_8.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_8.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_9.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_10.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_del.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_del2.setStyle("-fx-font-size: " + SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);

        bt_update.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(iAction!=null){
                    iAction.action(tovar);

                }
                bt_close1.fire();
            }
        });


    }

    void action(ActionEvent event) {

        Button button = (Button) event.getSource();
        String id = button.getId();

        switch (id) {
            case "bt_1": {
                pult("1");
                break;
            }
            case "bt_2": {
                pult("2");
                break;
            }
            case "bt_3": {
                pult("3");
                break;
            }
            case "bt_4": {
                pult("4");
                break;
            }
            case "bt_5": {
                pult("5");
                break;
            }
            case "bt_6": {
                pult("6");
                break;
            }
            case "bt_7": {
                pult("7");
                break;
            }
            case "bt_8": {
                pult("8");
                break;
            }
            case "bt_9": {
                pult("9");
                break;
            }
            case "bt_10": {
                pult("0");
                break;
            }
            case "bt_del": {

                if (first == false) {
                    tf_value.setText("");
                    first = true;
                } else {
                    String s = tf_value.getText();

                    if (s.length() == 0) {
                        tf_value.setText("");
                    } else {
                        tf_value.setText(s.substring(0, s.length() - 1));
                    }
                }
                break;
            }
            case "bt_del2": {
                tf_value.setText("");
                break;
            }
            default: {
                log.error("not find button");
                throw new RuntimeException("dsfsdf");

            }
        }
    }


    private void pult(String s) {
        if (first == false) {
            tf_value.setText(s);
            first = true;
        } else {
            tf_value.setText(tf_value.getText() + s);
        }
    }
}
