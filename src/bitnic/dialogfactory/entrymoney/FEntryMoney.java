package bitnic.dialogfactory.entrymoney;

import bitnic.dialogfactory.DialogBase;
import bitnic.keyboardsmail.FKeyBoardSmail;
import bitnic.utils.TabRoute;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Transaction;
import bitnic.utils.UtilsOmsk;

import java.net.URL;

public class FEntryMoney extends DialogBase {

    private TabRoute tabRoute;
    private double aDouble;
    public TextField tf_summ;
    public Button bt_execute, bt_open_board;
    public AnchorPane anchorbase;


    public FEntryMoney(URL location, Stage stage) {
        super(location, stage);

        label_title.setText(Support.str("name64"));
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        tf_summ.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                aDouble = Double.parseDouble(tf_summ.getText().trim().replace(",", "."));
            } catch (Exception ex) {
                aDouble = 0;
            }
            if (aDouble == 0) {
                bt_execute.setDisable(true);
            } else {
                bt_execute.setDisable(false);
            }
        });
        bt_execute.setDisable(true);
        tf_summ.setText(String.valueOf(SettingAppE.getInstance().summNallKmm));


        bt_execute.setOnAction(event -> {
            if (aDouble == 0d) return;
            bt_close1.fire();

            new Transaction().cashOut(aDouble, null);
            //new  bitnic.kassa.EntryMoney().run(aDouble);
        });

        Platform.runLater(() -> tf_summ.requestFocus());

        UtilsOmsk.painter3d(bt_close1, bt_execute, tf_summ,bt_open_board);

        bt_open_board.setOnAction(a -> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(() -> tf_summ.requestFocus());
        });

        tabRoute=new TabRoute();
        tabRoute.addNode(tf_summ, node -> bt_execute.fire());
        tabRoute.addNode(bt_execute);
        tabRoute.addNode(bt_close1);
        FKeyBoardSmail smail=new FKeyBoardSmail(tf_summ);
        smail.setPrefHeight(250);
        anchorbase.getChildren().add(smail);
    }
}
