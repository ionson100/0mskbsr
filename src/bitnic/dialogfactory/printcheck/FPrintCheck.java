package bitnic.dialogfactory.printcheck;

import bitnic.dialogfactory.DialogBase;
import bitnic.senders.IAction;
import bitnic.utils.UtilsOmsk;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.net.URL;

public class FPrintCheck extends DialogBase {
    private static final Logger log = Logger.getLogger ( FPrintCheck.class );
    private final IAction printIAction;
    public Button bt_cancel,bt_print;

    public FPrintCheck (URL location , Stage stage, String title, IAction printIAction ) {
        super ( location , stage );
        this.printIAction = printIAction;
        label_title.setText (title);
        bt_close1.setOnAction ( this );
        bt_close2.setOnAction ( this );
        UtilsOmsk.painter3d ( bt_close1,bt_cancel,bt_print );
        bt_cancel.setOnAction ( a->bt_close2.fire () );
        bt_print.setOnAction ( event -> {
            bt_close2.fire ();
            if(printIAction!=null){
                printIAction.action ( null );
            }

        } );

    }

}
