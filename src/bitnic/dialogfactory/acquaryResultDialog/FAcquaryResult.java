package bitnic.dialogfactory.acquaryResultDialog;

import bitnic.bank.sber.BankResult;
import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.URL;

public class FAcquaryResult extends DialogBase {

   // private final BankResult bankResult;
    @FXML
    public VBox vbox;

    public Node head;


    public FAcquaryResult(URL location, Stage stage, BankResult bankResult) {
        super(location, stage);
        //this.bankResult = bankResult;


        label_title.setText("Результат ответа");

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1);
        for (String s : bankResult.linesE) {
            Label label = new Label(s);
            label.setStyle("-fx-text-fill: white;-fx-font-size: 15");
            vbox.getChildren().add(label);
        }
        if(bankResult.linesP!=null){
            for (String s : bankResult.linesP) {
                Label label = new Label(s);
                label.setStyle("-fx-text-fill: white;-fx-font-size: 15");
                vbox.getChildren().add(label);
            }
        }


    }

}
