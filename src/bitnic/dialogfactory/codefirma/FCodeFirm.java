package bitnic.dialogfactory.codefirma;

import bitnic.core.Controller;
import bitnic.core.ExeScript;
import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.*;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.log4j.Logger;

import java.lang.management.ManagementFactory;
import java.net.URL;

public class FCodeFirm extends DialogBase {

    private static final Logger log = Logger.getLogger(FCodeFirm.class);

    public Button bt_open_board;
    public TextField tf_id;
    private int pointid;
    private int oldPoint;
    private TabRoute tabRoute;


    public FCodeFirm ( URL location , Stage stage ) {
        super ( location , stage );

        stage.setOnHidden ( event -> {
            if ( SettingAppE.getInstance ().getPointId () == 0 ) {
                Platform.exit ();
            }
            SettingAppE.setInstance ( FactorySettings.getSettingsE( SettingAppE.class ) );
        } );

        pointid = SettingAppE.getInstance ().getPointId ();
        oldPoint=pointid;
        tf_id.setText ( String.valueOf(SettingAppE.getInstance ().getPointId ()) );
        label_title.setText ( Support.str ( "name56" ) );
        bt_close1.setOnAction (this );
        bt_close2.setOnAction ( this );
        tf_id.textProperty ().addListener ( ( observable , oldValue , newValue ) -> {
            try {
                pointid = Integer.parseInt ( newValue );
                if(pointid>0)
                bt_save.setDisable ( false );
            } catch (Exception ex) {

                bt_save.setDisable ( true );

            }
        } );
        Platform.runLater ( () -> tf_id.requestFocus () );
        UtilsOmsk.painter3d ( bt_close1 , tf_id , bt_save,bt_open_board );
        bt_open_board.setOnAction ( a -> {
            UtilsOmsk.openKeyBoard ();
            Platform.runLater ( () -> tf_id.requestFocus () );
        } );

        bt_save.setDisable ( true );

        bt_save.setOnAction ( event -> {

            SettingAppE.getInstance ().setPointId ( pointid );
            SettingAppE.save ();
            Controller.getInstans().refreshPointid ();
            bt_close1.fire ();
            if(DesktopApi.getOs()==DesktopApi.EnumOS.linux){
                if(oldPoint!=pointid){
                    DialogFactory.infoDialog("Изменение id предприятия", "Изменение id предприятия.\nПрограмма будет перезапущена.", new IAction<Object>() {
                        @Override
                        public boolean action(Object o) {
                            String name = ManagementFactory.getRuntimeMXBean().getName();
                            String pid=name.substring(0,name.indexOf("@"));
                            String exe="sh "+ Pather.curdir+"/reload.sh "+pid+" &";
                            ExeScript testScript = new ExeScript();
                            testScript.runScript(exe);
                            return false;
                        }
                    });
                }
            }




        } );

        tabRoute=new TabRoute();
        tabRoute.addNode(tf_id, node -> Platform.runLater(new Runnable() {
            @Override
            public void run() {
                SettingAppE.getInstance ().setPointId ( pointid );
                SettingAppE.save ();
                Controller.getInstans().refreshPointid ();
                bt_close1.fire ();
            }
        }));
        tabRoute.addNode(bt_close1);


    }


}
