package bitnic.dialogfactory.error;

import bitnic.dialogfactory.DialogBase;
import bitnic.utils.TabRoute;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import bitnic.senders.IAction;

import java.net.URL;

public class CErrorMessage extends DialogBase {

    private TabRoute tabRoute;
    private final Stage stage;
    private final String error;
    private final IAction iAction;
    public TextArea text_error;


    public CErrorMessage(URL location, Stage stage, String error, IAction iAction) {
        super(location, stage);
        this.stage = stage;
        this.error = error;
        this.iAction = iAction;
        text_error.setText(error);
        if (iAction != null) {
            bt_close1.setOnAction(event -> {
                iAction.action(null);
                stage.close();
               DialogBase.deleteBlenda();
            });
            bt_close2.setOnAction(event -> {
                iAction.action(null);
                stage.close();
                DialogBase.deleteBlenda();
            });
        }
        else {
            bt_close1.setOnAction(this);
            bt_close2.setOnAction(this);
        }
        UtilsOmsk.painter3d(bt_close1);

        label_title.setText ( Support.str ("name67") );
        Platform.runLater ( () -> bt_close1.requestFocus () );
        tabRoute=new TabRoute ();
        tabRoute.addNode ( bt_close1 );

    }


}
