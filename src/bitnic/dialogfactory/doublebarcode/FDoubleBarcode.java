package bitnic.dialogfactory.doublebarcode;

import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;

public class FDoubleBarcode extends DialogBase {


    public Label label_content;



    public FDoubleBarcode(URL location, Stage stage) {
        super ( location , stage );
        label_title.setText ( "Задвоение штрих-кодов" );
        label_content.setText("Выберите нужный товар!");
        bt_close1.setOnAction ( this );
        bt_close2.setOnAction ( this );
        UtilsOmsk.painter3d ( bt_close1 );
        Platform.runLater ( () -> bt_close1.requestFocus () );
    }
}
