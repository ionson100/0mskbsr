package bitnic.dialogfactory.messagereguest;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.orm.Configure;
import bitnic.senders.IAction;
import bitnic.senders.SenderRequestToServer;
import bitnic.updateapp.Requests;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Date;
import java.util.List;

public class FMessageRequest extends DialogBase {


    @FXML
    public Label label_content;

    public ComboBox<String> combo_to;
    public TextArea tf_to;
    //public Node head;
    public int userto=-1;
    public String nameto;
    public Button bt_open_board;


    public FMessageRequest(URL location, Stage stage) {
        super(location, stage);


        label_title.setText(Support.str("name298"));

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1, bt_save,bt_open_board);
        Platform.runLater(() -> bt_close1.requestFocus());


        bt_save.setOnAction(event -> {

            if(tf_to.getText().trim().length()==0){
                return;
            }
            if(userto==-1){
                return;
            }
            ReguestMessage rm=new ReguestMessage();
            rm.date=new Date();
            rm.int_to=userto;
            rm.message=tf_to.getText().trim();
            rm.name_to=nameto;
            new SenderRequestToServer().send(rm, new IAction<String>() {
                @Override
                public boolean action(String s) {
                    if(s==null){
                        DialogFactory.infoDialog("Отправка запроса","Успешно");
                        bt_close1.fire();
                    }else {
                        DialogFactory.infoDialog("Отправка запроса","Ошибка"+System.lineSeparator()+s);
                    }
                        return false;
                }
            });
        });

        List<Requests> requests = Configure.getSession().getList(Requests.class,null);
        for (Requests request : requests) {

            combo_to.getItems().add(request.name);

        }
        if(requests.size()>0){
            combo_to.getSelectionModel().select(0);
            userto=requests.get(0).id;
            nameto=requests.get(0).name;
        }

        combo_to.getSelectionModel().selectedItemProperty().addListener((observable,
                                                                         oldValue,
                                                                         newValue) -> {
            int f = combo_to.getSelectionModel().getSelectedIndex();
            userto=requests.get(f).id;
            nameto=requests.get(f).name;
            nameto=newValue;
            tf_to.setText("");
            Platform.runLater ( () -> tf_to.requestFocus () );
        });

        bt_open_board.setOnAction(a ->

        {
            UtilsOmsk.openKeyBoard();
            Platform.runLater( () -> tf_to.requestFocus() );
        });


    }
}

