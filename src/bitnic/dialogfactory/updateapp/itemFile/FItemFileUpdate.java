package bitnic.dialogfactory.updateapp.itemFile;

import bitnic.dialogfactory.checkstory.FCheckStory;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FItemFileUpdate extends GridPane implements Initializable {

    private static final Logger log = Logger.getLogger(FItemFileUpdate.class);
    private final File file;
    private final IAction iAction;
    public Label label_name,label_size;

    public FItemFileUpdate(File file, IAction iAction) {
        this.file = file;
        this.iAction = iAction;


        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_file_update.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(file!=null){
            String name=file.getName().replace(".rar","").replace(".tar.gz","");
            label_name.setText(name);
            label_size.setText(String.valueOf(file.length()));
            this.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    iAction.action(file);
                }
            });
        }

    }
}
