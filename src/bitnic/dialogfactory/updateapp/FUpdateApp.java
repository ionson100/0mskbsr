package bitnic.dialogfactory.updateapp;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.dialogfactory.updateapp.itemFile.FItemFileUpdate;
import bitnic.partfilesender.Downloader;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FUpdateApp extends DialogBase {

    private static final Logger log = Logger.getLogger(FUpdateApp.class);

    public Button bt_run;

    public ScrollPane scrol_host;
    File selectFile;



    public FUpdateApp(URL location, Stage stage, List<File> files, IAction iAction) {
        super(location, stage);

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1,bt_run);


        try{
            label_title.setText(Support.str("name152")+String.valueOf(SettingAppE.getInstance().getVersion()));
        }catch (Exception ex){
            log.error(ex);
        }


        Collections.sort(files, Comparator.comparing(File::getName));
        VBox vBox=new VBox();
        for (File file : files) {
            if(file.getName().contains(Downloader.HLAM)) continue;
            FItemFileUpdate update=new FItemFileUpdate(file, new IAction() {
                @Override
                public boolean action(Object o) {
                    selectFile= (File) o;
                    bt_run.setDisable(false);
                    return false;
                }
            });
            vBox.getChildren().add(update);
        }
        scrol_host.setContent(vBox);
        bt_run.setOnAction(event -> {
            String s=selectFile.getName().replace(".tar.gz","").replace(".rar","");
            String msg=String.format(Support.str("name154"),s);
            DialogFactory.questionDialog(Support.str("name153"),msg,new ButtonAction(Support.str("name155"), new IAction<Object>() {
                @Override
                public boolean action(Object o) {
                    bt_close1.fire();
                    iAction.action(selectFile);
                        return false;
                }
            }));

        });
    }

}
