package bitnic.dialogfactory.productforcheck;


import bitnic.checkarchive.CheckArchiveE;
import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.productforcheck.itemProductforcheck.FItemProductForCheck;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.util.Callback;
import bitnic.model.MProduct;

import java.net.URL;

public class FProductCheck extends DialogBase {


    public ListView list_product;
    private CheckArchiveE mCheck;


    public FProductCheck(URL location, Stage stage, CheckArchiveE mCheck) {
        super(location, stage);
        this.mCheck = mCheck;
        label_title.setText ( Support.str ("name86") );
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        ObservableList observableList= FXCollections.observableList(mCheck.getProductList());
        list_product.getItems().addAll(observableList);
        list_product.setCellFactory((Callback<ListView<MProduct>, ListCell<MProduct>>) list -> new CheckCell()
        );
        UtilsOmsk.painter3d(bt_close1);

    }
    static class CheckCell extends ListCell<MProduct> {
        @Override
        public void updateItem(MProduct item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemProductForCheck check=new FItemProductForCheck(item);
                setGraphic(check);
            }
        }
    }
}
