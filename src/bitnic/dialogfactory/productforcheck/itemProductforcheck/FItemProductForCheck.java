package bitnic.dialogfactory.productforcheck.itemProductforcheck;


import bitnic.pagesale.CustomScrollEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import bitnic.model.MProduct;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemProductForCheck extends GridPane implements Initializable {
    private static final Logger log = Logger.getLogger ( FItemProductForCheck.class );

    private double lastYposition=0d;
    public  Label label_name,label_price,label_amount,label_itogo;


    private MProduct mProduct;

    public FItemProductForCheck(MProduct mProduct) {
        this.mProduct = mProduct;
        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_product_for_check.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        label_name.setText(mProduct.name_check);
        label_amount.setText(String.valueOf(mProduct.selectAmount));
        label_itogo.setText(String.valueOf((UtilsOmsk.round(mProduct.selectAmount*mProduct.price,2))));
        label_price.setText(String.valueOf(mProduct.price));

        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });
    }
}
