package bitnic.dialogfactory.edititemkkm;

import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.kassa.ItemSettingKkm;
import bitnic.kassa.SettingsKkmTable;
import bitnic.senders.IAction;
import java.net.URL;

public class FEditItemKkm extends DialogBase {

    private final IAction<Object> iAction;
    //public Button  bt_save;
    public TextField tf_value;
    public Label label_name,label_dec,label_type;
    private ItemSettingKkm kkm;


    public FEditItemKkm(URL location, Stage stage, ItemSettingKkm settingKkm, IAction<Object> iAction) {
        super(location, stage);
        this.iAction = iAction;
        label_title.setText ( "Изменение значения" );
        label_name.setText(settingKkm.name);
        label_dec.setText(settingKkm.description);
        label_type.setText(settingKkm.param);
        kkm=settingKkm;
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        tf_value.setText(String.valueOf(settingKkm.value));
        tf_value.textProperty().addListener((observable, oldValue, newValue) -> validate(newValue));
        bt_save.setOnAction(event -> {
            if(iAction!=null){
                iAction.action(tf_value.getText());

                bt_close1.fire();
            }
        });
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tf_value.requestFocus();
            }
        });
        UtilsOmsk.painter3d(bt_close1,bt_save,tf_value);
    }

    private void validate(String str){
        if(str==null||str.trim().length()==0){
            bt_save.setDisable(true);
        }else {

            if(kkm.param.equals(SettingsKkmTable.chislo)){
               // int i=0;
                try{
                  //  i=Integer.parseInt(str);
                    bt_save.setDisable(false);
                }catch (Exception ex){
                    bt_save.setDisable(true);
                }
            }else {
                bt_save.setDisable(false);
            }



        }
    }
}
