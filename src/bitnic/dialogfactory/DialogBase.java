package bitnic.dialogfactory;

import bitnic.core.Controller;
import bitnic.settingscore.SettingAppE;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

public abstract class DialogBase extends GridPane implements Initializable, EventHandler<ActionEvent> {

    private static final Logger log = Logger.getLogger(DialogBase.class);
    private final Stage stage;

    public  GridPane dialog_head;
    public Node head;
    public Label label_title;
    public Button bt_close1, bt_close2, bt_save;


    public DialogBase(URL location, Stage stage) {
        this.stage = stage;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(location,bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException e) {
            log.error(e);
            throw new RuntimeException(e);
        }



    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if( SettingAppE.getInstance().isTypeЫShow3D){
            getStyleClass().add("dialog_3d");
        }

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                deleteBlenda();
            }
        });
        if(head!=null){
            bt_close2 = (Button) head.lookup ( "#bt_close2" );
            dialog_head= (GridPane) head.lookup ( "#dialog_head" );
            label_title = (Label) head.lookup ( "#label_title" );

        }
        if(bt_close2!=null){
            if(SettingAppE.getInstance().isTypeЫShow3D){
                bt_close2.getStyleClass ().add ( "button_close_3d" );
            }
        }
        if(dialog_head!=null){
            if(SettingAppE.getInstance().isTypeЫShow3D){
                dialog_head.getStyleClass ().add ( "dialog_head_3d" );
            }
        }

    }

    @Override
    public void handle(ActionEvent event) {
        stage.close();
        deleteBlenda();

    }

    public static void deleteBlenda() {
        List<Node> deletelist=new ArrayList<>();

        for (int i = 1; i < Controller.getInstans().stack_panel.getChildren().size(); i++) {
            deletelist.add(Controller.getInstans().stack_panel.getChildren().get(i));
        }
        Collections.reverse(deletelist);
        for (Node node : deletelist) {
            Controller.getInstans().stack_panel.getChildren().remove(node);
        }
    }
}
