package bitnic.dialogfactory.editcountproduct;

import bitnic.dialogfactory.DialogBase;
import bitnic.model.MBarcode;
import bitnic.orm.Configure;
import bitnic.utils.TabRoute;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;


import java.awt.*;
import java.net.URL;
import java.util.List;

public class FEditCountProduct extends DialogBase {
    private static final Logger log = Logger.getLogger(FEditCountProduct.class);
    private final boolean isConstructor;
    private TabRoute tabRoute;
    private boolean first;

    public Button bt_update,bt_delete,bt_open_board;
    public TextField tf_value,tf_price;
    public Label label_barcode,label_price;
    public Button bt_1,bt_2,bt_3,bt_4,bt_5,bt_6,bt_7,bt_8,bt_9,bt_10,bt_del,bt_del2;

     TextField textField;

    public FEditCountProduct(URL location, Stage stage, MProduct mProduct, boolean isConstructor, IAction<Double> iAction) {
        super(location, stage);
        this.isConstructor = isConstructor;


        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        tf_value.setText(String.valueOf(mProduct.selectAmount));
        tf_price.setText(String.valueOf(mProduct.price));
        label_title.setText(mProduct.name_check);






        tf_value.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue){
                    textField=tf_value;
                }
            }
        });
        tf_price.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue){
                    textField=tf_price;
                }
            }
        });

        tf_value.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tf_value.requestFocus();
                    }
                });

            }
        });
        tf_price.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tf_price.requestFocus();
                    }
                });

            }
        });








        tf_value.textProperty().addListener( ( observable , oldValue , newValue ) -> {

            try{
              mProduct.selectAmount=Double.parseDouble(newValue.replace(",","."));
              bt_update.setDisable ( false );
            }catch (Exception ex){

               bt_update.setDisable ( true );
            }

        } );

        tf_price.textProperty().addListener( ( observable , oldValue , newValue ) -> {

            try{
                mProduct.price=Double.parseDouble(newValue.replace(",","."));
                bt_update.setDisable ( false );
            }catch (Exception ex){
                bt_update.setDisable ( true );
            }

        } );

        bt_update.setDisable ( true );


        bt_open_board.setOnAction(a-> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater( () -> tf_value.requestFocus() );
        });



        bt_update.setOnAction( event -> {

            if(iAction!=null){
                iAction.action(mProduct.selectAmount);
            }
            bt_close1.fire();
            SettingAppE.save();
        } );

        bt_delete.setOnAction( event -> {

            if(iAction!=null){
                SettingAppE.getInstance().selectProduct.remove(mProduct);
                SettingAppE.save();
                iAction.action(-1d);

            }
            bt_close1.fire();
        } );
        bt_1.setOnAction(event -> action(event));
        bt_2.setOnAction(event -> action(event));
        bt_3.setOnAction(event -> action(event));
        bt_4.setOnAction(event -> action(event));
        bt_5.setOnAction(event -> action(event));
        bt_6.setOnAction(event -> action(event));
        bt_7.setOnAction(event -> action(event));
        bt_8.setOnAction(event -> action(event));
        bt_8.setOnAction(event -> action(event));
        bt_9.setOnAction(event -> action(event));
        bt_10.setOnAction(event -> action(event));
        bt_del.setOnAction(event -> action(event));
        bt_del2.setOnAction(event -> action(event));

       UtilsOmsk.painter3d(tf_value,tf_price,bt_close1,bt_update,bt_delete,bt_open_board);
        if(SettingAppE.getInstance().isTypeЫShow3D){

            bt_1.getStyleClass().add("button_key_big_3d");
            bt_2.getStyleClass().add("button_key_big_3d");
            bt_3.getStyleClass().add("button_key_big_3d");
            bt_4.getStyleClass().add("button_key_big_3d");
            bt_5.getStyleClass().add("button_key_big_3d");
            bt_6.getStyleClass().add("button_key_big_3d");
            bt_7.getStyleClass().add("button_key_big_3d");
            bt_8.getStyleClass().add("button_key_big_3d");
            bt_8.getStyleClass().add("button_key_big_3d");
            bt_9.getStyleClass().add("button_key_big_3d");
            bt_10.getStyleClass().add("button_key_big_3d");
            bt_del.getStyleClass().add("button_key_big_3d");
            bt_del2.getStyleClass().add("button_key_big_3d");


        }else {

            bt_1.getStyleClass().add("button_key_big");
            bt_2.getStyleClass().add("button_key_big");
            bt_3.getStyleClass().add("button_key_big");
            bt_4.getStyleClass().add("button_key_big");
            bt_5.getStyleClass().add("button_key_big");
            bt_6.getStyleClass().add("button_key_big");
            bt_7.getStyleClass().add("button_key_big");
            bt_8.getStyleClass().add("button_key_big");
            bt_8.getStyleClass().add("button_key_big");
            bt_9.getStyleClass().add("button_key_big");
            bt_10.getStyleClass().add("button_key_big");
            bt_del.getStyleClass().add("button_key_big");
            bt_del2.getStyleClass().add("button_key_big");


        }

        bt_1.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_2.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_3.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_4.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_5.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_6.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_7.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_8.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_8.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_9.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_10.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_del.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);
        bt_del2.setStyle("-fx-font-size: "+SettingAppE.getInstance().fontSizeDeliveryButtonCountProduct);


        List<MBarcode>  barcodesList= Configure.getSession().getList(MBarcode.class," id_product =?",mProduct.code_prod);
        if(barcodesList.size()==0){

            label_barcode.setText("Не найден");

        }else {
            label_barcode.setText(barcodesList.get(0).barcode);
        }





        Platform.runLater(() -> tf_value.requestFocus());
        tabRoute=new TabRoute ();
        tabRoute.addNode ( tf_value , new TabRoute.IAction () {
            @Override
            public void action ( Node node ) {
                System.out.println ( "dsad" );

            }
        } );
        tabRoute.addNode ( bt_update );
        tabRoute.addNode ( bt_delete );
        tabRoute.addNode ( bt_close1 );

        if(isConstructor){
            tf_price.setVisible(true);
            label_price.setVisible(true);
        }else {
            tf_price.setVisible(false);
            label_price.setVisible(false);
        }
    }


    void action(ActionEvent event){


        Button button= (Button) event.getSource();
        String id=button.getId();

        switch (id){
            case "bt_1":{
                pult("1");
                break;
            }
            case "bt_2":{
                pult("2");
                break;
            }
            case "bt_3":{
                pult("3");
                break;
            }
            case "bt_4":{
                pult("4");
                break;
            }
            case "bt_5":{
                pult("5");
                break;
            }
            case "bt_6":{
                pult("6");
                break;
            }
            case "bt_7":{
                pult("7");
                break;
            }
            case "bt_8":{
                pult("8");
                break;
            }
            case "bt_9":{
                pult("9");
                break;
            }
            case "bt_10":{
                pult("0");
                break;
            }
            case "bt_del":{

                if(first==false){
                    textField.setText("");
                    first=true;
                }else {
                    String s=textField.getText();

                    if(s.length()==0){
                        textField.setText("");
                    }else {
                        textField.setText(s.substring(0,s.length()-1));
                    }
                }
                break;
            }
            case "bt_del2":{
                textField.setText("");
                break;
            }
            default:{
                log.error("not find button");
                throw new RuntimeException("dsfsdf");

            }
        }
    }

    private void pult(String s) {
        if(first==false){
            textField.setText(s);
            first=true;
        }else {
            textField.setText(textField.getText()+s);
        }
    }
}
