package bitnic.dialogfactory.contributingemoneyprocedure;

import bitnic.checkarchive.CheckArchiveE;
import bitnic.dialogfactory.DialogBase;
import bitnic.kassa.ProcedureInKassa;
import bitnic.keyboardsmail.FKeyBoardSmail;
import bitnic.senders.IAction;
import bitnic.transaction.Transaction;
import bitnic.utils.TabRoute;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.net.URL;

public class FContributingMoneyProcedure extends DialogBase {

    private static final Logger log = Logger.getLogger(FContributingMoneyProcedure.class);
    private TabRoute tabRoute;
    private boolean run=false;

    private double aDouble;
    public  TextField tf_summ;
    public Button bt_execute,bt_open_board,bt_no_execute;
    public AnchorPane anchorbase;


    public FContributingMoneyProcedure(URL location, Stage stage, IAction<ProcedureInKassa.ResultProcedureIn> iActionProcedure) {
        super(location, stage);



        tf_summ.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try{
                   aDouble=Double.parseDouble(tf_summ.getText().trim().replace(",","."));
                   if(aDouble>0){
                       bt_execute.setDisable ( false );
                   }else {
                       bt_execute.setDisable ( true );
                   }

                }catch (Exception ex){
                    aDouble=0;
                    bt_execute.setDisable ( true );
                }
            }
        });
        bt_execute.setDisable ( true );


        bt_execute.setOnAction(event -> {

            run=true;
            if(aDouble>0){
                new Transaction().casheInProcedure(aDouble,iActionProcedure);
            }
            FContributingMoneyProcedure.super.handle(event);



        });

        bt_no_execute.setOnAction(event -> {


                new Transaction().casheInProcedure(0d,iActionProcedure);

            FContributingMoneyProcedure.super.handle(event);



        });
        bt_close1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                log.info("Продавец нажала кнопку отказа открытия смены");
                run=true;
                FContributingMoneyProcedure.this.handle(event);
                if(iActionProcedure!=null){
                    iActionProcedure.action(null);
                }
            }
        });

        bt_close2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                run=true;
                FContributingMoneyProcedure.this.handle(event);
                if(iActionProcedure!=null){
                    iActionProcedure.action(null);
                }
            }
        });
        Platform.runLater(() -> tf_summ.requestFocus());

        UtilsOmsk.painter3d(bt_close1,tf_summ,bt_execute,bt_no_execute,bt_open_board);

        bt_open_board.setOnAction(a-> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(() -> tf_summ.requestFocus());
        });
        label_title.setText ( Support.str ("name58") );

        tabRoute=new TabRoute();
        tabRoute.addNode(tf_summ, new TabRoute.IAction() {
            @Override
            public void action(Node node) {
                bt_execute.fire();
            }
        });
        tabRoute.addNode(bt_no_execute);
        tabRoute.addNode(bt_execute);
        tabRoute.addNode(bt_close1);
        FKeyBoardSmail smail=new FKeyBoardSmail(tf_summ);
        smail.setPrefHeight(250);
        smail.setPrefWidth(830);
        anchorbase.getChildren().add(smail);
    }
}
