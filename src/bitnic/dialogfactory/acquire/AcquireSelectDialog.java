package bitnic.dialogfactory.acquire;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.appmode.FAppMode;
import bitnic.pagesale.MainForm;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.net.URL;

public class AcquireSelectDialog extends DialogBase {


    private static final Logger log = Logger.getLogger(FAppMode.class);

    public ComboBox com_box;

    private SettingAppE settings = SettingAppE.getInstance();



    public AcquireSelectDialog(URL location, Stage stage) {
        super(location, stage);

        try{
            UtilsOmsk.painter3d(bt_close1,bt_save);


            label_title.setText ( Support.str ("name161") );

            bt_close1.setOnAction(this);
            bt_close2.setOnAction(this);
            bt_save.setOnAction(e -> save());
            bt_save.setDisable(true);
            for (String name : settings.getNamesAcquire()) {
                com_box.getItems().add(name);
            }
            com_box.getSelectionModel().select(settings.getAcquire());
            com_box.getSelectionModel().selectedItemProperty().addListener((observable,
                                                                            oldValue,
                                                                            newValue) -> bt_save.setDisable(false));
        }catch (Exception e){
            DialogFactory.errorDialog(e);
            log.error(e);
        }
    }

    private void save() {
        settings.setAcquery(com_box.getSelectionModel().getSelectedIndex());
        SettingAppE.save();

        if(Controller.curnode instanceof MainForm){
            ((MainForm)Controller.curnode).refreshButtonPrintCardCheck();
        }
        bt_close1.fire();
    }

}
