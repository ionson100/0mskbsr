package bitnic.dialogfactory.appmode;

import bitnic.core.Controller;
import bitnic.core.Main;
import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.FactorySettings;
import org.apache.log4j.Logger;

import java.net.URL;

public class FAppMode extends DialogBase {

    private static final Logger log = Logger.getLogger(FAppMode.class);

    public ComboBox com_box;

    private SettingAppE appMode = SettingAppE.getInstance();



    public FAppMode(URL location, Stage stage) {
        super(location, stage);

        try{
            UtilsOmsk.painter3d(bt_close1,bt_save);


            label_title.setText ( Support.str ("name31") );

            bt_close1.setOnAction(this);
            bt_close2.setOnAction(this);
            bt_save.setOnAction(e -> save());
            bt_save.setDisable(true);
            for (String name : appMode.getNamesProfile()) {
                com_box.getItems().add(name);
            }
            com_box.getSelectionModel().select(appMode.getProfile());

            com_box.getSelectionModel().selectedItemProperty().addListener((observable,
                                                                            oldValue,
                                                                            newValue) -> bt_save.setDisable(false));

        }catch (Exception e){
            DialogFactory.errorDialog(e);
            log.error(e);
        }


    }

    private void save() {

        appMode.setProfile(com_box.getSelectionModel().getSelectedIndex());
        SettingAppE.setInstance(FactorySettings.getSettingsE(SettingAppE.class));
        bt_close1.fire();
        Controller.refreshProfileApp();
        Controller.getInstans().refreshMenu();
        Main.stopSocked();
        Main.startSocked();
        //new bitnic.kassa.UserSettungsKmm().print();
    }
}
