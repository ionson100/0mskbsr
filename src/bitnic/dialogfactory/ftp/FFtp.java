package bitnic.dialogfactory.ftp;

import bitnic.dialogfactory.DialogBase;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.Support;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FFtp extends DialogBase {


    private static final Logger log = Logger.getLogger(FFtp.class);
    public TextField t_port,  f_file_server, t_server, t_file_local, t_user, t_pasword;
    public ComboBox com_box;
    private SettingAppE settings = SettingAppE.getInstance();


    //public Button  bt_save;


    public FFtp(URL location, Stage stage) {
        super(location, stage);




        if( settings.getProfile ()==0){
            try {
                List<String> lines= Files.readAllLines ( Paths.get ( Pather.ftpSettings));
                if(lines.size ()>=5){

                    settings.ftp_remote_file=lines.get ( 0 );
                    settings.ftp_port=Integer.parseInt ( lines.get ( 1 ) );
                    settings.ftp_user=lines.get ( 2 );
                    settings.ftp_password=lines.get ( 3 );
                    settings.ftp_cahrset=lines.get ( 4 );
                    settings.save ();

                }

            } catch (IOException e) {
                log.error ( e );
            }
        }

        label_title.setText ( Support.str ("name68") );
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        bt_save.setOnAction(e -> save());
        bt_save.setDisable(true);

        t_port.setText(String.valueOf( settings.ftp_port));
        f_file_server.setText( settings.ftp_remote_file);
        t_server.setText( settings.getUrl());
        t_file_local.setText(Pather.inFilesData);
        t_user.setText( settings.ftp_user);
        t_pasword.setText( settings.ftp_password);

        t_port.textProperty().addListener((observable, oldValue, newValue) -> validate());
        f_file_server.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_server.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_file_local.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_user.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_pasword.textProperty().addListener((observable, oldValue, newValue) -> validate());
        com_box.getSelectionModel().selectedItemProperty().addListener( ( observable , oldValue , newValue ) -> validate() );
        if( settings.ftp_cahrset==null|| settings.ftp_cahrset.trim().length()==0){
            com_box.getSelectionModel().selectFirst();
        }else {
            com_box.getSelectionModel().select( settings.ftp_cahrset);
        }
    }

    private void validate() {

        if(t_port.getText()== null||
                f_file_server.getText()== null||
                t_server.getText()== null ||
                t_file_local.getText()== null ||
                t_user.getText()== null||
                t_pasword.getText()== null){
            bt_save.setDisable(true);
            return;
        }

        if(t_port.getText().trim().length() == 0 ||
                f_file_server.getText().trim().length() == 0 ||
                t_server.getText().trim().length() == 0 ||
                t_file_local.getText().trim().length() == 0 ||
                t_user.getText().trim().length() == 0 ||
                t_pasword.getText().trim().length() == 0){
            bt_save.setDisable(true);
        }else {
            bt_save.setDisable(false);
        }
        try {
           Integer.parseInt(t_port.getText().trim());
        } catch (Exception ignored) {
            bt_save.setDisable(true);
        }
       if(com_box.getSelectionModel().getSelectedIndex()<=0) {
           bt_save.setDisable(true);
       }

    }

    private void save() {

        settings.ftp_remote_file = f_file_server.getText();
        settings.ftp_port = Integer.parseInt(t_port.getText().trim());
      
        settings.ftp_user = t_user.getText().trim();
        settings.ftp_password = t_pasword.getText().trim();

        settings.ftp_cahrset= (String) com_box.getValue();
        SettingAppE.save();
        this.handle(null);
        if(SettingAppE.getInstance ().getProfile ()==0){
            StringBuilder sb=new StringBuilder (  );
            sb.append ( f_file_server.getText () );
            sb.append ( System.lineSeparator () );
            sb.append ( settings.ftp_port );
            sb.append ( System.lineSeparator () );
            sb.append ( settings.ftp_user );
            sb.append ( System.lineSeparator () );
            sb.append ( settings.ftp_password );
            sb.append ( System.lineSeparator () );
            sb.append ( settings.ftp_cahrset );
            try {
                UtilsOmsk.rewriteFile ( Pather.ftpSettings,sb.toString () );
            } catch (Exception e) {
                log.error ( e );
            }
        }
    }
}
