package bitnic.dialogfactory;

import bitnic.bank.sber.BankResult;


import bitnic.checkarchive.CheckArchiveE;
import bitnic.core.Controller;
import bitnic.core.Main;
import bitnic.dialogfactory.acquaryResultDialog.FAcquaryResult;
import bitnic.dialogfactory.acquire.AcquireSelectDialog;
import bitnic.dialogfactory.addroportrow.FAddReportRow;
import bitnic.dialogfactory.appmode.FAppMode;
import bitnic.dialogfactory.barcodeprint.FBarcodePrint;
import bitnic.dialogfactory.checkstory.FCheckStory;
import bitnic.dialogfactory.closesessionsber.FCloseSessionSber;
import bitnic.dialogfactory.codefirma.FCodeFirm;
import bitnic.dialogfactory.contributingemoneyprocedure.FContributingMoneyProcedure;
import bitnic.dialogfactory.contributingmoney.FContributingMoney;
import bitnic.dialogfactory.doublebarcode.FDoubleBarcode;
import bitnic.dialogfactory.editcountproduct.FEditCountProduct;
import bitnic.dialogfactory.edititemkkm.FEditItemKkm;
import bitnic.dialogfactory.editpagerequest.FEditPageRequest;
import bitnic.dialogfactory.entrymoney.FEntryMoney;
import bitnic.dialogfactory.error.CErrorMessage;
import bitnic.dialogfactory.ftp.FFtp;
import bitnic.dialogfactory.info.FInfo;
import bitnic.dialogfactory.mailmessage.FMailMessage;
import bitnic.dialogfactory.messagereguest.FMessageRequest;
import bitnic.dialogfactory.ofd.FOfd;
import bitnic.dialogfactory.printcheck.FPrintCheck;
import bitnic.dialogfactory.printoutfile.FPrinttOutFile;
import bitnic.dialogfactory.productforcheck.FProductCheck;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.dialogfactory.question.FQuestion;
import bitnic.dialogfactory.reguser.FRegUser;
import bitnic.dialogfactory.request.FRequest;
import bitnic.dialogfactory.statekkm.FStatekkm;
import bitnic.dialogfactory.product.ProductHar;
import bitnic.dialogfactory.tamplatescheck.FTemplatesCheck;
import bitnic.dialogfactory.updateapp.FUpdateApp;
import bitnic.kassa.ProcedureInKassa;
import bitnic.pagerequest.FPageRequest;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import bitnic.kassa.ItemSettingKkm;
import bitnic.kassa.MStateKKM;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class DialogFactory {

    private static final Logger log = Logger.getLogger(DialogFactory.class);

    public static void errorDialog(Exception ex) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String sStackTrace = sw.toString(); // stack trace as a string
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {
                    url = new URL(getClass().getResource("/bitnic/dialogfactory/error/f_error_message.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                CErrorMessage har = new CErrorMessage(url, stage, sStackTrace,null);
                activateDialog(stage, har);
            }
        });
    }

    public static void errorDialog(String stre) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {

                    url = new URL(getClass().getResource("/bitnic/dialogfactory/error/f_error_message.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                CErrorMessage har = new CErrorMessage(url, stage, stre,null);
                activateDialog(stage, har);
            }
        });

    }

    public static void errorDialog(String stre, IAction iAction) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {
                    url = new URL(getClass().getResource("/bitnic/dialogfactory/error/f_error_message.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                CErrorMessage har = new CErrorMessage(url, stage, stre,iAction);
                activateDialog(stage, har);
            }
        });

    }

    public static void rollBackDialog(List<File> files, IAction iAction) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {
                    url = new URL(FUpdateApp.class.getResource("f_update_app.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                FUpdateApp har = new FUpdateApp(url, stage, files,iAction);
                activateDialog(stage, har);
            }
        });

    }

    public static void productDialog(MProduct mProduct) {
        Stage stage = new Stage();
        URL url = null;
        try {
            url = new URL(ProductHar.class.getResource("f_product_har.fxml").toString());
        } catch (MalformedURLException e) {
            log.error(e);
            e.printStackTrace();
        }
        ProductHar har = new ProductHar(url, mProduct, stage);
        activateDialog(stage, har);
    }

    public static void activateDialog(Stage stage, GridPane har) {


        FXMLLoader loader = new FXMLLoader(DialogFactory.class.getResource("blenda.fxml"));
        Node pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        Scene dialog = new Scene(har);
        stage.setScene(dialog);
        Controller.getInstans().stack_panel.getChildren().add(pane);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.initOwner(Main.myStage);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);



    }

    public static void settingsOfdDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FOfd.class.getResource("f_ofd.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FOfd har = new FOfd(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void settingsFtpDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FFtp.class.getResource("f_ftp.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FFtp har = new FFtp(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void infoDialog(String title, String message) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FInfo.class.getResource("f_info.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FInfo har = new FInfo(url, stage, title, message);
            activateDialog(stage, har);

        });
    }

    public static void infoDialog(String title, String message,IAction<Object>  objectIAction) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FInfo.class.getResource("f_info.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FInfo har = new FInfo(url, stage, title, message,objectIAction);
            activateDialog(stage, har);

        });
    }

    public static void printBarcodeDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FBarcodePrint.class.getResource("f_barcode_print.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FBarcodePrint har = new FBarcodePrint(url, stage);
            activateDialog(stage, har);

        });
    }

    public static void contributindMoneyDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FContributingMoney.class.getResource("f_contributing_money.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FContributingMoney har = new FContributingMoney(url, stage);
            activateDialog(stage, har);

        });
    }

    public static void contributindMoneyProcedureDialog(IAction<ProcedureInKassa.ResultProcedureIn> iActionProcedure) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FContributingMoneyProcedure.class.getResource("f_contributing_money_procedure.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FContributingMoneyProcedure har = new FContributingMoneyProcedure(url, stage,iActionProcedure);
            activateDialog(stage, har);

        });
    }

    public static void entryMoneyDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FEntryMoney.class.getResource("f_entry_money.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FEntryMoney har = new FEntryMoney(url, stage);
            activateDialog(stage, har);

        });
    }

    public static void questionDialog(String title, String message, ButtonAction ...buttonActions) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FQuestion.class.getResource("f_question.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FQuestion har = new FQuestion(url, stage, title, message,buttonActions);
            activateDialog(stage, har);

        });
    }

    public static void stateKkmDialog(MStateKKM stateKKM) {



        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FStatekkm.class.getResource("f_statekkm.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FStatekkm har = new FStatekkm(url,stage,stateKKM);
            activateDialog(stage, har);

        });
    }

    public static void segCodeFirmDialig() {
        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FCodeFirm.class.getResource("f_code_firma.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FCodeFirm har = new FCodeFirm(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void regUserDialog() {
        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FRegUser.class.getResource("f_reg_user.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FRegUser har = new FRegUser(url,stage,null);
            activateDialog(stage, har);

        });
    }

    public static void regUserDialog(IAction iActionProcedure) {
        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FRegUser.class.getResource("f_reg_user.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FRegUser har = new FRegUser(url,stage,iActionProcedure);
            activateDialog(stage, har);

        });
    }

    public static void editItemKkmDialog(ItemSettingKkm value, IAction<Object> iAction) {
        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FEditItemKkm.class.getResource("f_edit_item_kkm.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FEditItemKkm har = new FEditItemKkm(url,stage,value,iAction);
            activateDialog(stage, har);

        });
    }

    public static void appModeDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FAppMode.class.getResource("f_app_mode.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FAppMode har = new FAppMode(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void editCountProduct(MProduct mProduct,boolean isConstructor, IAction<Double> iAction) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FEditCountProduct.class.getResource("f_edit_count_product.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FEditCountProduct har = new FEditCountProduct(url,stage,mProduct,isConstructor,iAction);
            activateDialog(stage, har);

        });
    }

    public static void productForCheckDialog(CheckArchiveE mCheck) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FProductCheck.class.getResource("f_product_check.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FProductCheck har = new FProductCheck(url,stage,mCheck);
            activateDialog(stage, har);

        });
    }

    public static void checkStoryDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FCheckStory.class.getResource("f_check_story.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FCheckStory har = new FCheckStory(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void printOutFileDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FPrinttOutFile.class.getResource("f_print_out_file.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FPrinttOutFile har = new FPrinttOutFile(url, stage);
            activateDialog(stage, har);

        });
    }

    public static void acquaryDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(AcquireSelectDialog.class.getResource("f_acquire_select.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            AcquireSelectDialog  har = new AcquireSelectDialog(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void acquaryResultDialog(BankResult bankResult) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FAcquaryResult.class.getResource("f_result_dialog.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FAcquaryResult har = new FAcquaryResult(url,stage,bankResult);
            activateDialog(stage, har);

        });
    }

    public static void printCheck(String title, IAction <Object> iAction ) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FPrintCheck.class.getResource("f_print_check.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FPrintCheck har = new FPrintCheck(url,stage, title,iAction);
            activateDialog(stage, har);

        });
    }

    public static void closeSessionCber(IAction <bitnic.dialogfactory.closesessionsber.CloseSession> iAction ) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FCloseSessionSber.class.getResource("f_close_session_sber.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FCloseSessionSber har = new FCloseSessionSber(url,stage,iAction);
            activateDialog(stage, har);

        });
    }

    public static void templateCheckDialog (  ) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FTemplatesCheck.class.getResource("f_templates_check.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FTemplatesCheck har = new FTemplatesCheck(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void doubleBarcodeDialog (List<MProduct> mProductList,IAction<MProduct> iAction  ) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FDoubleBarcode.class.getResource("f_double_barcode.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FDoubleBarcode har = new FDoubleBarcode(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void messageRequestDialog( ) {
            Platform.runLater(() -> {

                Stage stage = new Stage();
                URL url = null;
                try {
                    url = new URL(FMessageRequest.class.getResource("f_message_request.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                FMessageRequest har = new FMessageRequest(url,stage);
                activateDialog(stage, har);

            });
    }

    public static void dialogSendToEmail() {

        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FMailMessage.class.getResource("f_mail_message.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FMailMessage har = new FMailMessage(url,stage);
            activateDialog(stage, har);

        });
    }

    public static void dialogRequest( IAction iAction) {

        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FRequest.class.getResource("f_request.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FRequest har = new FRequest(url,stage,iAction);
            activateDialog(stage, har);

        });
    }

    public static void dialogEditPageRequest(FPageRequest.Tovar tovar,IAction iAction) {

        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FEditPageRequest.class.getResource("f_edit_page_request.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FEditPageRequest har = new FEditPageRequest(url,stage,tovar,iAction);
            activateDialog(stage, har);

        });
    }

    public static void dialogAddReport() {

        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FAddReportRow.class.getResource("f_add_report_row.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FAddReportRow har = new FAddReportRow(url,stage);
            activateDialog(stage, har);

        });
    }
}
