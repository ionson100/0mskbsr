package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MRequest;
import bitnic.model.MRequestProduct;
import bitnic.orm.Configure;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SenderRequestFromServer {
    private static final Logger log = Logger.getLogger(SenderRequestFromServer.class);
    private IAction<String> iAction;
    private Exception exception;

    public void send(IAction<String> iAction) {

        this.iAction = iAction;
        new MyWorker().execute(null);

    }


    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            innerBackground();
            return null;
        }

        @Override
        protected void onPreExecute() {
            Controller.writeMessage("Загрузка заявок поставщика");
            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {

            FactoryBlender.stop();
            if (exception != null) {
                Controller.writeMessage("Загрузка заявок поставщика ОШИБКА");
                DialogFactory.errorDialog(exception.getMessage());
            }else {
                Controller.writeMessage("Загрузка заявок поставщика УСПЕШНО");
            }

            if (iAction != null) {
                if (exception != null) {
                    iAction.action(exception.getMessage());
                } else {
                    iAction.action(null);
                }
            }

        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }

    void innerBackground() {
        InputStream is = null;
        BufferedReader reader = null;
        HttpsURLConnection conn = null;
        try {
            String uu = String.format("https://%s/pos_orders?point=%s",
                    SettingAppE.getInstance().getUrl(),
                    SettingAppE.getPointId());//
            URL u = new URL(uu);
            conn = (HttpsURLConnection) u.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(15000 /*milliseconds*/);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.connect();
            int status = conn.getResponseCode();

            if (status == 200) {

                is = conn.getInputStream();
                reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }


                Type listType = new TypeToken<ArrayList<MRequest>>() {
                }.getType();
                List<MRequest> list = UtilsOmsk.getGson().fromJson(stringBuilder.toString(), listType);


                Configure.getSession().deleteTable("request");
                Configure.getSession().deleteTable(MRequestProduct.TABLE_PRODUCT);
                for (MRequest mRequest : list) {
                    for (MRequestProduct product : mRequest.products) {
                        product.id_request = mRequest.id;
                        product.amount_fact=product.amount;
                    }
                    if (mRequest.products.size() > 0) {
                        Configure.bulk(MRequestProduct.class, mRequest.products, Configure.getSession());
                    }
                }
                Configure.bulk(MRequest.class, list, Configure.getSession());

            } else {
                throw new RuntimeException(uu + " ответ - " + String.valueOf(status));
            }

        } catch (Exception ex) {

            exception = ex;
            log.error(ex);

        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            try {
                if (is != null) {
                    is.close();
                }

                if (reader != null) {
                    reader.close();
                }
            } catch (Exception ex) {
                log.error(ex);
            }

        }
    }
}
