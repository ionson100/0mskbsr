package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senderstate.CheckStateImage;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.WorkingFile;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

public class SenderFileBitnic {


    private Exception exception;
    private static final Logger log = Logger.getLogger(SenderFileBitnic.class);
    int status;
    SettingAppE setting = SettingAppE.getInstance();
    private List<String> strings;
    private IAction iAction;

    public void send(List<String> strings, IAction iAction) {

        this.strings = strings;
        this.iAction = iAction;

        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {




            String url = "https://" + setting.getUrl() + "/pos_data?uuid="+setting.getUuid();

            log.info("Отсылка файла на сервер - \n" + url);
            log.info("Выходной файл содержит - " + strings.size() + " строк");
            StringBuilder sb = new StringBuilder();

            sb.append("#\r\n");// + System.lineSeparator());
            sb.append("1\r\n");// + System.lineSeparator());
            sb.append("22\r\n");// + System.lineSeparator());
            int i = 3;
            for (String line : strings) {

                if (line.trim().length() == 0) continue;
                if (line.indexOf(";") == -1) continue;
                String[] ss = line.split(";", -1);
                if (ss.length < 5) continue;
                if (ss[3].equals("300")) continue;
                sb.append(line + "\r\n");//System.lineSeparator()
                i++;
            }

            log.info("Количество строк после обработки - " + i);
            InputStream stream = null;
            OutputStream outputStreamToRequestBody = null;
            BufferedWriter httpRequestBodyWriter = null;
            OutputStreamWriter dd = null;
            InputStream input = null;

            HttpsURLConnection connection = null;
            try {


                URL serverUrl = new URL(url);
                connection = (HttpsURLConnection) serverUrl.openConnection();

                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                outputStreamToRequestBody = connection.getOutputStream();
                dd = new OutputStreamWriter(outputStreamToRequestBody);
                httpRequestBodyWriter =
                        new BufferedWriter(dd);

                httpRequestBodyWriter.flush();
                stream = new ByteArrayInputStream(sb.toString().getBytes());


                int bytesRead;
                byte[] dataBuffer = new byte[1024];
                while ((bytesRead = stream.read(dataBuffer)) != -1) {
                    outputStreamToRequestBody.write(dataBuffer, 0, bytesRead);
                }
                outputStreamToRequestBody.flush();
                httpRequestBodyWriter.flush();
                outputStreamToRequestBody.close();
                httpRequestBodyWriter.close();
                connection.connect();
                status = connection.getResponseCode();
                input = connection.getInputStream();
                String result="";
                try(BufferedReader d=new BufferedReader(new InputStreamReader(input))){
                     result = d.lines().collect(Collectors.joining("\n"));
                }


                if (status == 200) {
                    boolean d = result.toLowerCase().equals("ok");
                    if (d == false) {
                        WorkingFile.clearReportDate();

                        log.info(result + " Очистили файл отчетов по команде с сервера " + status);
                    }
                } else {
                    throw new RuntimeException("Ответ сервера: " + status + " " + result);
                }


                log.info("Ответ сервера строка -  " + result + " status - " + status);

                input.close();

            } catch (Exception ex) {

                exception=ex;
                if (iAction != null) {
                    DialogFactory.errorDialog(ex.getMessage());
                }
                log.error(ex);
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (stream != null) {
                        stream.close();
                    }
                    if (outputStreamToRequestBody != null) {
                        outputStreamToRequestBody.close();
                    }
                    if (httpRequestBodyWriter != null) {
                        httpRequestBodyWriter.close();
                    }
                    if (input != null) {
                        input.close();
                    }
                    if (dd != null) {
                        dd.close();
                    }
                } catch (IOException e) {
                    log.error(e);
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            if (iAction != null) {
                FactoryBlender.run();
                FactoryBlender.addMessage("Отправка данных на сервер.");
            }

        }

        @Override
        protected void onPostExecute(Void params) {

            if (iAction != null) {
                FactoryBlender.stop();
                if(exception==null){
                    CheckStateImage.refreshFileForSenderToServer();
                }


                iAction.action(null);
            } else {
                if(exception==null){
                    CheckStateImage.refreshFileForSenderToServer();
                }
            }

        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
