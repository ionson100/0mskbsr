package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.messagereguest.ReguestMessage;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.stream.Collectors;

public class SenderRequestToServer {

    private ReguestMessage rm;

    public void send(ReguestMessage rm, IAction<String> iAction) {

        MessageTo mst=new MessageTo();
        mst.date=rm.date.getTime();
        mst.to=rm.int_to;
        mst.text=rm.message;
        this.message= UtilsOmsk.getGson().toJson(mst);
        log.info(this.message);
        this.iAction = iAction;
        new MyWorker().execute(null);
    }

   static public    class MessageTo {
            public int to;
            public long date;
            public String text;
    }

    private static final Logger log = Logger.getLogger(SenderRequestToServer.class);
    private String message;
    private IAction<String> iAction;
    private int status;
    private Exception exception;
    String result;

    public void send(String message,int to, IAction<String> iAction) {
        MessageTo mst=new MessageTo();
        mst.date=new Date().getTime();
        mst.to=to;
        mst.text=message;
        this.message= UtilsOmsk.getGson().toJson(mst);
        this.iAction = iAction;
    }

    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            SettingAppE settingAppE = SettingAppE.getInstance();

            String url = String.format("https://%s/pos_req?point=%s&trader=%s",
                    settingAppE.getUrl(),
                    settingAppE.getPointId(),
                    settingAppE.user_id);
            log.info(url);


            InputStream stream=null;
            BufferedWriter httpRequestBodyWriter=null;
            OutputStream outputStreamToRequestBody=null;
            OutputStreamWriter dd=null;
            HttpsURLConnection connection = null;
            try {
                URL serverUrl = new URL(url);
                connection = (HttpsURLConnection) serverUrl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                outputStreamToRequestBody = connection.getOutputStream();
                dd=new OutputStreamWriter(outputStreamToRequestBody);
                httpRequestBodyWriter = new BufferedWriter(dd);

                httpRequestBodyWriter.flush();
                stream = new ByteArrayInputStream(message.getBytes());
                int bytesRead;
                byte[] dataBuffer = new byte[1024];
                while ((bytesRead = stream.read(dataBuffer)) != -1) {
                    outputStreamToRequestBody.write(dataBuffer, 0, bytesRead);
                }
                outputStreamToRequestBody.flush();
                httpRequestBodyWriter.flush();
                outputStreamToRequestBody.close();
                httpRequestBodyWriter.close();
                connection.connect();
                status = connection.getResponseCode();
                InputStream input = connection.getInputStream();
                try(BufferedReader d=new BufferedReader(new InputStreamReader(input))){
                    result = d.lines().collect(Collectors.joining("\n"));
                }


                if (status == 200) {

                }else {
                    String s=String.format("%d %s",status,result);
                    throw  new RuntimeException(s);
                }
                input.close();

            } catch (Exception ex) {

                exception = ex;
                log.error(ex);
                DialogFactory.errorDialog(ex.getMessage());

            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try{
                    if(stream!=null){
                        stream.close();
                    }
                    if(httpRequestBodyWriter!=null){
                        httpRequestBodyWriter.close();
                    }
                    if(outputStreamToRequestBody!=null){
                        outputStreamToRequestBody.close();
                    }
                    if(dd!=null){
                        dd.close();
                    }
                }catch (Exception ex){
                    log.error(ex);
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute()  {
            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {

            FactoryBlender.stop();
            if (exception != null) {
                if (iAction != null) {
                    iAction.action(exception.getMessage());
                }
            }
            else {
                if (iAction != null) {
                    iAction.action(null);
                }
            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
