package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.CheckHistory;
import bitnic.settingscore.SettingAppE;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SenderHistory  {

    public Exception  exception;

    private List<CheckHistory>  historyList;

    private static Logger log = Logger.getLogger(SenderHistory.class);
    private IAction<List<CheckHistory>> iAction;

    public void send(IAction<List<CheckHistory>>  iAction){
        this.iAction = iAction;

        new aсtion().execute(null);
    }

    class aсtion extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
             innerBackground();
             return null;
        }

        @Override
        public void onPreExecute() {
            FactoryBlender.run();
            FactoryBlender.addMessage("Запрос истории.");
        }

        @Override
        public void onPostExecute(Void params) {
            FactoryBlender.stop();
            if(iAction!=null){
                iAction.action(historyList);
            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }


    }

    void innerBackground() {
        InputStream is = null;
        BufferedReader reader = null;
        HttpsURLConnection conn = null;
        try {
            String uu=String.format("https://%s/pos_history?trader=%s",
                    SettingAppE.getInstance().getUrl(),
                    SettingAppE.getInstance().user_id);
            URL u = new URL(uu);
            conn = (HttpsURLConnection) u.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(15000 /*milliseconds*/);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.connect();
            int status = conn.getResponseCode();

            if(status==200){
                FactoryBlender.addMessage("Запрос получен.");
                FactoryBlender.addMessage("Обработка.");
                is = conn.getInputStream();
                reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder stringBuilder = new StringBuilder();
                String line ;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                historyList=new ArrayList<>();


                Type listType = new TypeToken<ArrayList<CheckHistory>>(){}.getType();
                historyList = new Gson().fromJson(stringBuilder.toString(), listType);
                //log.info(stringBuilder.toString());

            }else {
                throw new RuntimeException(uu+" ответ - "+String.valueOf(status));
            }


        } catch (Exception ex) {

            exception=ex;
            log.error(ex);

        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            try {
                if (is != null) {
                    is.close();
                }

                if (reader != null) {
                    reader.close();
                }
            } catch (Exception ex) {
                log.error(ex);
            }

        }
    }

}
