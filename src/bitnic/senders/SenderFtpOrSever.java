package bitnic.senders;


import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.IInsertValidator;
import bitnic.model.MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE;
import bitnic.model.MMarketingAction;
import bitnic.orm.Configure;
import bitnic.orm.ISession;
import bitnic.pagesale.MainForm;
import bitnic.parserfrontol.cacheMetaDateFrontol;
import bitnic.parserfrontol.CreatorInstace;
import bitnic.parserfrontol.IFrontolAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.HashTable;
import bitnic.utils.Pather;
import bitnic.utils.Support;
import javafx.application.Platform;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * ion100 on 11.01.2018.
 */


public class SenderFtpOrSever {
    static String hashTable;
    private static final Logger log = Logger.getLogger(SenderFtpOrSever.class);

    private boolean success;

    private StringBuilder error = new StringBuilder();
    //   private String server = "178.62.249.118";
//   private int port = 21;
//   private String user = "tester";
//   private String pass = "rU6a2";
//   private String remoteFile2 = "goods2.txt";
//   private File downloadFile2=new File("d:/goods22.txt");
    private Map<String, cacheMetaDateFrontol> dateFrontolMap;
    private IAction refresh;
    private SettingAppE settings = SettingAppE.getInstance();
    private boolean isUTF = true;


    public SenderFtpOrSever() {

        if (SettingAppE.getInstance().getUrl() == null || SettingAppE.getInstance().getUrl().trim().length() == 0) {
            DialogFactory.errorDialog("Не заданы настройки FTP");

        }
        if (settings.getProfile() == 0) {
            try {

                List<String> lines = Files.readAllLines(Paths.get(Pather.ftpSettings));
                if (lines.size() < 3) {
                    log.error(" Профиль 1с, файл настройки FTP (.settings) не существует или не заполнене");
                } else {
                    settings.ftp_remote_file = lines.get(0);
                    settings.ftp_port = Integer.parseInt(lines.get(1));
                    settings.ftp_user = lines.get(2);
                    settings.ftp_password = lines.get(3);
                    settings.ftp_cahrset = lines.get(4);
                    settings.save();
                }


            } catch (IOException e) {
                log.error(e);
                DialogFactory.errorDialog(e);
                return;
            }
        }

        if (settings.ftp_cahrset != null && settings.ftp_cahrset.toUpperCase().contains("UTF")) {
            isUTF = true;
        } else {
            isUTF = false;
        }
    }


    public void send(Map<String, cacheMetaDateFrontol> dateFrontolMap, IAction refresh) {
        this.dateFrontolMap = dateFrontolMap;
        this.refresh = refresh;
        new InnerTemp().execute(null);
    }

    public class InnerTemp extends AsyncTask<String, Integer, Boolean> {

        //ProgressBar bar;

        @Override
        public void onPreExecute() {
            Controller.writeMessage(Support.str("name140"));
            FactoryBlender.run();
        }

        @Override
        public Boolean doInBackground(String... params) {

            hashTable = new HashTable().getHashTable();
            log.info("hash table :" + String.valueOf(hashTable));


            return activate();
        }


        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if (params && error.length() == 0) {
                Controller.writeMessage(Support.str("name141"));


                String s = new HashTable().getHashTable();
                log.info("hash table  in:" + String.valueOf(hashTable));
                log.info("hash table out:" + String.valueOf(s));
                if (hashTable.equals(s)) {

                } else {
                    log.info("clear table self amount");
                    Configure.getSession().deleteTable("amountself");
                }

                if (refresh != null) {
                    refresh.action(null);
                }
                // new Sender_1C_rewrite().Send();
            } else {

                if (refresh != null) {
                    refresh.action("Ошибка загрузки данных с сервера. Смотри лог ошибок");
                }
                String ss="\nВозможно не правильно указан индентификатор предприятия.\nОтсутствие файла goods на сервере." +
                        "\nОтсутствие интернета.";
                DialogFactory.errorDialog(error.toString()+ss);

                Controller.writeMessage(Support.str("name142"));
            }
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    if (Controller.curnode != null) {
                        if (Controller.curnode instanceof MainForm) {

                            if (((MainForm) Controller.curnode).watcherScaner != null) {
                                ((MainForm) Controller.curnode).watcherScaner.refreshList();
                            }
                        }
                    }
                }
            });
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }

        boolean activate() {

            List<String> totalList = new ArrayList<>();


            if (SettingAppE.getInstance().getProfile() == 1 || SettingAppE.getInstance().getProfile() == 3) {
                senderBitnic(totalList);
            } else {
                ftp(totalList);
            }


            cacheMetaDateFrontol currentFrontol = null;

            List<Object> objects = new ArrayList<>();
            Set<String> set = new HashSet<>();
            log.info("totallist size - " + totalList.size());
            for (int i = 2; i < totalList.size(); i++) {


                String s = totalList.get(i);

                if (s.length() == 0 || s.trim().length() == 0) {
                    continue;
                }


                if (s.indexOf(";") == -1 && s.indexOf("$") == 0) {


                    if (objects.size() > 0) {
                        ISession ses = Configure.getSession();
                        ses.beginTransaction();

                        if (currentFrontol.aClass == MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE.class) {

                            List<MMarketingAction> mMarketingActions = ses.getList(MMarketingAction.class, null);
                            List<Integer> list = new ArrayList<>();
                            for (Object object : objects) {
                                MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE d = (MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE) object;
                                list.add(d.idaction);
                            }
                            List<MMarketingAction> delList = new ArrayList<>();
                            for (MMarketingAction ma : mMarketingActions) {
                                if (list.contains(ma.code)) {
                                    delList.add(ma);
                                }
                            }
                            for (MMarketingAction mMarketingAction : delList) {
                                ses.delete(mMarketingAction);
                            }

                        } else {
                            Configure.bulk(currentFrontol.aClass, objects, ses);
                        }


                        ses.commitTransaction();
                        objects = new ArrayList<>();
                        set.clear();
                    }


                    String ss = s.substring(3);
                    currentFrontol = dateFrontolMap.get(ss);

                    Object sd = null;
                    if (currentFrontol == null) {
                        continue;
                    }
                    try {
                        sd = currentFrontol.aClass.newInstance();
                    } catch (Exception e) {

                        log.error(e);
                        e.printStackTrace();
                    }
                    if (sd instanceof IFrontolAction) {
                        ((IFrontolAction) sd).action();
                    }
                } else {

                    Object sd;
                    try {
                        if (currentFrontol != null && currentFrontol.aClass != null) {
                            sd = currentFrontol.aClass.newInstance();
                            if (sd != null) {
                                CreatorInstace.create(sd, currentFrontol, totalList.get(i));
                                if (sd instanceof IInsertValidator) {
                                    IInsertValidator validator = (IInsertValidator) sd;
                                    String key = validator.getHashKey();
                                    if (set.contains(key)) {

                                    } else {
                                        objects.add(sd);
                                        set.add(key);
                                    }
                                } else {
                                    objects.add(sd);
                                }
                            }
                        }


                    } catch (Exception e) {
                        log.error(e);
                        return false;
                    }

                }


            }
            if (objects.size() > 0) {
                ISession ses = Configure.getSession();
                ses.beginTransaction();
                FactoryBlender.addMessage("Вставка : " + currentFrontol.aClass.getName());
                Configure.bulk(currentFrontol.aClass, objects, ses);
                ses.commitTransaction();

            }
            FactoryBlender.addMessage("Успех заргузки");
            return true;

        }

    }

    private void senderBitnic(List<String> totalList) {


        HttpsURLConnection connection = null;
        InputStream input = null;
        OutputStream output = null;
        int count = 0;
        try {
            String ss = "https://" + SettingAppE.getInstance().getUrl() + "/pos_goods?point=" + SettingAppE.getInstance().getPointId();
            log.info("dowload goods.txt - " + ss);
            URL url = new URL(ss);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setInstanceFollowRedirects(false);
            connection.setReadTimeout(15000 /*milliseconds*/);
            connection.setConnectTimeout(20000 /* milliseconds */);

            connection.setRequestMethod("GET");
            connection.connect();
            final int status = connection.getResponseCode();
            if (status != 200) {
                String msg="Запрос goods, ответ сервера " + status;
                error.append(msg);
                log.error(msg);
            }
            log.info("status - " + status);
            int lenghtOfFile = connection.getContentLength() + 100;
            input = connection.getInputStream();
            output = new FileOutputStream(Pather.inFilesData);
            byte data[] = new byte[lenghtOfFile];
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();


        } catch (Exception ex) {
            error.append(ex.getMessage());
            log.error(ex);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (input != null) {
                    input.close();
                }
                if (output != null) {
                    output.close();
                }
            } catch (Exception e) {
                log.error(e);
            }
        }
        if (error.length() > 0) {
            return;
        }

        BufferedReader br = null;
        FileReader fr = null;


        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(Pather.inFilesData), "windows-1251"));
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                totalList.add(sCurrentLine);
            }

        } catch (IOException e) {
            log.error(e);
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                log.error(ex);
            }
        }

        if (totalList.size() == 0) {
            error.append(" Ошибка, отсутствие данных в файле");
        }

    }

    private boolean ftp(List<String> totalList) {
        FactoryBlender.addMessage("Подготовка к передаче");
        FTPClient ftpClient = new FTPClient();
        try {


            log.info("sender settings " + settings.getUrl());
            log.info("url : " + settings.getUrl());
            log.info("port: " + settings.ftp_port);
            log.info("login: " + settings.ftp_user);
            log.info("pwd: " + settings.ftp_password);
            ftpClient.connect(settings.getUrl(), settings.ftp_port);
            ftpClient.login(settings.ftp_user.trim(), settings.ftp_password.trim());
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);


            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(new File(Pather.inFilesData)));
            String fileftp = "/" + settings.getPointId() + "/" + settings.ftp_remote_file;
            log.info("file local: " + Pather.inFilesData);
            log.info("file: " + fileftp);
            success = ftpClient.retrieveFile(fileftp, outputStream1);

            outputStream1.close();

            log.info("sender settings success " + success);
            if (success) {

                String ss = "Успешная загрузка файла settings: " + settings.ftp_remote_file;
                FactoryBlender.addMessage(ss);
                log.info(ss);
                System.out.println(ss);

            } else {
                String er = "Ошибка загрузки файла settings: " + settings.ftp_remote_file;
                FactoryBlender.addMessage(er);
                System.out.println(er);
                log.info(er);
                DialogFactory.errorDialog(er);
            }

        } catch (IOException ex) {
            log.error(ex);
            error.append(ex.getMessage());
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
            DialogFactory.errorDialog(ex);
            return true;
        } finally {

            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                log.error(ex);
                ex.printStackTrace();
                DialogFactory.errorDialog(ex);
            }
        }

        BufferedReader br = null;
        FileReader fr = null;


        try {

            if (isUTF) {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(Pather.inFilesData)));
            } else {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(Pather.inFilesData), "windows-1251"));
            }


            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                if (isUTF) {

                } else {
                    sCurrentLine = new String(sCurrentLine.getBytes(), 0, sCurrentLine.getBytes().length, "UTF-8");
                }

                totalList.add(sCurrentLine);

            }

        } catch (IOException e) {
            error.append("\r\n");
            error.append(e.getMessage());
            e.printStackTrace();
            log.error(e);
            DialogFactory.errorDialog(e);
            return true;
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                log.error(ex);
                ex.printStackTrace();
                DialogFactory.errorDialog(ex);
            }
        }

        if (totalList.size() == 0) {
            String s = "код предприятия - " + settings.getPointId() + System.lineSeparator() +
                    "файл на settings - " + settings.ftp_remote_file + System.lineSeparator() +
                    "файл на локали- " + Pather.inFilesData + System.lineSeparator() +
                    "успешность запроса - " + String.valueOf(success);
            error.append(new RuntimeException(s));
            return true;
        }

        FactoryBlender.addMessage("Получено : " + totalList.size() + " строк");
        FactoryBlender.addMessage(" Определено : " + dateFrontolMap.size() + " типов");
        return false;
    }


}
