package bitnic.senders;

/**
 * шщт100 on 11.01.2018.
 */

public interface IAction<T> {

     boolean action(T t);

}
