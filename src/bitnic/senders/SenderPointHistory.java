package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Support;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class SenderPointHistory {

    private transient static final Logger log = Logger.getLogger(SenderPointHistory.class);
    class temphistory {
        public double total;
        public double avg;
    }

    private temphistory temphistoryE;
    private Exception exception;

    public void send() {
        new aсtion().execute(null);
    }

    class aсtion extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params) {
            innerBackground();
            return null;
        }

        @Override
        public void onPreExecute() {
            FactoryBlender.run();
            FactoryBlender.addMessage("Запрос истории по точке.");
        }

        @Override
        public void onPostExecute(Void params) {
            FactoryBlender.stop();
            String x="Нет данных";
            if(exception!=null){
                x="Во время запроса на сервер произошла ошибка:\n"+exception.getMessage();
            }
            else {
                if(temphistoryE!=null){
                    x=String.format(Support.str("name317"),String.valueOf(temphistoryE.total),String.valueOf(temphistoryE.avg));
                }
            }
            DialogFactory.infoDialog(Support.str("name318"),x);

        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }

        void innerBackground() {
            InputStream is = null;
            BufferedReader reader = null;
            HttpsURLConnection conn = null;
            try {
                String uu = String.format("https://%s/pos_point_history?point=%s",
                        SettingAppE.getInstance().getUrl(),
                        String.valueOf(SettingAppE.getPointId()));
                URL u = new URL(uu);
                conn = (HttpsURLConnection) u.openConnection();
                conn.setInstanceFollowRedirects(false);
                conn.setReadTimeout(15000 /*milliseconds*/);
                conn.setConnectTimeout(20000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.connect();
                int status = conn.getResponseCode();

                if (status == 200) {

                    is = conn.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    temphistoryE = new Gson().fromJson(stringBuilder.toString(), temphistory.class);
                } else {
                    throw new RuntimeException(uu + " ответ - " + String.valueOf(status));
                }


            } catch (Exception ex) {

                exception = ex;
                log.error(ex);

            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                try {
                    if (is != null) {
                        is.close();
                    }

                    if (reader != null) {
                        reader.close();
                    }
                } catch (Exception ex) {
                    log.error(ex);
                }

            }
        }


    }
}
