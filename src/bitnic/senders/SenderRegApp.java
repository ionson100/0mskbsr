package bitnic.senders;


import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by ion on 12.01.2018.
 */
public class SenderRegApp {
    private static Logger log = Logger.getLogger(SenderRegApp.class);
    private IAction<Boolean> stringIAction;
    private String url;
    private String key1;

    public void send(String key1, IAction<Boolean> stringIAction) {

        this.stringIAction = stringIAction;
        this.key1 = key1;

        String key2 = bitnic.utils.UtilsOmsk.getKey2();


        try {
            url = "https://178.62.249.118/check_key/?key1=" + key1 + "&key2=" + key2;
        } catch (Exception e) {
            log.error(e);
            DialogFactory.errorDialog(e);
            e.printStackTrace();
        }
        new ation().execute(null);

    }

    class ation extends AsyncTask<String, Integer, Boolean> {

        @Override
        public Boolean doInBackground(String... params) {
            return innerBackgrount();
        }

        @Override
        public void onPreExecute() {


        }

        @Override
        public void onPostExecute(Boolean params) {
            if (params) {

            } else {

            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {

            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }


    }

    boolean innerBackgrount() {
        InputStream is = null;
        BufferedReader reader = null;
        HttpsURLConnection conn = null;
        try {
            URL u = new URL(url);
            conn = (HttpsURLConnection) u.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(15000 /*milliseconds*/);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.connect();
            int status = conn.getResponseCode();

            is = conn.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            System.out.println(stringBuilder.toString());
            if (stringBuilder.toString().trim().equals("0")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {

            log.error(ex);
            return false;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            try {
                if (is != null) {
                    is.close();
                }

                if (reader != null) {
                    reader.close();
                }
            } catch (Exception ex) {
                log.error(ex);
            }

        }
    }
}

