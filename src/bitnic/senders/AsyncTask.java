package bitnic.senders;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import org.apache.log4j.Logger;

/**
 * Created by ion on 12.01.2018.
 */
public abstract class AsyncTask<T1, T2, T3> extends Task {

    private static Logger log = Logger.getLogger ( AsyncTask.class );
    private boolean isDeamon = true;

    public void setDemon ( boolean b ) {
        isDeamon = b;
    }

    public boolean isDamon () {
        return isDeamon;
    }

    private T1[] params;

    protected abstract T3 doInBackground ( T1... params );

    protected abstract void onPreExecute ();

    protected abstract void onPostExecute ( T3 params );

    protected abstract void onErrorInner(Throwable ex);


    //public abstract void onError(WorkerStateEvent event);

    T3 res = null;

    @Override
    protected T3 call () {

        res = doInBackground ( params );
        return res;
    }

    public void execute ( T1[] params ) {
        this.params = params;

        setOnScheduled ( ( e ) -> {
            onPreExecute ();
        } );

        this.setOnSucceeded ( ( e ) -> {
            onPostExecute ( res );
        } );

        this.setOnFailed(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {

                onErrorInner( event.getSource().getException());

            }
        });

        Thread thread = new Thread ( this );
        thread.start ();


    }


}
