//package bitnic.senders;
//
//import bitnic.blenderloader.FactoryBlender;
//import bitnic.core.Controller;
//import bitnic.dialogfactory.DialogFactory;
//
//import bitnic.pagesale.MainForm;
//import bitnic.utils.Pather;
//import javafx.application.Platform;
//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;
//import org.apache.commons.net.ftp.FTPFile;
//
//import org.apache.log4j.Logger;
//import org.jetbrains.annotations.NotNull;
//import bitnic.settingscore.SettingAppE;
//
//import bitnic.utils.UtilsOmsk;
//
//import java.io.*;
//import java.nio.ByteBuffer;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.nio.file.StandardCopyOption;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//public class Sender_1C {
//    private static final Logger log = Logger.getLogger(Sender_1C.class);
//    private boolean isDeleteReport;
//
//    private Exception exception;
//
//    private SettingAppE settings = SettingAppE.getInstance();
//
//    public void send() {
//        if(new File(Pather.sender_report_file).exists()==false){
//            DialogFactory.infoDialog("Отсылка файла отчета не сервер 1с","Файла пока не существует");
//            return;
//        }
//        new MyWorker().execute(null);
//    }
//
//
//    private class MyWorker extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            try {
//                log.info("Отсылка файла на 1 с");
//                List<String> lines = new ArrayList<>();
//                Charset cset = Charset.forName("windows-1251");
//                List<String> lines1 = Files.readAllLines(Paths.get(Pather.sender_report_file), cset);
//                for (String s : lines1) {
//                    if (s.trim().length() == 0) continue;
//                    if (!s.contains(";")) continue;
//                    String[] ss = s.split(";", -1);
//                    if (ss[3].equals("300")) continue;
//                    lines.add(s);
//                }
//                List<String> listFiles = getListFiles();// получаем все файлы из дериктории
//                String report = null;
//                for (String file : listFiles) {
//                    if (file.contains("report")) {
//                        report = file;
//                    }
//                }
//
//                if (report == null) {//если файла нет
//                    // говорит о том что файла отчета нет.
//                    //# - не прочитаный
//                    //@ - обработан
//
//                    List<String> stringList = getStringList(lines);
//                    writeFile(stringList, null);
//                    isDeleteReport = true;
//                } else {
//
//                    List<String> status = getSeverStatusFile(report);
//                    if (status.size() == 0) {// если файл естьно он пустой
//                        List<String> stringList = getStringList(lines);
//                        writeFile(stringList, report);
//                    } else {
//                        String s = status.get(0);
//                        if (s.contains("@")) { // если файл обработан мы его перезаписываем
//                            List<String> stringList = getStringList(lines);
//                            writeFile(stringList, null);
//                            isDeleteReport = true;
//                        } else { // если файл не обработан то мы дописывам в конец
//                            List<String> stringList = new ArrayList<>(lines.size() + 4);
//                            for (String line : status) {
//                                if (line.trim().length() == 0) continue;
//                                stringList.add(line + System.lineSeparator());
//                            }
//                            for (String line : lines) {
//                                if (line.trim().length() == 0) continue;
//                                stringList.add(line + System.lineSeparator());
//                            }
//                            writeFile(stringList, report);
//                        }
//                    }
//                }
//
//                if (isDeleteReport) {
//                    File file = new File(Pather.reportstory);
//                    if (file.exists() == false) {
//                        if(file.mkdir()==false){
//                            log.info(" not create "+Pather.reportstory);
//                        }
//                    }
//                    String str = Pather.reportstory + File.separator + "report_"+UtilsOmsk.simpleDateFormatforFileNameStory(new Date())+".txt";
//
//                    Files.move(Paths.get(Pather.sender_report_file), Paths.get(str), StandardCopyOption.REPLACE_EXISTING);
//                    UtilsOmsk.clearReportDate(Pather.sender_report_file,"");
//                   // new File(Pather.sender_report_file).delete();
//
//                }
//
//            } catch (Exception ex) {
//                exception = ex;
//                log.info(ex);
//            }
//
//            return null;
//        }
//
//        @NotNull
//        private List<String> getStringList(List<String> lines) {
//            List<String> stringList = new ArrayList<>(lines.size() + 4);
//            stringList.add("#\r\n");// + System.lineSeparator());
//            stringList.add("1\r\n");// + System.lineSeparator());
//            stringList.add("22\r\n");// + System.lineSeparator());
//            if(lines!=null){
//                for (String line : lines) {
//                    if (line.trim().length() == 0) continue;
//                    stringList.add(line + "\r\n");//System.lineSeparator()
//                }
//            }
//
//            return stringList;
//        }
//
//        private void writeFile(List<String> listFiles, String filename) throws IOException {
//            if (filename == null) {
//                filename = "report.txt";
//            }
//            StringBuilder sb = new StringBuilder();
//            for (String listFile : listFiles) {
//                sb.append(listFile);
//            }
//            FTPClient ftpClient = new FTPClient();
//            ftpClient.connect(settings.getUrl(), settings.ftp_port);
//            ftpClient.login(settings.ftp_user, settings.ftp_password);
//            ftpClient.enterLocalPassiveMode();
//            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
//            String firstRemoteFile = String.valueOf(settings.getPointId()) + "/" + filename;
//
//            Charset cset = Charset.forName("windows-1251");
//            ByteBuffer buf = cset.encode(sb.toString());
//            byte[] b = buf.array();
//
//            InputStream inputStream = new ByteArrayInputStream(b);
//            System.out.println("Стар начала загрузки");
//            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
//            inputStream.close();
//            if (done) {
//                System.out.println("Файл загружен успешно.");
//            }
//            inputStream.close();
//
//            if (ftpClient.isConnected()) {
//                ftpClient.logout();
//                ftpClient.disconnect();
//            }
//
//        }
//
//        @Override
//        protected void onPreExecute()  {
//
//            FactoryBlender.run();
//        }
//
//        @Override
//        protected void onPostExecute(Void params) {
//
//            FactoryBlender.stop();
//            if (exception != null) {
//                DialogFactory.errorDialog(exception);
//            }
//            Platform.runLater ( new Runnable () {
//                @Override
//                public void run () {
//                    if(Controller.curnode instanceof MainForm){
//                        ((MainForm)Controller.curnode).watcherScaner.refreshList();
//                    }
//                }
//            } );
//        }
//
//        @Override
//        protected void onErrorInner(Throwable ex) {
//            FactoryBlender.stop();
//            log.error(ex);
//            DialogFactory.errorDialog(ex.getMessage());
//        }
//
//        private List<String> getListFiles() throws IOException {
//            log.info("получения списка фалов ftp 1c");
//            List<String> files = new ArrayList<>();
//            FTPClient ftpClient = new FTPClient();
//
//            ftpClient.connect( settings.getUrl(), settings.ftp_port);
//            ftpClient.login( settings.ftp_user.trim(), settings.ftp_password.trim());
//            ftpClient.enterLocalPassiveMode();
//            String dir="/" + String.valueOf(settings.getPointId()+"/");
//
//            FTPFile[] filesE = ftpClient.listFiles(dir);
//            if (filesE != null && filesE.length > 0) {
//                for (FTPFile file : filesE) {
//                    String name = file.getName();
//                    files.add(name);
//                    System.out.println(name);
//                }
//            }
//
//            if (ftpClient.isConnected()) {
//                ftpClient.logout();
//                ftpClient.disconnect();
//            }
//
//
//
//            return files;
//        }
//
//        List<String> getSeverStatusFile(String filename) throws IOException {
//
//            List<String> strings = new ArrayList<>();
//
//            FTPClient ftpClient = new FTPClient();
//
//            ftpClient.connect(settings.getUrl(), settings.ftp_port);
//            ftpClient.login(settings.ftp_user, settings.ftp_password);
//            ftpClient.enterLocalPassiveMode();
//            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
//
//
//            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(new File(Pather.inFilesData)));
//            String fileftp = settings.getPointId() + "/" + filename;
//            System.out.println("file name - " + fileftp);
//            boolean success = ftpClient.retrieveFile(fileftp, outputStream1);
//            outputStream1.close();
//
//            if (success) {
//                log.info("Успешная загрузка файла report на сервер 1с: " + settings.ftp_remote_file);
//                System.out.println("File #1 has been downloaded successfully.");
//
//            } else {
//                String er = "Ошибка загрузки файла report на сервер 1с: " + settings.ftp_remote_file;
//                System.out.println(er);
//                log.info(er);
//                DialogFactory.errorDialog(er);
//            }
//            if (ftpClient.isConnected()) {
//                ftpClient.logout();
//                ftpClient.disconnect();
//            }
//            try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(Pather.inFilesData)))){
//                String item;
//                while ((item = br.readLine()) != null) {
//                    item = new String(item.getBytes(), 0, item.getBytes().length, "UTF-8");
//                    strings.add(item);
//                }
//            }
//
//            return strings;
//
//        }
//    }
//}
//
