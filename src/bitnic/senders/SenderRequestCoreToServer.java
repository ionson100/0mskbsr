package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.messagereguest.ReguestMessage;
import bitnic.model.RequestToServer;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.stream.Collectors;

public class SenderRequestCoreToServer {
    private static final Logger log = Logger.getLogger(SenderRequestCoreToServer.class);
    private ReguestMessage rm;
    private RequestToServer requestToServer;
    private IAction<String> iAction;

    private int status;
    private String message;
    private String result;
    private Exception exception;

    public void send(RequestToServer requestToServer, IAction<String> iAction) {
        this.requestToServer = requestToServer;
        this.iAction = iAction;
        message=new Gson().toJson(requestToServer);
        new MyWorker().execute(null);
    }






    private class MyWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            SettingAppE settingAppE = SettingAppE.getInstance();

            String url = String.format("https://%s/pos_order_status?point=%s&trader=%s",
                    settingAppE.getUrl(),
                    settingAppE.getPointId(),
                    settingAppE.user_id);
            log.info(url);


            InputStream stream=null;
            BufferedWriter httpRequestBodyWriter=null;
            OutputStream outputStreamToRequestBody=null;
            OutputStreamWriter dd=null;
            HttpsURLConnection connection = null;
            try {
                URL serverUrl = new URL(url);
                connection = (HttpsURLConnection) serverUrl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                outputStreamToRequestBody = connection.getOutputStream();
                dd=new OutputStreamWriter(outputStreamToRequestBody);
                httpRequestBodyWriter = new BufferedWriter(dd);

                httpRequestBodyWriter.flush();
                String dded=message;
                stream = new ByteArrayInputStream(dded.getBytes());
                int bytesRead;
                byte[] dataBuffer = new byte[1024];
                while ((bytesRead = stream.read(dataBuffer)) != -1) {
                    outputStreamToRequestBody.write(dataBuffer, 0, bytesRead);
                }
                outputStreamToRequestBody.flush();
                httpRequestBodyWriter.flush();
                outputStreamToRequestBody.close();
                httpRequestBodyWriter.close();
                connection.connect();
                status = connection.getResponseCode();
                InputStream input = connection.getInputStream();
                try(BufferedReader d=new BufferedReader(new InputStreamReader(input))){
                    result = d.lines().collect(Collectors.joining("\n"));
                }

                if (status == 200) {

                }else {
                    String s=String.format("%d %s",status,result);
                    throw  new RuntimeException(s);
                }
                input.close();

            } catch (Exception ex) {

                exception = ex;
                log.error(ex);
                DialogFactory.errorDialog(ex.getMessage());

            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try{
                    if(stream!=null){
                        stream.close();
                    }
                    if(httpRequestBodyWriter!=null){
                        httpRequestBodyWriter.close();
                    }
                    if(outputStreamToRequestBody!=null){
                        outputStreamToRequestBody.close();
                    }
                    if(dd!=null){
                        dd.close();
                    }
                }catch (Exception ex){
                    log.error(ex);
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute()  {
            Controller.writeMessage("Отправка согласования заявки постащика");
            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {

            FactoryBlender.stop();
            if (exception != null) {
                Controller.writeMessage("Отправка согласования заявки постащика - ОШИБКА");
            }
            else {
                Controller.writeMessage("Отправка согласования заявки постащика - УСПЕШНО");
                if (iAction != null) {
                    iAction.action(null);
                }
            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {

        }
    }
}
