package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;

public class SenderLogToServer {
    private static final Logger log = Logger.getLogger(SenderLogToServer.class);
    Exception exception;

    String path=System.getProperty("user.home")+ File.separator+"omsk.log";
    public void send(){


        new InnerTemp().execute(null);

    }
    public class InnerTemp extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            HttpsURLConnection connection=null;
            BufferedWriter httpRequestBodyWriter=null;
            OutputStream outputStreamToRequestBody=null;
            InputStream stream=null;
            OutputStreamWriter dd=null;
            try{
                File file=new File(path);
                if(file.exists()==false){
                    throw new RuntimeException("Файл лога не создан! \n( "+path+" )");
                }
                String url="https://"+ SettingAppE.getInstance().getUrl()+"/pos_data";
                log.info("Отсылка файла лога на сервер - "+url);
                URL serverUrl =new URL(url);
                connection = (HttpsURLConnection) serverUrl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                outputStreamToRequestBody = connection.getOutputStream();
                dd=new OutputStreamWriter(outputStreamToRequestBody);
                httpRequestBodyWriter =new BufferedWriter(dd);
                httpRequestBodyWriter.flush();
                stream =  new FileInputStream(file);
                int bytesRead;
                byte[] dataBuffer = new byte[1024];
                while((bytesRead = stream.read(dataBuffer)) != -1) {
                    outputStreamToRequestBody.write(dataBuffer, 0, bytesRead);
                }
                outputStreamToRequestBody.flush();
                httpRequestBodyWriter.flush();
                outputStreamToRequestBody.close();
                httpRequestBodyWriter.close();
                connection.connect();
                final int status = connection.getResponseCode();
                if (status != 200) {
                    throw new RuntimeException("Отсылка на сервер report, ответ сервера " + status);
                }
                log.info("Статус - "+status);


            }catch (Exception ex){
                log.error ( ex );
                exception=ex;
            }finally {
                if(connection!=null){
                    connection.disconnect();
                }
                try{
                    if(httpRequestBodyWriter!=null){
                        httpRequestBodyWriter.close();
                    }
                    if(outputStreamToRequestBody!=null){
                        outputStreamToRequestBody.close();
                    }
                    if(stream!=null){
                        stream.close();
                    }
                    if(dd!=null){
                        dd.close();
                    }
                }catch (Exception ex){
                    log.error(ex);
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute()  {
            FactoryBlender.run();
        }

        @Override
        protected void onPostExecute(Void params) {
            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.errorDialog(exception);
            }
        }

        @Override
        protected void onErrorInner(Throwable ex) {
            FactoryBlender.stop();
            log.error(ex);
            DialogFactory.errorDialog(ex.getMessage());
        }
    }
}
